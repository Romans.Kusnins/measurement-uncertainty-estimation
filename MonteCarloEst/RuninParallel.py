from subprocess import (Popen, PIPE)

#subprocess.Popen('python3 MC_two_rods.py sens S11abs', shell=True)

commands = list()

modelname = 'MC_two_slabs_guide'

commands.append('python3 ' + modelname + '.py total')
commands.append('python3 ' + modelname + '.py sens S11abs')
commands.append('python3 ' + modelname + '.py sens d1')
commands.append('python3 ' + modelname + '.py sens d2')
commands.append('python3 ' + modelname + '.py sens L')
commands.append('python3 ' + modelname + '.py sens a')
commands.append('python3 ' + modelname + '.py sens f')
commands.append('python3 ' + modelname + '.py sens ep1')
commands.append('python3 ' + modelname + '.py sens tan1')
commands.append('python3 ' + modelname + '.py sens tan2')


#procs = [ Popen(i, shell=True) for i in commands ]
procs = [ Popen(i, shell=True, stdin=PIPE, stdout=PIPE) for i in commands ]
for p in procs:
	p.wait()
