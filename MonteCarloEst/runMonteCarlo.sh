#!/bin/bash

MPATH=/home/student_etf/GitMonteCarlo/measurement-uncertainty-estimation/MonteCarloEst

cd  $MPATH

rm -f ./MonteCarlo

python3 ./paramprep.py

mpic++  MonteCarlo.cpp -o MonteCarlo -L./lib -lrod -lewald -lslatec -llapack -lrefblas -lgfortran  -DMP

mpirun ./MonteCarlo

cd ~
