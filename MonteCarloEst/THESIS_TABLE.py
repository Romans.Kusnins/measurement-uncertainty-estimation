import numpy as np
import os

filename = 'overleaf_test'

fontsize = 11

bhlnwd = 1.5
chlnwd = 0.5

bvlnwd = 1.5
cvlnwd = 0.5

colwd = [80, 80, 80]
rowwd = [40, 40, 40]

TableName = 'My Table'

ColNames = ('Uncertainty', 'Number of trials', 'Best estimate')
#======================================================================
def n2s(x, *numdec):
	if abs(x-int(x)) < 1e-15:	# Check whether x is an integer or not.
		return '%d' % (int(x))	# If this is the case, omit the comma 					# and the fractional part
	else:
		if len(numdec)>0 and int(numdec[0])>0:# if the number of decimal places is 					# specified print, the number with the 
					# numdec decimal>0 places, else display 6 decimal places 		# decimal places
			fstr = "%%1.%df" % (int(numdec[0]))
			return fstr % (x)
		else:
			return '%1.6f' % (x)
#======================================================================
tabtext = list()
for m in range(len(rowwd)):
	rowtext = list()
	for n in range(len(colwd)):
		rowtext.append('row: ' + n2s(m) + '; col: ' + n2s(n))
	tabtext.append(rowtext)
#======================================================================
fp = open(filename + '.tex', 'w')
#======================================================================
strn = ""

strn += '\\documentclass[fleqn,a4paper,12pt]{article}%%\n\n'

strn += '\\usepackage[left=1in, right=1in, top=1in, bottom=1in]{geometry}%%\n'
strn += '\\usepackage{amsmath}%%\n'
strn += '\\usepackage{tabulary}%%\n'
strn += '\\usepackage{polyglossia}%%\n'
strn += '\\usepackage{upgreek}%%\n'
strn += '\\usepackage{siunitx}%%\n'
strn += '\\usepackage{titlesec}%%\n'
strn += '\\usepackage{tikz}%%\n'
strn += '\\usepackage{caption}%%\n'
strn += '\\usepackage{graphicx}%%\n'
strn += '\\usepackage{listings}%%\n'
strn += '\\usepackage{color}%%\n'
strn += '\\usepackage[american,siunitx]{circuitikz}%%\n'
strn += '\\usetikzlibrary{calc,math}%%\n\n'

strn += '\\begin{document}%%\n\n'
#======================================================
strn +='\\def\\ccdot{\\mskip-3mu\\cdot\\mskip-3mu}%%\n\n'
#======================================================
#======================================================
# define dimension and counter registers
strn += '\\newbox\\bxa%\n\n'

#===================================================================
rownum = len(rowwd)
colnum = len(colwd)

vlnwd = list()
vlnwd.append(bvlnwd)
for n in range(len(colwd)-1):
	vlnwd.append(cvlnwd)
	
vlnwd.append(bvlnwd)

#===================================================================
strn += '%%=============================================================\n\n'

strn += '''\\def\\textsize [#1,#2]#3{%%
	{\\fontsize{#1}{#2} \\selectfont#3}%%\n}%%\n\n'''

strn += '%%=============================================================\n\n'
#=====================================================================
#===================================================================
fnt = n2s(fontsize)
#=====================================================================
def tablebegin(alignment = 'left'):
	global strn	
	tablewd = float(np.sum(np.asarray(colwd)) + np.sum(np.asarray(vlnwd)))
	strn += '\\hbox to \\textwidth {%%\n\t'	
	if alignment == 'right' or alignment == 'center':
		strn += '\\hfil\n\t'		
	strn += '\\vbox{%%\n'
#=====================================================================
def tablend(alignment = 'left'):
	global strn	
	strn +='\t}%%\n'
	if alignment == 'left' or alignment == 'center':		
		strn +='\t\\hfil%%\n'
	strn +='}%%\n\n'
#=====================================================================
def tablecaption(alignment = 'center'):
	global strn
	tablewd = float(np.sum(np.asarray(colwd)) + np.sum(np.asarray(vlnwd)))
	strn += '\t\t\\hbox to ' + n2s(tablewd,2) + 'pt {'
	if alignment == 'right' or alignment == 'center':
		strn += '\\hfil'		
	strn +=  ' Table 1: ' + TableName
	if alignment == 'left' or alignment == 'center':
		strn += '\\hfil'
	strn += '}%%\n\t\t\\kern8pt%%\n'
#=====================================================================
def addcell(m,n, **key):
	valign = key.get('valign', 'center')
	halign = key.get('halign', 'center')
	global strn
	addvline(vlnwd[n])
	strn += '\t\t\t\\vbox to ' + n2s(rowwd[m],2) + 'pt {%%\n\t\t\t\t'
	if valign == 'bottom' or valign == 'center':
		strn += '\\vfil%%'
	strn += '\n\t\t\t\t\\hbox to '
	strn +=  n2s(colwd[n],2) + 'pt {'
	if halign == 'right' or halign == 'center':
		strn += '\\hfil'
	strn += '\\textsize['+ fnt + ',' + fnt + ']'
	strn += '{' + tabtext[m][n] + '}'
	if halign == 'left' or halign == 'center':
		strn += '\\hfil'
	strn += '}%%\n\t\t\t\t'
	if valign == 'top' or valign == 'center':
		strn += '\\vfil%%'
	strn += '\n\t\t\t}%%\n'
#=====================================================================
def addrow(m, **key):
	global strn
	strn += '\t\t\\hbox{%%\n'
	for n in range(colnum):
		addcell(m, n, **key)
	strn += '\t\t\t\\vrule width ' + n2s(vlnwd[-1],2) + 'pt%%\n\t\t}%%\n'
#==================================================================
def addhline(linewd):
	global strn
	tablewd = float(np.sum(np.asarray(colwd)) + np.sum(np.asarray(vlnwd)))
	strn += '\t\t\\hrule height ' + n2s(linewd,2) + 'pt width ' + n2s(tablewd,2) + 'pt%%\n'
#=====================================================================	
def addvline(linewd):
	global strn
	strn += '\t\t\t\\vrule width ' + n2s(linewd,2) + 'pt%%\n'
#==================================================================
#strn += '\\setbox\\bxa=\\hbox{}\\relax%%\n'
#==================================================================
# Create a table
#==================================================================
tablebegin('center')
tablecaption('center')
addhline(bhlnwd)
for m in range(rownum):
	addrow(m, valign='center', halign='center')
	if m < rownum-1:
		addhline(chlnwd)
	else:
		addhline(bhlnwd)
		
tablend('left')
#===================================================================
strn += '\\end{document}%%\n\n'
#===========================================================
fp.write(strn)
fp.close()
#===========================================================
os.system('xelatex ' + filename + '.tex > /dev/null 2>&1')

# open the generated PDF file with the default linux viewer
os.system('xdg-open ' + filename + '.pdf')

