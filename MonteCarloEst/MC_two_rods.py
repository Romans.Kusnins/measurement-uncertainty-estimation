import numpy as np
import sys

if len(sys.argv) == 2 and sys.argv[1] == 'total':
	tasktype = 'total'
elif len(sys.argv) == 3 and sys.argv[1] == 'sens':
	tasktype = 'sens'
	senparam = sys.argv[2]
else:
	tasktype = 'total'      # estimate the total uncertainty (default option)
	#tasktype = 'sens'      # estimate the sensitivity coefficient
	senparam = 'S11abs'     # specify the model parameter to find the sensitivity to

try:
	matplot = True
	from matplotlib import pyplot as plt
except ModuleNotFoundError:
	print("The matplotlib module is not installed.")
	matplot = False


from slab_res3md import (root_finding, POW2)
from slab_res3md import (three_slab_guide, three_slab_free_space, root_finding, POW2)
from slab_res3md import (single_slab_guide_matrix)
from plot2pdf import plot2latex
from MoM import *
from MonteCarlo_estimation import *
from Uncertainty_vs_S11 import *
#==================================================================================
#==================================================================================
tasktype = 'total' 	# estimate the total uncertainty (default option)
#tasktype = 'sens' 	# estimate the sensitivity coefficient

#senparam = 'S11abs'	# specify the model parameter to find the sensitivity to
#senparam  = 'ro1'	# specify the model parameter to find the sensitivity to
#senparam  = 'ro2'	# specify the model parameter to find the sensitivity to
#senparam  = 'xo1'	# specify the model parameter to find the sensitivity to
#senparam = 'tan1'	# specify the model parameter to find the sensitivity to
#senparam = 'ep1'	# specify the model parameter to find the sensitivity to
#senparam = 'L'		# specify the model parameter to find the sensitivity to
#senparam = 'a'		# specify the model parameter to find the sensitivity to
#senparam = 'f'		# specify the model parameter to find the sensitivity to
#==================================================================================
M	= 100		# number of Monte-Carlo trials
MAXIT	= 20		# maximum ieteration number of Newton's method
#==================================================================================
isPDF 	= False
isPRINT	= False
#==================================================================================
modeldata	= dict()
sigma	 	= dict()

modelname 	= 'two_rods'
#==================================================================================
# MODEL PARAMETERS
#==================================================================================
modeldata['ep1']        = 4.3
modeldata['tan1']       = 0.003

modeldata['ep2']        = [1.0, 20.0, 0.1]
#modeldata['ep2']       = 10.2
modeldata['tan2']       = 0.0023

modeldata['ro1']        = 5.7
#modeldata['ro1']       = [1.0, 8.0, 0.1]
modeldata['xo1']        = 0.0

modeldata['ro2']        = 2.5
modeldata['xo2']        = 0.0

modeldata['L']         = 8.6
#modeldata['L']        = [1.0, 21.0, 0.2]

modeldata['f']          = 10.0

modeldata['a']          = 22.86



sigma['S11abs']         = S11fun

sigma['ro1']            = 0.01		# corresponds to 0.02mm expanded uncertainty
sigma['ro2']            = 0.01

sigma['xo1']            = 0.01
sigma['xo2']            = 0.01

sigma['L']              = 0.01
sigma['a']              = 0.01

sigma['f']              = modeldata['f']*7.0e-6/2.0  	# corresponds to 7 ppm accuracy

sigma['ep1']            = modeldata['ep1']*0.01		# set according to Krupka 0.5%-2%
sigma['tan1']           = 5.0e-6			# also set according to Krupka

sigma['tan2']           = modeldata['tan2']*0.05
#================================================================
list_found = False
for param in modeldata.keys():
	if type(modeldata[param]) is list:
		if len(modeldata[param]) > 1 and not list_found:
			var_range = modeldata[param]
			var_param = param
			list_found = True
		else:
			modeldata[param] = modeldata[param][0]

if list_found:
        var_values      = np.arange(*var_range)
else:
        var_param       = 'ep1'
        var_values      = np.asarray([modeldata['ep1']])

NUM             = var_values.shape[0]

mean  		= np.empty(var_values.shape)
total_delta 	= np.empty(var_values.shape)

for n in range(NUM):

	modeldata[var_param] 	= var_values[n]

	sigma_mc = sigma.copy()
	if tasktype == 'sens':
		for param in sigma_mc.keys():
			if param == senparam:
				sigma_mc[param] = sigma[param]
			else:
				sigma_mc[param] = 0.0

	epv, failed = MC_two_rods(modeldata, sigma_mc, M, MAXIT)

	print(epv.shape)

	if failed:
		mean[n]  	= np.nan
		total_delta[n] 	= np.nan
	else:
		epv 		= epv[np.logical_not(np.isnan(epv))]
		mean[n]  	= np.mean(epv)
		total_delta[n] 	= np.sqrt(np.var(epv))
#================================================================
# SAVE THE OBTAINED DATA TO FILE
#================================================================
if tasktype == 'sens':
	filename = modelname + '_var_' + var_param + '_sens_' + senparam + '_MCnum_' + '{:}'.format(M)
else:
	filename = modelname + '_var_' + var_param + '_total_MCnum_' + '{:}'.format(M)

np.save(filename, np.vstack([var_values, total_delta]))
#================================================================
# PLOT THE OBTAINED DATA
#================================================================
ylabeltotal 	= 'Standard uncertainty associated with'
ylabelsens      = 'Model sensitivity to'
paramname       = {'S11abs': '$S_{11}$', 'ro1': '$r_{1}$', 'ro2': '$r_{2}$', 'L': '$L$', 'ep1': '$\\varepsilon_{\\mathrm{r}}$', 'tan1': '$ \\tan{\\delta}$', 'tan2': '$\\tan{\\delta}$', 'a': 'a', 'f': 'f'}

xlabels = dict()

xlabels['ep1']  = 'Dielectric constant of the auxiliary rod'
xlabels['ep2']  = 'Dielectric constant of MUT'
xlabels['ro1']  = 'Radius of the auxiliary rod'
xlabels['xo1']  = 'Auxiliary rod offset'
xlabels['ro2']  = 'Radius of the MUT rod'
xlabels['xo2']  = 'MUT rod offset'
xlabels['L']    = 'Separation between the auxiliary and MUT rods'
#================================================================
if isPDF:
	if tasktype == 'sens':
		plot2latex(var_values, total_delta, labels={'xlabel': xlabels[var_param], 'ylabel': ylabelsens + ' ' + paramname[senparam]}, ylabel_vert=True)
	else:
		plot2latex(var_values, total_delta, labels={'xlabel': xlabels[var_param], 'ylabel': 'Total uncertainty'}, ylabel_vert=True)
elif isPRINT:
	print(total_delta)
elif matplot:
	plt.plot(var_values, total_delta)
	plt.xlabel(xlabels[var_param])
	if tasktype == 'sens':
		plt.ylabel('Sensitivity to ' + paramname[senparam])
	else:
		plt.ylabel('Total uncertainy')

	plt.grid()
	plt.show()
else:
        print('Calculated data have been saved into a file.')
