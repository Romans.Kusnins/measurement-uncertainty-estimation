//#include<complex>
#include<complex.h>
#include<math.h>
#include<stdio.h>

#define PI 3.1415926535897931

#define BJo(m)  BJo[m+N+1]
#define BHo(m)  BHo[m+N+1]
#define BJoD(m) BJoD[m+N]
#define BHoD(m) BHoD[m+N]

#define BJp(m)  BJp[m+N+1]
#define BHp(m)  BHp[m+N+1]
#define BJpD(m) BJpD[m+N]
#define BHpD(m) BHpD[m+N]

#define EHo(m)  EHo[m+N]
#define EX(m,n,l)   EX[m+N + (n-1)*(2*N+1) + (l-1)*(2*N+1)*NM]

#define kw(m)   kw[m-1]
#define phk(m)  phk[m-1]

#define XO(m)   XO[m-1]
#define XOI(m)  XOI[m-1]

#define ZO(m)   ZO[m-1]
#define ZOI(m)  ZOI[m-1]

#define PH(m,n)   PH[m+N + (n-1)*(2*N+1)]
#define PHI(m,n)  PHI[m+N + (n-1)*(2*N+1)]

#define RT(m,n,l,o)   RT[m-1 + (n-1)*NM + (l-1)*NM*NM + (o-1)*NM*NM*2]

#define res_p(m)  res_p[2*N+m]
#define res_n(m)  res_n[2*N+m]

#define SM(m,n) SM[m+N+(n+N)*(2*N+1)]

#define cdouble _Complex double


//typedef std::complex<double> cdouble;
//cdouble I = std::complex<double>(0.0,1.0);


void besseljCPX(int, cdouble, cdouble *,int);
void besselyCPX(int, cdouble, cdouble *,int);


double expint(double x, int n, char mode);

void  exp_exp_int_ss(double * la, double x, double len, const int M, const int LAS, cdouble * res);
//void  exp_exp_int(double * la, double x, double y, double len, const int M, const int LAS, std::complex<double> * res);

extern "C" void zgesv_(int*,int*,cdouble*,int*,int*,cdouble*,int*,int*);

void MoMoff_GENS(cdouble epo, double la, double ro, double xo, cdouble * RT, const int N, const int NM){ //default value of N is 11

	const int L = 2*N+1;
//	const int NM = 4;	//number of modes taken into account
	int nrhs = NM*2, info, pivot[4*N+2];
	int M = 2*N+1;

	double zo = 0.0;
	double Z = 120.0*PI;
	double k;

	cdouble kw[NM], phk[NM];
	cdouble ZO[NM], ZOI[NM];
	cdouble XO[NM], XOI[NM];
	cdouble BJo[2*N+3], BJp[2*N+3], BJoD[2*N+1], BJpD[2*N+1];
	cdouble BHo[2*N+3], BHp[2*N+3], BHoD[2*N+1], BHpD[2*N+1];
	cdouble SM[(2*N+1)*(2*N+1)];
	cdouble res_p[4*N+1], res_n[4*N+1];

	cdouble PH[(2*N+1)*NM];
	cdouble PHI[(2*N+1)*NM];
	cdouble EX[(2*N+1)*NM*2];
	cdouble EHo[2*N+1];
	cdouble E11o, H11o;
	cdouble CFP;
	cdouble CFN;

	xo += 0.5;	// change the coordinae system
    //----------------------------------------------------------------------------

	epo = csqrt(epo);

	for(int m=1; m<=NM; ++m){
		XO(m)  = cexp(I*static_cast<double>(m)*PI*xo);
		XOI(m) = 1.0/XO(m);
	}

	exp_exp_int_ss(&la, 0.0, 2.0, 2*N, 1, res_n);
	exp_exp_int_ss(&la, 2.0*xo, 2.0, 2*N, 1, res_p);

	k = 2.0*PI*la;

	besseljCPX(0, 	  k*ro, &BJo[N+1], N+2);
	besseljCPX(0, epo*k*ro, &BJp[N+1], N+2);
	besselyCPX(0,	  k*ro, &BHo[N+1], N+2);
	besselyCPX(0, epo*k*ro, &BHp[N+1], N+2);

	for(int n=0;n<=N+1;++n){
		BHo(n) = BJo(n) + I*BHo(n);
		BHp(n) = BJp(n) + I*BHp(n);
	}
	for(int n=1;n<=N+1;++n){
		BJo(-n) = pow(-1.0,n)*BJo(n);
		BJp(-n) = pow(-1.0,n)*BJp(n);
		BHo(-n) = pow(-1.0,n)*BHo(n);
		BHp(-n) = pow(-1.0,n)*BHp(n);
	}
	for(int n=-N;n<=N;++n){
		BJoD(n) = 0.5*(BJo(n-1) - BJo(n+1));
		BJpD(n) = 0.5*(BJp(n-1) - BJp(n+1));

		BHoD(n) = 0.5*(BHo(n-1) - BHo(n+1));
		BHpD(n) = 0.5*(BHp(n-1) - BHp(n+1));
	}

	for(int m=1; m<=NM; ++m){
		if(m==1){
			kw(1)  = sqrt(k*k - PI*PI);
			phk(1) = atan2(PI, creal(kw(1)));
		}else{
			kw(m) = I*sqrt(PI*PI*m*m - k*k);
			double kxz = static_cast<double>(m)*PI/cimag(kw(m));
			phk(m) = 0.5*I*log(fabs((kxz-1.0)/(kxz+1.0)));
		}

		ZO(m)  = cexp(I*kw(m)*zo);
		ZOI(m) = 1.0/ZO(m);
//		printf("m = %d => %5.12f, %5.12f\n", m, creal(ZO(m)), cimag(ZO(m)));

	}

	for(int n=-N; n<=N; ++n){
		for(int m=1; m<=NM; ++m){
			PH(n,m)  = cexp(I*static_cast<double>(n)*phk(m));
			PHI(n,m) = 1.0/PH(n,m);

			EX(n,m,1) = -I*(cpow( I,n)*0.5*BJo(n)*ZO(m)*(XO(m)*PHI(n,m) - XOI(m)*PH(n,m)));
			EX(n,m,2) = -I*(cpow(-I,n)*0.5*BJo(n)*ZOI(m)*(XO(m)*PH(n,m) - XOI(m)*PHI(n,m)));
		}
		EHo(n) = BJpD(n)/BJp(n);
	}

	for(int m=-N;m<=N;++m){
		for(int n=-N;n<=N;++n){
			if(n%2==0){
				H11o = BJo(m)*(res_n(m-n) - res_p(n+m))*BJo(n);
				E11o = BJo(m)*(res_n(m-n) - res_p(n+m))*BJoD(n);
			}else{
				H11o = BJo(m)*(res_n(m-n) + res_p(n+m))*BJo(n);
				E11o = BJo(m)*(res_n(m-n) + res_p(n+m))*BJoD(n);
			}
			if(m==n){
				SM(m,n) = 1.0 + PI/2.0/I*ro*k*((E11o + BHo(m)*BJoD(m)) - epo*(H11o + BHo(m)*BJo(m))*EHo(n));
			}else{
				SM(m,n) = PI/2.0/I*ro*k*(E11o - epo*H11o*EHo(n));
			}
		}
	}

	zgesv_(&M, &nrhs, SM, &M, pivot, EX, &M, &info);

	for(int p=-N; p<=N; ++p){
//		printf("%d => %5.12f, %5.12f\n", p, creal(EX(p,1,2)), cimag(EX(p,1,2)));
	}

	for(int m=1; m<=NM; ++m){
		for(int n=1; n<=NM; ++n){
			if(m==n){
				RT(m,n,1,1) = 0.0;
				RT(m,n,2,1) = 1.0;
				RT(m,n,1,2) = 1.0;
				RT(m,n,2,2) = 0.0;
			}else{
				RT(m,n,1,1) = 0.0;
				RT(m,n,2,1) = 0.0;
				RT(m,n,1,2) = 0.0;
				RT(m,n,2,2) = 0.0;
			}
			for(int p=-N; p<=N; ++p){

				CFP = PI*k*ro*cpow(I,p)*(BJo(p)*epo*EHo(p) - BJoD(p))*EX(p,n,1)/kw(m);
				CFN = PI*k*ro*cpow(I,p)*(BJo(p)*epo*EHo(p) - BJoD(p))*EX(p,n,2)/kw(m);

				RT(m,n,1,1) -= (ZO(m)*(XO(m)*PH(p,m) - XOI(m)*PHI(p,m)))*CFP;
				RT(m,n,1,2) -= (ZO(m)*(XO(m)*PH(p,m) - XOI(m)*PHI(p,m)))*CFN;
				if(p%2==0){
					RT(m,n,2,1) -= (ZOI(m)*(XO(m)*PHI(p,m) - XOI(m)*PH(p,m)))*CFP;
					RT(m,n,2,2) -= (ZOI(m)*(XO(m)*PHI(p,m) - XOI(m)*PH(p,m)))*CFN;
				}else{
					RT(m,n,2,1) += (ZOI(m)*(XO(m)*PHI(p,m) - XOI(m)*PH(p,m)))*CFP;
					RT(m,n,2,2) += (ZOI(m)*(XO(m)*PHI(p,m) - XOI(m)*PH(p,m)))*CFN;
				}

			}
		}
	}


}


