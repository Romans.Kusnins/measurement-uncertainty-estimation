//#include<complex>
#include<complex.h>
#include<math.h>
#include<stdio.h>


#define PI 3.1415926535897931


#define BJo1(m)  BJo1[m+N+1]
#define BHo1(m)  BHo1[m+N+1]
#define BJoD1(m) BJoD1[m+N]
#define BHoD1(m) BHoD1[m+N]

#define BJo2(m)  BJo2[m+N+1]
#define BHo2(m)  BHo2[m+N+1]
#define BJoD2(m) BJoD2[m+N]
#define BHoD2(m) BHoD2[m+N]

#define BJp1(m)  BJp1[m+N+1]
#define BHp1(m)  BHp1[m+N+1]
#define BJpD1(m) BJpD1[m+N]
#define BHpD1(m) BHpD1[m+N]

#define BJp2(m)  BJp2[m+N+1]
#define BHp2(m)  BHp2[m+N+1]
#define BJpD2(m) BJpD2[m+N]
#define BHpD2(m) BHpD2[m+N]

#define EHo1(m)  EHo1[m+N]
#define EHo2(m)  EHo2[m+N]

#define EX1(m,n,l)  EX[m+N + (n-1)*2*WN      + (l-1)*2*WN*NM]
#define EX2(m,n,l)  EX[m+N + (n-1)*2*WN + WN + (l-1)*2*WN*NM]

#define kw(m)   kw[m-1]
#define phk(m)  phk[m-1]

#define  XO1(m)   XO1[m-1]
#define XOI1(m)  XOI1[m-1]

#define  XO2(m)   XO2[m-1]
#define XOI2(m)  XOI2[m-1]

#define  ZO1(m)   ZO1[m-1]
#define ZOI1(m)  ZOI1[m-1]

#define  ZO2(m)   ZO2[m-1]
#define ZOI2(m)  ZOI2[m-1]

#define  PH(m,n)   PH[m+N + (n-1)*WN]
#define PHI(m,n)  PHI[m+N + (n-1)*WN]

#define RT(m,n,l,o)   RT[m-1 + (n-1)*NM + (l-1)*NM*NM + (o-1)*NM*NM*2]

#define res11_p(m)  res11_p[2*N+m]
#define res11_n(m)  res11_n[2*N+m]

#define res12_p(m)  res12_p[2*N+m]
#define res12_n(m)  res12_n[2*N+m]

#define res21_p(m)  res21_p[2*N+m]
#define res21_n(m)  res21_n[2*N+m]

#define res22_p(m)  res22_p[2*N+m]
#define res22_n(m)  res22_n[2*N+m]

#define SM11(m,n)  SM[m+N +      (n+N)*2*WN]
#define SM21(m,n)  SM[m+N + WN + (n+N)*2*WN]
#define SM12(m,n)  SM[m+N +      (n+N)*2*WN + 2*WN*WN]
#define SM22(m,n)  SM[m+N + WN + (n+N)*2*WN + 2*WN*WN]


#define cdouble _Complex double

//typedef std::complex<double> cdouble;
//cdouble I = std::complex<double>(0.0,1.0);

void besseljCPX(int, cdouble, cdouble *, int);
void besselyCPX(int, cdouble, cdouble *, int);


double expint(double x, int n, char mode);

void  exp_exp_int_ss(double * la, double x, double len, const int M, const int LAS, cdouble * res);
void  exp_exp_int(double * la, double x, double y, double len, const int M, const int LAS, cdouble * res);

extern "C" void zgesv_(int*,int*,cdouble*,int*,int*,cdouble*,int*,int*);


void MoMoff_GENS(cdouble ep1, cdouble ep2, double la, double ro1, double ro2, double xo1, double xo2, double zo1, double zo2, cdouble * RT, const int N, const int NM){ //default value of N is 11

	const int L = 2*N+1;
//	const int NM = 4;	//number of modes taken into account

	int nrhs = NM*2, info, pivot[2*(4*N+2)];
	const int WN = 2*N+1;
	int M = 2*WN;

	double Z = 120.0*PI;
	double k;

	cdouble  kw[NM], phk[NM];

	cdouble  ZO1[NM], ZOI1[NM];
	cdouble  XO1[NM], XOI1[NM];

	cdouble  ZO2[NM], ZOI2[NM];
	cdouble  XO2[NM], XOI2[NM];

	cdouble  BJo1[WN+2], BJp1[WN+2], BJoD1[WN], BJpD1[WN];
	cdouble  BHo1[WN+2], BHp1[WN+2], BHoD1[WN], BHpD1[WN];

	cdouble  BJo2[WN+2], BJp2[WN+2], BJoD2[WN], BJpD2[WN];
	cdouble  BHo2[WN+2], BHp2[WN+2], BHoD2[WN], BHpD2[WN];

	cdouble  SM[4*WN*WN];

	cdouble  res11_p[4*N+1], res11_n[4*N+1];
	cdouble  res12_p[4*N+1], res12_n[4*N+1];
	cdouble  res21_p[4*N+1], res21_n[4*N+1];
	cdouble  res22_p[4*N+1], res22_n[4*N+1];

	cdouble   PH[WN*NM];
	cdouble  PHI[WN*NM];

	cdouble  EX[4*WN*NM];

	cdouble  EHo1[WN];
	cdouble  EHo2[WN];

	cdouble  E11o, H11o;
	cdouble  E12o, H12o;
	cdouble  E21o, H21o;
	cdouble  E22o, H22o;

	cdouble CFP1;
	cdouble CFN1;

	cdouble CFP2;
	cdouble CFN2;

	xo1 += 0.5;	// change the coordinae system
	xo2 += 0.5;
    //----------------------------------------------------------------------------

	ep1 = csqrt(ep1);
	ep2 = csqrt(ep2);

	for(int m=1; m<=NM; ++m){
		XO1(m)  = cexp(I*static_cast<double>(m)*PI*xo1);
		XOI1(m) = 1.0/XO1(m);

		XO2(m)  = cexp(I*static_cast<double>(m)*PI*xo2);
		XOI2(m) = 1.0/XO2(m);
	}

	exp_exp_int_ss(&la,     0.0, 2.0, 2*N, 1, res11_n);
	exp_exp_int_ss(&la, 2.0*xo1, 2.0, 2*N, 1, res11_p);

	exp_exp_int_ss(&la,    0.0,  2.0, 2*N, 1, res22_n);
	exp_exp_int_ss(&la, 2.0*xo2, 2.0, 2*N, 1, res22_p);

	exp_exp_int(&la, xo1-xo2, zo2-zo1, 2.0, 2*N, 1, res12_n);
	exp_exp_int(&la, xo1+xo2, zo2-zo1, 2.0, 2*N, 1, res12_p);

	exp_exp_int(&la, xo2-xo1, zo1-zo2, 2.0, 2*N, 1, res21_n);
	exp_exp_int(&la, xo2+xo1, zo1-zo2, 2.0, 2*N, 1, res21_p);

	for(int p=-2*N; p<=2*N; ++p){
//		printf("%d => %5.12f, %5.12f\n", p, creal(res21_p[p+2*N]), cimag(res21_p[p+2*N]));
	}

//============================================================

	k = 2.0*PI*la;

	besseljCPX(0, 	  k*ro1, &BJo1[N+1], N+2);
	besseljCPX(0, ep1*k*ro1, &BJp1[N+1], N+2);
	besselyCPX(0,	  k*ro1, &BHo1[N+1], N+2);
	besselyCPX(0, ep1*k*ro1, &BHp1[N+1], N+2);

	besseljCPX(0, 	  k*ro2, &BJo2[N+1], N+2);
	besseljCPX(0, ep2*k*ro2, &BJp2[N+1], N+2);
	besselyCPX(0,	  k*ro2, &BHo2[N+1], N+2);
	besselyCPX(0, ep2*k*ro2, &BHp2[N+1], N+2);

	for(int n=0;n<=N+1;++n){
		BHo1(n) = BJo1(n) + I*BHo1(n);
		BHp1(n) = BJp1(n) + I*BHp1(n);

		BHo2(n) = BJo2(n) + I*BHo2(n);
		BHp2(n) = BJp2(n) + I*BHp2(n);
	}
	for(int n=1;n<=N+1;++n){
		BJo1(-n) = pow(-1.0,n)*BJo1(n);
		BJp1(-n) = pow(-1.0,n)*BJp1(n);
		BHo1(-n) = pow(-1.0,n)*BHo1(n);
		BHp1(-n) = pow(-1.0,n)*BHp1(n);

		BJo2(-n) = pow(-1.0,n)*BJo2(n);
		BJp2(-n) = pow(-1.0,n)*BJp2(n);
		BHo2(-n) = pow(-1.0,n)*BHo2(n);
		BHp2(-n) = pow(-1.0,n)*BHp2(n);
	}
	for(int n=-N;n<=N;++n){
		BJoD1(n) = 0.5*(BJo1(n-1) - BJo1(n+1));
		BJpD1(n) = 0.5*(BJp1(n-1) - BJp1(n+1));

		BHoD1(n) = 0.5*(BHo1(n-1) - BHo1(n+1));
		BHpD1(n) = 0.5*(BHp1(n-1) - BHp1(n+1));

		BJoD2(n) = 0.5*(BJo2(n-1) - BJo2(n+1));
		BJpD2(n) = 0.5*(BJp2(n-1) - BJp2(n+1));

		BHoD2(n) = 0.5*(BHo2(n-1) - BHo2(n+1));
		BHpD2(n) = 0.5*(BHp2(n-1) - BHp2(n+1));
	}

	for(int m=1; m<=NM; ++m){
		if(m==1){
			kw(1)  = sqrt(k*k - PI*PI);
			phk(1) = atan2(PI, creal(kw(1)));
		}else{
			kw(m) = I*sqrt(PI*PI*m*m - k*k);
			double kxz = cabs(static_cast<double>(m)*PI/kw(m));
			phk(m) = 0.5*I*log(fabs((1.0-kxz)/(1.0+kxz)));
			if(kxz > 1.0){
				phk(m) += PI/2.0;
			}
		}
		printf("kw = %d => %5.12f, %5.12f\n", m, creal(kw(m)), cimag(kw(m)));
		printf("phk = %d => %5.12f, %5.12f\n", m, creal(phk(m)), cimag(phk(m)));

		ZO1(m)  = cexp(I*kw(m)*zo1);
		ZOI1(m) = 1.0/ZO1(m);

		ZO2(m)  = cexp(I*kw(m)*zo2);
		ZOI2(m) = 1.0/ZO2(m);

	}
	for(int n=-N; n<=N; ++n){
		for(int m=1; m<=NM; ++m){
			PH(n,m)  = cexp(I*static_cast<double>(n)*phk(m));
			PHI(n,m) = 1.0/PH(n,m);

			EX1(n,m,1) = -I*(cpow( I,n)*0.5*BJo1(n)*ZO1(m)*(XO1(m)*PHI(n,m) - XOI1(m)*PH(n,m)));
			EX1(n,m,2) = -I*(cpow(-I,n)*0.5*BJo1(n)*ZOI1(m)*(XO1(m)*PH(n,m) - XOI1(m)*PHI(n,m)));

			EX2(n,m,1) = -I*(cpow( I,n)*0.5*BJo2(n)*ZO2(m)*(XO2(m)*PHI(n,m) - XOI2(m)*PH(n,m)));
			EX2(n,m,2) = -I*(cpow(-I,n)*0.5*BJo2(n)*ZOI2(m)*(XO2(m)*PH(n,m) - XOI2(m)*PHI(n,m)));

		}
		EHo1(n) = BJpD1(n)/BJp1(n);
		EHo2(n) = BJpD2(n)/BJp2(n);
	}

	for(int n=-N; n<=N; ++n){
//		printf("%d => %5.12f, %5.12f\n", n, creal(EX2(n,1,1)), cimag(EX2(n,1,1)));
	}



	for(int m=-N;m<=N;++m){
		for(int n=-N;n<=N;++n){
			if(n%2==0){
				H11o = BJo1(m)*(res11_n(m-n) - res11_p(n+m))*BJo1(n);
				E11o = BJo1(m)*(res11_n(m-n) - res11_p(n+m))*BJoD1(n);

				H12o = BJo1(m)*(res12_n(m-n) - res12_p(n+m))*BJo2(n);
				E12o = BJo1(m)*(res12_n(m-n) - res12_p(n+m))*BJoD2(n);

				H21o = BJo2(m)*(res21_n(m-n) - res21_p(n+m))*BJo1(n);
				E21o = BJo2(m)*(res21_n(m-n) - res21_p(n+m))*BJoD1(n);

				H22o = BJo2(m)*(res22_n(m-n) - res22_p(n+m))*BJo2(n);
				E22o = BJo2(m)*(res22_n(m-n) - res22_p(n+m))*BJoD2(n);
			}else{
				H11o = BJo1(m)*(res11_n(m-n) + res11_p(n+m))*BJo1(n);
				E11o = BJo1(m)*(res11_n(m-n) + res11_p(n+m))*BJoD1(n);

				H12o = BJo1(m)*(res12_n(m-n) + res12_p(n+m))*BJo2(n);
				E12o = BJo1(m)*(res12_n(m-n) + res12_p(n+m))*BJoD2(n);

				H21o = BJo2(m)*(res21_n(m-n) + res21_p(n+m))*BJo1(n);
				E21o = BJo2(m)*(res21_n(m-n) + res21_p(n+m))*BJoD1(n);

				H22o = BJo2(m)*(res22_n(m-n) + res22_p(n+m))*BJo2(n);
				E22o = BJo2(m)*(res22_n(m-n) + res22_p(n+m))*BJoD2(n);
			}
			if(m==n){
				SM11(m,n) = 1.0 + PI/2.0/I*ro1*k*((E11o + BHo1(m)*BJoD1(m)) - ep1*(H11o + BHo1(m)*BJo1(m))*EHo1(n));
				SM22(m,n) = 1.0 + PI/2.0/I*ro2*k*((E22o + BHo2(m)*BJoD2(m)) - ep2*(H22o + BHo2(m)*BJo2(m))*EHo2(n));

				SM12(m,n) = PI/2.0/I*ro2*k*(E12o - ep2*H12o*EHo2(n));
				SM21(m,n) = PI/2.0/I*ro1*k*(E21o - ep1*H21o*EHo1(n));
			}else{
				SM11(m,n) = PI/2.0/I*ro1*k*(E11o - ep1*H11o*EHo1(n));
				SM22(m,n) = PI/2.0/I*ro2*k*(E22o - ep2*H22o*EHo2(n));

				SM12(m,n) = PI/2.0/I*ro2*k*(E12o - ep2*H12o*EHo2(n));
				SM21(m,n) = PI/2.0/I*ro1*k*(E21o - ep1*H21o*EHo1(n));
			}
		}
	}

	zgesv_(&M, &nrhs, SM, &M, pivot, EX, &M, &info);

	for(int p=-N; p<=N; ++p){
//		printf("%d => %5.12f, %5.12f\n", p, creal(EX(p,1,2)), cimag(EX(p,1,2)));
	}

	for(int m=1; m<=NM; ++m){
		for(int n=1; n<=NM; ++n){
			if(m==n){
				RT(m,n,1,1) = 0.0;
				RT(m,n,2,1) = 1.0;
				RT(m,n,1,2) = 1.0;
				RT(m,n,2,2) = 0.0;
			}else{
				RT(m,n,1,1) = 0.0;
				RT(m,n,2,1) = 0.0;
				RT(m,n,1,2) = 0.0;
				RT(m,n,2,2) = 0.0;
			}
			for(int p=-N; p<=N; ++p){

				CFP1 = PI*k*ro1*cpow(I,p)*(BJo1(p)*ep1*EHo1(p) - BJoD1(p))*EX1(p,n,1)/kw(m);
				CFN1 = PI*k*ro1*cpow(I,p)*(BJo1(p)*ep1*EHo1(p) - BJoD1(p))*EX1(p,n,2)/kw(m);

				CFP2 = PI*k*ro2*cpow(I,p)*(BJo2(p)*ep2*EHo2(p) - BJoD2(p))*EX2(p,n,1)/kw(m);
				CFN2 = PI*k*ro2*cpow(I,p)*(BJo2(p)*ep2*EHo2(p) - BJoD2(p))*EX2(p,n,2)/kw(m);

				RT(m,n,1,1) -= (ZO1(m)*(XO1(m)*PH(p,m) - XOI1(m)*PHI(p,m)))*CFP1;
				RT(m,n,1,1) -= (ZO2(m)*(XO2(m)*PH(p,m) - XOI2(m)*PHI(p,m)))*CFP2;

				RT(m,n,1,2) -= (ZO1(m)*(XO1(m)*PH(p,m) - XOI1(m)*PHI(p,m)))*CFN1;
				RT(m,n,1,2) -= (ZO2(m)*(XO2(m)*PH(p,m) - XOI2(m)*PHI(p,m)))*CFN2;

				if(p%2==0){
					RT(m,n,2,1) -= (ZOI1(m)*(XO1(m)*PHI(p,m) - XOI1(m)*PH(p,m)))*CFP1;
					RT(m,n,2,1) -= (ZOI2(m)*(XO2(m)*PHI(p,m) - XOI2(m)*PH(p,m)))*CFP2;

					RT(m,n,2,2) -= (ZOI1(m)*(XO1(m)*PHI(p,m) - XOI1(m)*PH(p,m)))*CFN1;
					RT(m,n,2,2) -= (ZOI2(m)*(XO2(m)*PHI(p,m) - XOI2(m)*PH(p,m)))*CFN2;
				}else{
					RT(m,n,2,1) += (ZOI1(m)*(XO1(m)*PHI(p,m) - XOI1(m)*PH(p,m)))*CFP1;
					RT(m,n,2,1) += (ZOI2(m)*(XO2(m)*PHI(p,m) - XOI2(m)*PH(p,m)))*CFP2;

					RT(m,n,2,2) += (ZOI1(m)*(XO1(m)*PHI(p,m) - XOI1(m)*PH(p,m)))*CFN1;
					RT(m,n,2,2) += (ZOI2(m)*(XO2(m)*PHI(p,m) - XOI2(m)*PH(p,m)))*CFN2;
				}

			}
		}
	}


}


