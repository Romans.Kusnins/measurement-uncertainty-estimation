import os
import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *
from Derivatives import *

try:
        matplot = True
        from matplotlib import pyplot as plt
except ModuleNotFoundError:
        print("The matplotlib module is not installed.")
        matplot = False


from plot2pdf import *

from Uncertainty_vs_S11 import *

import GlobalVARS
#==================================================================================
xMin 		= None
xMax 		= None
MaxVal 		= None

#task 		= 'sensitivity'		# set to plot one of the sensitivity coefficients
task 		= 'total'		# set to plot the estimated measurement uncertainty and the means value for the model at hand
#---------------------------------------
sensparam 	= 'S11abs'		# name of the model parameter for plotting the sensitivity to it
#---------------------------------------
var_param 	= 'ep2'			# name of the variable model parameter
#---------------------------------------
quantity 	= 'var'			# set to "var" to plot the measurement uncertainty or to "mean" to plot the estimated mean value

LANG 		= 'LV'

GlobalVARS.LANG = LANG


#PREFIX = '_29_'

#PREFIX = ['_6_0_', '_6_5_', '', '_7_5_']

PREFIX1 = ''		# for the single slab model
PREFIX2 = ''		# for the two slab model
PREFIX3 = ''		# for the three slab model

estim_method 	= 'MC' # set to MC for the Monte-Carlo method or to EPM for the Error Propagation Method

ext_type 	= 'three'


if var_param == 'L':
	MCNUM = 100000
else:
	MCNUM = 20000
#==================================================================================
isPDF 		= True
#isPRINT	= False
#==================================================================================
xlabels		= dict()

xlabels['ep1']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,aux}}$'
xlabels['ep2']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,mut}}$'
xlabels['tan1'] = '$\\tan_{\\delta^{\\mathrm{aux}}}$'
xlabels['tan2'] = '$\\tan_{\\delta^{\\mathrm{mut}}}$'
xlabels['d1']   = '$d_{\\mathrm{aux}}$ mm'
xlabels['d2']   = '$d_{\\mathrm{mut}}$ mm'
xlabels['L']    = '$d_{\\mathrm{int}}$ mm'
xlabels['f']    = 'f, GHz'

paramname       = {'S11abs': '$u_{|S_{11}|}$', 'd1': '$u_{d_{\\mathrm{aux}}}$', 'd2': '$u_{d_{\\mathrm{mut}}}$', 'L': '$u_{d_{\\mathrm{in}}}$', 'ep1': '$u_{\\varepsilon^{\\prime}_{\\mathrm{r,aux}}}$', 'tan1': '$ u_{\\tan{\\delta^{\\mathrm{aux}}}}$', 'tan2': 'u_{\\tan{\\delta^{\\mathrm{mut}}}}', 'f': 'u_{f}'}
#==================================================================================
# PLOTTING CALCULATED DATA
#==================================================================================
FOLDER1 = './RESULT_1SL_FREE_OLD/'
#FOLDER1 = './RESULT_1SL_FREE/'
FOLDER2 = './RESULTS_2SL_FREE/'
FOLDER3 = './RESULTS_3SL_FREE/'

MCSTR = '{:}'.format(MCNUM)

#if estim_method == 'MC' and  var_param == 'ep2':
if var_param == 'ep2':
	var_param1 = 'ep1'
#else:
#	var_param1 = 'ep2'


if task == 'sensitivity':

	if estim_method == 'MC':
		MCfilename1 	 = FOLDER1 + 'single_slab_free_var_' + var_param1 + '_sens_' + sensparam + '_MCnum'+ PREFIX1 + '_' + MCSTR + '.npy'
		MCfilename_mean1 = FOLDER1 + 'single_slab_free_var_' + var_param1 + '_sens_' + sensparam + '_MCnum_mean'+ PREFIX1 + '_' + MCSTR + '.npy'
	else:
		EPMfilename1 	 = FOLDER1 + 'single-slab-free_var_' + var_param1 + '_standard_' + sensparam + '_EPM' + PREFIX1 + '.npy'
else:
	if estim_method == 'MC':
		MCfilename1 	 = FOLDER1 + 'single_slab_free_var_' + var_param1 + '_total_MCnum' + PREFIX1 + '_' + MCSTR + '.npy'
		MCfilename_mean1 = FOLDER1 + 'single_slab_free_var_' + var_param1 + '_total_MCnum_mean' + PREFIX1 + '_' + MCSTR + '.npy'
	else:
		EPMfilename1 	 = FOLDER1 + 'single-slab-free_var_' + var_param1 + '_total_EPM' + PREFIX1 + '.npy'

if estim_method == 'MC':
	MCdata1		= np.load(MCfilename1)
	MCdata_mean1 	= np.load(MCfilename_mean1)
else:
	EPMdata1 	= np.load(EPMfilename1)
#==================================================================================
if MaxVal != None:
	if estim_method == 'MC':
		Mdata_mean1[1, MCdata_mean1[1,:] > MaxVal]	= np.nan
		MCdata1[1, MCdata1[1,:]  > MaxVal]		= np.nan
	else:
		EPMdata1_mean[1, EPMdata_mean1[1,:] > MaxVal]	= np.nan
		EPMdata1[1, EPMdata1[1,:]  > MaxVal]		= np.nan

if xMin != None:
	if estim_method == 'MC':
		ID 	 = MCdata1[0,:] >= xMin
		MCdata1  = MCdata1[:,ID]
	else:
		ID 	 = EPMdata1[0,:] >= xMin
		EPMdata1 = EPMdata1[:,ID]

if xMax != None:
	if estim_method == 'MC':
		ID 	 = MCdata1[0,:] <= xMax
		MCdata1  = MCdata1[:,ID]
	else:
		ID 	 = EPMdata1[0,:] <= xMax
		EPMdata1 = EPMdata1[:,ID]
#---------------------------------------------------------------------------------------------------
if ext_type == 'two':

	if task == 'sensitivity':
		if estim_method == 'MC':
			MCfilename2 	 = FOLDER2 + 'two_slabs_free_var_' + var_param + '_sens_' + sensparam + '_MCnum'+ PREFIX2 + '_' + MCSTR + '.npy'
			MCfilename_mean2 = FOLDER2 + 'two_slabs_free_var_' + var_param + '_sens_' + sensparam + '_MCnum_mean'+ PREFIX2 + '_' + MCSTR + '.npy'
		else:
			EPMfilename2 	 = FOLDER2 + 'two-slabs-free_var_' + var_param + '_standard_' + sensparam + '_EPM' + PREFIX2 + '.npy'
	else:
		if estim_method == 'MC':
			MCfilename2 	 = FOLDER2 + 'two_slabs_free_var_' + var_param + '_total_MCnum' + PREFIX2 + '_' + MCSTR + '.npy'
			MCfilename_mean2 = FOLDER2 + 'two_slabs_free_var_' + var_param + '_total_MCnum_mean' + PREFIX2 + '_' + MCSTR + '.npy'
		else:
			EPMfilename2 	 = FOLDER2 + 'two-slabs-free_var_' + var_param + '_total_EPM' + PREFIX2 + '.npy'

	if estim_method == 'MC':
		MCdata2		= np.load(MCfilename2)
		MCdata_mean2 	= np.load(MCfilename_mean2)
	else:
		EPMdata2 	= np.load(EPMfilename2)
	#==================================================================================
	if MaxVal != None:
		if estim_method == 'MC':
			Mdata_mean2[1, MCdata_mean2[1,:] > MaxVal]	= np.nan
			MCdata2[1, MCdata2[1,:]  > MaxVal]		= np.nan
		else:
			EPMdata2_mean[1, EPMdata_mean2[1,:] > MaxVal]	= np.nan
			EPMdata2[1, EPMdata2[1,:]  > MaxVal]		= np.nan

	if xMin != None:
		if estim_method == 'MC':
			ID 	 = MCdata2[0,:] >= xMin
			MCdata2  = MCdata2[:,ID]
		else:
			ID 	 = EPMdata2[0,:] >= xMin
			EPMdata2 = EPMdata2[:,ID]

	if xMax != None:
		if estim_method == 'MC':
			ID 	 = MCdata2[0,:] <= xMax
			MCdata2  = MCdata2[:,ID]
		else:
			ID 	 = EPMdata2[0,:] <= xMax
			EPMdata2 = EPMdata2[:,ID]

elif ext_type == 'three':


	if task == 'sensitivity':
		if estim_method == 'MC':
			MCfilename2 	 = FOLDER3 + 'three_slabs_free_var_' + var_param + '_sens_' + sensparam + '_MCnum'+ PREFIX2 + '_' + MCSTR + '.npy'
#			MCfilename_mean2 = FOLDER3 + 'three_slabs_free_var_' + var_param + '_sens_' + sensparam + '_MCnum_mean'+ PREFIX2 + '_' + MCSTR + '.npy'
		else:
			EPMfilename2 	 = FOLDER3 + 'three-slabs-free_var_' + var_param + '_standard_' + sensparam + '_EPM' + PREFIX2 + '.npy'
	else:
		if estim_method == 'MC':
			MCfilename2 	 = FOLDER3 + 'three_slabs_free_var_' + var_param + '_total_MCnum' + PREFIX2 + '_' + MCSTR + '.npy'
#			MCfilename_mean2 = FOLDER3 + 'three_slabs_free_var_' + var_param + '_total_MCnum_mean' + PREFIX2 + '_' + MCSTR + '.npy'
		else:
			EPMfilename2 	 = FOLDER3 + 'three-slabs-free_var_' + var_param + '_total_EPM' + PREFIX2 + '.npy'

	if estim_method == 'MC':
		MCdata2		= np.load(MCfilename2)
#		MCdata_mean2 	= np.load(MCfilename_mean2)
	else:
		EPMdata2 	= np.load(EPMfilename2)
	#==================================================================================
	if MaxVal != None:
		if estim_method == 'MC':
#			Mdata_mean2[1, MCdata_mean2[1,:] > MaxVal]	= np.nan
			MCdata2[1, MCdata2[1,:]  > MaxVal]		= np.nan
		else:
			EPMdata2_mean[1, EPMdata_mean2[1,:] > MaxVal]	= np.nan
			EPMdata2[1, EPMdata2[1,:]  > MaxVal]		= np.nan

	if xMin != None:
		if estim_method == 'MC':
			ID 	 = MCdata2[0,:] >= xMin
			MCdata2  = MCdata2[:,ID]
		else:
			ID 	 = EPMdata2[0,:] >= xMin
			EPMdata2 = EPMdata2[:,ID]

	if xMax != None:
		if estim_method == 'MC':
			ID 	 = MCdata2[0,:] <= xMax
			MCdata2  = MCdata2[:,ID]
		else:
			ID 	 = EPMdata2[0,:] <= xMax
			EPMdata2 = EPMdata2[:,ID]
#==================================================================================
if isPDF:

	LEGPOS = [0.05, 0.7]

	if ext_type == 'two':

		if LANG == 'EN':
			legend = ['CFSM', 'ETSFSM']
		else:
			legend = ['KBTMM', 'BTDMM']

		if task == 'sensitivity':
			filename  = 'TWO_AND_SINGLE_SL_FREE_VAR_' + var_param + '_SENS_' + sensparam + PREFIX1
			labels = {'xlabel': xlabels[var_param], 'ylabel': paramname[sensparam]}
		else:
			filename  = 'TWO_AND_SINGLE_SL_FREE_VAR_' + var_param + '_TOTAL' + PREFIX1
			labels = {'xlabel': xlabels[var_param], 'ylabel': '$u(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})$'}

		if quantity == 'mean':
			filename  = 'TWO_AND_SINGLE_SL_FREE_VAR_' + var_param + 'MEAN' + PREFIX1
			labels = {'xlabel': xlabels[var_param], 'ylabel': '$\\bar{u}(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})$'}
		#	labels = {'xlabel': xlabels[var_param], 'ylabel': ylabeldelta + ' ' + paramname[param]}
			plot2latex([MCdata_mean1[0,:], MCdata_mean2[0,:]], [MCdata_mean1[1,:], MCdata_mean2[1,:]], LineType=['',''], xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, YZero=False, LegendPosition=LEGPOS, FileName=filename)
		else:
			if estim_method == 'MC':
				plot2latex([MCdata1[0,:], MCdata2[0,:]], [MCdata1[1,:], MCdata2[1,:]], LineType=['',''], xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, LegendPosition=LEGPOS, FileName=filename)
			else:
				plot2latex([EPMdata1[0,:], EPMdata2[0,:]], [EPMdata1[1,:], EPMdata2[1,:]], LineType=['',''], xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, LegendPosition=LEGPOS, FileName=filename)

	elif ext_type == 'three':

		if LANG == 'EN':
			legend = ['CFSM', 'ETrSFSM']
		else:
			legend = ['KBTMM', 'BTTMM']

		if task == 'sensitivity':
			filename  = 'THREE_AND_SINGLE_SL_FREE_VAR_' + var_param + '_SENS_' + sensparam + PREFIX1
			labels = {'xlabel': xlabels[var_param], 'ylabel': paramname[sensparam]}
		else:
			filename  = 'THREE_AND_SINGLE_SL_FREE_VAR_' + var_param + '_TOTAL' + PREFIX1
			labels = {'xlabel': xlabels[var_param], 'ylabel': '$u(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})$'}

		if quantity == 'mean':
			filename  = 'THREE_AND_SINGLE_SL_FREE_VAR_' + var_param + 'MEAN' + PREFIX1
			labels = {'xlabel': xlabels[var_param], 'ylabel': '$\\bar{u}(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})$'}
		#	labels = {'xlabel': xlabels[var_param], 'ylabel': ylabeldelta + ' ' + paramname[param]}
			plot2latex([MCdata_mean1[0,:], MCdata_mean2[0,:]], [MCdata_mean1[1,:], MCdata_mean2[1,:]], LineType=['',''], xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, YZero=False, LegendPosition=LEGPOS, FileName=filename)
		else:
			if estim_method == 'MC':
				plot2latex([MCdata1[0,:], MCdata2[0,:]], [MCdata1[1,:], MCdata2[1,:]], LineType=['','h'], xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, LegendPosition=LEGPOS, FileName=filename)
			else:
				plot2latex([EPMdata1[0,:], EPMdata2[0,:]], [EPMdata1[1,:], EPMdata2[1,:]], LineType=['',''], xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, LegendPosition=LEGPOS, FileName=filename)


#	plot2latex([EPMdata[0,:], MCdata[0,:]], [EPMdata[1,:], MCdata[1,:]], ylabel_vert=True, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels)
elif matplot:
#	plt.ylabel(ylabeldelta + ' ' + paramname[param])
	plt.plot(MCdata[0,:], EPMdata[1,:])
	plt.grid()
	plt.show()
else:
	pass
#       print('Calculated data have been saved into a file.')

print('--------------------------------------------------------')
