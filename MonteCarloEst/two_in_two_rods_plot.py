import numpy as np
from matplotlib import pyplot as plt
from slab_res3md import (POW2)
from MoM import *

#===========================================================================
ep1     = [7.2]			# auxiliary holed rod dielectric constant
ep2     = [12.0]		# MUT holed rod dielectric constant

ro1     = 5.0           	# auxiliary rod radius in mm
ro2     = 5.0			# MUT rod radius in mm

ro1in   = [0.5]           	# auxilairy rod hole radius in mm
ro2in   = [0.5]			# MUT rod hole radius in mm

d1	= 2.0           	# auxilairy rod hole-to-hole distance in mm
d2	= 2.0			# MUT rod hole-to-hole distance in mm

ph1  	= 0.0           	# auxiliary rod hole line angle in radians
ph2 	= 0.0			# MUT rod hole line angle in radians

L       = [10.0, 20.0, 0.01]	# interrod separation distance in mm
f       = [10.0]		# operating frequency in GHz

a       = 22.86			# waveguide width in mm
#===========================================================================
NM      = 3			# number of higher order waveguide modes considered
N       = 5			# number of basis functions for each rod surface
#===========================================================================
MoMinit()			# perform library initialization
#===========================================================================
def evalparam(roddata, lam):

	param 		= MoMparam()

	param.ep1 	= roddata['ep'] + 0.0j
	param.ep2 	= 1.0 + 0.0j
	param.ep3 	= 1.0 + 0.0j

	param.la 	= a/lam

	param.ro1 	= roddata['ro']/a
	param.ro2 	= roddata['roin']/a
	param.ro3 	= roddata['roin']/a

	param.xo1 	= 0.5
	param.xo2 	= 0.5 + 0.5*roddata['d']/a*np.cos(roddata['ph'])
	param.xo3 	= 0.5 - 0.5*roddata['d']/a*np.cos(roddata['ph'])

	param.zo1 	= 0.0
	param.zo2 	= 0.5 - 0.5*roddata['d']/a*np.sin(roddata['ph'])
	param.zo3 	= 0.5 + 0.5*roddata['d']/a*np.sin(roddata['ph'])

	return param
#===========================================================================
def calcT(L, lam):
	if NM == 1:
		T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
	else:
		T = np.eye(NM, dtype='complex')
		for n in range(NM):
			T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)

	return T
#===========================================================================
def MoM_calc_S_matrix(roddata, lam):

	param = evalparam(roddata, lam)
	S11, S12, S21, S22 = MoM_two_in_one_(param, NM, N)

	return S11, S12, S21, S22
#===========================================================================
def calcS11(roddata1, roddata2, L, lam):

	S11a, S12a, S21a, S22a = MoM_calc_S_matrix(roddata1, lam)
	S11b, S12b, S21b, S22b = MoM_calc_S_matrix(roddata2, lam)

	T = calcT(L, lam)

	if NM == 1:
		S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
	else:
		S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

	return S11[0,0]
#===========================================================================
xlabels  = dict()
xlabels1 = dict()
xlabels2 = dict()

xlabels1['ep'] = '1st rod dielectric constant, mm'
xlabels2['ep'] = '2nd rod dielectric constant, mm'

xlabels1['ro'] = '1st rod radius, mm'
xlabels2['ro'] = '2nd rod radius, mm'

xlabels1['roin'] = '1st rod hole radius, mm'
xlabels2['roin'] = '2nd rod hole radius, mm'

xlabels1['d'] = '1st rod interhole distance, mm'
xlabels2['d'] = '2nd rod interhole distance, mm'

xlabels1['ph'] = '1st rod hole angle, rad'
xlabels2['ph'] = '2nd rod hole angle, rad'

xlabels['L'] = 'Rod separation distance , mm'
xlabels['f'] = 'Frequency, GHz'
#============================================================================
roddata1 = {'ep': ep1, 'ro': ro1, 'roin': ro1in, 'd': d1, 'ph': ph1}
roddata2 = {'ep': ep2, 'ro': ro2, 'roin': ro2in, 'd': d2, 'ph': ph2}
#============================================================================

list_found 	= False
rodnum		= None

for par in roddata1.keys():
	if type(roddata1[par]) == list:
		if len(roddata1[par]) > 1 and not list_found:
			var_range	= roddata1[par];
			parname 	= par
			rodnum  	= 1
			list_found 	= True
		else:
			roddata1[par] = roddata1[par][0];
for par in roddata2.keys():
	if type(roddata2[par]) == list:
		if len(roddata2[par]) > 1 and not list_found:
			var_range	= roddata2[par];
			parname 	= par
			rodnum  	= 2
			list_found 	= True
		else:
			roddata2[par] = roddata2[par][0];


if type(L) == list:
	if len(L) > 1 and not list_found:
		var_range 	=  L
		parname 	= 'L'
		list_found 	= True
	else:
		L = L[0]

if type(f) == list:
	if len(f) > 1 and not list_found:
		var_range 	=  f
		parname 	= 'f'
		list_found 	= True
	else:
		f = f[0]

if not list_found:
	print("There is nothing to plot.")
	quit()
#=====================================================================
var_values 	= np.arange(*var_range)
S11 		= np.ndarray((var_values.shape), dtype='complex')

if rodnum == 1:
	lam = 300.0/f
	for n in range(var_values.shape[0]):
		roddata1[parname] = var_values[n]
		S11[n] = calcS11(roddata1, roddata2, L, lam)
elif rodnum == 2:
	lam = 300.0/f
	for n in range(var_values.shape[0]):
		roddata2[parname] = var_values[n]
		S11[n] = calcS11(roddata1, roddata2, L, lam)
else:
	if parname == 'L':
		lam = 300.0/f
		for n in range(var_values.shape[0]):
			S11[n] = calcS11(roddata1, roddata2, var_values[n], lam)
	else:
		for n in range(var_values.shape[0]):
			lam = 300.0/var_values[n]
			S11[n] = calcS11(roddata1, roddata2, L, lam)

plt.plot(var_values, np.abs(S11))

if rodnum == 1:
	plt.xlabel(xlabels1[parname])
elif rodnum == 2:
	plt.xlabel(xlabels2[parname])
else:
	plt.xlabel(xlabels[parname])

plt.ylabel('|S11|')
plt.grid()
plt.show()
