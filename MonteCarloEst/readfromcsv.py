import numpy as np


def readcsv(filename):

#	filename = 'two_slabs_S11abs.csv'
#	filename = 'three_slabs_S11abs.csv'

	fp = open(filename)

	x = list()
	y = list()

	n = 1
	for line in fp:
		if n > 1:
			com1  	= line.find(',')
			nline 	= line[com1+1:]
			com2  	= nline.find(',')

			x.append(float(nline[:com2]))
			y.append(float(nline[com2+1:]))
		n += 1

	return np.asarray(x), np.asarray(y)
