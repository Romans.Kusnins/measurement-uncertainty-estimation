#include<stdio.h>
#include<math.h>
#include<complex.h>

typedef _Complex double cdouble;

struct DataB{
        cdouble         ep1;
        cdouble         ep2;
        double          la;
        double          ro1;
        double          ro2;
        double          xo1;
        double          xo2;
        double          zo1;
        double          zo2;
};

struct DataC{
        cdouble         ep1;
        cdouble         ep2;
        cdouble         ep3;
        double          la;
        double          ro1;
        double          ro2;
        double          ro3;
        double          xo1;
        double          xo2;
        double          xo3;
        double          zo1;
        double          zo2;
        double          zo3;
};


void MoM_two_solid_rods(cdouble, cdouble, double, double, double, double, double, double, double,  cdouble*, cdouble*, cdouble*, cdouble*, const int, const int);
void MoM_two_in_two(cdouble, cdouble, cdouble, double, double, double, double, double, double, double, double, double, double, cdouble*, cdouble*, cdouble*, cdouble*, const int, const int);
void MoM_two_in_one(cdouble, cdouble, cdouble, double, double, double, double, double, double, double, double, double, double, cdouble*, cdouble*, cdouble*, cdouble*, const int, const int);

//---------------------------------------------------------------------------

void funb(cdouble * SMN[], struct DataB DC, const int N, const int MN){
	MoM_two_solid_rods(DC.ep1, DC.ep2, DC.la, DC.ro1, DC.ro2, DC.xo1, DC.xo2, DC.zo1, DC.zo2, SMN[0], SMN[1], SMN[2], SMN[3], N, MN);
}

void func(cdouble * SMN[], struct DataC DC, const int N, const int MN){
	MoM_two_in_one(DC.ep1, DC.ep2, DC.ep3, DC.la, DC.ro1, DC.ro2, DC.ro3, DC.xo1, DC.xo2, DC.xo3, DC.zo1, DC.zo2, DC.zo3, SMN[0], SMN[1], SMN[2], SMN[3], N, MN);
}


