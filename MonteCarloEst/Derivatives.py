import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, root_finding, single_slab_guide_matrix, POW2)
from MoM import *

#=================================================================================
#=================================================================================
def Derivative_single_slab_free_space(slabdata, derparam, delta, NTOL, MAXIT):

	ep1	= slabdata['ep1']
	tan1	= slabdata['tan1']

	d1	= slabdata['d1']

	f	= slabdata['f']
	#-------------------------------------------------------------------------
	# ep1		# auxiliary slab dielectric constant
	# ep2		# MUT slab dielectric constant
	#
	# d1  		# auxiliary slab thickness in mm
	# d2 		# MUT slab thickness in mm
	#
	# L  		# interslab separation distance in mm
	# f  		# operating frequency in GHz
	#
	# MAXIT 	# max number of iterations for Newton's method
	# NTOL 		# solution tolerance for Newton's method
	#-------------------------------------------------------------------------
	lam = 300.0/f

	S11abs = np.abs(three_slab_free_space(1.0, 1.0, ep1*(1.0+1.0j*tan1), 0.1, d1/lam, 0.1))
	#=========================================================================
	if derparam == 'd1':
		delta_sc = d1*delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(1.0, 1.0, epvar*(1.0+1.0j*tan1), 0.1, d1*(1.0+delta)/lam, 0.1)
			return np.abs(S11)
	elif derparam == 'tan1':
		delta_sc = tan1*delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(1.0, 1.0, epvar*(1.0+1.0j*tan1*(1.0+delta)), 0.1, d1/lam, 0.1)
			return np.abs(S11)
	elif derparam == 'f':
		delta_sc = f*delta
		lam = 300.0/(f*(1.0+delta));
		def func(epvar, **kwd):
			S11 = three_slab_free_space(1.0, 1.0, epvar*(1.0+1.0j*tan1), 0.1, d1/lam, 0.1)
			return np.abs(S11)
	elif derparam == 'S11abs':
		delta_sc = delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(1.0, 1.0, epvar*(1.0+1.0j*tan1), 0.1, d1/lam, 0.1)
			return np.abs(S11)
	else:
		print('Incorrect parameter.')
	#==========================================================================
	if derparam == 'S11abs':
		eps_inc = root_finding(func, ep1, 1e-6, NTOL, MAXIT, val=S11abs, delta=delta, deriv=None)
	else:
		eps_inc = root_finding(func, ep1, 1e-6, NTOL, MAXIT, val=S11abs, deriv=None)
	#==========================================================================
	return np.real((eps_inc-ep1*(1.0+1.0j*tan1))/delta_sc), S11abs


def Derivative_two_slabs_free_space(slabdata, derparam, delta, NTOL, MAXIT):

	ep1	= slabdata['ep1']
	tan1	= slabdata['tan1']

	ep2	= slabdata['ep2']
	tan2	= slabdata['tan2']

	d1	= slabdata['d1']
	d2	= slabdata['d2']

	L	= slabdata['L']
	f	= slabdata['f']
	#-------------------------------------------------------------------------
	# ep1		# auxiliary slab dielectric constant
	# ep2		# MUT slab dielectric constant
	#
	# d1  		# auxiliary slab thickness in mm
	# d2 		# MUT slab thickness in mm
	#
	# L  		# interslab separation distance in mm
	# f  		# operating frequency in GHz
	#
	# MAXIT 	# max number of iterations for Newton's method
	# NTOL 		# solution tolerance for Newton's method
	#-------------------------------------------------------------------------
	lam = 300.0/f

	S11abs = np.abs(three_slab_free_space(ep1*(1.0+1.0j*tan1), 1.0, ep2*(1.0+1.0j*tan2), d1/lam, d2/lam, L/lam))

	if derparam != 'ep1' and derparam != 'tan1':
		ep1 = ep1*(1.0 + 1.0j*tan1)
	#=========================================================================
	if derparam == 'd1':
		delta_sc = d1*delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1, 1.0, epvar*(1.0+1.0j*tan2), d1*(1.0+delta)/lam, d2/lam, L/lam)
			return np.abs(S11)
	elif derparam == 'd2':
		delta_sc = d2*delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1, 1.0, epvar*(1.0+1.0j*tan2), d1/lam, d2*(1.0+delta)/lam, L/lam)
			return np.abs(S11)
	elif derparam == 'ep1':
		delta_sc = ep1*delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1*(1.0+delta)*(1.0+1.0j*tan1), 1.0, epvar*(1.0+1.0j*tan2), d1/lam, d2/lam, L/lam)
			return np.abs(S11)
	elif derparam == 'tan1':
		delta_sc = tan1*delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1*(1.0+1.0j*tan1*(1.0+delta)), 1.0, epvar*(1.0+1.0j*tan2), d1/lam, d2/lam, L/lam)
			return np.abs(S11)
	elif derparam == 'tan2':
		delta_sc = tan2*delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1, 1.0, epvar*(1.0+1.0j*tan2*(1.0+delta)), d1/lam, d2/lam, L/lam)
			return np.abs(S11)
	elif derparam == 'L':
		delta_sc = L*delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1, 1.0, epvar*(1.0+1.0j*tan2), d1/lam, d2/lam, L*(1.0+delta)/lam)
			return np.abs(S11)
	elif derparam == 'f':
		delta_sc = f*delta
		lam = 300.0/(f*(1.0+delta));
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1, 1.0, epvar*(1.0+1.0j*tan2), d1/lam, d2/lam, L/lam)
			return np.abs(S11)
	elif derparam == 'S11abs':
		delta_sc = delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1, 1.0, epvar*(1.0+1.0j*tan2), d1/lam, d2/lam, L/lam)
			return np.abs(S11)
	else:
		print('Incorrect parameter.')
	#==========================================================================
	if derparam == 'S11abs':
		eps_inc = root_finding(func, ep2, 1e-6, NTOL, MAXIT, val=S11abs, delta=delta, deriv=None)
	else:
		eps_inc = root_finding(func, ep2, 1e-6, NTOL, MAXIT, val=S11abs, deriv=None)
	#==========================================================================
	return np.real((eps_inc-ep2*(1.0+1.0j*tan2))/delta_sc), S11abs

def Derivative_three_slabs_free_space(slabdata, derparam, delta, NTOL, MAXIT):

	ep1	= slabdata['ep1']
	tan1	= slabdata['tan1']

	ep2	= slabdata['ep2']
	tan2	= slabdata['tan2']

	epm	= slabdata['epm']
	tanm	= slabdata['tanm']

	d1	= slabdata['d1']
	d2	= slabdata['d2']

	L	= slabdata['L']
	f	= slabdata['f']
	#-------------------------------------------------------------------------
	# ep1		# auxiliary slab dielectric constant
	# ep2		# MUT slab dielectric constant
	# epm		# middle slab dielectric constant
	#
	# d1  		# auxiliary slab thickness in mm
	# d2 		# MUT slab thickness in mm
	#
	# L  		# interslab separation distance in mm
	# f  		# operating frequency in GHz
	#
	# MAXIT 	# max number of iterations for Newton's method
	# NTOL 		# solution tolerance for Newton's method
	#-------------------------------------------------------------------------
	lam = 300.0/f

	S11abs = np.abs(three_slab_free_space(ep1*(1.0+1.0j*tan1), epm*(1.0+1.0j*tanm), ep2*(1.0+1.0j*tan2), d1/lam, d2/lam, L/lam))

	if derparam != 'ep1' and derparam != 'tan1':
		ep1 = ep1*(1.0 + 1.0j*tan1)

	if derparam != 'epm' and derparam != 'tanm':
		epm = epm*(1.0 + 1.0j*tanm)

	#=========================================================================
	if derparam == 'd1':
		delta_sc = d1*delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1, epm, epvar*(1.0+1.0j*tan2), d1*(1.0+delta)/lam, d2/lam, L/lam)
			return np.abs(S11)
	elif derparam == 'd2':
		delta_sc = d2*delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1, epm, epvar*(1.0+1.0j*tan2), d1/lam, d2*(1.0+delta)/lam, L/lam)
			return np.abs(S11)
	elif derparam == 'ep1':
		delta_sc = ep1*delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1*(1.0+delta)*(1.0+1.0j*tan1), epm, epvar*(1.0+1.0j*tan2), d1/lam, d2/lam, L/lam)
			return np.abs(S11)
	elif derparam == 'tan1':
		delta_sc = tan1*delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1*(1.0+1.0j*(tan1*(1.0+delta))), epm, epvar*(1.0+1.0j*tan2), d1/lam, d2/lam, L/lam)
			return np.abs(S11)
	elif derparam == 'epm':
		delta_sc = epm*delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1, epm*(1.0+delta)*(1.0+1.0j*tanm), epvar*(1.0+1.0j*tan2), d1/lam, d2/lam, L/lam)
			return np.abs(S11)
	elif derparam == 'tanm':
		delta_sc = tanm*delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1, epm*(1.0+1.0j*(tanm*(1.0+delta))), epvar*(1.0+1.0j*tan2), d1/lam, d2/lam, L/lam)
			return np.abs(S11)
	elif derparam == 'tan2':
		delta_sc = tan2*delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1, epm, epvar*(1.0+1.0j*tan2*(1.0+delta)), d1/lam, d2/lam, L/lam)
			return np.abs(S11)
	elif derparam == 'L':
		delta_sc = L*delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1, epm, epvar*(1.0+1.0j*tan2), d1/lam, d2/lam, L*(1.0+delta)/lam)
			return np.abs(S11)
	elif derparam == 'f':
		delta_sc = f*delta
		lam = 300.0/(f*(1.0+delta));
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1, epm, epvar*(1.0+1.0j*tan2), d1/lam, d2/lam, L/lam)
			return np.abs(S11)
	elif derparam == 'S11abs':
		delta_sc = delta
		def func(epvar, **kwd):
			S11 = three_slab_free_space(ep1, epm, epvar*(1.0+1.0j*tan2), d1/lam, d2/lam, L/lam)
			return np.abs(S11)
	else:
		print('Incorrect parameter.')
	#==========================================================================
	if derparam == 'S11abs':
		eps_inc = root_finding(func, ep2, 1e-6, NTOL, MAXIT, val=S11abs, delta=delta, deriv=None)
	else:
		eps_inc = root_finding(func, ep2, 1e-6, NTOL, MAXIT, val=S11abs, deriv=None)
	#==========================================================================
	return np.real((eps_inc-ep2*(1.0+1.0j*tan2))/delta_sc), S11abs


def Derivative_single_slab_guide(slabdata, derparam, delta, NTOL, MAXIT):

	ep1	= slabdata['ep1']
	tan1	= slabdata['tan1']

	d1	= slabdata['d1']

	f	= slabdata['f']

	a	= slabdata['a']
	#--------------------------------------------------------------------------
	# ep2		# auxiliary slabs dielectric constant
	#
	# d2 		# auxiliary slab thickness in mm
	#
	# f  		# operating frequency in GHz
	#
	# a		# width of the waveguide broader wall
	#
	# MAXIT 	# max number of iterations for Newton's method
	# NTOL 		# solution tolerance for Newton's method
	#--------------------------------------------------------------------------
	lam = 300.0/f

	S11abs = np.abs(three_slab_guide(a/lam, 1.0, 1.0, ep1*(1.0+1.0j*tan1), 0.1, d1/a, 0.1))

	#==========================================================================================
	if derparam == 'd1':
		delta_sc = d1*delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, 1.0, 1.0, epvar*(1.0+1.0j*tan1), 0.1, d1*(1.0+delta)/a, 0.1)
			return np.abs(S11)
	elif derparam == 'tan1':
		delta_sc = tan1*delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, 1.0, 1.0,	epvar*(1.0+1.0j*tan1*(1.0+delta)), 0.1, d1/a, 0.1)
			return np.abs(S11)
	elif derparam == 'f':
		delta_sc = f*delta
		lam = 300.0/(f*(1.0+delta));
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, 1.0, 1.0, epvar*(1.0+1.0j*tan1), 0.1, d1/a, 0.1)
			return np.abs(S11)
	elif derparam == 'a':
		delta_sc = a*delta
		a = a*(1.0+delta);
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, 1.0, 1.0, epvar*(1.0+1.0j*tan1), 0.1, d1/a, 0.1)
			return np.abs(S11)
	elif derparam == 'S11abs':
		delta_sc = delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, 1.0, 1.0, epvar*(1.0+1.0j*tan1), 0.1, d1/a, 0.1)
			return np.abs(S11)
	else:
		print('Incorrect parameter.')
	#==========================================================================
	if derparam == 'S11abs':
		eps_inc = root_finding(func, ep1, 1e-6, NTOL, MAXIT, val=S11abs, delta=delta, deriv=None)
	else:
		eps_inc = root_finding(func, ep1, 1e-6, NTOL, MAXIT, val=S11abs, deriv=None)
	#==========================================================================
	return np.real((eps_inc-ep1*(1.0+1.0j*tan1))/delta_sc), S11abs

def Derivative_two_slabs_guide(slabdata, derparam, delta, NTOL, MAXIT):

	ep1	= slabdata['ep1']
	tan1	= slabdata['tan1']

	ep2	= slabdata['ep2']
	tan2	= slabdata['tan2']

	d1	= slabdata['d1']
	d2	= slabdata['d2']

	L	= slabdata['L']
	f	= slabdata['f']

	a	= slabdata['a']
	#--------------------------------------------------------------------------
	# ep1		# MUT dielectric constant
	# ep2		# auxiliary slabs dielectric constant
	#
	# d1  		# measurable slab thickness in mm
	# d2 		# auxiliary slab thickness in mm
	#
	# L  		# interslab separation distance in mm
	# f  		# operating frequency in GHz
	#
	# a		# width of the waveguide broader wall
	#
	# MAXIT 	# max number of iterations for Newton's method
	# NTOL 		# solution tolerance for Newton's method
	#--------------------------------------------------------------------------
	lam = 300.0/f

	S11abs = np.abs(three_slab_guide(a/lam, ep1*(1.0+1.0j*tan1), 1.0, ep2*(1.0+1.0j*tan2), d1/a, d2/a, L/a))

	if derparam != 'ep1' and derparam != 'tan1':
		ep1 = ep1*(1.0 + 1.0j*tan1)
	#==========================================================================================
	if derparam == 'd1':
		delta_sc = d1*delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1, 1.0, epvar*(1.0+1.0j*tan2), d1*(1.0+delta)/a, d2/a, L/a)
			return np.abs(S11)
	elif derparam == 'd2':
		delta_sc = d2*delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1, 1.0, epvar*(1.0+1.0j*tan2), d1/a, d2*(1.0+delta)/a, L/a)
			return np.abs(S11)
	elif derparam == 'ep1':
		delta_sc = ep1*delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1*(1.0+delta)*(1.0+1.0j*tan1), 1.0, epvar*(1.0+1.0j*tan2), d1/a, d2/a, L/a)
			return np.abs(S11)
	elif derparam == 'tan1':
		delta_sc = tan1*delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1*(1.0+1.0j*tan1*(1.0+delta)), 1.0, epvar*(1.0+1.0j*tan2), d1/a, d2/a, L/a)
			return np.abs(S11)
	elif derparam == 'tan2':
		delta_sc = tan2*delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1, 1.0, epvar*(1.0+1.0j*tan2*(1.0+delta)), d1/a, d2/a, L/a)
			return np.abs(S11)
	elif derparam == 'L':
		delta_sc = L*delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1, 1.0, epvar*(1.0+1.0j*tan2), d1/a, d2/a, L*(1.0+delta)/a)
			return np.abs(S11)
	elif derparam == 'f':
		delta_sc = f*delta
		lam = 300.0/(f*(1.0+delta));
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1, 1.0, epvar*(1.0+1.0j*tan2), d1/a, d2/a, L/a)
			return np.abs(S11)
	elif derparam == 'a':
		delta_sc = a*delta
		a = a*(1.0+delta);
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1, 1.0, epvar*(1.0+1.0j*tan2), d1/a, d2/a, L/a)
			return np.abs(S11)
	elif derparam == 'S11abs':
		delta_sc = delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1, 1.0, epvar*(1.0+1.0j*tan2), d1/a, d2/a, L/a)
			return np.abs(S11)
	else:
		print('Incorrect parameter.')
	#==========================================================================
	if derparam == 'S11abs':
		eps_inc = root_finding(func, ep2, 1e-6, NTOL, MAXIT, val=S11abs, delta=delta, deriv=None)
	else:
		eps_inc = root_finding(func, ep2, 1e-6, NTOL, MAXIT, val=S11abs, deriv=None)
	#==========================================================================
	return np.real((eps_inc-ep2*(1.0+1.0j*tan2))/delta_sc), S11abs


def Derivative_three_slabs_guide(slabdata, derparam, delta, NTOL, MAXIT):

	ep1	= slabdata['ep1']
	tan1	= slabdata['tan1']

	epm	= slabdata['epm']
	tanm	= slabdata['tanm']

	ep2	= slabdata['ep2']
	tan2	= slabdata['tan2']

	d1	= slabdata['d1']
	d2	= slabdata['d2']

	L	= slabdata['L']
	f	= slabdata['f']

	a	= slabdata['a']

	#--------------------------------------------------------------------------
	# ep1		# auxiliary slab dielectric constant
	# ep2		# MUT slab dielectric constant
	# epm		# middle slab dielectric constant
	#
	# d1 		# auxiliary slab thickness in mm
	# d2 		# MUT slab thickness in mm
	#
	# L  		# interslab separation distance in mm
	# f  		# operating frequency in GHz
	#
	# a		# width of the waveguide broader wall
	#
	# MAXIT 	# max number of iterations for Newton's method
	# NTOL 		# solution tolerance for Newton's method
	#--------------------------------------------------------------------------
	lam = 300.0/f

	S11abs = np.abs(three_slab_guide(a/lam, ep1*(1.0+1.0j*tan1), epm*(1.0+1.0j*tanm), ep2*(1.0+1.0j*tan2), d1/a, d2/a, L/a))

	if derparam != 'ep1' and derparam != 'tan1':
		ep1 = ep1*(1.0 + 1.0j*tan1)

	if derparam != 'epm' and derparam != 'tanm':
		epm = epm*(1.0 + 1.0j*tanm)
	#==========================================================================
	if derparam == 'd1':
		delta_sc = d1*delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1, epm, epvar*(1.0+1.0j*tan2), d1*(1.0+delta)/a, d2/a, L/a)
			return np.abs(S11)
	elif derparam == 'd2':
		delta_sc = d2*delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1, epm, epvar*(1.0+1.0j*tan2), d1/a, d2*(1.0+delta)/a, L/a)
			return np.abs(S11)
	elif derparam == 'ep1':
		delta_sc = ep1*delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1*(1.0+delta)*(1.0+1.0j*tan1), epm, epvar*(1.0+1.0j*tan2), d1/a, d2/a, L/a)
			return np.abs(S11)
	elif derparam == 'tan1':
		delta_sc = tan1*delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1*(1.0+1.0j*(tan1*(1.0+delta))), epm, epvar*(1.0+1.0j*tan2), d1/a, d2/a, L/a)
			return np.abs(S11)
	elif derparam == 'tan2':
		delta_sc = tan2*delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1, epm, epvar*(1.0+1.0j*(tan2*(1.0+delta))), d1/a, d2/a, L/a)
			return np.abs(S11)
	elif derparam == 'epm':
		delta_sc = epm*delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1, epm*(1.0+delta)*(1.0+1.0j*tanm), epvar*(1.0+1.0j*tan2), d1/a, d2/a, L/a)
			return np.abs(S11)
	elif derparam == 'tanm':
		delta_sc = tanm*delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1, epm*(1.0+1.0j*(tanm*(1.0+delta))), epvar*(1.0+1.0j*tan2), d1/a, d2/a, L/a)
			return np.abs(S11)
	elif derparam == 'L':
		delta_sc = L*delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1, epm, epvar*(1.0+1.0j*tan2), d1/a, d2/a, L*(1.0+delta)/a)
			return np.abs(S11)
	elif derparam == 'f':
		delta_sc = f*delta
		lam = 300.0/(f*(1.0+delta));
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1, epm, epvar*(1.0+1.0j*tan2), d1/a, d2/a, L/a)
			return np.abs(S11)
	elif derparam == 'a':
		delta_sc = a*delta
		a = a*(1.0+delta);
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1, epm, epvar*(1.0+1.0j*tan2), d1/a, d2/a, L/a)
			return np.abs(S11)
	elif derparam == 'S11abs':
		delta_sc = delta
		def func(epvar, **kwd):
			S11 = three_slab_guide(a/lam, ep1, epm, epvar*(1.0+1.0j*tan2), d1/a, d2/a, L/a)
			return np.abs(S11)
	else:
		print('Incorrect parameter.')
	#==========================================================================
	if derparam == 'S11abs':
		eps_inc = root_finding(func, ep2, 1e-6, NTOL, MAXIT, val=S11abs, delta=delta, deriv=None)
	else:
		eps_inc = root_finding(func, ep2, 1e-6, NTOL, MAXIT, val=S11abs, deriv=None)
	#==========================================================================
	return np.real((eps_inc-ep2*(1.0+1.0j*tan2))/delta_sc), S11abs


def Derivative_single_rod(roddata, derparam, delta, NTOL, MAXIT, NM, N):

	ep1	= roddata['ep1']
	tan1	= roddata['tan1']

	ro1	= roddata['ro1']
	xo1	= roddata['xo1']

	f	= roddata['f']
	a	= roddata['a']

	NM 	= 1
	#--------------------------------------------------------------------------
	# ep1		# auxiliary rod dielectric constant
	# ep2		# MUT rod dielectric constant
	#
	# ro1 		# auxiliary rod radius in mm
	# ro2		# MUT rod radius in mm
	#
	# L  		# interrod separation distance in mm
	# f  		# operating frequency in GHz
	#
	# a   		# waveguide width in mm
	#
	# MAXIT		# max number of iterations for Newton's method
	# NTOL 		# solution tolerance for Newton's method
	#
	# NM		# number of higher order waveguide modes considered
	# N		# number of basis functions for each rod surface
	#--------------------------------------------------------------------------
	#--------------------------------------------------------------------------
	MoMinit()	# C++ library initialization

	lam = 300.0/f
	#==========================================================================
	def evalparam(ep, ro, xo):

		param 		= MoMparam()
		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5 + xo/a
		param.zo 	= 0.0

		return param
	#==========================================================================
	def MoM_calc_S_matrix(ep, ro, xo):
		param = evalparam(ep, ro, xo)
		S11a, S12a, S21a, S22a = MoM_single_solid_rod_(param, NM, N)
		return S11a, S12a, S21a, S22a
	#==========================================================================
	def calcS11(S11b):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#==========================================================================
	def calcT(L, lam):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#==========================================================================
	# calculate S11meas - the total reflection coefficient
	S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1*(1.0+1.0j*tan1), ro1, xo1)
	S11abs 			= np.abs(S11a)
	#--------------------------------------------------------------------------
	if derparam == 'ro1':
		delta_sc = ro1*delta
		param = evalparam(ep1, ro1*(1.0+delta), xo1)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan1)
			S11a, S12a, S21a, S22a 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(S11a)
	elif derparam == 'xo1':
		delta_sc = a*delta
		param = evalparam(ep1, ro1, xo1 + a*delta)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan1)
			S11a, S12a, S21a, S22a 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(S11a)
	elif derparam == 'tan1':
		delta_sc = tan1*delta
		param = evalparam(ep1, ro1, xo1)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan1*(1.0+delta))
			S11a, S12a, S21a, S22a 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(S11a)
	elif derparam == 'f':
		delta_sc = f*delta
		lam = 300.0/(f*(1.0+delta))
		param 			= evalparam(ep1, ro1, xo1)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan1)
			S11a, S12a, S21a, S22a 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(S11a)
	elif derparam == 'a':
		delta_sc = a*delta
		a = a*(1.0+delta)
		param 			= evalparam(ep1, ro1, xo1)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan1)
			S11a, S12a, S21a, S22a 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(S11a)
	elif derparam == 'S11abs':
		delta_sc = delta
		param = evalparam(ep1, ro1, xo1)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan1)
			S11a, S12a, S21a, S22a 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(S11a)
	else:
		print('Incorrect parameter.')
	#==========================================================================
	if derparam == 'S11abs':
		eps_inc = root_finding(func, ep1, 1e-4, NTOL, MAXIT, val=S11abs, delta=delta, deriv=None)
	else:
		eps_inc = root_finding(func, ep1, 1e-4, NTOL, MAXIT, val=S11abs, deriv=None)
	#==========================================================================
	return np.real((eps_inc-ep1*(1.0+1.0j*tan1))/delta_sc), S11abs


def Derivative_two_rods(roddata, derparam, delta, NTOL, MAXIT, NM, N):

	ep1	= roddata['ep1']
	tan1	= roddata['tan1']

	ep2	= roddata['ep2']
	tan2	= roddata['tan2']

	ro1	= roddata['ro1']
	ro2	= roddata['ro2']

	xo1	= roddata['xo1']
	xo2	= roddata['xo2']

	L	= roddata['L']
	f	= roddata['f']

	a	= roddata['a']
	#--------------------------------------------------------------------------
	# ep1		# auxiliary rod dielectric constant
	# ep2		# MUT rod dielectric constant
	#
	# ro1 		# auxiliary rod radius in mm
	# ro2		# MUT rod radius in mm
	#
	# L  		# interrod separation distance in mm
	# f  		# operating frequency in GHz
	#
	# a   		# waveguide width in mm
	#
	# MAXIT		# max number of iterations for Newton's method
	# NTOL 		# solution tolerance for Newton's method
	#
	# NM		# number of higher order waveguide modes considered
	# N		# number of basis functions for each rod surface
	#--------------------------------------------------------------------------
	#--------------------------------------------------------------------------
	MoMinit()	# C++ library initialization

	lam = 300.0/f
	#==========================================================================
	def evalparam(ep, ro, xo):

		param 		= MoMparam()
		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5 + xo/a
		param.zo 	= 0.0

		return param
	#==========================================================================
	def MoM_calc_S_matrix(ep, ro, xo):
		param = evalparam(ep, ro, xo)
		S11a, S12a, S21a, S22a = MoM_single_solid_rod_(param, NM, N)
		return S11a, S12a, S21a, S22a
	#==========================================================================
	def calcS11(S11b):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#==========================================================================
	def calcT(L, lam):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#==========================================================================
	# calculate S11meas - the total reflection coefficient
	S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1*(1.0+1.0j*tan1), ro1, xo1)
	T 			= calcT(L, lam)
	S11b, S12b, S21b, S22b 	= MoM_calc_S_matrix(ep2*(1.0+1.0j*tan2), ro2, xo2)
	S11abs 			= np.abs(calcS11(S11b))
	#--------------------------------------------------------------------------
	if derparam != 'ep1' and derparam != 'tan1':
		ep1 = ep1*(1.0 + 1.0j*tan1)
	#--------------------------------------------------------------------------
	if derparam == 'ro1':
		delta_sc = ro1*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1*(1.0+delta), xo1)
		T 			= calcT(L, lam)
		param = evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(calcS11(S11b))
	elif derparam == 'xo1':
		delta_sc = a*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1+a*delta)
		T 			= calcT(L, lam)
		param = evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(calcS11(S11b))
	elif derparam == 'ro2':
		delta_sc = ro2*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T 			= calcT(L, lam)
		param = evalparam(ep2, ro2*(1.0+delta), xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(calcS11(S11b))
	elif derparam == 'xo2':
		delta_sc = a*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T 			= calcT(L, lam)
		param = evalparam(ep2, ro2, xo2 + a*delta)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(calcS11(S11b))
	elif derparam == 'ep1':
		delta_sc = ep1*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1*(1.0+delta)*(1.0+1.0j*tan1), ro1, xo1)
		T 			= calcT(L, lam)
		param = evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(calcS11(S11b))
	elif derparam == 'tan1':
		delta_sc = tan1*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1*(1.0+1.0j*tan1*(1.0+delta)), ro1, xo1)
		T 			= calcT(L, lam)
		param = evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(calcS11(S11b))
	elif derparam == 'tan2':
		delta_sc = tan2*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T 			= calcT(L, lam)
		param = evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2*(1.0+delta))
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(calcS11(S11b))
	elif derparam == 'L':
		delta_sc = L*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T 			= calcT(L*(1.0+delta), lam)
		param = evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(calcS11(S11b))
	elif derparam == 'f':
		delta_sc = f*delta
		lam = 300.0/(f*(1.0+delta))
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T 			= calcT(L, lam)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(calcS11(S11b))
	elif derparam == 'a':
		delta_sc = a*delta
		a = a*(1.0+delta)
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T 			= calcT(L, lam)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(calcS11(S11b))
	elif derparam == 'S11abs':
		delta_sc = delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T 			= calcT(L, lam)
		param = evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			return  np.abs(calcS11(S11b))
	else:
		print('Incorrect parameter.')
	#==========================================================================
	if derparam == 'S11abs':
		eps_inc = root_finding(func, ep2, 1e-4, NTOL, MAXIT, val=S11abs, delta=delta, deriv=None)
	else:
		eps_inc = root_finding(func, ep2, 1e-4, NTOL, MAXIT, val=S11abs, deriv=None)
	#==========================================================================
	return np.real((eps_inc-ep2*(1.0+1.0j*tan2))/delta_sc), S11abs


def Derivative_three_rods(roddata, derparam, delta, NTOL, MAXIT, NM, N):

	ep1	= roddata['ep1']
	tan1	= roddata['tan1']

	ep2	= roddata['ep2']
	tan2	= roddata['tan2']

	ep3	= roddata['ep3']
	tan3	= roddata['tan3']

	ro1	= roddata['ro1']
	ro2	= roddata['ro2']
	ro3	= roddata['ro3']

	xo1	= roddata['xo1']
	xo2	= roddata['xo2']
	xo3	= roddata['xo3']

	L1	= roddata['L1']
	L2	= roddata['L2']

	f	= roddata['f']
	a	= roddata['a']
	#--------------------------------------------------------------------------
	# ep1		# 1st auxiliary rod dielectric constant
	# ep2		# MUT rod dielectric constant
	# ep3		# 2nd auxiliary rod dielectric constant
	#
	# ro1		# 1st auxiliary rod radius in mm
	# ro2 		# MUT rod radius in mm
	# ro3		# 2nd auxiliary rod radius in mm
	#
	# L1 		# distance between the 1st and the 2nd rods in mm
	# L2 		# distance between the 2nd and the 3rd rods in mm
	#
	# f  		# operating frequency in GHz
	#
	# a  		# waveguide width in mm
	#
	# MAXIT		# max number of iterations for Newton's method
	# NTOL 		# solution tolerance for Newton's method
	#
	# NM		# number of higher order waveguide modes considered
	# N		# number of basis functions for each rod surface
	#--------------------------------------------------------------------------
	#--------------------------------------------------------------------------
	MoMinit()	# C++ library initialization

	lam = 300.0/f
	#==========================================================================
	def evalparam(ep, ro, xo):
		param 		= MoMparam()
		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5 + xo/a
		param.zo 	= 0.0
		return param
	#==========================================================================
	def MoM_calc_S_matrix(ep, ro, xo):
		param = evalparam(ep, ro, xo)
		S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
		return S11, S12, S21, S22
	#==========================================================================
	def calcS(S11b, S12b, S21b, S22b, T):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
			S21 = S21a*T/(1.0 - S11b*T*S22a*T)*S21b
			S12 = S12b*T/(1.0 - S22a*T*S11b*T)*S12a
			S22 = S22b + S12b*T*S22a*T/(1.0 - S11b*T*S22a*T)*S21b
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a
			S21 = S21a*T*np.linalg.inv(np.eye(NM) - S11b*T*S22a*T)*S21b
			S12 = S12b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a
			S22 = S22b + S12b*T*S22a*T*np.linalg.inv(np.eye(NM) - S11b*T*S22a*T)*S21b

		return S11, S12, S21, S22
	#==========================================================================
	def calcS11(S11a, S12a, S21a, S22a, S11b, T):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#==========================================================================
	def calcT(L, lam):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#==========================================================================
	# calculate S11meas - the total reflection coefficient
	S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1*(1.0+1.0j*tan1), ro1, xo1)
	T1 			= calcT(L1, lam)
	T2 			= calcT(L2, lam)
	S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3*(1.0+1.0j*tan3), ro3, xo3)
	#--------------------------------------------------------------------------
	S11b, S12b, S21b, S22b 	= MoM_calc_S_matrix(ep2*(1.0+1.0j*tan2), ro2, xo2)
	S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
	S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
	#--------------------------------------------------------------------------
	if derparam != 'ep1' and derparam != 'tan1':
		ep1 = ep1*(1.0 + 1.0j*tan1)

	if derparam != 'ep3' and derparam != 'tan3':
		ep3 = ep3*(1.0 + 1.0j*tan3)
	#--------------------------------------------------------------------------
	if derparam == 'ro1':
		delta_sc = ro1*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1*(1.0+delta), xo1)
		T1 			= calcT(L1, lam)
		T2 			= calcT(L2, lam)
		S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3, ro3, xo3)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
			S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
			return S11abs
	elif derparam == 'xo1':
		delta_sc = a*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1+a*delta)
		T1 			= calcT(L1, lam)
		T2 			= calcT(L2, lam)
		S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3, ro3, xo3)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
			S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
			return S11abs
	elif derparam == 'ro2':
		delta_sc = ro2*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T1 			= calcT(L1, lam)
		T2 			= calcT(L2, lam)
		S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3, ro3, xo3)
		param 			= evalparam(ep2, ro2*(1.0+delta), xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
			S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
			return S11abs
	elif derparam == 'xo2':
		delta_sc = a*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T1 			= calcT(L1, lam)
		T2 			= calcT(L2, lam)
		S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3, ro3, xo3)
		param 			= evalparam(ep2, ro2, xo2+a*delta)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
			S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
			return S11abs
	elif derparam == 'ro3':
		delta_sc = ro3*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T1 			= calcT(L1, lam)
		T2 			= calcT(L2, lam)
		S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3, ro3*(1.0+delta), xo3)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
			S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
			return S11abs
	elif derparam == 'xo3':
		delta_sc = a*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T1 			= calcT(L1, lam)
		T2 			= calcT(L2, lam)
		S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3, ro3, xo3+a*delta)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
			S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
			return S11abs
	elif derparam == 'ep1':
		delta_sc = ep1*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1*(1.0+delta)*(1.0+1.0j*tan1), ro1, xo1)
		T1 			= calcT(L1, lam)
		T2 			= calcT(L2, lam)
		S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3, ro3, xo3)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
			S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
			return S11abs
	elif derparam == 'tan1':
		delta_sc = tan1*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1*(1.0+1.0j*(tan1*(1.0+delta))), ro1, xo1)
		T1 			= calcT(L1, lam)
		T2 			= calcT(L2, lam)
		S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3, ro3, xo3)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
			S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
			return S11abs
	elif derparam == 'tan2':
		delta_sc = tan2*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T1 			= calcT(L1, lam)
		T2 			= calcT(L2, lam)
		S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3, ro3, xo3)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2*(1.0+delta))
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
			S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
			return S11abs
	elif derparam == 'ep3':
		delta_sc = ep3*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T1 			= calcT(L1, lam)
		T2 			= calcT(L2, lam)
		S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3*(1.0+delta)*(1.0+1.0j*tan3), ro3, xo3)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
			S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
			return S11abs
	elif derparam == 'tan3':
		delta_sc = tan3*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T1 			= calcT(L1, lam)
		T2 			= calcT(L2, lam)
		S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3*(1.0+1.0j*tan3*(1.0+delta)), ro3, xo3)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
			S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
			return S11abs
	elif derparam == 'L1':
		delta_sc = L1*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T1 			= calcT(L1*(1.0+delta), lam)
		T2 			= calcT(L2, lam)
		S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3, ro3, xo3)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
			S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
			return S11abs
	elif derparam == 'L2':
		delta_sc = L2*delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T1 			= calcT(L1, lam)
		T2 			= calcT(L2*(1.0+delta), lam)
		S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3, ro3, xo3)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
			S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
			return S11abs
	elif derparam == 'f':
		delta_sc = f*delta
		lam 			= 300.0/(f*(1.0+delta))
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T1 			= calcT(L1, lam)
		T2 			= calcT(L2, lam)
		S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3, ro3, xo3)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
			S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
			return S11abs
	elif derparam == 'a':
		delta_sc = a*delta
		a 			= a*(1.0+delta)
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T1 			= calcT(L1, lam)
		T2 			= calcT(L2, lam)
		S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3, ro3, xo3)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
			S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
			return S11abs
	elif derparam == 'S11abs':
		delta_sc = delta
		S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1, xo1)
		T1 			= calcT(L1, lam)
		T2 			= calcT(L2, lam)
		S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3, ro3, xo3)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
			S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
			return S11abs
	else:
		print('Incorrect parameter specified')
	#--------------------------------------------------------------------------
	if derparam == 'S11abs':
		eps_inc = root_finding(func, ep2, 1e-4, NTOL, MAXIT, val=S11abs, delta=delta, deriv=None)
	else:
		eps_inc = root_finding(func, ep2, 1e-4, NTOL, MAXIT, val=S11abs, deriv=None)

	return np.real((eps_inc-ep2*(1.0+1.0j*tan2))/delta_sc), S11abs


def Derivative_slab_and_rod(rodslabdata, derparam, delta, NTOL, MAXIT, NM, N):

	ep1	= rodslabdata['ep1']
	tan1	= rodslabdata['tan1']

	ep2	= rodslabdata['ep2']
	tan2	= rodslabdata['tan2']

	d1	= rodslabdata['d1']
	ro2	= rodslabdata['ro2']
	xo2	= rodslabdata['xo2']

	L	= rodslabdata['L']

	f	= rodslabdata['f']
	a	= rodslabdata['a']
	#--------------------------------------------------------------------------
	# ep1		# auxiliary slab dielectric constant
	# ep2 		# MUT rod dielectric constant
	#
	# d1  		# auxiliary slab thickness in mm
	# ro2  		# MUT rod radius in mm
	#
	# L   		# slab-to-rod separation in mm
	# f   		# operating frequency in GHz
	#
	# a   		# waveguide width in mm
	#
	# MAXIT 	# max number of iterations for Newton's method
	# NTOL 		# solution tolerance for Newton's method
	#
	# NM		# number of higher order waveguide modes considered
	# N		# number of basis functions for each rod surface
	#--------------------------------------------------------------------------
	#--------------------------------------------------------------------------
	MoMinit()	# C++ library initialization

	lam = 300.0/f
	#==========================================================================
	def evalparam(ep, ro, xo):
		param 		= MoMparam()
		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5 + xo/a
		param.zo 	= 0.0

		return param
	#===========================================================================
	def MoM_calc_S_matrix(ep, ro, xo):
		param = evalparam(ep, ro, xo)
		S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
		return S11, S12, S21, S22
	#===========================================================================
	def calcS11(S11b, T):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#===========================================================================
	def calcT(L, lam):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#===========================================================================
	# calculate S11meas - the total reflection coefficient
	S11a, S12a, S21a, S22a 	= single_slab_guide_matrix(a/lam, ep1*(1.0+1.0j*tan1), d1/a, NM)
	T 			= calcT(L, lam)
	S11b, S12b, S21b, S22b 	= MoM_calc_S_matrix(ep2*(1.0+1.0j*tan2), ro2, xo2)
	S11abs			= np.abs(calcS11(S11b, T))
	#---------------------------------------------------------------------------
	if derparam != 'ep1' and derparam != 'tan1':
		ep1 = ep1*(1.0 + 1.0j*tan1)
	#---------------------------------------------------------------------------
	if derparam == 'd1':
		delta_sc = d1*delta
		S11a, S12a, S21a, S22a 	= single_slab_guide_matrix(a/lam, ep1, d1*(1.0+delta)/a, NM)
		T 			= calcT(L, lam)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b, T))
			return S11abs
	elif derparam == 'ro2':
		delta_sc = ro2*delta
		S11a, S12a, S21a, S22a 	= single_slab_guide_matrix(a/lam, ep1, d1/a, NM)
		T 			= calcT(L, lam)
		param 			= evalparam(ep2, ro2*(1.0+delta), xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b, T))
			return S11abs
	elif derparam == 'xo2':
		delta_sc = a*delta
		S11a, S12a, S21a, S22a 	= single_slab_guide_matrix(a/lam, ep1, d1/a, NM)
		T 			= calcT(L, lam)
		param 			= evalparam(ep2, ro2, xo2+a*delta)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b, T))
			return S11abs
	elif derparam == 'ep1':
		delta_sc = ep1*delta
		S11a, S12a, S21a, S22a 	= single_slab_guide_matrix(a/lam, ep1*(1.0+delta)*(1.0+1.0j*tan1), d1/a, NM)
		T 			= calcT(L, lam)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b, T))
			return S11abs
	elif derparam == 'tan1':
		delta_sc = tan1*delta
		S11a, S12a, S21a, S22a 	= single_slab_guide_matrix(a/lam, ep1*(1.0+1.0j*tan1*(1.0+delta)), d1/a, NM)
		T 			= calcT(L, lam)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b, T))
			return S11abs
	elif derparam == 'tan2':
		delta_sc = tan2*delta
		S11a, S12a, S21a, S22a 	= single_slab_guide_matrix(a/lam, ep1, d1/a, NM)
		T 			= calcT(L, lam)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2*(1.0+delta))
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b, T))
			return S11abs
	elif derparam == 'L':
		delta_sc = L*delta
		S11a, S12a, S21a, S22a 	= single_slab_guide_matrix(a/lam, ep1, d1/a, NM)
		T 			= calcT(L*(1.0+delta), lam)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b, T))
			return S11abs
	elif derparam == 'f':
		delta_sc = f*delta
		lam 			= 300.0/(f*(1.0+delta))
		S11a, S12a, S21a, S22a 	= single_slab_guide_matrix(a/lam, ep1, d1/a, NM)
		T 			= calcT(L, lam)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b, T))
			return S11abs
	elif derparam == 'a':
		delta_sc = a*delta
		a 			= a*(1.0+delta)
		S11a, S12a, S21a, S22a 	= single_slab_guide_matrix(a/lam, ep1, d1/a, NM)
		T 			= calcT(L, lam)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b, T))
			return S11abs
	elif derparam == 'S11abs':
		delta_sc = delta
		S11a, S12a, S21a, S22a 	= single_slab_guide_matrix(a/lam, ep1, d1/a, NM)
		T 			= calcT(L, lam)
		param 			= evalparam(ep2, ro2, xo2)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b, T))
			return S11abs
	else:
		print('Incorrect parameter specified')
	#---------------------------------------------------------------------------
	if derparam == 'S11abs':
		eps_inc = root_finding(func, ep2, 1e-4, NTOL, MAXIT, delta=delta, val=S11abs, deriv=None)
	else:
		eps_inc = root_finding(func, ep2, 1e-4, NTOL, MAXIT, val=S11abs, deriv=None)

	return np.real((eps_inc-ep2*(1.0+1.0j*tan2))/delta_sc), S11abs


def Derivative_rod_and_slab(rodslabdata, derparam, delta, NTOL, MAXIT, NM, N):

	ep1	= rodslabdata['ep1']
	tan1	= rodslabdata['tan1']

	ep2	= rodslabdata['ep2']
	tan2	= rodslabdata['tan2']

	ro1	= rodslabdata['ro1']
	xo1	= rodslabdata['xo1']
	d2	= rodslabdata['d2']

	L	= rodslabdata['L']

	f	= rodslabdata['f']
	a	= rodslabdata['a']
	#---------------------------------------------------------------------------
	# ep1	- auxiliary rod dielectric constant
	# ep2	- MUT slab dielectric constant
	#
	# ro1	- auxiliary rod radius in mm
	# d2 	- MUT slab thickness in mm
	#
	# L  	- rod-to-slab separarion in mm
	# f  	- operating frequency in GHz
	#
	# a  	- waveguide width in mm
	#
	# MAXIT	- max number of iterations for Newton's method
	# NTOL 	- solution tolerance for Newton's method
	#
	# NM	- number of higher order waveguide modes considered
	# N	- number of basis functions for each rod surface
	#---------------------------------------------------------------------------
	#---------------------------------------------------------------------------
	MoMinit()		# C++ library initialization
	#
	lam = 300.0/f
	#===========================================================================
	def evalparam(ep, ro, xo):
		param 		= MoMparam()
		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5 + xo/a
		param.zo 	= 0.0

		return param
	#===========================================================================
	def MoM_calc_S_matrix(ep, ro, xo):
		param = evalparam(ep, ro, xo)
		S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
		return S11, S12, S21, S22
	#===========================================================================
	def calcS11(S11b):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#===========================================================================
	def calcT(L, lam):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#===========================================================================
	# calculate S11meas - the total reflection coefficient
	#===========================================================================
	S11a, S12a, S21a, S22a 		= MoM_calc_S_matrix(ep1*(1.0+1.0j*tan1), ro1, xo1)
	T 				= calcT(L, lam)
	S11b, S12b, S21b, S22b 		= single_slab_guide_matrix(a/lam, ep2*(1.0+1.0j*tan2), d2/a, NM)
	S11abs				= np.abs(calcS11(S11b))
	#---------------------------------------------------------------------------
	if derparam != 'ep1' and derparam != 'tan1':
		ep1 = ep1*(1.0 + 1.0j*tan1)
	#---------------------------------------------------------------------------
	if derparam == 'ro1':
		delta_sc = ro1*delta
		S11a, S12a, S21a, S22a 		= MoM_calc_S_matrix(ep1, ro1*(1.0+delta), xo1)
		T 				= calcT(L, lam)
		def func(epvar, **kwd):
			S11b, S12b, S21b, S22b 	= single_slab_guide_matrix(a/lam, epvar*(1.0+1.0j*tan2), d2/a, NM)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'xo1':
		delta_sc = a*delta
		S11a, S12a, S21a, S22a 		= MoM_calc_S_matrix(ep1, ro1, xo1+a*delta)
		T 				= calcT(L, lam)
		def func(epvar, **kwd):
			S11b, S12b, S21b, S22b 	= single_slab_guide_matrix(a/lam, epvar*(1.0+1.0j*tan2), d2/a, NM)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'd2':
		delta_sc = d2*delta
		S11a, S12a, S21a, S22a 		= MoM_calc_S_matrix(ep1, ro1, xo1)
		T 				= calcT(L, lam)
		def func(epvar, **kwd):
			S11b, S12b, S21b, S22b 	= single_slab_guide_matrix(a/lam, epvar*(1.0+1.0j*tan2), d2*(1.0+delta)/a, NM)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'ep1':
		delta_sc = ep1*delta
		S11a, S12a, S21a, S22a 		= MoM_calc_S_matrix(ep1*(1.0+delta)*(1.0+1.0j*tan1), ro1, xo1)
		T 				= calcT(L, lam)
		def func(epvar, **kwd):
			S11b, S12b, S21b, S22b 	= single_slab_guide_matrix(a/lam, epvar*(1.0+1.0j*tan2), d2/a, NM)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'tan1':
		delta_sc = tan1*delta
		S11a, S12a, S21a, S22a 		= MoM_calc_S_matrix(ep1*(1.0 + 1.0j*tan1*(1.0+delta)), ro1, xo1)
		T 				= calcT(L, lam)
		def func(epvar, **kwd):
			S11b, S12b, S21b, S22b 	= single_slab_guide_matrix(a/lam, epvar*(1.0+1.0j*tan2), d2/a, NM)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'tan2':
		delta_sc = tan2*delta
		S11a, S12a, S21a, S22a 		= MoM_calc_S_matrix(ep1, ro1, xo1)
		T 				= calcT(L, lam)
		def func(epvar, **kwd):
			S11b, S12b, S21b, S22b 	= single_slab_guide_matrix(a/lam, epvar*(1.0+1.0j*tan2*(1.0+delta)), d2/a, NM)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'L':
		delta_sc = L*delta
		S11a, S12a, S21a, S22a 		= MoM_calc_S_matrix(ep1, ro1, xo1)
		T 				= calcT(L*(1.0+delta), lam)
		def func(epvar, **kwd):
			S11b, S12b, S21b, S22b 	= single_slab_guide_matrix(a/lam, epvar*(1.0+1.0j*tan2), d2/a, NM)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'f':
		delta_sc = f*delta
		lam				= 300.0/(f*(1.0+delta))
		S11a, S12a, S21a, S22a 		= MoM_calc_S_matrix(ep1, ro1, xo1)
		T 				= calcT(L, lam)
		def func(epvar, **kwd):
			S11b, S12b, S21b, S22b 	= single_slab_guide_matrix(a/lam, epvar*(1.0+1.0j*tan2), d2/a, NM)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'a':
		delta_sc = a*delta
		a				= a*(1.0+delta)
		S11a, S12a, S21a, S22a 		= MoM_calc_S_matrix(ep1, ro1, xo1)
		T 				= calcT(L, lam)
		def func(epvar, **kwd):
			S11b, S12b, S21b, S22b 	= single_slab_guide_matrix(a/lam, epvar*(1.0+1.0j*tan2), d2/a, NM)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'S11abs':
		delta_sc = delta
		S11a, S12a, S21a, S22a 		= MoM_calc_S_matrix(ep1, ro1, xo1)
		T 				= calcT(L, lam)
		def func(epvar, **kwd):
			S11b, S12b, S21b, S22b 	= single_slab_guide_matrix(a/lam, epvar*(1.0+1.0j*tan2), d2/a, NM)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	else:
		print('Incorrect parameter specified')
	#---------------------------------------------------------------------------
	if derparam == 'S11abs':
		eps_inc = root_finding(func, ep2, 1e-4, NTOL, MAXIT, delta=delta, val=S11abs, deriv=None)
	else:
		eps_inc = root_finding(func, ep2, 1e-4, NTOL, MAXIT, val=S11abs, deriv=None)

	return np.real((eps_inc-ep2*(1.0+1.0j*tan2))/delta_sc), S11abs


def Derivative_holed_and_solid_rods(roddata, derparam, delta, NTOL, MAXIT, NM, N):

	ep1	= roddata['ep1']
	tan1	= roddata['tan1']

	ep2	= roddata['ep2']
	tan2	= roddata['tan2']

	ro1	= roddata['ro1']
	xo1	= roddata['xo1']
	ro1in	= roddata['ro1in']
	d1	= roddata['d1']
	ph1	= roddata['ph1']

	ro2	= roddata['ro2']
	xo2	= roddata['xo2']

	L	= roddata['L']

	f	= roddata['f']
	a	= roddata['a']
	#===========================================================================
	# ep1	- auxiliary holed rod dielectric constant
	# ep2	- MUT solid rod dielectric constant

	# ro1  	- auxiliary rod radius in mm
	# ro2	- MUT rod radius in mm

	# ro1in - auxiliary rod hole radius in mm
	# d1    - auxiliary rod hole-to-hole separation in mm
	# ph1   - auxiliary rod hole line angle in mm

	# L	- interrod separation distance in mm
	# f	- operating frequency in GHz

	# a	- waveguide width in mm
	#===========================================================================
	# NM	= 3			# number of higher order waveguide modes considered
	# N	= 5			# number of basis functions for each rod surface
	#===========================================================================
	MoMinit()	# perform library initialization

	lam = 300.0/f
	#===========================================================================
	def evalparam_holed(roddata, lam):

		param 		= MoMparam()

		param.ep1 	= roddata['ep'] + 0.0j
		param.ep2 	= 1.0 + 0.0j
		param.ep3 	= 1.0 + 0.0j

		param.la 	= a/lam

		param.ro1 	= roddata['ro']/a
		param.ro2 	= roddata['roin']/a
		param.ro3 	= roddata['roin']/a

		param.xo1 	= 0.5 + roddata['xo']/a
		param.xo2 	= 0.5 + 0.5*roddata['d']/a*np.cos(roddata['ph']) + roddata['xo']/a
		param.xo3 	= 0.5 - 0.5*roddata['d']/a*np.cos(roddata['ph']) + roddata['xo']/a

		param.zo1 	= 0.0
		param.zo2 	= 0.5 - 0.5*roddata['d']/a*np.sin(roddata['ph'])
		param.zo3 	= 0.5 + 0.5*roddata['d']/a*np.sin(roddata['ph'])

		return param
	#===========================================================================
	def evalparam_solid(roddata, lam):

		param 		= MoMparam()

		param.ep 	= roddata['ep'] + 0.0j

		param.la 	= a/lam

		param.ro 	= roddata['ro']/a
		param.xo 	= 0.5 + roddata['xo']/a
		param.zo 	= 0.0

		return param
	#===========================================================================
	def calcT(L, lam):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)

		return T
	#===========================================================================
	def MoM_holed_rod_S_matrix(roddata, lam):
		param = evalparam_holed(roddata, lam)
		S11, S12, S21, S22 = MoM_two_in_one_(param, NM, N)
		return S11, S12, S21, S22
	#===========================================================================
	def MoM_solid_rod_S_matrix(roddata, lam):
		param = evalparam_solid(roddata, lam)
		S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
		return S11, S12, S21, S22
	#===========================================================================
	def calcS11(S11b):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#===========================================================================
	roddata1 		= {'ep': ep1*(1.0+1.0j*tan1), 'ro': ro1, 'xo': xo1, 'roin': ro1in, 'd': d1, 'ph': ph1}
	S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
	T 			= calcT(L, lam)
	roddata2 		= {'ep': ep2*(1.0+1.0j*tan2), 'ro': ro2, 'xo': xo2}
	S11b, S12b, S21b, S22b 	= MoM_solid_rod_S_matrix(roddata2, lam)
	S11abs 			= np.abs(calcS11(S11b))
	#---------------------------------------------------------------------------
	if derparam != 'ep1' and derparam != 'tan1':
		ep1 = ep1*(1.0 + 1.0j*tan1)
	#---------------------------------------------------------------------------
	if derparam == 'ro1':
		delta_sc = ro1*delta
		roddata1 		= {'ep': ep1, 'ro': ro1*(1.0+delta), 'xo': xo1, 'roin': ro1in, 'd': d1, 'ph': ph1}
		S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
		T 			= calcT(L, lam)
		roddata2		= {'ep': ep2, 'ro': ro2, 'xo': xo2}
		param 			= evalparam_solid(roddata2, lam)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'xo1':
		delta_sc = a*delta
		roddata1 		= {'ep': ep1, 'ro': ro1, 'xo': xo1+a*delta, 'roin': ro1in, 'd': d1, 'ph': ph1}
		S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
		T 			= calcT(L, lam)
		roddata2		= {'ep': ep2, 'ro': ro2, 'xo': xo2}
		param 			= evalparam_solid(roddata2, lam)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'ro1in':
		delta_sc = ro1in*delta
		roddata1 		= {'ep': ep1, 'ro': ro1, 'xo': xo1, 'roin': ro1in*(1.0+delta), 'd': d1, 'ph': ph1}
		S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
		T 			= calcT(L, lam)
		roddata2		= {'ep': ep2, 'ro': ro2, 'xo': xo2}
		param 			= evalparam_solid(roddata2, lam)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'd1':
		delta_sc = d1*delta
		roddata1 		= {'ep': ep1, 'ro': ro1, 'xo': xo1, 'roin': ro1in, 'd': d1*(1.0+delta), 'ph': ph1}
		S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
		T 			= calcT(L, lam)
		roddata2		= {'ep': ep2, 'ro': ro2, 'xo': xo2}
		param 			= evalparam_solid(roddata2, lam)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'ph1':
		delta_sc = delta
		roddata1 		= {'ep': ep1, 'ro': ro1, 'xo': xo1, 'roin': ro1in, 'd': d1, 'ph': ph1+delta}
		S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
		T 			= calcT(L, lam)
		roddata2		= {'ep': ep2, 'ro': ro2, 'xo': xo2}
		param 			= evalparam_solid(roddata2, lam)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'ro2':
		delta_sc = ro2*delta
		roddata1 		= {'ep': ep1, 'ro': ro1, 'xo': xo1, 'roin': ro1in, 'd': d1, 'ph': ph1}
		S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
		T 			= calcT(L, lam)
		roddata2		= {'ep': ep2, 'ro': ro2*(1.0+delta), 'xo': xo2}
		param 			= evalparam_solid(roddata2, lam)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'xo2':
		delta_sc = a*delta
		roddata1 		= {'ep': ep1, 'ro': ro1, 'xo': xo1, 'roin': ro1in, 'd': d1, 'ph': ph1}
		S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
		T 			= calcT(L, lam)
		roddata2		= {'ep': ep2, 'ro': ro2, 'xo': xo2+a*delta}
		param 			= evalparam_solid(roddata2, lam)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'ep1':
		delta_sc = ep1*delta
		roddata1 		= {'ep': ep1*(1.0+delta)*(1.0+1.0j*tan1), 'ro': ro1, 'xo': xo1, 'roin': ro1in, 'd': d1, 'ph': ph1}
		S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
		T 			= calcT(L, lam)
		roddata2		= {'ep': ep2, 'ro': ro2, 'xo': xo2}
		param 			= evalparam_solid(roddata2, lam)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'tan1':
		delta_sc = tan1*delta
		roddata1 		= {'ep': ep1*(1.0+1.0j*tan1*(1.0+delta)), 'ro': ro1, 'xo': xo1, 'roin': ro1in, 'd': d1, 'ph': ph1}
		S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
		T 			= calcT(L, lam)
		roddata2		= {'ep': ep2, 'ro': ro2, 'xo': xo2}
		param 			= evalparam_solid(roddata2, lam)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'tan2':
		delta_sc = tan2*delta
		roddata1 		= {'ep': ep1, 'ro': ro1, 'xo': xo1, 'roin': ro1in, 'd': d1, 'ph': ph1}
		S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
		T 			= calcT(L, lam)
		roddata2		= {'ep': ep2, 'ro': ro2, 'xo': xo2}
		param 			= evalparam_solid(roddata2, lam)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2*(1.0+delta))
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'L':
		delta_sc = L*delta
		roddata1 		= {'ep': ep1, 'ro': ro1, 'xo': xo1, 'roin': ro1in, 'd': d1, 'ph': ph1}
		S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
		T 			= calcT(L*(1.0+delta), lam)
		roddata2		= {'ep': ep2, 'ro': ro2, 'xo': xo2}
		param 			= evalparam_solid(roddata2, lam)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'f':
		delta_sc = f*delta
		lam			= 300.0/(f*(1.0+delta))
		roddata1 		= {'ep': ep1, 'ro': ro1, 'xo': xo1, 'roin': ro1in, 'd': d1, 'ph': ph1}
		S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
		T 			= calcT(L, lam)
		roddata2		= {'ep': ep2, 'ro': ro2, 'xo': xo2}
		param 			= evalparam_solid(roddata2, lam)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'a':
		delta_sc = a*delta
		a			= a*(1.0+delta)
		roddata1 		= {'ep': ep1, 'ro': ro1, 'xo': xo1, 'roin': ro1in, 'd': d1, 'ph': ph1}
		S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
		T 			= calcT(L, lam)
		roddata2		= {'ep': ep2, 'ro': ro2, 'xo': xo2}
		param 			= evalparam_solid(roddata2, lam)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	elif derparam == 'S11abs':
		delta_sc = delta
		roddata1 		= {'ep': ep1, 'ro': ro1, 'xo': xo1, 'roin': ro1in, 'd': d1, 'ph': ph1}
		S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
		T 			= calcT(L, lam)
		roddata2		= {'ep': ep2, 'ro': ro2, 'xo': xo2}
		param 			= evalparam_solid(roddata2, lam)
		def func(epvar, **kwd):
			param.ep 		= epvar*(1.0+1.0j*tan2)
			S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
			S11abs			= np.abs(calcS11(S11b))
			return S11abs
	else:
		print('Incorrect parameter specified')
	#---------------------------------------------------------------------------
	if derparam == 'S11abs':
		eps_inc = root_finding(func, ep2, 1e-4, NTOL, MAXIT, delta=delta, val=S11abs, deriv=None)
	else:
		eps_inc = root_finding(func, ep2, 1e-4, NTOL, MAXIT, val=S11abs, deriv=None)

	return np.real((eps_inc-ep2*(1.0+1.0j*tan2))/delta_sc), S11abs

