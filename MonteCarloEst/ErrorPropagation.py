import os
import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *
from Derivatives import *
#=======================================================================================
#=======================================================================================
def ErrorPropagationDerivative(model, derpar):

	# specify the name of the output pdf file
	filename = 'THESIS_FIG_1'
	#========================================================================
	# MEASUREMENT MODEL PARAMETERS
	#========================================================================
	# slab model type: free space ('free') or waveguide model ('guide') (by default free space model is assumed)

#	model 	= 'two-slabs-guide'
#	model 	= 'three-slabs-guide'
#	model 	= 'three-slabs-free'
#	model	= 'threerods'
#	model	= 'tworods'
#	model	= 'slab-rod'
#	model	= 'rod-slab'
#	model	= 'holed-solid'

#	derpar  =  'S11abs'		# parameter to take the derivative with respect to
	#========================================================================
	if model == 'two-slabs-guide':

		ep1     = 4.2		# auxiliary slab dielectric constant
		ep2     = 11.0		# MUT slab dielectric constant

		d1	= 10.0		# auxiliary slab thickness in mm
		d2	= 10.0		# MUT slab thickness in mm

		L	= 30.0		# interslab separation distance in mm
		f	= 10.0		# operating frequency in GHz

		a	= 22.86		# waveguide width in mm
	#========================================================================
	elif model == 'three-slabs-guide':

		ep1     = 4.2		# auxiliary slab dielectric constant
		ep2     = 11.0		# middle slab dielectric constant
		epm     = 2.2		# MUT slab dielectric constant

		d1	= 10.0		# auxiliary slab thickness in mm
		d2	= 10.0		# MUT slab thickness in mm
		L	= 30.0		# middle slab thickness in mm

		f	= 10.0		# operating frequency in GHz

		a	= 22.86		# waveguide width in mm
	#========================================================================
	elif model == 'three-slabs-free':

		ep1     = 4.2		# auxiliary slab dielectric constant
		ep2     = 12.5		# middle slab dielectric constant
		epm     = 2.2		# MUT slab dielectric constant

		d1	= 10.0		# auxiliary slab thickness in mm
		d2	= 5.0		# MUT slab thickness in mm

		L	= 30.0		# middle slab thickness in mm
		f	= 10.0		# operating frequency in GHz
	#========================================================================
	elif model == 'tworods':

		ep1	= 4.2		# auxiliary rod dielectric constant
		ep2	= 12.5		# MUT rod dielectric constant

		ro1	= 7.0		# auxiliary rod radius in mm
		ro2	= 3.5		# MUT rod radius in mm

		L	= 30.0		# interrod separation distance in mm
		f	= 10.0		# operating frequency in GHz

		a	= 22.86		# waveguide width in mm

		NM	= 3		# number of higher order waveguide modes considered
		N	= 5		# number of basis functions for each rod surface
	#========================================================================
	elif model == 'threerods':

		ep1	= 4.2		# 1st auxiliary rod dielectric constant
		ep2	= 12.5		# MUT rod dielectric constant
		ep3	= 4.2		# 2nd auxiliary rod dielectric constant

		ro1	= 6.0		# 1st auxiliary rod radius in mm
		ro2	= 3.0		# MUT rod radius in mm
		ro3	= 6.0		# 2nd auxiliary rod radius in mm

		L1	= 16.0		# distance between the 1st and the 2nd rods in mm
		L2	= 16.0		# distance between the 2nd and the 3rd rods in mm

		f	= 10.0		# operating frequency in GHz
		a	= 22.86		# waveguide width in mm

		NM	= 3		# number of higher order waveguide modes considered
		N	= 5		# number of basis functions for each rod surface
	#========================================================================
	elif model == 'slab-rod':

		ep1	= 4.2		# auxiliary slab complex permittivity
		ep2	= 12.5		# MUT complex permittivity

		d1	= 12.0		# auxiliary slab thickness in mm
		ro2	= 4.7		# measurable rod radius in mm

		L	= 19.5		# distance between the auxiliary slab and the MUT rods in mm
		f	= 10.0		# operating frequency in GHz

		a	= 22.86		# waveguide width in mm

		NM	= 3		# number of higher order waveguide modes considered
		N	= 5		# number of basis functions for each rod surface
	#========================================================================
	elif model == 'rod-slab':

		ep1	= 4.2		# auxiliary rod dielectric constant
		ep2	= 12.5		# MUT slab dielectric constant

		ro1	= 12.0		# auxiliary rod thickness in mm
		d2	= 4.7		# MUT slab thickness in mm

		L	= 19.5		# rod-to-slab separation distance mm
		f	= 10.0		# operating frequency in GHz

		a	= 22.86		# waveguide width in mm

		NM	= 3		# number of higher order waveguide modes considered
		N	= 5		# number of basis functions for each rod surface
	#========================================================================
	elif model == 'holed-solid':

		ep1	= 4.2		# auxiliary holed rod dielectric constant
		ep2	= 12.5		# MUT solid rod dielectric constant

		ro1	= 7.0		# auxiliary rod radius in mm
		ro2	= 4.5		# MUT rod thicknes in mm

		ro1in	= 0.5		# auxiliary rod hole radius in mm
		d1	= 2.0		# auxiliary rod hole-to-hole separation in mm
		ph1	= 0.0		# auxiliary rod hole line angle in mm

		L	= 27.6		# interrod separation distance in mm
		f	= 10.0		# operating frequency in GHz

		a       = 22.86		# waveguide width in mm

		NM	= 3		# number of higher order waveguide modes considered
		N	= 5		# number of basis functions for each rod surface
	#========================================================================

	else:				# parameters of the free space slab model

		d1	= 10.0		# measurable slab thickness in mm
		d2	= 6.0		# auxiliary slab thickness in mm

		L	= 20.0		# interslab separation distance in mm
		f	= 9.0		# operating frequency in GHz

		ep1     = 4.7		# MUT complex permittivity
		ep2     = 12.5		# auxiliary slabs complex permittivity
	#========================================================================
	lam = 300.0/f;
	#========================================================================

	delta =	1e-3

	if model == 'two-slabs-guide':
		slabdata	= {'ep1': ep1, 'ep2': ep2, 'd1': d1, 'd2': d2, 'L': L, 'f': f, 'a': a}
		eps_inc		= Derivative_two_slabs_guide(slabdata, derpar, delta, 1e-4, 100)
	elif model == 'three-slabs-guide':
		slabdata	= {'ep1': ep1, 'epm': epm, 'ep2': ep2, 'd1': d1, 'd2': d2, 'L': L, 'f': f, 'a': a}
		eps_inc		= Derivative_three_slabs_guide(slabdata, derpar, delta, 1e-4, 100)
	elif model == 'three-slabs-free':
		slabdata	= {'ep1': ep1, 'epm': epm, 'ep2': ep2, 'd1': d1, 'd2': d2, 'L': L, 'f': f}
		eps_inc		= Derivative_three_slabs_free_space(slabdata, derpar, delta, 1e-4, 100)
	elif model == 'tworods':
		roddata		= {'ep1': ep1, 'ep2': ep2, 'ro1': ro1, 'ro2': ro2, 'L': L, 'f': f, 'a': a}
		eps_inc 	= Derivative_two_rods(roddata, derpar, delta, 1e-4, 100, NM, N)
	elif model == 'threerods':
		roddata		= {'ep1': ep1, 'ep2': ep2, 'ep3': ep3, 'ro1': ro1, 'ro2': ro2, 'ro3': ro3, 'L1': L1, 'L2': L2, 'f': f, 'a': a}
		eps_inc		= Derivative_three_rods(roddata, derpar, delta, 1e-4, 100, NM, N)
	elif model == 'slab-rod':
		slabroddata	= {'ep1': ep1, 'ep2': ep2, 'd1': d1, 'ro2': ro2, 'L': L, 'f': f, 'a': a}
		eps_inc		= Derivative_slab_and_rod(slabroddata, derpar, delta, 1e-4, 100, NM, N)
	elif model == 'rod-slab':
		rodslabdata	= {'ep1': ep1, 'ep2': ep2, 'ro1': ro1, 'd2': d2, 'L': L, 'f': f, 'a': a}
		eps_inc		= Derivative_rod_and_slab(rodslabdata, derpar, delta, 1e-4, 100, NM, N)
	elif model == 'holed-solid':
		roddata		= {'ep1': ep1, 'ep2': ep2, 'ro1': ro1, 'ro1in': ro1in, 'd1': d1, 'ph1': ph1, 'ro2': ro2, 'L': L, 'f': f, 'a': a}
		eps_inc		= Derivative_holed_and_solid_rods(roddata, derpar, delta, 1e-4, 100, NM, N)
	else:
		slabdata	= {'ep1': ep1, 'ep2': ep2, 'd1': d1, 'd2': d2, 'L': L, 'f': f}
		eps_inc		= Derivative_two_slabs_free_space(slabdata, derpar, delta, 1e-4, 100)

	return eps_inc
#======================================================================================
model 		= 'two-slabs-guide'
print(model)

S11abs_sens 	= ErrorPropagationDerivative(model, 'S11abs')
d1_sens	 	= ErrorPropagationDerivative(model, 'd1')
d2_sens 	= ErrorPropagationDerivative(model, 'd2')
L_sens 		= ErrorPropagationDerivative(model, 'L')

sigma_S11abs    = 0.01
sigma_d1        = 0.015
sigma_d2        = 0.015
sigma_L         = 0.015

tot_delta 	= np.sqrt(POW2(S11abs_sens*sigma_S11abs) + POW2(d1_sens*sigma_d1) + POW2(d2_sens*sigma_d2) + POW2(L_sens*sigma_L))

print(tot_delta)
print('--------------------------------------------------------')
#======================================================================================
model 		= 'three-slabs-guide'
print(model)

S11abs_sens 	= ErrorPropagationDerivative(model, 'S11abs')
d1_sens	 	= ErrorPropagationDerivative(model, 'd1')
d2_sens 	= ErrorPropagationDerivative(model, 'd2')
L_sens 		= ErrorPropagationDerivative(model, 'L')

sigma_S11abs    = 0.01
sigma_d1        = 0.015
sigma_d2        = 0.015
sigma_L         = 0.015

tot_delta 	= np.sqrt(POW2(S11abs_sens*sigma_S11abs) + POW2(d1_sens*sigma_d1) + POW2(d2_sens*sigma_d2) + POW2(L_sens*sigma_L))

print(tot_delta)
print('--------------------------------------------------------')
#======================================================================================
model 		= 'two-slabs-free'
print(model)

S11abs_sens 	= ErrorPropagationDerivative(model, 'S11abs')
d1_sens	 	= ErrorPropagationDerivative(model, 'd1')
d2_sens 	= ErrorPropagationDerivative(model, 'd2')
L_sens 		= ErrorPropagationDerivative(model, 'L')

sigma_S11abs    = 0.01
sigma_d1        = 0.015
sigma_d2        = 0.015
sigma_L         = 0.015

tot_delta 	= np.sqrt(POW2(S11abs_sens*sigma_S11abs) + POW2(d1_sens*sigma_d1) + POW2(d2_sens*sigma_d2) + POW2(L_sens*sigma_L))

print(tot_delta)
print('--------------------------------------------------------')
#======================================================================================
model 		= 'three-slabs-free'
print(model)

S11abs_sens 	= ErrorPropagationDerivative(model, 'S11abs')
d1_sens	 	= ErrorPropagationDerivative(model, 'd1')
d2_sens 	= ErrorPropagationDerivative(model, 'd2')
L_sens 		= ErrorPropagationDerivative(model, 'L')

sigma_S11abs    = 0.01
sigma_d1        = 0.015
sigma_d2        = 0.015
sigma_L         = 0.015

tot_delta 	= np.sqrt(POW2(S11abs_sens*sigma_S11abs) + POW2(d1_sens*sigma_d1) + POW2(d2_sens*sigma_d2) + POW2(L_sens*sigma_L))

print(tot_delta)
print('--------------------------------------------------------')
#===============================================================
model 		= 'tworods'
print(model)

S11abs_sens 	= ErrorPropagationDerivative(model, 'S11abs')
ro1_sens 	= ErrorPropagationDerivative(model, 'ro1')
ro2_sens 	= ErrorPropagationDerivative(model, 'ro2')
L_sens 		= ErrorPropagationDerivative(model, 'L')

sigma_S11abs    = 0.01
sigma_ro1       = 0.015
sigma_ro2       = 0.015
sigma_L         = 0.015

tot_delta = np.sqrt(POW2(S11abs_sens*sigma_S11abs) + POW2(ro1_sens*sigma_ro1) + POW2(ro2_sens*sigma_ro2) + POW2(L_sens*sigma_L))

print(tot_delta)
print('--------------------------------------------------------')
#===============================================================
#===============================================================
model 		= 'threerods'
print(model)

S11abs_sens 	= ErrorPropagationDerivative(model, 'S11abs')
ro1_sens 	= ErrorPropagationDerivative(model, 'ro1')
ro2_sens 	= ErrorPropagationDerivative(model, 'ro2')
ro3_sens 	= ErrorPropagationDerivative(model, 'ro3')
L1_sens		= ErrorPropagationDerivative(model, 'L1')
L2_sens		= ErrorPropagationDerivative(model, 'L2')

sigma_S11abs    = 0.01
sigma_ro1       = 0.015
sigma_ro2       = 0.015
sigma_ro3       = 0.015
sigma_L1        = 0.015
sigma_L2        = 0.015

tot_delta = np.sqrt(POW2(S11abs_sens*sigma_S11abs) + POW2(ro1_sens*sigma_ro1) + POW2(ro2_sens*sigma_ro2) + POW2(ro3_sens*sigma_ro3) + POW2(L1_sens*sigma_L1) + POW2(L2_sens*sigma_L2))

print(tot_delta)
print('--------------------------------------------------------')
#===============================================================
model 		= 'slab-rod'
print(model)

S11abs_sens 	= ErrorPropagationDerivative(model, 'S11abs')
d1_sens 	= ErrorPropagationDerivative(model, 'd1')
ro2_sens 	= ErrorPropagationDerivative(model, 'ro2')
L_sens 		= ErrorPropagationDerivative(model, 'L')

sigma_S11abs    = 0.01
sigma_d1        = 0.015
sigma_ro2       = 0.015
sigma_L         = 0.015

tot_delta = np.sqrt(POW2(S11abs_sens*sigma_S11abs) + POW2(d1_sens*sigma_d1) + POW2(ro2_sens*sigma_ro2) + POW2(L_sens*sigma_L))

print(tot_delta)
print('--------------------------------------------------------')
#===============================================================
model 		= 'rod-slab'
print(model)

S11abs_sens 	= ErrorPropagationDerivative(model, 'S11abs')
ro1_sens 	= ErrorPropagationDerivative(model, 'ro1')
d2_sens 	= ErrorPropagationDerivative(model, 'd2')
L_sens 		= ErrorPropagationDerivative(model, 'L')

sigma_S11abs    = 0.01
sigma_ro1	= 0.015
sigma_d2	= 0.015
sigma_L		= 0.015

tot_delta = np.sqrt(POW2(S11abs_sens*sigma_S11abs) + POW2(ro1_sens*sigma_ro1) + POW2(d2_sens*sigma_d2) + POW2(L_sens*sigma_L))

print(tot_delta)
print('--------------------------------------------------------')
#===============================================================
model 		= 'holed-solid'
print(model)

S11abs_sens 	= ErrorPropagationDerivative(model, 'S11abs')
ro1_sens 	= ErrorPropagationDerivative(model, 'ro1')
ro1in_sens 	= ErrorPropagationDerivative(model, 'ro1in')
d1_sens 	= ErrorPropagationDerivative(model, 'd1')
ph1_sens 	= ErrorPropagationDerivative(model, 'ph1')
ro2_sens 	= ErrorPropagationDerivative(model, 'ro2')
L_sens 		= ErrorPropagationDerivative(model, 'L')

sigma_S11abs    = 0.01
sigma_ro1	= 0.015
sigma_ro1in	= 0.015
sigma_d1	= 0.015
sigma_ph1	= 0.05
sigma_ro2	= 0.015
sigma_L		= 0.015

tot_delta = np.sqrt(POW2(S11abs_sens*sigma_S11abs) + POW2(ro1_sens*sigma_ro1) + POW2(ro1in_sens*sigma_ro1in) + POW2(d1_sens*sigma_d1) + POW2(ph1_sens*sigma_ph1) + POW2(ro2_sens*sigma_ro2) + POW2(L_sens*sigma_L))

print(tot_delta)
print('--------------------------------------------------------')

