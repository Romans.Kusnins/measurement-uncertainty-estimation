import numpy as np
from matplotlib import pyplot as plt
from slab_res3md import (three_slab_guide, POW2)
from plot2pdf import *

#===========================================================================
ep1     = [1.0]			# auxiliary slab complex permittivity Arlon AD430 (ep-4.3, tan-0.003)
ep2     = [1.0, 20.0, 0.1]	# MUT slab complex permittivity (Duroid 10.2)
#ep2     = [7.0]		# MUT slab complex permittivity (Duroid 10.2)

d1	= [4.0]			# auxiliary slab thickness in mm
d2	= [2.5]			# MUT slab thickness in mm

L       = [20.0]		# interslab separation distance in mm
f       = [10.0]		# operating frequency in GHz

a       = 22.86			# waveguide width in mm
#===========================================================================
isPDF	= False			# generate pdf file with the plot
#===========================================================================
def calcS11(slabdata1, slabdata2, L, lam):

	ep1 	= slabdata1['ep']
	ep2 	= slabdata2['ep']

	d1 	= slabdata1['d']
	d2 	= slabdata2['d']

	S11 	= three_slab_guide(a/lam, ep1, 1.0, ep2, d1/a, d2/a, L/a)

	return S11
#===========================================================================
xlabels  = dict()
xlabels1 = dict()
xlabels2 = dict()

xlabels1['ep'] 	= 'Auxiliary slab dielectric constant, mm'
xlabels2['ep'] 	= 'MUT dielectric constant, mm'

xlabels1['d'] 	= 'MUT slab thickness, mm'
xlabels2['d'] 	= 'auxiliary slab thickness, mm'

xlabels['L'] 	= 'Rod separation distance , mm'
xlabels['f'] 	= 'Frequency, GHz'
#============================================================================
slabdata1 = {'ep': ep1, 'd': d1}
slabdata2 = {'ep': ep2, 'd': d2}
#============================================================================

list_found 	= False

isSlab1		= False
isSlab2		= False

for par in slabdata1.keys():
	if type(slabdata1[par]) == list:
		if len(slabdata1[par]) > 1 and not list_found:
			var_range	= slabdata1[par];
			parname 	= par
			isSlab1		= True
			list_found 	= True
		else:
			slabdata1[par] = slabdata1[par][0];
for par in slabdata2.keys():
	if type(slabdata2[par]) == list:
		if len(slabdata2[par]) > 1 and not list_found:
			var_range	= slabdata2[par];
			parname 	= par
			isSlab2		= True
			list_found 	= True
		else:
			slabdata2[par] = slabdata2[par][0];

if type(L) == list:
	if len(L) > 1 and not list_found:
		var_range 	=  L
		parname 	= 'L'
		list_found 	= True
	else:
		L = L[0]

if type(f) == list:
	if len(f) > 1 and not list_found:
		var_range 	=  f
		parname 	= 'f'
		list_found 	= True
	else:
		f = f[0]

if not list_found:
	print("There is nothing to plot.")
	quit()
#=====================================================================
var_values 	= np.arange(*var_range)
S11 		= np.ndarray((var_values.shape), dtype='complex')

if isSlab1:
	lam = 300.0/f
	for n in range(var_values.shape[0]):
		slabdata1[parname] = var_values[n]
		S11[n] = calcS11(slabdata1, slabdata2, L, lam)
elif isSlab2:
	lam = 300.0/f
	for n in range(var_values.shape[0]):
		slabdata2[parname] = var_values[n]
		S11[n] = calcS11(slabdata1, slabdata2, L, lam)
else:
	if parname == 'L':
		lam = 300.0/f
		for n in range(var_values.shape[0]):
			S11[n] = calcS11(slabdata1, slabdata2, var_values[n], lam)
	else:
		for n in range(var_values.shape[0]):
			lam = 300.0/var_values[n]
			S11[n] = calcS11(slabdata1, slabdata2, L, lam)
#=====================================================================
if isPDF:
	if isSlab1:
		labels ={'xlabel': xlabels1[parname], 'ylabel': '$|S_{11}|$'}
	elif isSlab2:
		labels ={'xlabel': xlabels2[parname], 'ylabel': '$|S_{11}|$'}
	else:
		labels ={'xlabel': xlabels[parname],  'ylabel': '$|S_{11}|$'}

	plot2latex(var_values, np.abs(S11), labels=labels, legend=['two slab model'])

else:
	plt.plot(var_values, np.abs(S11))

	if isSlab1:
		plt.xlabel(xlabels1[parname])
	elif isSlab2:
		plt.xlabel(xlabels2[parname])
	else:
		plt.xlabel(xlabels[parname])

	plt.ylabel('|S11|')
	plt.grid()
	plt.show()
