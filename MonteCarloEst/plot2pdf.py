import os
import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *
#from PDFPlotModule import *
from PlottingRoutines import *
#================================================================================
#================================================================================

def plot2latex_aux(xo, yo, **kwd):

	#========================================================================
	# CHECK AND PREPROCESSING OF THE INPUT DATA
	#========================================================================
	openPDF = kwd.get('openPDF', False)

	if type(yo) is np.ndarray:
		if len(yo.shape) == 1:
			CurveNum = 1
		elif len(yo.shape) == 2:
			CurveNum = yo.shape[0]
		else:
			print('Error: the number of input array dimensions is greater than two.')
	elif type(xo) is list and type(yo) is list:
		CurveNum	 = len(xo)
		xo 		= xo.copy()
		yo 		= yo.copy()
	else:
		print('Error: The array with the data to be plotted is not of numpy type.')
		quit()
	#========================================================================
	labels = kwd.get('labels', '')
	if type(labels) is dict:
		xlabel = labels.get('xlabel', '')
		ylabel = labels.get('ylabel', '')
	else:
		xlabel = ''
		ylabel = ''

	legend = kwd.get('legend', None)
	display_legend = True
	if type(legend) is list or type(legend) is tuple:
		if len(legend) != CurveNum:
			display_legend = False
			print('Warning: The number of elements in the legend array does not agree with the number of curves to be plotted; therefore, the legend will not be displayed.')
		else:
			for n in range(CurveNum):
				if type(legend[n]) is not str:
					display_legend = False
					print('Warning: The legend cannot be displayed since the legend data array entries are not of str type.')
	elif legend == None:
		print('No legend data have been provided.')
		display_legend = False
	else:
		display_legend = False
		print('Warning: The legend item text array is of incorrect type. The legend cannot be displayed.')
	#========================================================================
	linewidth = kwd.get('LineWidth', 0.5)		# curve line width
	#========================================================================
	# LABEL PARAMETERS
	#========================================================================
	xlabelpos 	= kwd.get('xLabelPosition', 0.5) 	# relative position of the horizontal axis label text
	ylabelpos 	= kwd.get('yLabelPosition', 0.5)	# relative position of the vertical axis label text

	xlabelsep 	= kwd.get('xLableSep', 0.10) 		# spacing between the lower side of the graph and the horizontal axis label text
	ylabelsep 	= kwd.get('yLablePos', 0.12) 		# spacing between the left side of the graph and the vertical axis label text

	labelx_scale 	= 2.5					# text scale for the x-axis label
	labely_scale 	= 2.5					# text scale for the y-axis label
	#========================================================================
	# LEGEND PARAMETERS
	#========================================================================
	legend_position 	= kwd.get('LegendPosition', [ 0.0, 0.0 ])	# relative x and y coordinates of the legend
	legend_scale 		= 2.0						# scaling coefficient for the legend text
	#========================================================================
	# SPECIFY LEGEND TEXT
	#========================================================================
	#legend_text = 'ABC', 'ABCD'
	#========================================================================
	# LEGEND PARAMETERS
	#========================================================================
	tickscale 		= kwd.get('TickTextScale', 2.2)		# scaling coefficient for the ticks labels
	legend_scale 		= 2.0					# scaling coefficient for the legend text
	labelx_scale 		= 2.6					# text scale for the x-axis label
	labely_scale 		= 2.4					# text scale for the y-axis label
	#========================================================================
	colour = list()
	colour.append('blue')
	colour.append('red')
	colour.append('black')
	colour.append('magenta')
	colour.append('green')
	colour.append('yellow')
	colour.append('cyan')
	colour.append('brown')
	colour.append('purple')
	colour.append('orange')
	#========================================================================
	filename	= kwd.get('FileName', 'FIGURE')
	fid 		= CreateLatexFile(filename)
	#========================================================================
	#========================================================================
	# DRAW GRID LINES AND TICKS FOR THE GRAPH
	#========================================================================
#	yo		= yo - 3.0

	xGridLineNUM 	= kwd.get('xGridLineNUM', 6)	# number of vertical grid lines
	yGridLineNUM 	= kwd.get('yGridLineNUM', 8)	# number of horizontal grid line

	xdecnum 	= kwd.get('xDecNum', 1)		# number of decimal places displayed in the xticks
	ydecnum 	= kwd.get('yDecNum', 1)		# number of decimal places displayed in the yticks

	XZero 		= kwd.get('XZero', False)
	YZero 		= kwd.get('YZero', False)

	if len(kwd.get('DrawRectangle', [])) > 0:
		RECT = kwd['DrawRectangle']
		fid.write('\\draw[magenta, line width = 1.6pt, dash pattern = on 2mm off 0.6mm] (' + n2s(RECT[0]) + ',' + n2s(RECT[1]) + ') rectangle (' + n2s(RECT[2]) + ',' + n2s(RECT[3]) + ');\n')

		arr_wd  	= 0.002
		arr_len 	= 0.2
		arr_tip_wd 	= 0.007
		arr_tip_len 	= 0.01
		arr_draft 	= 0.1975

		ROT_ANG 	= 30.0/180.0*np.pi

		ROT_MAT		= np.asarray([[np.cos(ROT_ANG), np.sin(ROT_ANG)], [-np.sin(ROT_ANG), np.cos(ROT_ANG)]])

		ARR = np.asarray([[-arr_wd, 0.0], [arr_wd, 0.0], [arr_wd, arr_len], [arr_tip_wd, arr_draft], [0.0, arr_len+arr_tip_len], [-arr_tip_wd, arr_draft], [-arr_wd, arr_len]])

		ARR = np.matmul(ARR, ROT_MAT)

		ARR += np.asarray([0.42, 0.43])

		arrstr = '\\draw[magenta, fill=magenta, line width = 1pt] (' + n2s(ARR[0,0]) + ',' + n2s(ARR[0,1]) + ')'
		for n in range(ARR.shape[0] - 1):
			arrstr += '-- (' + n2s(ARR[n+1,0]) + ',' + n2s(ARR[n+1,1]) + ')'
		arrstr += '-- cycle;\n'

		fid.write(arrstr)


	# Calculate the upper and the lower limits for the y-axis
	if type(xo) is list:

		xmin = np.empty((len(yo),))
		xmax = np.empty((len(yo),))
		ymin = np.empty((len(yo),))
		ymax = np.empty((len(yo),))

		for n in range(len(yo)):
			# Normalize yo to max value of unity
			xmin[n] = np.min(xo[n])
			xmax[n] = np.max(xo[n])
			ymin[n] = np.min(yo[n][np.logical_not(np.isnan(yo[n]))])
			ymax[n] = np.max(yo[n][np.logical_not(np.isnan(yo[n]))])

		xmin = np.min(xmin)
		xmax = np.max(xmax)
		ymin = np.min(ymin)
		ymax = np.max(ymax)

		if XZero:
			xmax_new, xmin_new, xexp = findtrickvalues(xmin, xmax, xdecnum, xGridLineNUM, IncZero=True)
		else:
			xmax_new, xmin_new, xexp = findtrickvalues(xmin, xmax, xdecnum, xGridLineNUM)
		if YZero:
			ymax_new, ymin_new, yexp = findtrickvalues(ymin, ymax, ydecnum, yGridLineNUM, IncZero=True)
		else:
			ymax_new, ymin_new, yexp = findtrickvalues(ymin, ymax, ydecnum, yGridLineNUM)

		for n in range(len(yo)):
			xo[n] = (xo[n] - xmin_new)/(xmax_new - xmin_new)
			yo[n] = (yo[n] - ymin_new)/(ymax_new - ymin_new)

	else:

		xmin = np.min(xo)
		xmax = np.max(xo)
		ymin = np.min(yo[np.logical_not(np.isnan(yo))])
		ymax = np.max(yo[np.logical_not(np.isnan(yo))])

		if XZero:
			xmax_new, xmin_new, xexp = findtrickvalues(xmin, xmax, xdecnum, xGridLineNUM, IncZero=True)
		else:
			xmax_new, xmin_new, xexp = findtrickvalues(xmin, xmax, xdecnum, xGridLineNUM)

		if YZero:
			ymax_new, ymin_new, yexp = findtrickvalues(ymin, ymax, ydecnum, yGridLineNUM, IncZero=True)
		else:
			ymax_new, ymin_new, yexp = findtrickvalues(ymin, ymax, ydecnum, yGridLineNUM)

		xo = (xo - xmin_new)/(xmax_new - xmin_new)
		yo = (yo - ymin_new)/(ymax_new - ymin_new)
	#-----------------------------------------------------------------------------------
	x = np.linspace(0.0, 1.0, xGridLineNUM)
	y = np.linspace(0.0, 1.0, yGridLineNUM)

	grid_line_V(fid, x,   x, -0.01, x*(xmax_new-xmin_new) + xmin_new, tickscale, [0, 1], xexp=xexp)
	grid_line_H(fid, y, 0.0,     y, y*(ymax_new-ymin_new) + ymin_new, tickscale, [0, 1], yexp=yexp)

	axis_new(fid, [0.0, 1.0, 0.0, 1.0], 0.5)
	#========================================================================
	# PLOT CURVES
	#========================================================================
	plotopt = {'LineType': kwd.get('LineType', [])}

	plotcurves(fid, xo, yo, colour, linewidth, CurveNum, **plotopt)
	#========================================================================
	# ADD LABELS TO BOTH X AND Y AXIS
	#========================================================================
	fid.write('\\node[scale = ' + n2s(labelx_scale) + '](xlabel) at (' + n2s(xlabelpos)  + ',' + n2s(-xlabelsep) + '){' + xlabel +  '};\n')

	if kwd.get('ylabel_vert', False):
		fid.write('\\node[scale = ' + n2s(labely_scale) + ', rotate=90](ylabel) at (' + n2s(-ylabelsep) + ',' + n2s(ylabelpos) + '){' + ylabel + '};\n')
	else:
		fid.write('\\node[scale = ' + n2s(labely_scale) + '](ylabel) at (' + n2s(-ylabelsep) + ',' + n2s(ylabelpos) + '){' + ylabel + '};\n')
	#========================================================================
	# ADD LEGEND IF ANY
	#========================================================================
	if display_legend:
		legend_options = {'opacity': 1.0, 'colour': 'white', 'linecolour': 'white', 'LineType': kwd.get('LineType',[])}
		legend_box(fid, filename, CurveNum, legend_position, colour, legend, legend_scale, **legend_options)
	#========================================================================
	fid.write('\end{tikzpicture} \n')
	fid.write('\end{document} \n')
	fid.close()
	#========================================================================
	if not openPDF:
		if os.path.isfile(filename + '.aux'):
			os.remove(filename + '.aux')

	os.system('pdflatex ' + filename + '.tex > /dev/null 2>&1')

	# open the generated PDF file with the default linux viewer
	if openPDF:
		os.system('xdg-open ' + filename + '.pdf')
#================================================================================
def plot2latex(epv, yo, **kwd):

	plot2latex_aux(epv, yo, **kwd)

	kwd['openPDF'] = True
	plot2latex_aux(epv, yo, **kwd)

