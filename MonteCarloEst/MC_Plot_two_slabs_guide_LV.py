import os
import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *
from Derivatives import *

try:
        matplot = True
        from matplotlib import pyplot as plt
except ModuleNotFoundError:
        print("The matplotlib module is not installed.")
        matplot = False


from plot2pdf import *

from Uncertainty_vs_S11 import *
#==================================================================================
xMin 		= 15.0
xMax 		= 30.0
MaxVal 		= 6.0

task 		= 'sensitivity'
task 		= 'total'
#---------------------------------------
#sensparam 	= 'S11abs'
sensparam 	= 'd2'
#---------------------------------------
var_param 	= 'L'
#---------------------------------------
quantity 	= 'var'
#quantity 	= 'mean'


#PREFIX = '_20_'
#PREFIX = ''	# d1 = 4.0
#PREFIX = '_4_5_'
PREFIX = '_5_0_'

MCNUM = 20000
#==================================================================================
isPDF 		= True
#isPRINT	= False
#==================================================================================
xlabels		= dict()

xlabels['ep1']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,aux}}$'
xlabels['ep2']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,mut}}$'
xlabels['tan1'] = '$\\tan_{\\delta^{\\mathrm{aux}}}$'
xlabels['tan2'] = '$\\tan_{\\delta^{\\mathrm{mut}}}$'
xlabels['d1']   = '$d_{\\mathrm{aux}}$ mm'
xlabels['d2']   = '$d_{\\mathrm{mut}}$ mm'
xlabels['a']    = '$a mm'
xlabels['L']    = '$d_{\\mathrm{int}}$ mm'
xlabels['f']    = 'f, GHz'

#paramname       = {'S11abs': '$u_{|S_{11}|(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})}$', 'd1': '$u_{d_{\\mathrm{aux}}}(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})$',  'd2': '$u_{d_{\\mathrm{mut}}}(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})$',  'L': '$u_{d_{\\mathrm{in}}}(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})$', 'ep1': '$u_{\\varepsilon^{\\prime}_{\\mathrm{r,aux}}}(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})$',  'tan1': '$ u_{\\tan{\\delta^{\\mathrm{aux}}}}(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})$', 'tan2': 'u_{\\tan{\\delta^{\\mathrm{mut}}}}(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})', 'a': 'u_{a}(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})', 'f': 'u_{f}(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})'}

paramname       = {'S11abs': '$u_{|S_{11}|}$', 'd1': '$u_{d_{\\mathrm{aux}}}$',  'd2': '$u_{d_{\\mathrm{mut}}}$',  'L': '$u_{d_{\\mathrm{in}}}$', 'ep1': '$u_{\\varepsilon^{\\prime}_{\\mathrm{r,aux}}}$',  'tan1': '$ u_{\\tan{\\delta^{\\mathrm{aux}}}}$', 'tan2': 'u_{\\tan{\\delta^{\\mathrm{mut}}}}', 'a': 'u_{a}', 'f': 'u_{f}'}
#==================================================================================
# PLOTTING CALCULATED DATA
#==================================================================================
FOLDER = './RESULTS_2SL_GUIDE/'

MCSTR = '{:}'.format(MCNUM)

if task == 'sensitivity':
	MCfilename 	= FOLDER + 'two_slabs_guide_var_' + var_param + '_sens_' + sensparam + '_MCnum'+ PREFIX + '_' + MCSTR + '.npy'
	MCfilename_mean	= FOLDER + 'two_slabs_guide_var_' + var_param + '_sens_' + sensparam + '_MCnum_mean'+ PREFIX + '_' + MCSTR + '.npy'
	EPMfilename 	= FOLDER + 'two-slabs-guide_var_' + var_param + '_standard_' + sensparam + '_EPM' + PREFIX + '.npy'
else:
	MCfilename 	= FOLDER + 'two_slabs_guide_var_' + var_param + '_total_MCnum' + PREFIX + '_' + MCSTR + '.npy'
	MCfilename_mean	= FOLDER + 'two_slabs_guide_var_' + var_param + '_total_MCnum_mean' + PREFIX + '_' + MCSTR + '.npy'
	EPMfilename 	= FOLDER + 'two-slabs-guide_var_' + var_param + '_total_EPM' + PREFIX + '.npy'

MCdata 		= np.load(MCfilename)
MCdata_mean 	= np.load(MCfilename_mean)
EPMdata 	= np.load(EPMfilename)
#==================================================================================
if MaxVal != None:
	MCdata_mean[1, MCdata_mean[1,:] > MaxVal]	= np.nan
	MCdata[1, MCdata[1,:] > MaxVal] 		= np.nan
	EPMdata[1, EPMdata[1,:] > MaxVal] 		= np.nan

if xMin != None:
	ID 	= EPMdata[0,:] >= xMin
	EPMdata = EPMdata[:,ID]

	ID 	= MCdata[0,:] >= xMin
	MCdata 	= MCdata[:,ID]

if xMax != None:
	ID 	= EPMdata[0,:] <= xMax
	EPMdata = EPMdata[:,ID]

	ID 	= MCdata[0,:] <= xMax
	MCdata 	= MCdata[:,ID]
#==================================================================================
if isPDF:
	legend = ['EPM', 'MC']

	if task == 'sensitivity':
		filename  = 'TWO_SL_GUIDE_VAR_' + var_param + '_SENS_' + sensparam + PREFIX + '_LV'
		labels = {'xlabel': xlabels[var_param], 'ylabel': paramname[sensparam]}
	else:
		filename  = 'TWO_SL_GUIDE_VAR_' + var_param + '_TOTAL' + PREFIX + '_LV'
		labels = {'xlabel': xlabels[var_param], 'ylabel': '$u(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})$'}

	legendpos = [0.7, 0.8]

	if quantity == 'mean':
		labels = {'xlabel': xlabels[var_param], 'ylabel': '$\\bar{u}(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})$'}
		filename  = 'TWO_SL_GUIDE_VAR_' + var_param + '_MEAN' + PREFIX + '_LV'
	#	labels = {'xlabel': xlabels[var_param], 'ylabel': ylabeldelta + ' ' + paramname[param]}
		plot2latex(MCdata_mean[0,:], MCdata_mean[1,:], xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, LineType=['','dash'], YZero=False, LegendPosition=legendpos, FileName=filename)
	else:
		plot2latex([EPMdata[0,:], MCdata[0,:]], [EPMdata[1,:], MCdata[1,:]], xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, LineType=['','dash'], LegendPosition=legendpos, FileName=filename)
#	plot2latex([EPMdata[0,:], MCdata[0,:]], [EPMdata[1,:], MCdata[1,:]], ylabel_vert=True, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels)
elif matplot:
#	plt.ylabel(ylabeldelta + ' ' + paramname[param])
	plt.plot(MCdata[0,:], EPMdata[1,:])
	plt.grid()
	plt.show()
else:
	pass
#       print('Calculated data have been saved into a file.')

print('--------------------------------------------------------')
