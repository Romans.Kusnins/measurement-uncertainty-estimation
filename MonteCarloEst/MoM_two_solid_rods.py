import numpy as np
from MoM import *


N = 7

param = MoMparam()

param.ep1 = 4.2
param.ep2 = 12.4

param.la  = 0.75

param.ro1 = 0.2
param.ro2 = 0.1

param.xo1 = 0.7
param.xo2 = 0.6

param.zo1 = -0.4
param.zo2 =  0.4


MN = int(input("Enter the mode number: "))

MoMinit()

S11, S12, S21, S22 = MoM_two_solid_rods_(param, MN, N)

#========================================================
print("S11 matrix:")
for row in S11:
        for s11 in row:
                print(s11)

print("\nS12 matrix:")
for row in S12:
        for s12 in row:
                print(s12)

print("\nS21 matrix:")
for row in S21:
        for s21 in row:
                print(s21)

print("\nS22 matrix:")
for row in S22:
        for s22 in row:
                print(s22)

print("\n\n")

