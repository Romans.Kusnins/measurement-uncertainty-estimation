import numpy as np

def S11fun(S11abs):
	c 	= np.asarray([-0.000036651464834, 0.000977646287290, 0.000200722176130, 0.001140840138187])
	pow	= np.asarray([3, 2, 1, 0])
	return  np.sum(c*np.power(S11abs, pow))
