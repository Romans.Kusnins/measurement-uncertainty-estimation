import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *

#==============================================================================
def Measurement_model_eval_S11(model, epv, modeldata, **kwd):

	yo = np.empty((2, epv.shape[-1]))

	NM = kwd.get('NM', 	3)
	N  = kwd.get('N',  	5)
	a  = kwd.get('a',   22.86)

	if model == 'two-slabs-guide':
		ep1 	= modeldata['ep1']
		d1 	= modeldata['d1']
		d2 	= modeldata['d2']
		L 	= modeldata['L']
		f 	= modeldata['f']
		a 	= modeldata['a']
		lam	= 300.0/f
		for n in range(epv.shape[0]):
			yo[0,n] = np.abs(three_slab_guide(a/lam, 1.0, 1.0, epv[n], d1/a, d2/a, L/a))
			yo[1,n] = np.abs(three_slab_guide(a/lam, ep1, 1.0, epv[n], d1/a, d2/a, L/a))
	elif model == 'three-slabs-guide':
		ep1 	= modeldata['ep1']
		epm 	= modeldata['epm']
		d1 	= modeldata['d1']
		d2 	= modeldata['d2']
		L 	= modeldata['L']
		f 	= modeldata['f']
		a 	= modeldata['a']
		lam	= 300.0/f
		for n in range(epv.shape[0]):
			yo[1,n] = np.abs(three_slab_guide(a/lam, ep1, epm, epv[n], d1/a, d2/a, L/a))
			yo[0,n] = np.abs(three_slab_guide(a/lam, 1.0, 1.0, epv[n], d1/a, d2/a, L/a))
	elif model == 'three-slabs-free':
		ep1 	= modeldata['ep1']
		epm 	= modeldata['epm']
		d1 	= modeldata['d1']
		d2 	= modeldata['d2']
		L 	= modeldata['L']
		f 	= modeldata['f']
		lam	= 300.0/f
		for n in range(epv.shape[0]):
			yo[0,n] = np.abs(three_slab_free_space(1.0, 1.0, epv[n], d1/lam, d2/lam, L/lam))
			yo[1,n] = np.abs(three_slab_free_space(ep1, epm, epv[n], d1/lam, d2/lam, L/lam))
	elif model == 'tworods':
		ep1 	= modeldata['ep1']
		ro1 	= modeldata['ro1']
		ro2 	= modeldata['ro2']
		L 	= modeldata['L']
		f 	= modeldata['f']
		a 	= modeldata['a']
		lam	= 300.0/f
		for n in range(epv.shape[0]):
			yo[0,n]	= np.abs(two_solid_rods(ep1, epv[n], ro1, ro2, L, f, a, NM, N))
			yo[1,n]	= np.abs(two_solid_rods(1.0, epv[n], ro1, ro2, L, f, a, NM, N))
	elif model == 'threerods':
		ep1 	= modeldata['ep1']
		ep3 	= modeldata['ep3']
		ro1 	= modeldata['ro1']
		ro2 	= modeldata['ro2']
		ro3 	= modeldata['ro3']
		L1 	= modeldata['L1']
		L2 	= modeldata['L2']
		f 	= modeldata['f']
		a 	= modeldata['a']
		lam	= 300.0/f
		for n in range(epv.shape[0]):
			yo[0,n]	= np.abs(three_solid_rods(ep1, epv[n], ep3, ro1, ro2, ro3, L1, L2, f, a, NM, N))
			yo[1,n]	= np.abs(three_solid_rods(1.0, epv[n], 1.0, ro1, ro2, ro3, L1, L2, f, a, NM, N))
	elif model == 'slab-rod':
		ep1 	= modeldata['ep1']
		d1 	= modeldata['d1']
		ro2 	= modeldata['ro2']
		L 	= modeldata['L']
		f 	= modeldata['f']
		a 	= modeldata['a']
		lam	= 300.0/f
		for n in range(epv.shape[0]):
			yo[0,n]	= np.abs(slab_and_rod(ep1, epv[n], d1, ro2, L, f, a, NM, N))
			yo[1,n]	= np.abs(slab_and_rod(1.0, epv[n], d1, ro2, L, f, a, NM, N))
	elif model == 'rod-slab':
		ep1 	= modeldata['ep1']
		ro1 	= modeldata['ro1']
		d2 	= modeldata['d2']
		L 	= modeldata['L']
		f 	= modeldata['f']
		a 	= modeldata['a']
		lam	= 300.0/f
		for n in range(epv.shape[0]):
			yo[0,n]	= np.abs(rod_and_slab(ep1, epv[n], ro1, d2, L, f, a, NM, N))
			yo[1,n]	= np.abs(rod_and_slab(1.0, epv[n], ro1, d2, L, f, a, NM, N))
	elif model == 'holed-solid':
		ep1 	= modeldata['ep1']
		ro1 	= modeldata['ro1']
		ro1in 	= modeldata['ro1in']
		d1 	= modeldata['d1']
		ph1 	= modeldata['ph1']
		ro2 	= modeldata['ro2']
		L 	= modeldata['L']
		f 	= modeldata['f']
		a 	= modeldata['a']
		lam	= 300.0/f
		for n in range(epv.shape[0]):
			yo[0,n] = np.abs(holed_and_solid_rods(a, ep1, epv[n], ro1, ro1in, d1, ph1, ro2, L, f, NM, N))
			yo[1,n] = np.abs(holed_and_solid_rods(a, 1.0, epv[n], ro1, ro1in, d1, ph1, ro2, L, f, NM, N))
	else:
		ep1 	= modeldata['ep1']
		d1 	= modeldata['d1']
		d2 	= modeldata['d2']
		L 	= modeldata['L']
		f 	= modeldata['f']
		lam	= 300.0/f
		for n in range(epv.shape[0]):
        		yo[0,n] = np.abs(three_slab_free_space(1.0, 1.0, epv[n], d1/lam, d2/lam, L/lam))
        		yo[1,n] = np.abs(three_slab_free_space(ep1, 1.0, epv[n], d1/lam, d2/lam, L/lam))

	return yo
#=====================================================================================
def Measurement_model_confidence_interval(model, modeldata, modeltype, S11_del, **kwd):

	NM = kwd.get('NM', 	3)
	N  = kwd.get('N',  	5)
	a  = kwd.get('a',   22.86)

	if model == 'two-slabs-guide':

		ep1 	= modeldata['ep1']
		ep2 	= modeldata['ep2']
		d1 	= modeldata['d1']
		d2 	= modeldata['d2']
		L 	= modeldata['L']
		f 	= modeldata['f']
		a 	= modeldata['a']
		lam	= 300.0/f

		if modeltype == 'extended':
			S11abs, epl, epc, epu = Confidence_interval_two_slabs_guide(a, ep1, ep2, d1, d2, L, f, S11_del, 1e-4, 100)
		else:
			S11abs, epl, epc, epu = Confidence_interval_two_slabs_guide(a, 1.0, ep2, d1, d2, L, f, S11_del, 1e-4, 100)

	elif model == 'three-slabs-guide':

		ep1 	= modeldata['ep1']
		epm 	= modeldata['epm']
		ep2 	= modeldata['ep2']
		d1 	= modeldata['d1']
		d2 	= modeldata['d2']
		L 	= modeldata['L']
		f 	= modeldata['f']
		a 	= modeldata['a']
		lam	= 300.0/f

		if modeltype == 'extended':
			S11abs, epl, epc, epu = Confidence_interval_three_slabs_guide(a, ep1, epm, ep2, d1, d2, L, f, S11_del, 1e-6, 100)
		else:
			S11abs, epl, epc, epu = Confidence_interval_three_slabs_guide(a, 1.0, 1.0, ep2, d1, d2, L, f, S11_del, 1e-6, 100)

	elif model == 'three-slabs-free':

		ep1 	= modeldata['ep1']
		epm 	= modeldata['epm']
		ep2 	= modeldata['ep2']
		d1 	= modeldata['d1']
		d2 	= modeldata['d2']
		L 	= modeldata['L']
		f 	= modeldata['f']
		lam	= 300.0/f

		if modeltype == 'extended':
			S11abs, epl, epc, epu = Confidence_interval_three_slabs_free(ep1, epm, ep2, d1, d2, L, f, S11_del, 1e-6, 100)
		else:
			S11abs, epl, epc, epu = Confidence_interval_three_slabs_free(1.0, 1.0, ep2, d1, d2, L, f, S11_del, 1e-6, 100)

	elif model == 'tworods':

		ep1 	= modeldata['ep1']
		ep2 	= modeldata['ep2']
		ro1 	= modeldata['ro1']
		ro2 	= modeldata['ro2']
		L 	= modeldata['L']
		f 	= modeldata['f']
		lam	= 300.0/f

		if modeltype == 'extended':
			S11abs, epl, epc, epu = Confidence_interval_two_rods(a, ep1, ep2, ro1, ro2, L, f, S11_del, 1e-6, 100, NM, N)
		else:
			S11abs, epl, epc, epu = Confidence_interval_two_rods(a, 1.0, ep2, ro1, ro2, L, f, S11_del, 1e-6, 100, NM, N)

	elif model == 'threerods':

		ep1 	= modeldata['ep1']
		ep2 	= modeldata['ep2']
		ep3 	= modeldata['ep3']
		ro1 	= modeldata['ro1']
		ro2 	= modeldata['ro2']
		ro3 	= modeldata['ro3']
		L1 	= modeldata['L1']
		L2 	= modeldata['L2']
		f 	= modeldata['f']
		lam	= 300.0/f

		if modeltype == 'extended':
			S11abs, epl, epc, epu = Confidence_interval_three_rods(a, ep1, ep2, ep3, ro1, ro2, ro3, L1, L2, f, S11_del, 1e-6, 100, NM, N)
		else:
			S11abs, epl, epc, epu = Confidence_interval_three_rods(a, 1.0, ep2, 1.0, ro1, ro2, ro3, L1, L2, f, S11_del, 1e-6, 100, NM, N)

	elif model == 'slab-rod':

		ep1 	= modeldata['ep1']
		ep2 	= modeldata['ep2']
		ro1 	= modeldata['ro1']
		d2 	= modeldata['d2']
		L 	= modeldata['L']
		f 	= modeldata['f']
		lam	= 300.0/f

		if modeltype == 'extended':
			S11abs, epl, epc, epu = Confidence_interval_slab_and_rod(a, ep1, ep2, d1, ro2, L, f, S11_del, 1e-6, 100, NM, N)
		else:
			S11abs, epl, epc, epu = Confidence_interval_slab_and_rod(a, 1.0, ep2, d1, ro2, L, f, S11_del, 1e-6, 100, NM, N)

	elif model == 'rod-slab':

		ep1 	= modeldata['ep1']
		ep2 	= modeldata['ep2']
		ro1 	= modeldata['ro1']
		ro2 	= modeldata['ro2']
		L 	= modeldata['L']
		f 	= modeldata['f']
		lam	= 300.0/f

		if modeltype == 'extended':
			S11abs, epl, epc, epu = Confidence_interval_rod_and_slab(a, ep1, ep2, ro1, d2, L, f, S11_del, 1e-6, 100, NM, N)
		else:
			S11abs, epl, epc, epu = Confidence_interval_rod_and_slab(a, 1.0, ep2, ro1, d2, L, f, S11_del, 1e-6, 100, NM, N)

	elif model == 'holed-solid':

		ep1 	= modeldata['ep1']
		ep2 	= modeldata['ep2']
		ro1 	= modeldata['ro1']
		ro1in 	= modeldata['ro1in']
		d1 	= modeldata['d1']
		ph1 	= modeldata['ph1']
		ro2 	= modeldata['ro2']
		L 	= modeldata['L']
		f 	= modeldata['f']
		a 	= modeldata['a']
		lam	= 300.0/f

		if modeltype == 'extended':
			S11abs, epl, epc, epu = Confidence_interval_holed_and_solid_rods(a, ep1, ep2, ro1, ro1in, d1, ph1, ro2, L, f, S11_del, 1e-6, 100, NM, N)
		else:
			S11abs, epl, epc, epu = Confidence_interval_holed_and_solid_rods(a, 1.0, ep2, ro1, ro1in, d1, ph1, ro2, L, f, S11_del, 1e-6, 100, NM, N)
	else:

		ep1 	= modeldata['ep1']
		ep2 	= modeldata['ep2']
		d1 	= modeldata['d1']
		d2 	= modeldata['d2']
		L 	= modeldata['L']
		f 	= modeldata['f']
		lam	= 300.0/f

		if modeltype == 'extended':
			S11abs, epl, epc, epu = Confidence_interval_two_slabs_free(ep1, ep2, d1, d2, L, f, S11_del, 1e-6, 100)
		else:
			S11abs, epl, epc, epu = Confidence_interval_two_slabs_free(1.0, ep2, d1, d2, L, f, S11_del, 1e-6, 100)

	return S11abs, epl, epc, epu
