import os
import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *
from Derivatives import *

try:
        matplot = True
        from matplotlib import pyplot as plt
except ModuleNotFoundError:
        print("The matplotlib module is not installed.")
        matplot = False


from plot2pdf import *

from Uncertainty_vs_S11 import *
#==================================================================================
xMin 		= 0.0
xMax 		= 100.0
MaxVal 		= 10.0
#==================================================================================
isPDF 		= True
#isPRINT	= False
#==================================================================================

S11abs 		= np.arange(0, 1.0, 0.01)
Uncert_S11	= np.empty(S11abs.shape)

for n in range(S11abs.shape[0]):
	Uncert_S11[n] 	= S11fun(S11abs[n])

#==================================================================================
# PLOTTING CALCULATED DATA
#==================================================================================

xlabel		= '$|S_{11}|$'
ylabel		= '$u_{|S_{11}|}$'

if isPDF:

	labels = {'xlabel': xlabel, 'ylabel': ylabel}

	filename = 'Uncert_S11abs'

	LegendPos = [0.1, 0.5]

#	plot2latex(S11abs, Uncert_S11, xDecNum=1, LineWidth=0.75, labels=labels, LegendPosition=legendpos, FileName=filename)
	plot2latex(S11abs, Uncert_S11, xDecNum=1, LineWidth=0.75, labels=labels, LegendPosition=LegendPos, FileName=filename, YZero=True)

elif matplot:
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)

	plt.plot(S11abs, Uncert_S11)

	plt.grid()
	plt.show()
else:
	pass
#       print('Calculated data have been saved into a file.')

print('--------------------------------------------------------')
