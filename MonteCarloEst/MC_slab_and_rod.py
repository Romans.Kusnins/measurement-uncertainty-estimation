import numpy as np
import sys

if len(sys.argv) == 2 and sys.argv[1] == 'total':
	tasktype = 'total'
elif len(sys.argv) == 3 and sys.argv[1] == 'sens':
	tasktype = 'sens'
	senparam = sys.argv[2]
else:
	tasktype = 'total'      # estimate the total uncertainty (default option)
	#tasktype = 'sens'      # estimate the sensitivity coefficient
	senparam = 'S11abs'     # specify the model parameter to find the sensitivity to

try:
	matplot = True
	from matplotlib import pyplot as plt
except ModuleNotFoundError:
	print("The matplotlib module is not installed.")
	matplot = False

from slab_res3md import (root_finding, POW2)
from slab_res3md import (three_slab_guide, three_slab_free_space, root_finding, POW2)
from slab_res3md import (single_slab_guide_matrix)
from plot2pdf import plot2latex
from MoM import *
from MonteCarlo_estimation import *
from Uncertainty_vs_S11 import *
#==================================================================================
#==================================================================================
#tasktype = 'total' 	# estimate the total uncertainty (default option)
tasktype = 'sens' 	# estimate the sensitivity coefficient

#senparam = 'S11abs'	# specify the model parameter to find the sensitivity to
#senparam = 'd1'		# specify the model parameter to find the sensitivity to
#senparam = 'ro2'	# specify the model parameter to find the sensitivity to
#senparam = 'L'		# specify the model parameter to find the sensitivity to
#senparam = 'a'		# specify the model parameter to find the sensitivity to
#senparam = 'ep1'	# specify the model parameter to find the sensitivity to
#senparam = 'tan1'	# specify the model parameter to find the sensitivity to
#senparam = 'tan2'	# specify the model parameter to find the sensitivity to
#senparam = 'ep1'	# specify the model parameter to find the sensitivity to
senparam = 'f'		# specify the model parameter to find the sensitivity to
#==================================================================================
M	= 1000		# number of Monte-Carlo trials
MAXIT	= 20		# maximum ieteration number of Newton's method
#==================================================================================
isPDF 	= False
isPRINT	= True
#==================================================================================
modeldata	= dict()
sigma	 	= dict()

modelname 	= 'slab_and_rod'
#==================================================================================
# MODEL PARAMETERS
#==================================================================================
modeldata['ep1']        = 4.3
modeldata['tan1']       = 0.01

modeldata['ep2'] 	= [24.0]
#modeldata['ep2'] = [1.0, 20.0, 0.1]
modeldata['tan2'] 	= 0.0001

modeldata['d1']         = 9.25
modeldata['ro2']        = 3.3
modeldata['xo2']        = 0.0

modeldata['L']          = 22.0
#modeldata['L']          = [10.0, 30.0, 0.1]
modeldata['f']          = 10.0

modeldata['a']          = 22.86


sigma['S11abs']         = 0.01
sigma['d1']             = 0.015
sigma['ro2']            = 0.015
sigma['xo2']            = 0.015
sigma['L']              = 0.025
sigma['a']              = 0.015

sigma['f']              = 0.001

sigma['ep1']            = modeldata['ep1']*0.01
sigma['tan1']           = modeldata['tan1']*0.05

sigma['tan2']           = modeldata['tan2']*0.05
#================================================================
list_found = False
for param in modeldata.keys():
	if type(modeldata[param]) is list:
		if len(modeldata[param]) > 1 and not list_found:
			var_range = modeldata[param]
			var_param = param
			list_found = True
		else:
			modeldata[param] = modeldata[param][0]

if list_found:
        var_values      = np.arange(*var_range)
else:
        var_param       = 'ep1'
        var_values      = np.asarray([modeldata['ep1']])

NUM             = var_values.shape[0]

mean  		= np.empty(var_values.shape)
total_delta 	= np.empty(var_values.shape)

for n in range(NUM):

	modeldata[var_param] 	= var_values[n]

	sigma_mc = sigma.copy()
	if tasktype == 'sens':
		for param in sigma_mc.keys():
			if param == senparam:
				sigma_mc[param] = sigma[param]
			else:
				sigma_mc[param] = 0.0

	epv, failed = MC_slab_and_rod(modeldata, sigma_mc, M, MAXIT)

	print(epv.shape)

	if failed:
		mean[n]  	= np.nan
		total_delta[n] 	= np.nan
	else:
		epv 		= epv[np.logical_not(np.isnan(epv))]
		mean[n]  	= np.mean(epv)
		total_delta[n] 	= np.sqrt(np.var(epv))
#================================================================
# SAVE THE OBTAINED DATA TO FILE
#================================================================
if tasktype == 'sens':
	filename = modelname + '_var_' + var_param + '_sens_' + senparam + '_MCnum_' + '{:}'.format(M)
else:
	filename = modelname + '_var_' + var_param + '_total_MCnum_' + '{:}'.format(M)

np.save(filename, np.vstack([var_values, total_delta]))
#================================================================
# PLOT THE OBTAINED DATA
#================================================================
ylabeltotal 	= 'Standard uncertainty associated with'
ylabelsens      = 'Model sensitivity to'
paramname       = {'S11abs': '$S_{11}$', 'd1': '$d_{1}$', 'ro2': '$r_{2}$', 'xo2': '$x_{\\mathrm{o,2}}$', 'L': '$L$', 'ep1': '$\\varepsilon_{\\mathrm{r}}$', 'tan1': '$ \\tan{\\delta}$', 'tan2': '$\\tan{\\delta}$', 'a': 'a', 'f': 'f'}

xlabels = dict()

xlabels['ep1']  = 'Dielectric constant of the auxiliary slab'
xlabels['ep2']  = 'Dielectric constant of MUT'
xlabels['d1']   = 'Thickness of the auxiliary slab'
xlabels['ro2']  = 'Radius of the MUT rod'
xlabels['L']    = 'Separation between the auxiliary slab and MUT rod'
#================================================================
if isPDF:
	if tasktype == 'sens':
		plot2latex(var_values, total_delta, labels={'xlabel': xlabels[var_param], 'ylabel': ylabelsens + ' ' + paramname[senparam]}, ylabel_vert=True)
	else:
		plot2latex(var_values, total_delta, labels={'xlabel': xlabels[var_param], 'ylabel': 'Total uncertainty'}, ylabel_vert=True)
elif isPRINT:
	print(total_delta)
elif matplot:
	plt.plot(var_values, total_delta)
	plt.xlabel(xlabels[var_param])
	if tasktype == 'sens':
		plt.ylabel('Sensitivity to ' + paramname[senparam])
	else:
		plt.ylabel('Total uncertainy')

	plt.grid()
	plt.show()
else:
        print('Calculated data have been saved into a file.')

