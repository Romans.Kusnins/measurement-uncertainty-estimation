import numpy as np

try:
	matplot = True
	from matplotlib import pyplot as plt
except ModuleNotFoundError:
	print("The matplotlib module is not installed.")
	matplot = False

from slab_res3md import (root_finding, POW2)
from slab_res3md import (three_slab_guide, three_slab_free_space, root_finding, POW2)
from slab_res3md import (single_slab_guide_matrix)

from MoM import *
#==================================================================================
#==================================================================================
def MC_two_slabs_guide(modeldata, sigma, M=1000, MAXIT=100, NTOL=1e-4, **kwd):
	#--------------------------------------------------------------------------
	ep1 	= modeldata['ep1']	# auxiliary dielectric constant
	tan1 	= modeldata['tan1']	# auxiliary loss tangent

	ep2 	= modeldata['ep2'] 	# MUT slab dielectric constant
	tan2 	= modeldata['tan2']	# MUT loss tangent

	d1 	= modeldata['d1'] 	# measurable slab thickness in mm
	d2 	= modeldata['d2'] 	# auxiliary slab thickness in mm
	L 	= modeldata['L']  	# interslab separation distance in mm
	f 	= modeldata['f']  	# operating frequency in GHz
	a 	= modeldata['a']  	# waveguide width in mm
	#==========================================================================
	sigma_S11abs 	= sigma.get('S11abs', 	0.01)
	sigma_d1 	= sigma.get('d1', 	0.0)
	sigma_d2	= sigma.get('d2', 	0.0)
	sigma_L 	= sigma.get('L',	0.0)
	sigma_ep1 	= sigma.get('ep1',      0.0)
	sigma_tan1 	= sigma.get('tan1',     0.0)
	sigma_tan2 	= sigma.get('tan2',     0.0)
	sigma_a 	= sigma.get('a', 	0.0)
	sigma_f 	= sigma.get('f', 	0.0)
	#--------------------------------------------------------------------------
	lam = 300.0/f
	#--------------------------------------------------------------------------
	S11abs 		= np.abs(three_slab_guide(a/lam, ep1*(1.0+1.0j*tan1), 1.0, ep2*(1.0+1.0j*tan2), d1/a, d2/a, L/a))
	S11abs_p 	= np.abs(three_slab_guide(a/lam, ep1*(1.0+1.0j*tan1), 1.0, (ep2+1e-3)*(1.0+1.0j*tan2), d1/a, d2/a, L/a))
	S11_der 	= (S11abs_p - S11abs)/1e-3

	if callable(sigma_S11abs):
		sigma_S11abs = sigma_S11abs(S11abs)
	#==========================================================================
	def func(epvar, **kwd):
		lam = 300.0/del_f
		S11 = three_slab_guide(del_a/lam, del_ep1*(1.0+1.0j*del_tan1), 1.0, epvar*(1.0+1.0j*del_tan2), del_d1/del_a, del_d2/del_a, del_L/del_a)
		return np.abs(S11)
	#==========================================================================
	epv = np.ndarray((M,))
	#==========================================================================
	nan_count 	= 0
	MAX_NAN_NUM 	= np.round(M*kwd.get('nan_ratio', 0.1))
	failed 		= False
	for n in range(M):

		del_S11abs 	= np.random.normal(S11abs, 	sigma_S11abs)
		del_d1 		= np.random.normal(d1,		sigma_d1)
		del_d2	 	= np.random.normal(d2, 		sigma_d2)
		del_L	 	= np.random.normal(L,  		sigma_L)
		del_ep1	 	= np.random.normal(ep1,		sigma_ep1)
		del_tan1	= np.random.normal(tan1,	sigma_tan1)
		del_tan2	= np.random.normal(tan2,	sigma_tan2)
		del_a		= np.random.normal(a,		sigma_a)
		del_f		= np.random.normal(f,		sigma_f)

		epv[n] = root_finding(func, ep2, 1e-6, NTOL, MAXIT, val=del_S11abs, deriv=S11_der)

		if np.isnan(epv[n]):
			nan_count += 1
		if nan_count == MAX_NAN_NUM:
			failed = True
			break
	return epv, failed
#==================================================================================
def MC_three_slabs_guide(modeldata, sigma, M=1000, MAXIT=100, NTOL=1e-4, **kwd):
	#--------------------------------------------------------------------------
	ep1 	= modeldata['ep1']	# auxiliary dielectric constant
	tan1 	= modeldata['tan1']	# auxiliary dielectric loss tangent

	ep2 	= modeldata['ep2'] 	# MUT slab dielectric constant
	tan2 	= modeldata['tan2']	# auxiliary dielectric loss tangent

	epm 	= modeldata['epm'] 	# middle slab dielectric constant
	tanm 	= modeldata['tanm']	# middle slab dielectric loss tangent

	d1 	= modeldata['d1'] 	# measurable slab thickness in mm
	d2 	= modeldata['d2'] 	# auxiliary slab thickness in mm
	L 	= modeldata['L']  	# interslab separation distance in mm
	f 	= modeldata['f']  	# operating frequency in GHz
	a 	= modeldata['a']  	# waveguide width in mm
	#==========================================================================
	sigma_S11abs 	= sigma.get('S11abs', 	0.01)
	sigma_d1 	= sigma.get('d1', 	0.001)
	sigma_d2	= sigma.get('d2', 	0.001)
	sigma_L 	= sigma.get('L',	0.001)
	sigma_ep1 	= sigma.get('ep1',      0.0  )
	sigma_tan1 	= sigma.get('tan1',     0.0  )
	sigma_tan2 	= sigma.get('tan2',     0.0  )
	sigma_epm 	= sigma.get('epm',      0.0  )
	sigma_tanm 	= sigma.get('tanm',     0.0  )
	sigma_a 	= sigma.get('a',	0.0  )
	sigma_f 	= sigma.get('f',	0.0  )
	#--------------------------------------------------------------------------
	lam = 300.0/f
	#--------------------------------------------------------------------------
	S11abs 		= np.abs(three_slab_guide(a/lam, ep1*(1.0+1.0j*tan1), epm*(1.0+1.0j*tanm), ep2*(1.0+1.0j*tan2), d1/a, d2/a, L/a))
	S11abs_p 	= np.abs(three_slab_guide(a/lam, ep1*(1.0+1.0j*tan1), epm*(1.0+1.0j*tanm), (ep2+1e-3)*(1.0+1.0j*tan2), d1/a, d2/a, L/a))
	S11_der 	= (S11abs_p - S11abs)/1e-3

	if callable(sigma_S11abs):
		sigma_S11abs = sigma_S11abs(S11abs)
	#==========================================================================
	def func(epvar, **kwd):
		lam = 300.0/del_f
		S11 = three_slab_guide(del_a/lam, del_ep1*(1.0+1.0j*del_tan1), del_epm*(1.0+1.0j*del_tanm), epvar*(1.0+1.0j*del_tan2), del_d1/del_a, del_d2/del_a, del_L/del_a)
		return np.abs(S11)
	#==========================================================================
	epv = np.ndarray((M,))
	#==========================================================================
	nan_count 	= 0
	MAX_NAN_NUM 	= np.round(M*kwd.get('nan_ratio', 0.1))
	failed 		= False
	for n in range(M):
		del_S11abs 	= np.random.normal(S11abs,	sigma_S11abs)
		del_d1 		= np.random.normal(d1,		sigma_d1)
		del_d2	 	= np.random.normal(d2, 		sigma_d2)
		del_L	 	= np.random.normal(L,  		sigma_L)
		del_ep1	 	= np.random.normal(ep1,		sigma_ep1)
		del_tan1	= np.random.normal(tan1,	sigma_tan1)
		del_tan2	= np.random.normal(tan2,	sigma_tan2)
		del_epm	 	= np.random.normal(epm,		sigma_epm)
		del_tanm	= np.random.normal(tanm,	sigma_tanm)
		del_a		= np.random.normal(a,		sigma_a)
		del_f		= np.random.normal(f,		sigma_f)

		epv[n] = root_finding(func, ep2, 1e-6, NTOL, MAXIT, val=del_S11abs, deriv=S11_der)

		if np.isnan(epv[n]):
			nan_count += 1
		if nan_count == MAX_NAN_NUM:
			failed = True
			break
	return epv, failed
#==================================================================================
def MC_two_slabs_free(modeldata, sigma, M=1000, MAXIT=100, NTOL=1e-4, **kwd):
	#--------------------------------------------------------------------------
	ep1 	= modeldata['ep1']	# auxliliary dielectric constant
	tan1 	= modeldata['tan1']	# auxliliary dielectric loss tangent

	ep2 	= modeldata['ep2'] 	# MUT slabs dielectric constant
	tan2 	= modeldata['tan2']	# MUT dielectric loss tangent

	d1 	= modeldata['d1'] 	# measurable slab thickness in mm
	d2 	= modeldata['d2'] 	# auxiliary slab thickness in mm
	L 	= modeldata['L']  	# interslab separation distance in mm
	f 	= modeldata['f']  	# operating frequency in GHz
	#==========================================================================
	sigma_S11abs 	= sigma.get('S11abs', 	0.01 )
	sigma_d1 	= sigma.get('d1', 	0.001)
	sigma_d2 	= sigma.get('d2', 	0.001)
	sigma_L 	= sigma.get('L', 	0.001)
	sigma_f 	= sigma.get('f', 	0.00)
	sigma_ep1 	= sigma.get('ep1',	0.0  )
	sigma_tan1 	= sigma.get('tan1', 	0.0  )
	sigma_tan2 	= sigma.get('tan2', 	0.0  )
	#--------------------------------------------------------------------------
	lam = 300.0/f
	#--------------------------------------------------------------------------
	S11abs 		= np.abs(three_slab_free_space(ep1*(1.0+1.0j*tan1), 1.0,      ep2*(1.0+1.0j*tan2), d1/lam, d2/lam, L/lam))
	S11abs_p 	= np.abs(three_slab_free_space(ep1*(1.0+1.0j*tan1), 1.0, (ep2+1e-3)*(1.0+1.0j*tan2), d1/lam, d2/lam, L/lam))
	S11_der 	= (S11abs_p - S11abs)/1e-3

	if callable(sigma_S11abs):
		sigma_S11abs = sigma_S11abs(S11abs)
	#==========================================================================
	def func(epvar, **kwd):
		lam = 300.0/del_f
		S11 = three_slab_free_space(del_ep1*(1.0+1.0j*del_tan1), 1.0, epvar*(1.0+1.0j*del_tan2), del_d1/lam, del_d2/lam, del_L/lam)
		return np.abs(S11)
	#==========================================================================
	epv = np.ndarray((M,))
	#==========================================================================
	nan_count 	= 0
	MAX_NAN_NUM 	= np.round(M*kwd.get('nan_ratio', 0.1))
	failed 		= False
	for n in range(M):
		del_S11abs 	= np.random.normal(S11abs,	sigma_S11abs)

		del_d1 		= np.random.normal(d1,		sigma_d1)
		del_d2	 	= np.random.normal(d2, 		sigma_d2)
		del_L	 	= np.random.normal(L,  		sigma_L)

		del_ep1	 	= np.random.normal(ep1,		sigma_ep1)
		del_tan1	= np.random.normal(tan1,	sigma_tan1)

		del_tan2	= np.random.normal(tan2,	sigma_tan2)

		del_f		= np.random.normal(f,		sigma_f)


		epv[n] = root_finding(func, ep2, 1e-6, NTOL, MAXIT, val=del_S11abs, deriv=S11_der)

		if np.isnan(epv[n]):
			nan_count += 1
		if nan_count == MAX_NAN_NUM:
			failed = True
			break

	return epv, failed
#==================================================================================
def MC_three_slabs_free(modeldata, sigma, M=1000, MAXIT=100, NTOL=1e-4, **kwd):
	#--------------------------------------------------------------------------
	ep1 	= modeldata['ep1']	# auxiliary dielectric constant
	tan1 	= modeldata['tan1']	# auxiliary dielectric loss tangent

	ep2 	= modeldata['ep2'] 	# MUT slab dielectric constant
	tan2 	= modeldata['tan2']	# MUT dielectric loss tangent

	epm 	= modeldata['epm'] 	# middle slab dielectric constant
	tanm 	= modeldata['tanm']	# middle slab dielectric loss tangent

	d1 	= modeldata['d1'] 	# measurable slab thickness in mm
	d2 	= modeldata['d2'] 	# auxiliary slab thickness in mm
	L 	= modeldata['L']  	# interslab separation distance in mm
	f 	= modeldata['f']  	# operating frequency in GHz
	#==========================================================================
	sigma_S11abs 	= sigma.get('S11abs', 	0.01)
	sigma_d1 	= sigma.get('d1', 	0.001)
	sigma_d2	= sigma.get('d2', 	0.001)
	sigma_L 	= sigma.get('L',	0.001)
	sigma_f 	= sigma.get('f',	0.00)
	sigma_ep1 	= sigma.get('ep1',      0.0  )
	sigma_tan1 	= sigma.get('tan1',     0.0  )
	sigma_tan2 	= sigma.get('tan2',     0.0  )
	sigma_epm 	= sigma.get('epm',      0.0  )
	sigma_tanm 	= sigma.get('tanm',     0.0  )
	#--------------------------------------------------------------------------
	lam = 300.0/f
	#--------------------------------------------------------------------------
	S11abs 		= np.abs(three_slab_free_space(ep1*(1.0+1.0j*tan1), epm*(1.0+1.0j*tanm),     ep2*(1.0+1.0j*tan2), d1/lam, d2/lam, L/lam))
	S11abs_p 	= np.abs(three_slab_free_space(ep1*(1.0+1.0j*tan1), epm*(1.0+1.0j*tanm), (ep2+1e-3)*(1.0+1.0j*tan2), d1/lam, d2/lam, L/lam))
	S11_der 	= (S11abs_p - S11abs)/1e-3

	if callable(sigma_S11abs):
		sigma_S11abs = sigma_S11abs(S11abs)
	#==========================================================================
	def func(epvar, **kwd):
		lam = 300.0/del_f
		S11 = three_slab_free_space(del_ep1*(1.0+1.0j*del_tan1), del_epm*(1.0+1.0j*del_tanm), epvar*(1.0+1.0j*del_tan2), del_d1/lam, del_d2/lam, del_L/lam)
		return np.abs(S11)
	#==========================================================================
	epv = np.ndarray((M,))
	#==========================================================================
	nan_count 	= 0
	MAX_NAN_NUM 	= np.round(M*kwd.get('nan_ratio', 0.1))
	failed 		= False
	for n in range(M):

		del_S11abs 	= np.random.normal(S11abs, 	sigma_S11abs)

		del_d1 		= np.random.normal(d1,		sigma_d1)
		del_d2	 	= np.random.normal(d2, 		sigma_d2)
		del_L	 	= np.random.normal(L,  		sigma_L)

		del_ep1	 	= np.random.normal(ep1,		sigma_ep1)
		del_tan1	= np.random.normal(tan1,	sigma_tan1)

		del_tan2	= np.random.normal(tan2,	sigma_tan2)

		del_epm	 	= np.random.normal(epm,		sigma_epm)
		del_tanm	= np.random.normal(tanm,	sigma_tanm)

		del_f		= np.random.normal(f,		sigma_f)


		epv[n] = root_finding(func, ep2, 1e-6, NTOL, MAXIT, val=del_S11abs, deriv=S11_der)

		if np.isnan(epv[n]):
			nan_count += 1
		if nan_count == MAX_NAN_NUM:
			failed = True
			break

	return epv, failed
#==================================================================================
def MC_two_rods(modeldata, sigma, M=1000, MAXIT=100, NTOL=1e-4, NM=3, N=5, **kwd):
	#==========================================================================
	ep1 	= modeldata['ep1']	# auxiliary rod dielectric constant
	tan1 	= modeldata['tan1']	# auxiliary rod dielectric loss tangent

	ep2 	= modeldata['ep2']	# MUT rod dielectric constant
	tan2 	= modeldata['tan2']	# MUT rod dielectric loss tangent

	ro1 	= modeldata['ro1']	# auxiliary rod radius in mm
	xo1 	= modeldata['xo1']	# auxiliary rod offset in mm

	ro2 	= modeldata['ro2']	# MUT rod radius in mm
	xo2 	= modeldata['xo2']	# MUT rod offset in mm

	L 	= modeldata['L']	# interrod separation distance in mm
	f 	= modeldata['f']	# operating frequency in GHz
	a	= modeldata['a']	# waveguide width in mm
	#==========================================================================
	sigma_S11abs 	= sigma.get('S11abs', 	0.01)

	sigma_ro1 	= sigma.get('ro1', 	0.001)
	sigma_ro2	= sigma.get('ro2', 	0.001)

	sigma_xo1 	= sigma.get('xo1', 	0.00)
	sigma_xo2	= sigma.get('xo2', 	0.00)

	sigma_L 	= sigma.get('L',	0.001)

	sigma_ep1 	= sigma.get('ep1',      0.0  )
	sigma_tan1 	= sigma.get('tan1',     0.0  )

	sigma_tan2 	= sigma.get('tan2',     0.0  )

	sigma_a 	= sigma.get('a', 	0.0  )
	sigma_f 	= sigma.get('f', 	0.0  )
	#==========================================================================
	# M   	- Monte-Carlo algorithm trial number
	# MAXIT - max number of iterations for Newton's method
	# NTOL 	- solution tolerance for Newton's method

	# NM	- number of higher order waveguide modes considered
	# N	- number of basis functions for each rod surface
	#==========================================================================
	MoMinit()		# perform C++ library initialization

	lam = 300.0/f
	#==========================================================================
	def evalparam(ep, ro, xo):
		param 		= MoMparam()

		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5 + xo/a
		param.zo 	= 0.0

		return param
	#==========================================================================
	def MoM_calc_S_matrix(ep, ro, xo):
		param = evalparam(ep, ro, xo)
		S11a, S12a, S21a, S22a = MoM_single_solid_rod_(param, NM, N)
		return S11a, S12a, S21a, S22a
	#==========================================================================
	def calcS11(S11b):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a
		return S11[0,0]
	#==========================================================================
	def calcT(L):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#==========================================================================
	#==========================================================================
	# calculate S11meas - the total reflection coefficient
	#==========================================================================
	S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1*(1.0+1.0j*tan1), ro1, xo1)
	T = calcT(L)
	S11b, S12b, S21b, S22b 	= MoM_calc_S_matrix(ep2*(1.0+1.0j*tan2), ro2, xo2)
	S11abs			= np.abs(calcS11(S11b))
	#==========================================================================
	# calculate the derivative of S11meas
	#==========================================================================
	S11b, S12b, S21b, S22b 	= MoM_calc_S_matrix((ep2+1e-3)*(1.0+1.0j*tan2), ro2, xo2)
	S11abs_p 		= np.abs(calcS11(S11b))
	S11abs_der 		= (S11abs_p - S11abs)/1e-3

	if callable(sigma_S11abs):
		sigma_S11abs = sigma_S11abs(S11abs)
	#--------------------------------------------------------------------------
	# EVALUATING MEASUREMENT UNCERTAINTY
	#--------------------------------------------------------------------------
	epm 		= np.ndarray((M,))
	#==========================================================================
	nan_count 	= 0
	MAX_NAN_NUM 	= np.round(M*kwd.get('nan_ratio', 0.1))
	failed 		= False
	def func(epvar, **kwd):
		param.ep 		= epvar*(1.0+1.0j*del_tan2)
		S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
		return  np.abs(calcS11(S11b))
	#==========================================================================
	a_actual = a
	f_actual = f

	for n in range(M):

		del_S11abs 		= np.random.normal(S11abs, 	sigma_S11abs)

		del_ro1			= np.random.normal(ro1,		sigma_ro1)
		del_ro2	 		= np.random.normal(ro2,		sigma_ro2)

		del_xo1			= np.random.normal(xo1,		sigma_xo1)
		del_xo2	 		= np.random.normal(xo2,		sigma_xo2)

		del_L	 		= np.random.normal(L,		sigma_L)
		del_a	 		= np.random.normal(a_actual,	sigma_a)
		del_f	 		= np.random.normal(f_actual,	sigma_f)

		del_ep1	 		= np.random.normal(ep1,		sigma_ep1)
		del_tan1		= np.random.normal(tan1,	sigma_tan1)

		del_tan2		= np.random.normal(tan2,	sigma_tan2)
		#=============================================================================
		a 	= del_a
		f 	= del_f
		lam 	= 300.0/f

		S11a, S12a, S21a, S22a  = MoM_calc_S_matrix(del_ep1*(1.0+1.0j*del_tan1), del_ro1, del_xo1)

		T 			= calcT(del_L)
		param 			= evalparam(ep2, del_ro2, del_xo2)

		epm[n] 			= root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11abs_der, val=del_S11abs)

		if np.isnan(epm[n]):
			nan_count += 1
		if nan_count == MAX_NAN_NUM:
			failed = True
			break

	return epm, failed
#==========================================================================
def MC_slab_and_rod(modeldata, sigma, M=1000, MAXIT=100, NTOL=1e-4, NM=3, N=5, **kwd):
	#===========================================================================
	#===========================================================================
	ep1 	= modeldata['ep1'] 	# auxiliary slab dielectric constant
	tan1 	= modeldata['tan1'] 	# auxiliary slab loss tangent

	ep2 	= modeldata['ep2'] 	# MUT rod dielectric constant
	tan2 	= modeldata['tan2'] 	# MUT slab loss tangent

	d1 	= modeldata['d1']	# auxiliary slab thickness in mm
	ro2 	= modeldata['ro2']	# MUT rod radius in mm
	xo2 	= modeldata['xo2']	# MUT rod offset in mm

	L 	= modeldata['L']  	# slab-to-rod separation in mm

	f 	= modeldata['f']   	# operating frequency in GHz
	a 	= modeldata['a']   	# waveguide width in mm
	#==========================================================================
	sigma_S11abs 	= sigma.get('S11abs', 	0.01)

	sigma_d1 	= sigma.get('d1', 	0.00)
	sigma_ro2	= sigma.get('ro2', 	0.00)
	sigma_xo2	= sigma.get('xo2', 	0.00)

	sigma_L 	= sigma.get('L',	0.00)
	sigma_a 	= sigma.get('a',	0.00)
	sigma_f 	= sigma.get('f',	0.00)

	sigma_ep1 	= sigma.get('ep1',      0.0  )
	sigma_tan1 	= sigma.get('tan1',     0.0  )

	sigma_tan2 	= sigma.get('tan2',     0.0  )
	#===========================================================================
	MoMinit()

	lam = 300.0/f
	#===========================================================================
	def evalparam(ep, ro, xo):
		param 		= MoMparam()
		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5 + xo/a
		param.zo 	= 0.0
		return param
	#===========================================================================
	def MoM_calc_S_matrix(ep, ro, xo):

		param = evalparam(ep, ro, xo)
		S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
		if NM == 1:
			return S11[0,0], S12[0,0], S21[0,0], S22[0,0]
		else:
			return S11, S12, S21, S22
	#===========================================================================
	def calcS11(S11b):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
			return S11
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#===========================================================================
	def calcT(L):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#===========================================================================
	S11a, S12a, S21a, S22a 	= single_slab_guide_matrix(a/lam, ep1*(1.0+1.0j*tan1), d1/a, NM)
	T 			= calcT(L)
	S11b, S12b, S21b, S22b 	= MoM_calc_S_matrix(ep2*(1.0+1.0j*tan2), ro2, xo2)
	S11abs 			= np.abs(calcS11(S11b))
	#===========================================================================
	# calculate the derivative of S11abs with respect to epsilon
	#===========================================================================
	S11b, S12b, S21b, S22b 	= MoM_calc_S_matrix((ep2+1e-3)*(1.0+1.0j*tan2), ro2, xo2)
	S11abs_p 		= np.abs(calcS11(S11b))

	S11_der 		= (S11abs_p - S11abs)/1e-3

	if callable(sigma_S11abs):
		sigma_S11abs = sigma_S11abs(S11abs)
	#---------------------------------------------------------------------------
	# EVALUATE MEASUREMENT UNCERTAINTY
	#---------------------------------------------------------------------------
	epm 		= np.ndarray((M,))
	#===========================================================================
	nan_count 	= 0
	MAX_NAN_NUM 	= np.round(M*kwd.get('nan_ratio', 0.1))
	failed 		= False
	def func(epvar, **kwd):
		param.ep 		= epvar*(1.0+1.0j*del_tan2)
		S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
		if NM == 1:
			S11b = S11b[0,0]
			S12b = S12b[0,0]
			S21b = S21b[0,0]
			S22b = S22b[0,0]

		return  np.abs(calcS11(S11b))
	#===========================================================================
	a_actual = a
	f_actual = f

	for n in range(M):

		del_S11abs 		= np.random.normal(S11abs,	sigma_S11abs)

		del_d1			= np.random.normal(d1,		sigma_d1)
		del_ro2	 		= np.random.normal(ro2,		sigma_ro2)
		del_xo2	 		= np.random.normal(xo2,		sigma_xo2)

		del_L	 		= np.random.normal(L,		sigma_L)
		del_a	 		= np.random.normal(a_actual,	sigma_a)
		del_f	 		= np.random.normal(f_actual,	sigma_f)

		del_ep1	 		= np.random.normal(ep1,		sigma_ep1)
		del_tan1		= np.random.normal(tan1,	sigma_tan1)

		del_tan2		= np.random.normal(tan2,	sigma_tan2)
		#=========================================================================
		a 	= del_a
		f 	= del_f
		lam 	= 300.0/f

		S11a, S12a, S21a, S22a  = single_slab_guide_matrix(a/lam, del_ep1*(1.0+1.0j*del_tan1), del_d1/a, NM)
		T 			= calcT(del_L)
		param 			= evalparam(ep2, del_ro2, del_xo2)


		epm[n] 			= root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=del_S11abs)

		if np.isnan(epm[n]):
			nan_count += 1
		if nan_count == MAX_NAN_NUM:
			failed = True
			break

	return epm, failed
#===========================================================================
def MC_rod_and_slab(modeldata, sigma, M=1000, MAXIT=100, NTOL=1e-4, NM=3, N=5, **kwd):
	#===========================================================================
	#===========================================================================
	ep1 	= modeldata['ep1']	# auxiliary slab dielectric constant
	tan1 	= modeldata['tan1'] 	# auxiliary slab loss tangent

	ep2 	= modeldata['ep2'] 	# MUT slabs dielectric constant
	tan2 	= modeldata['tan2'] 	# MUT slab loss tangent

	ro1 	= modeldata['ro1']	# auxiliary rod radius in mm
	xo1 	= modeldata['xo1']	# auxiliary rod offset in mm
	d2 	= modeldata['d2']  	# MUT slab thickness in mm

	L 	= modeldata['L']   	# rod-to-slab separation in mm
	f 	= modeldata['f']   	# operating frequency in GHz

	a 	= modeldata['a']   	# waveguide width in mm
	#===========================================================================
	sigma_S11abs 	= sigma.get('S11abs', 	0.01)
	sigma_ro1 	= sigma.get('ro1', 	0.00)
	sigma_xo1 	= sigma.get('xo1', 	0.00)
	sigma_d2	= sigma.get('d2', 	0.00)
	sigma_L 	= sigma.get('L',	0.00)
	sigma_a 	= sigma.get('a',	0.00)
	sigma_f 	= sigma.get('f',	0.00)

	sigma_ep1 	= sigma.get('ep1',      0.0  )
	sigma_tan1 	= sigma.get('tan1',     0.0  )

	sigma_tan2 	= sigma.get('tan2',     0.0  )
	#===========================================================================
	MoMinit()

	lam = 300.0/f
	#===========================================================================
	def evalparam(ep, ro, xo):
		param 		= MoMparam()
		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5 + xo/a
		param.zo 	= 0.0
		return param
	#===========================================================================
	def MoM_calc_S_matrix(ep, ro, xo):
		param = evalparam(ep, ro, xo)
		S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
		if NM == 1:
			return S11[0,0], S12[0,0], S21[0,0], S22[0,0]
		else:
			return S11, S12, S21, S22
	#===========================================================================
	def calcS11(S11b):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
			return S11
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a
			return S11[0,0]
	#===========================================================================
	def calcT(L):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#===========================================================================
	# calculate S11meas - the total reflection coefficient
	#===========================================================================
	S11a, S12a, S21a, S22a = MoM_calc_S_matrix(ep1*(1.0+1.0j*tan1), ro1, xo1)
	T = calcT(L)
	S11b, S12b, S21b, S22b = single_slab_guide_matrix(a/lam, ep2*(1.0+1.0j*tan2), d2/a, NM)
	S11abs = np.abs(calcS11(S11b))
	#===========================================================================
	# calculate the derivative of S11meas
	#===========================================================================
	S11b, S12b, S21b, S22b = single_slab_guide_matrix(a/lam, (ep2+1e-3)*(1.0+1.0j*tan2), d2/a, NM)
	S11abs_p = np.abs(calcS11(S11b))

	S11_der = (S11abs_p - S11abs)/1e-3

	if callable(sigma_S11abs):
		sigma_S11abs = sigma_S11abs(S11abs)
	#---------------------------------------------------------------------------
	# EVALUATING MEASUREMENT UNCERTAINTY
	#---------------------------------------------------------------------------
	epm 		= np.ndarray((M,))
	#===========================================================================
	nan_count 	= 0
	MAX_NAN_NUM 	= np.round(M*kwd.get('nan_ratio', 0.1))
	failed 		= False
	def func(epvar, **kwd):

		S11b, S12b, S21b, S22b = single_slab_guide_matrix(a/lam, epvar*(1.0+1.0j*del_tan2), del_d2/a, NM)

		if NM == 1:
			S11b = S11b[0,0]
			S12b = S12b[0,0]
			S21b = S21b[0,0]
			S22b = S22b[0,0]

		return  np.abs(calcS11(S11b))
	#===========================================================================
	a_actual = a
	f_actual = f

	for n in range(M):

		del_S11abs 		= np.random.normal(S11abs,	sigma_S11abs)

		del_ro1			= np.random.normal(ro1,		sigma_ro1)
		del_xo1			= np.random.normal(xo1,		sigma_xo1)
		del_d2	 		= np.random.normal(d2,		sigma_d2)
		del_L	 		= np.random.normal(L,		sigma_L)
		del_a	 		= np.random.normal(a_actual,	sigma_a)
		del_f	 		= np.random.normal(f_actual,	sigma_f)

		del_ep1	 		= np.random.normal(ep1,		sigma_ep1)
		del_tan1		= np.random.normal(tan1,	sigma_tan1)

		del_tan2		= np.random.normal(tan2,	sigma_tan2)
		#===============================================================================
		a 	= del_a
		f 	= del_f
		lam 	= 300.0/f

		S11a, S12a, S21a, S22a  = MoM_calc_S_matrix(del_ep1*(1.0+1.0j*del_tan1), del_ro1, del_xo1)
		T 			= calcT(del_L)

		epm[n] 			= root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=del_S11abs)

		if np.isnan(epm[n]):
			nan_count += 1
		if nan_count == MAX_NAN_NUM:
			failed = True
			break

	return epm, failed
#===========================================================================
def MC_holed_and_solid_rods(modeldata, sigma, M=1000, MAXIT=100, NTOL=1e-4, NM=3, N=5, **kwd):
	#=========================================================================
	#=========================================================================
	ep1 	= modeldata['ep1']	# auxiliary rod dielectric constant
	tan1 	= modeldata['tan1']	# auxiliary rod loss tangent

	ep2 	= modeldata['ep2']	# MUT rod dielectric constant
	tan2 	= modeldata['tan2']	# MUT rod loss tangent

	ro1 	= modeldata['ro1']    	# auxiliary rod radius in mm
	ro2 	= modeldata['ro2']    	# MUT rod radius in mm
	xo1 	= modeldata['xo1']    	# auxiliary rod offset in mm
	xo2 	= modeldata['xo2']    	# MUT rod offset in mm

	ro1in 	= modeldata['ro1in']  	# auxiliary rod holes radius in mm
	d1 	= modeldata['d1']     	# auxiliary rod hole-to-hole distance in mm
	ph1 	= modeldata['ph1']    	# auxiliary rod hole line angle

	L 	= modeldata['L']	# interslab separation distance in mm
	f 	= modeldata['f']	# operating frequency in GHz

	a 	= modeldata['a']	# waveguide width in mm
	#=========================================================================
	sigma_S11abs 	= sigma.get('S11abs', 	0.01)

	sigma_ro1 	= sigma.get('ro1', 	0.00)
	sigma_xo1 	= sigma.get('xo1', 	0.00)
	sigma_ro1in 	= sigma.get('ro1in', 	0.00)
	sigma_d1 	= sigma.get('d1', 	0.00)
	sigma_ph1 	= sigma.get('ph1', 	0.00)
	sigma_ro2	= sigma.get('ro2', 	0.00)
	sigma_xo2	= sigma.get('xo2', 	0.00)
	sigma_L 	= sigma.get('L',	0.00)
	sigma_a 	= sigma.get('a',	0.00)
	sigma_f 	= sigma.get('f',	0.00)

	sigma_ep1 	= sigma.get('ep1',      0.0  )
	sigma_tan1 	= sigma.get('tan1',     0.0  )

	sigma_tan2 	= sigma.get('tan2',     0.0  )
	#=========================================================================
	MoMinit()

	lam = 300.0/f
	#=========================================================================
	def evalparam_holed(roddata):
		param           = MoMparam()

		param.ep1       = roddata['ep'] + 0.0j
		param.ep2       = 1.0 + 0.0j
		param.ep3       = 1.0 + 0.0j

		param.la        = a/lam

		param.ro1       = roddata['ro']/a
		param.ro2       = roddata['roin']/a
		param.ro3       = roddata['roin']/a

		param.xo1       = 0.5 + roddata['xo']/a
		param.xo2       = 0.5 + 0.5*roddata['d']/a*np.cos(roddata['ph']) + roddata['xo']/a
		param.xo3       = 0.5 - 0.5*roddata['d']/a*np.cos(roddata['ph']) + roddata['xo']/a

		param.zo1       = 0.0
		param.zo2       = 0.5 - 0.5*roddata['d']/a*np.sin(roddata['ph'])
		param.zo3       = 0.5 + 0.5*roddata['d']/a*np.sin(roddata['ph'])

		return param
	#=========================================================================
	def evalparam_solid(roddata):

		param		= MoMparam()

		param.ep	= roddata['ep'] + 0.0j

		param.la	= a/lam

		param.ro	= roddata['ro']/a
		param.xo	= 0.5 + roddata['xo']/a
		param.zo	= 0.0

		return param
	#=========================================================================
	def MoM_holed_rod_S_matrix(roddata):
		param = evalparam_holed(roddata)
		S11, S12, S21, S22 = MoM_two_in_one_(param, NM, N)
		return S11, S12, S21, S22
	#=========================================================================
	def MoM_solid_rod_S_matrix(roddata):
		param = evalparam_solid(roddata)
		S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
		return S11, S12, S21, S22
	#=========================================================================
	def calcS11(S11b):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#=========================================================================
	def calcT(L):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#=========================================================================
	# calculate S11abs - the total reflection coefficient
	#=========================================================================
	roddata1 = {'ep': ep1*(1.0+1.0j*tan1), 'ro': ro1, 'xo': xo1, 'roin': ro1in, 'd': d1, 'ph': ph1}
	S11a, S12a, S21a, S22a = MoM_holed_rod_S_matrix(roddata1)
	T = calcT(L)

	roddata2 		= {'ep': ep2*(1.0+1.0j*tan2), 'ro': ro2, 'xo': xo2}
	S11b, S12b, S21b, S22b 	= MoM_solid_rod_S_matrix(roddata2)
	S11abs 			= np.abs(calcS11(S11b))
	# calculate the derivative of S11meas
	roddata2 		= {'ep': (ep2+1e-3)*(1.0+1.0j*tan2), 'ro': ro2, 'xo': xo2}
	S11b, S12b, S21b, S22b 	= MoM_solid_rod_S_matrix(roddata2)
	S11abs_p 		= np.abs(calcS11(S11b))

	S11_der 		= (S11abs_p - S11abs)/1e-3

	if callable(sigma_S11abs):
		sigma_S11abs = sigma_S11abs(S11abs)
	#--------------------------------------------------------------------------
	# EVALUATING MEASUREMENT UNCERTAINTY
	#--------------------------------------------------------------------------
	epm 		= np.ndarray((M,))
	#--------------------------------------------------------------------------
	nan_count 	= 0
	MAX_NAN_NUM 	= np.round(M*kwd.get('nan_ratio', 0.1))
	failed 		= False
	def func(epvar, **kwd):
		param.ep 		= epvar*(1.0+1.0j*del_tan2)
		S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
		return  np.abs(calcS11(S11b))
	#--------------------------------------------------------------------------
	a_actual 	= a
	f_actual 	= f

	for n in range(M):

		del_S11abs 		= np.random.normal(S11abs,	sigma_S11abs)
		del_ro1			= np.random.normal(ro1,		sigma_ro1)
		del_xo1			= np.random.normal(xo1,		sigma_xo1)
		del_ro1in		= np.random.normal(ro1in,	sigma_ro1in)
		del_d1			= np.random.normal(d1,		sigma_d1)
		del_ph1			= np.random.normal(ph1,		sigma_ph1)

		del_ro2	 		= np.random.normal(ro2,		sigma_ro2)
		del_xo2	 		= np.random.normal(xo2,		sigma_xo2)
		del_L	 		= np.random.normal(L,		sigma_L)

		del_f	 		= np.random.normal(f_actual,	sigma_f)
		del_a	 		= np.random.normal(a_actual,	sigma_a)

		del_ep1	 		= np.random.normal(ep1,		sigma_ep1)
		del_tan1		= np.random.normal(tan1,	sigma_tan1)

		del_tan2		= np.random.normal(tan2,	sigma_tan2)

		a 	= del_a
		f 	= del_f
		lam	= 300.0/f

		roddata1 		= {'ep': del_ep1*(1.0+1.0j*del_tan1), 'ro': del_ro1, 'xo': del_xo1, 'roin': del_ro1in, 'd': del_d1, 'ph': del_ph1}
		S11a, S12a, S21a, S22a  = MoM_holed_rod_S_matrix(roddata1)

		T 			= calcT(del_L)

		roddata2 		= {'ep': ep2, 'ro': del_ro2, 'xo': del_xo2}
		param 			= evalparam_solid(roddata2)

		epm[n] 			= root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=del_S11abs)

		if np.isnan(epm[n]):
			nan_count += 1
		if nan_count == MAX_NAN_NUM:
			failed = True
			break

	return epm, failed
#==========================================================================
def MC_three_rods(modeldata, sigma, M=1000, MAXIT=100, NTOL=1e-4, NM=3, N=5, **kwd):
#=============================================================================
#=============================================================================
	ep1 	= modeldata['ep1']	# 1st auxiliary rod dielectric constant
	tan1 	= modeldata['tan1']	# 1st auxiliary rod loss tangent

	ep2 	= modeldata['ep2']	# MUT rod dielectric constant
	tan2 	= modeldata['tan2']	# MUT rod loss tangent

	ep3 	= modeldata['ep3']	# 2nd auxiliary rod dielectric constant
	tan3 	= modeldata['tan3']	# 1st auxiliary rod loss tangent

	ro1 	= modeldata['ro1']	# 1st auxiliary rod radius in mm
	ro2 	= modeldata['ro2']	# auxiliary slab thickness in mm
	ro3 	= modeldata['ro3']  	# 2nd auxiliary rod radius in mm

	xo1 	= modeldata['xo1']	# 1st auxiliary rod offset in mm
	xo2 	= modeldata['xo2']	# MUT rod offset in mm
	xo3 	= modeldata['xo3']  	# 2nd auxiliary rod offset in mm

	L1 	= modeldata['L1']	# interslab separation distance in mm
	L2 	= modeldata['L2']	# interslab separation distance in mm
	f 	= modeldata['f'] 	# operating frequency in GHz
	a 	= modeldata['a'] 	# waveguide width in mm
	#=============================================================================
	sigma_S11abs 	= sigma.get('S11abs', 	0.01)
	sigma_ro1 	= sigma.get('ro1', 	0.001)
	sigma_ro2	= sigma.get('ro2', 	0.001)
	sigma_ro3	= sigma.get('ro3', 	0.001)
	sigma_xo1 	= sigma.get('xo1', 	0.00 )
	sigma_xo2	= sigma.get('xo2', 	0.00 )
	sigma_xo3	= sigma.get('xo3', 	0.00 )
	sigma_L1	= sigma.get('L1',	0.00)
	sigma_L2 	= sigma.get('L2',	0.00 )
	sigma_a 	= sigma.get('a',	0.00 )
	sigma_f 	= sigma.get('f',	0.00 )

	sigma_ep1 	= sigma.get('ep1',      0.0  )
	sigma_tan1 	= sigma.get('tan1',     0.0  )

	sigma_tan2 	= sigma.get('tan2',     0.0  )
	sigma_ep3 	= sigma.get('ep3',      0.0  )

	sigma_tan3 	= sigma.get('tan3',     0.0  )
	#=========================================================================
	MoMinit()		# initialize C++ function library

	lam = 300.0/f
	#=============================================================================
	def evalparam(ep, ro, xo):
		param 		= MoMparam()
		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5 + xo/a
		param.zo 	= 0.0
		return param
	#=============================================================================
	def MoM_calc_S_matrix(ep, ro, xo):
		param = evalparam(ep, ro, xo)
		S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
		if NM == 1:
			return S11[0,0], S12[0,0], S21[0,0], S22[0,0]
		else:
			return S11, S12, S21, S22
	#=============================================================================
	def calcSab(S11b, S12b, S21b, S22b, T):
		if NM == 1:
			S11ab = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
			S22ab = S22b + S11b*T*S22a*T/(1.0 - S11b*T*S22a*T)*S21b
			S21ab = S21a*T/(1.0 - S11b*T*S22a*T)*S21b
			S12ab = S12b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11ab = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a
			S22ab = S22b + S12b*T*S22a*T*np.linalg.inv(np.eye(NM) - S11b*T*S22a*T)*S21b
			S21ab = S21a*T*np.linalg.inv(np.eye(NM) - S11b*T*S22a*T)*S21b
			S12ab = S12b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a
		return S11ab, S12ab, S21ab, S22ab
	#=============================================================================
	def calcS11(S11ab, S12ab, S21ab, S22ab, T):
		if NM == 1:
			S11 = S11ab + S21ab*T*S11c*T/(1.0 - S22a*T*S11c*T)*S12ab
			return S11
		else:
			S11 = S11ab + S21ab*T*S11c*T*np.linalg.inv(np.eye(NM) - S22ab*T*S11c*T)*S12ab
			return S11[0,0]
	#=============================================================================
	def calcT(L):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#=============================================================================
	# calculate S11abs - the total reflection coefficient
	#=============================================================================
	S11a, S12a, S21a, S22a 		= MoM_calc_S_matrix(ep1*(1.0+1.0j*tan1), ro1, xo1)
	T1 = calcT(L1)
	T2 = calcT(L2)
	S11b, S12b, S21b, S22b 		= MoM_calc_S_matrix(ep2*(1.0+1.0j*tan2), ro2, xo2)
	S11c, S12c, S21c, S22c 		= MoM_calc_S_matrix(ep3*(1.0+1.0j*tan3), ro3, xo3)

	S11ab, S12ab, S21ab, S22ab 	= calcSab(S11b, S12b, S21b, S22b, T1)
	S11abs 				= np.abs(calcS11(S11ab, S12ab, S21ab, S22ab, T2))
	# calculate the derivative of S11meas
	S11b, S12b, S21b, S22b 		= MoM_calc_S_matrix((ep2+1e-2)*(1.0+1.0j*tan2), ro2, xo2)
	S11ab, S12ab, S21ab, S22ab 	= calcSab(S11b, S12b, S21b, S22b, T1)
	S11abs_p 			= np.abs(calcS11(S11ab, S12ab, S21ab, S22ab, T2))

	S11_der = (S11abs_p - S11abs)/1e-2

	if callable(sigma_S11abs):
		sigma_S11abs = sigma_S11abs(S11abs)
	#=============================================================================
	# EVALUATING MEASUREMENT UNCERTAINTY
	#=============================================================================
	epm 		= np.ndarray((M,))
	#=============================================================================
	nan_count 	= 0
	MAX_NAN_NUM 	= np.round(M*kwd.get('nan_ratio', 0.1))
	failed 		= False
	def func(epvar, **kwd):
		param.ep 		= epvar*(1.0+1.0j*del_tan2)
		S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
		if NM == 1:
			S11b = S11b[0,0]
			S12b = S12b[0,0]
			S21b = S21b[0,0]
			S22b = S22b[0,0]

		S11ab, S12ab, S21ab, S22ab = calcSab(S11b, S12b, S21b, S22b, T1)
		return np.abs(calcS11(S11ab, S12ab, S21ab, S22ab, T2))
	#=============================================================================
	a_actual = a
	f_actual = f

	for n in range(M):

		del_S11abs 		= np.random.normal(S11abs,	sigma_S11abs)
		del_ro1			= np.random.normal(ro1,		sigma_ro1)
		del_ro2	 		= np.random.normal(ro2,		sigma_ro2)
		del_ro3	 		= np.random.normal(ro3,		sigma_ro3)

		del_xo1			= np.random.normal(xo1,		sigma_xo1)
		del_xo2	 		= np.random.normal(xo2,		sigma_xo2)
		del_xo3	 		= np.random.normal(xo3,		sigma_xo3)

		del_L1	 		= np.random.normal(L1,		sigma_L1)
		del_L2		 	= np.random.normal(L2,		sigma_L2)

		del_a		 	= np.random.normal(a_actual,	sigma_a)
		del_f		 	= np.random.normal(f_actual,	sigma_f)

		del_ep1	 		= np.random.normal(ep1,		sigma_ep1)
		del_tan1		= np.random.normal(tan1,	sigma_tan1)

		del_tan2		= np.random.normal(tan2,	sigma_tan2)

		del_ep3	 		= np.random.normal(ep3,		sigma_ep3)
		del_tan3		= np.random.normal(tan3,	sigma_tan3)
		#=======================================================================
		a 	= del_a
		f 	= del_f
		lam 	= 300.0/f

		S11a, S12a, S21a, S22a  = MoM_calc_S_matrix(del_ep1*(1.0+1.0j*del_tan1), del_ro1, del_xo1)
		S11c, S12c, S21c, S22c  = MoM_calc_S_matrix(del_ep3*(1.0+1.0j*del_tan3), del_ro3, del_xo3)

		T1 			= calcT(del_L1)
		T2 			= calcT(del_L2)

		param 			= evalparam(ep2, del_ro2, del_xo2)

		epm[n] 			= root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=del_S11abs)

		if np.isnan(epm[n]):
			nan_count += 1
		if nan_count == MAX_NAN_NUM:
			failed = True
			break

	return epm, failed
#===========================================================================
#===========================================================================
