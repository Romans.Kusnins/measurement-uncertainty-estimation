import os
import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *
from Derivatives import *

try:
        matplot = True
        from matplotlib import pyplot as plt
except ModuleNotFoundError:
        print("The matplotlib module is not installed.")
        matplot = False


from plot2pdf import *

from Uncertainty_vs_S11 import *
#==================================================================================
xMin 		= None
xMax 		= None
MaxVal 		= None

task 		= 'sensitivity'
#task 		= 'total'
#---------------------------------------
#sensparam 	= 'S11abs'
#sensparam 	= 'd1'
#sensparam 	= 'd2'
#sensparam 	= 'ep1'
sensparam 	= 'tan1'
#---------------------------------------
var_param 	= 'L'
#---------------------------------------
#quantity 	= 'mean'
quantity 	= 'std'


#PREFIX = '_29_'

#PREFIX = ['_6_0_', '_6_5_', '', '_7_5_']
PREFIX = ''


if var_param == 'L':
	MCNUM = 20000
else:
	MCNUM = 25000
#==================================================================================
isPDF 		= True
#isPRINT	= False
#==================================================================================
xlabels		= dict()

xlabels['ep1']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,aux}}$'
xlabels['ep2']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,mut}}$'
xlabels['tan1'] = '$\\tan_{\\delta^{\\mathrm{aux}}}$'
xlabels['tan2'] = '$\\tan_{\\delta^{\\mathrm{mut}}}$'
xlabels['d1']   = '$d_{\\mathrm{aux}}$ mm'
xlabels['d2']   = '$d_{\\mathrm{mut}}$ mm'
xlabels['L']    = '$d_{\\mathrm{int}}$ mm'
xlabels['f']    = 'f, GHz'

paramname       = {'S11abs': '$u_{|S_{11}|}\\left(\\varepsilon^{\\prime}_{\\mathrm{r,mut}}\\right)$', 'd1': '$u_{d_{\\mathrm{aux}}}\\left(\\varepsilon^{\\prime}_{\\mathrm{r,mut}}\\right)$', 'd2': '$u_{d_{\\mathrm{mut}}}\\left(\\varepsilon^{\\prime}_{\\mathrm{r,mut}}\\right)$', 'L': '$u_{d_{\\mathrm{in}}}\\left(\\varepsilon^{\\prime}_{\\mathrm{r,mut}}\\right)$', 'ep1': '$u_{\\varepsilon^{\\prime}_{\\mathrm{r,aux}}}\\left(\\varepsilon^{\\prime}_{\\mathrm{r,mut}}\\right)$', 'tan1': '$u_{\\tan{\\delta^{\\mathrm{aux}}}}\\left(\\varepsilon^{\\prime}_{\\mathrm{r,mut}}\\right)$', 'tan2': 'u_{\\tan{\\delta^{\\mathrm{mut}}}}\\left(\\varepsilon^{\\prime}_{\\mathrm{r,mut}}\\right)', 'f': 'u_{f}\\left(\\varepsilon^{\\prime}_{\\mathrm{r,mut}}\\right)'}
#==================================================================================
# PLOTTING CALCULATED DATA
#==================================================================================
FOLDER = './RESULTS_2SL_FREE/'

MCSTR = '{:}'.format(MCNUM)

if task == 'sensitivity':
	MCfilename 	= FOLDER + 'two_slabs_free_var_' + var_param + '_sens_' + sensparam + '_MCnum'+ PREFIX + '_' + MCSTR + '.npy'
	MCfilename_mean	= FOLDER + 'two_slabs_free_var_' + var_param + '_sens_' + sensparam + '_MCnum_mean'+ PREFIX + '_' + MCSTR + '.npy'
	EPMfilename 	= FOLDER + 'two-slabs-free_var_' + var_param + '_standard_' + sensparam + '_EPM' + PREFIX + '.npy'
else:
	MCfilename 	= FOLDER + 'two_slabs_free_var_' + var_param + '_total_MCnum' + PREFIX + '_' + MCSTR + '.npy'
	MCfilename_mean	= FOLDER + 'two_slabs_free_var_' + var_param + '_total_MCnum_mean' + PREFIX + '_' + MCSTR + '.npy'
	EPMfilename 	= FOLDER + 'two-slabs-free_var_' + var_param + '_total_EPM' + PREFIX + '.npy'

MCdata 		= np.load(MCfilename)
MCdata_mean 	= np.load(MCfilename_mean)
EPMdata 	= np.load(EPMfilename)
#==================================================================================
if MaxVal != None:
	Mdata_mean[1, MCdata_mean[1,:] > MaxVal]	= np.nan
	MCdata[1, MCdata[1,:]  > MaxVal]		= np.nan

	EPMdata_mean[1, EPMdata_mean[1,:] > MaxVal]	= np.nan
	EPMdata[1, EPMdata[1,:]  > MaxVal]		= np.nan

if xMin != None:
	ID 	= MCdata[0,:] >= xMin
	MCdata 	= MCdata[:,ID]

	ID 	= EPMdata[0,:] >= xMin
	EPMdata	= EPMdata[:,ID]

if xMax != None:
	ID 	= MCdata[0,:] <= xMax
	MCdata 	= MCdata[:,ID]

	ID 	= EPMdata[0,:] <= xMax
	EPMdata	= EPMdata[:,ID]
#==================================================================================
if isPDF:
	legend = ['EPM', 'MCM']


	if task == 'sensitivity':
		filename  = 'TWO_SL_FREE_VAR_' + var_param + '_SENS_' + sensparam + PREFIX
		labels = {'xlabel': xlabels[var_param], 'ylabel': paramname[sensparam]}
	else:
		filename  = 'TWO_SL_FREE_VAR_' + var_param + '_TOTAL' + PREFIX
		labels = {'xlabel': xlabels[var_param], 'ylabel': '$u(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})$'}

	LEGPOS = [0.15, 0.7]
	if quantity == 'mean':
		filename  = 'TWO_SL_FREE_VAR_' + var_param + 'MEAN' + PREFIX
		labels = {'xlabel': xlabels[var_param], 'ylabel': '$\\bar{u}(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})$'}
	#	labels = {'xlabel': xlabels[var_param], 'ylabel': ylabeldelta + ' ' + paramname[param]}
		plot2latex(MCdata_mean[0,:], MCdata_mean[1,:], LineType=['','dash'], xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, YZero=False, LegendPosition=LEGPOS, FileName=filename)
	else:
		plot2latex([EPMdata[0,:], MCdata[0,:]], [EPMdata[1,:], MCdata[1,:]], LineType=['','dash'], xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, LegendPosition=LEGPOS, FileName=filename)
#	plot2latex([EPMdata[0,:], MCdata[0,:]], [EPMdata[1,:], MCdata[1,:]], ylabel_vert=True, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels)
elif matplot:
#	plt.ylabel(ylabeldelta + ' ' + paramname[param])
	plt.plot(MCdata[0,:], EPMdata[1,:])
	plt.grid()
	plt.show()
else:
	pass
#       print('Calculated data have been saved into a file.')

print('--------------------------------------------------------')
