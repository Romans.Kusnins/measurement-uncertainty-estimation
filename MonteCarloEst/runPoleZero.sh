#!/bin/bash

MPATH=/home/student_etf/GitMonteCarlo/measurement-uncertainty-estimation/MonteCarloEst

cd $MPATH

rm -f ./PoleZero

mpic++  PoleZero.cpp -o PoleZero -L./lib -lrod -lewald -lslatec -llapack -lrefblas -lgfortran  -DMP

mpirun ./PoleZero

cd ~
