#include<stdio.h>
#include<math.h>

#define MAXIT  100 				// Maximum allowed number of iterations.
#define EULER  0.5772156649015328606065120 	// Euler's constant γ.
#define FPMIN  1.0e-30 				// Close to smallest representable floating-point number.
#define EPS    1.0e-12 				// Desired relative error, not smaller than the machine precision.


double cf1[] = {-5.7721566369244781e-01,\
                 9.9999999962750130e-01,\
		-4.9999999743542584e-01,\
		 6.2499995994275123e-02,\
		-6.9444394377626180e-03,\
		 6.5103695889618470e-04,\
		-5.2080004415126863e-05,\
		 3.6151233799385444e-06,\
		-2.2073142676539123e-07,\
		 1.1898188468030051e-08,\
		-5.5208948534002943e-10,\
		 1.9932592095456338e-11,\
		-4.1223956038041033e-13};

double cf2[] = {-5.7720815868864872e-01,\
		 9.9999683109495929e-01,\
		 -7.4998633688954619e-01,\
		 1.4061153079502720e-01,\
		 -2.3426783971851852e-02,\
		 3.2894228875959878e-03,\
		 -3.9253229083728710e-04,\
		 4.0153933084564787e-05,\
		 -3.5029865838885866e-06,\
		 2.5301256845184201e-07,\
		 -1.4151228344766210e-08,\
		 5.3802824138327002e-10,\
		 -1.0257895443954342e-11};

double cf3[] = {-5.7585414275292790e-01,\
		 9.9923544136147580e-01,\
		 -9.9755175392637274e-01,\
		 2.4819657696439906e-01,\
		 -5.4475615638796299e-02,\
		 9.9210292006963168e-03,\
		 -1.4916743538565150e-03,\
		 1.8352223321835989e-04,\
		 -1.8066248001848390e-05,\
		 1.3669460472564689e-06,\
		 -7.4363058042022338e-08,\
		 2.5777119929374079e-09,\
		 -4.2590718486108577e-11};


double expint(double x, int n, char mode) // Evaluates the exponential integral En(x).
{


int i, ii, nm1, p;
double  a, b, c, d, del, fact, h, psi, ans;
nm1 = n-1;
double xl, xp;

if(n < 0 || x < 0.0 || (x==0.0 && n==0))
// nrerror("bad arguments in expint");
   printf("bad arguments in expint \n");
else{
   if(n == 0) ans=exp(-x)/x; 	// Special case.
   else{
      if(x == 0.0 && n != 1) ans=1.0/nm1; 	// Another special case.
      else{
         if(x >= 5.0){ 		// Lentz's algorithm (x5.2).
           b = x+n;
           c = 1.0/FPMIN;
           d = 1.0/b;
           h = d;
           for(i = 1; i <= MAXIT; i++){
              a = -i*(nm1+i);
              b += 2.0;
              d = 1.0/(a*d+b); 	// Denominators cannot be zero.
              c = b+a/c;
              del = c*d;
              h *= del;
              if(fabs(del-1.0) < EPS){
                ans = h*exp(-x);
//              ans = h;
//              ans = exp(x);
                return ans;
              }
           }
//         nrerror("continued fraction failed in expint");
           printf("continued fraction failed in expint \n");
	 }else if(x >= 0.5 && x < 1.5){
 	    ans = cf1[0] - cf1[1]*log(x);
            xp = xl = x/0.5;    
            ans -= cf1[2]*xp;
            for(p = 2; p <= 11; ++p){
               xp *= xl;
               ans -= cf1[p+1]*xp;
	    }
            return ans;
	 }else if(x >= 1.5 && x < 3.0){
 	    ans = cf2[0] - cf2[1]*log(x);
            xp = xl = x/0.75;        
            ans -= cf2[2]*xp;
            for(p = 2; p <= 11; ++p){
               xp *= xl;
               ans -= cf2[p+1]*xp;
	    }
            return ans;
	 }else if(x >= 3.0 && x < 5.0){
 	    ans = cf3[0] - cf3[1]*log(x);
            xp = xl = x;        
            ans -= cf3[2]*xp;
            for(p = 2; p <= 11; ++p){
               xp *= xl;
               ans -= cf3[p+1]*xp;
	    }
            return ans;
	 }else if(x < 0.5 && n == 1){
            if(mode == 's'){
               ans =  -0.577215664901532755;
	    }else{
               ans =  -0.577215664901532755 - log(x);
            }
            fact = x;
            ans += x;
            for(i = 2; i <= 12; ++i){
               fact *= -x/i;
               ans += fact/i;
            }
            return ans;
         }else{						// Evaluate series.
           ans = (nm1!=0 ? 1.0/nm1 : -log(x)-EULER); 	// Set first term.
           fact = 1.0;
           for(i=1;i<=MAXIT;i++){
              fact *= -x/i;
              if(i != nm1) del = -fact/(i-nm1);
              else{
                 psi = -EULER; // Compute(n).
                 for(ii=1;ii<=nm1;ii++) psi += 1.0/ii;
                 del = fact*(-log(x)+psi);
              }
              ans += del;
              if(fabs(del) < fabs(ans)*EPS) return ans;
           }
//         nrerror("series failed in expint");
           printf("series failed in expint \n");
         }
      }
   }
}

return ans;
}

/*
int main(int argc, char ** argv){    
  printf("res = %5.14e \n", expint(X, 1, 's'));
  return 1;
}
*/

//EULER = 0.5772156649015328606065120;


