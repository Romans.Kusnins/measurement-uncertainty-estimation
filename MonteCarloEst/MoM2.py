import os
from ctypes import *
import numpy as np


N = 7

ep1 = 4.2
ep2 = 1.0
ep3 = 1.0

la  = 0.75

ro1 = 0.2
ro2 = 0.025
ro3 = 0.025

xo1 = 0.75
xo2 = 0.7
xo3 = 0.8

zo1 = 0.0
zo2 = 0.0
zo3 = 0.0

# define auxiliary data types and structures that will be used later
class ComplexDouble(Structure):
	_fields_ = [("real", c_double), ("imag", c_double)]

class DataB(Structure):
	_fields_ = [("ep1", ComplexDouble),\
	 ("ep2", ComplexDouble), \
	 ("la",  c_double), \
	 ("ro1", c_double), \
	 ("ro2", c_double), \
	 ("xo1", c_double), \
	 ("xo2", c_double), \
	 ("zo1", c_double), \
	 ("zo2", c_double)]

class DataC(Structure):
	_fields_ = [("ep1", ComplexDouble),\
	 ("ep2", ComplexDouble), \
	 ("ep3", ComplexDouble), \
	 ("la",  c_double), \
	 ("ro1", c_double), \
	 ("ro2", c_double), \
	 ("ro3", c_double), \
	 ("xo1", c_double), \
	 ("xo2", c_double), \
	 ("xo3", c_double), \
	 ("zo1", c_double), \
	 ("zo2", c_double), \
	 ("zo3", c_double) ]

#----------------------------------------------------------
# Assign variable values to the corresponding structure fields
data = DataC()

data.ep1.real = ep1.real
data.ep1.imag = ep1.imag

data.ep2.real = ep2.real
data.ep2.imag = ep2.imag

data.ep3.real = ep3.real
data.ep3.imag = ep3.imag

data.la = la

data.ro1 = ro1
data.ro2 = ro2
data.ro3 = ro3

data.xo1 = xo1
data.xo2 = xo2
data.xo3 = xo3

data.zo1 = zo1
data.zo2 = zo2
data.zo3 = zo3

#----------------------------------------------------------
#MN = 1
MN = int(input("Enter the mode number: "))
#=============================================================
CWD = os.getcwd()
LIBPATH = os.environ.get('LD_LIBRARY_PATH', '')
#print(LIBPATH)

if LIBPATH.find(CWD + '/lib')  < 0:
	print('Please execute the following command first: "source libinit"')
	quit()
#	os.environ['LD_LIBRARY_PATH'] = LIBPATH + CWD + '/lib'
#=============================================================
WN = 2*N+1

#ComplexDoubleArray = ComplexDouble * 10
DoubleArray = c_double * (2*MN*MN)
#SMatrixArray = DoubleArray * 4

PtDoubleArray = POINTER(DoubleArray)	# define a pointer to double array data type

PTARRAY = PtDoubleArray * 4		# define a pointer array data type consisting of 4 pointers

SMN = PTARRAY()
#==============================================================
# S11 = ComplexDoubleArray()
#S11 = DoubleArray()
#S12 = DoubleArray()
#S21 = DoubleArray()
#S22 = DoubleArray()

SMN[0] = pointer(DoubleArray())
SMN[1] = pointer(DoubleArray())
SMN[2] = pointer(DoubleArray())
SMN[3] = pointer(DoubleArray())
#==============================================================
shlib = CDLL("MoM2.so")

#shlib.func(SMN, c_int(N), c_int(MN));
shlib.func(SMN, data, c_int(N), c_int(MN));

S11 = np.asarray(list(SMN[0].contents))
S12 = np.asarray(list(SMN[1].contents))
S21 = np.asarray(list(SMN[2].contents))
S22 = np.asarray(list(SMN[3].contents))

S11 = S11[::2] + 1.0j*S11[1::2]
S12 = S12[::2] + 1.0j*S12[1::2]
S21 = S21[::2] + 1.0j*S21[1::2]
S22 = S22[::2] + 1.0j*S22[1::2]
#==============================================================
print("S11 matrix:")
for z in S11:
	print(z)

print("\nS12 matrix:")
for z in S12:
	print(z)

print("\nS21 matrix:")
for z in S21:
	print(z)

print("\nS22 matrix:")
for z in S22:
	print(z)
print("\n\n")
