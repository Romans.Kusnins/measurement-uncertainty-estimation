import os
import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *
from Derivatives import *

try:
        matplot = True
        from matplotlib import pyplot as plt
except ModuleNotFoundError:
        print("The matplotlib module is not installed.")
        matplot = False


from plot2pdf import *

from Uncertainty_vs_S11 import *
#==================================================================================
xMin = None
xMax = None

task 		= 'sensitivity'
#task 		= 'standard'
task 		= 'total'

#sensparam 	= 'S11abs'
sensparam 	= 'S11abs'

var_param 	= 'L'
#var_param 	= 'ro1'

PREFIX1		= '_6_0_'
PREFIX2		= '_6_0_'

estim_method	= MC 	# EPM

MCNUM		= 20000

isPDF 		= True
#isPRINT	= False
#==================================================================================
xlabels		= dict()

#xlabels['ep1']  = 'Dielectric constant of the auxiliary slab'
#xlabels['ep2']  = 'Dielectric constant of MUT'
#xlabels['tan1'] = 'Loss tangent of the auxiliary slab'
#xlabels['tan2'] = 'Loss tangent of MUT'
#xlabels['ro1']  = 'Radius of the auxiliary rod, mm'
#xlabels['ro2']  = 'Radius of the MUT rod, mm'
#xlabels['xo1']  = 'Offset of the auxiliary rod, mm'
#xlabels['xo2']  = 'Offset of the MUT rod, mm'
#xlabels['L']    = 'Separation between the slabs, mm'
#xlabels['a']    = 'Waveguide width, mm'
#xlabels['f']    = 'Frequency, GHz'

xlabels['ep1']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,aux}}$'
xlabels['ep2']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,mut}}$'
xlabels['tan1'] = '$\\tan_{\\delta^{\\mathrm{aux}}}$'
xlabels['tan2'] = '$\\tan_{\\delta^{\\mathrm{mut}}}$'
xlabels['d1']   = '$d_{\\mathrm{aux}}$ mm'
xlabels['d2']   = '$d_{\\mathrm{mut}}$ mm'
xlabels['a']    = '$a mm'
xlabels['L']    = '$d_{\\mathrm{int}}$ mm'
xlabels['f']    = 'f, GHz'

paramname       = {'S11abs': '$u_{|S_{11}|}$', 'ro1': '$u_{r_{\\mathrm{aux}}}$', 'ro2': '$u_{r_{\\mathrm{mut}}}$', 'L': '$u_{d_{\\mathrm{in}}}$', 'ep1': '$u_{\\varepsilon^{\\prime}_{\\mathrm{r,aux}}}$', 'tan1': '$ u_{\\tan{\\delta^{\\mathrm{aux}}}}$', 'tan2': 'u_{\\tan{\\delta^{\\mathrm{mut}}}}', 'a': 'u_{a}', 'f': 'u_{f}'}
#==================================================================================
# PLOTTING CALCULATED DATA
#==================================================================================
FOLDER1	= './RESULTS_1ROD_GUIDE/'
FOLDER2	= './RESULTS/'


MCSTR 	= n2s(MCNUM)

if task == 'sensitivity':
	MCfilename1 	= FOLDER1 + 'single_rod_var_' + var_param + '_sens_' + sensparam + '_MCnum' + PREFIX1 + '_' + MCSTR + '.npy'
	EPMfilename1 	= FOLDER1 + 'singlerod_var_' + var_param + '_standard_'+ sensparam + '_EPM' + PREFIX1 + '.npy'
else:
	MCfilename1 	= FOLDER1 + 'single_rod_var_' + var_param + '_total_MCnum' + PREFIX1 + '_' + MCSTR + '.npy'
	EPMfilename1 	= FOLDER1 + 'singlerod_var_' + var_param + '_total_EPM' + PREFIX1 + '.npy'

if task == 'sensitivity':
	MCfilename2 	= FOLDER1 + 'two_rods_var_' + var_param + '_sens_' + sensparam + '_MCnum' + PREFIX2 + '_' + MCSTR + '.npy'
	EPMfilename2 	= FOLDER1 + 'tworods_var_' + var_param + '_standard_'+ sensparam + '_EPM' + PREFIX2 + '.npy'
else:
	MCfilename2 	= FOLDER1 + 'two_rods_var_' + var_param + '_total_MCnum' + PREFIX2 + '_' + MCSTR + '.npy'
	EPMfilename2 	= FOLDER1 + 'tworods_var_' + var_param + '_total_EPM' + PREFIX2 + '.npy'


MCdata1  = np.load(MCfilename1)
EPMdata1 = np.load(EPMfilename1)

MCdata2  = np.load(MCfilename2)
EPMdata2 = np.load(EPMfilename2)
#==================================================================================
EPMdata1[1, EPMdata1[1,:] > 1.0] = np.nan
EPMdata2[1, EPMdata2[1,:] > 1.0] = np.nan

if xMin != None:
	ID 		= EPMdata1[0,:] >= xMin
	EPMdata1 	= EPMdata1[:,ID]

	ID 		= MCdata1[0,:] >= xMin
	MCdata1 	= MCdata1[:,ID]

	ID 		= EPMdata2[0,:] >= xMin
	EPMdata2 	= EPMdata2[:,ID]

	ID 		= MCdata2[0,:] >= xMin
	MCdata2		= MCdata2[:,ID]

if xMax != None:
	ID 	 	= EPMdata1[0,:] <= xMax
	EPMdata1 	= EPMdata1[:,ID]

	ID 		= MCdata1[0,:] <= xMax
	MCdata1 	= MCdata1[:,ID]

	ID 	 	= EPMdata2[0,:] <= xMax
	EPMdata2 	= EPMdata2[:,ID]

	ID 		= MCdata2[0,:] <= xMax
	MCdata2 	= MCdata2[:,ID]
#==================================================================================
if isPDF:
	legend = ['viens cilindrs', 'divi cilindri']

	if task == 'sensitivity':
		labels = {'xlabel': xlabels[var_param], 'ylabel': 'Sensitivity to ' + sensparam}
	else:
		labels = {'xlabel': xlabels[var_param], 'ylabel': 'Total uncertainty'}

#	labels = {'xlabel': xlabels[var_param], 'ylabel': ylabeldelta + ' ' + paramname[param]}
	if estim_method = 'MC':
		plot2latex([MCdata1[0,:], MCdata2[0,:]], [MCdata1[1,:],  MCdata2[1,:]], ylabel_vert=True, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, LineType=['','dash'])
	else:
		plot2latex([EPMdata1[0,:], EPMdata2[0,:]], [EPMdata1[1,:], EPMdata2[1,:]], ylabel_vert=True, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, LineType=['','dash'])
elif matplot:
#	plt.ylabel(ylabeldelta + ' ' + paramname[param])
	plt.plot(MCdata[0,:], EPMdata[1,:])
	plt.grid()
	plt.show()
else:
	pass
#       print('Calculated data have been saved into a file.')

print('--------------------------------------------------------')
