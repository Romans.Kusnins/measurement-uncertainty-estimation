import numpy as np

try:
	matplot = True
	from matplotlib import pyplot as plt
except ModuleNotFoundError:
	print("The matplotlib module is not installed.")
	matplot = False

from slab_res3md import (root_finding, POW2)
from slab_res3md import (three_slab_guide, three_slab_free_space, root_finding, POW2)
from slab_res3md import (single_slab_guide_matrix)

from MoM import *
from MonteCarlo_estimation import *
#==================================================================================

model = 'two-rods'

M	= 1000
#==================================================================================
modeldata	= dict()
sigma	 	= dict()

if model == "two-rods":
	#==========================================================================
	# two rods in a waveguide
	#==========================================================================
	modeldata['ep1'] = 4.2
	modeldata['ep2'] = 12.5
	modeldata['ro1'] = 7.0
	modeldata['ro2'] = 3.5
	modeldata['L'] 	= 30.0
	modeldata['f'] 	= 10.0
	modeldata['a'] 	= 22.86

	sigma['S11abs'] = 0.01
	sigma['ro1']	= 0.015
	sigma['ro2'] 	= 0.015
	sigma['L']	= 0.015

	epm = MC_two_rods(modeldata, sigma, M)
	#===========================================================================
	# two slabs in a guide
	#===========================================================================
elif model == "two-slabs-guide":

	modeldata['ep1'] = 4.2
	modeldata['ep2'] = 11.0
	modeldata['d1'] = 10.0
	modeldata['d2'] = 10.0
	modeldata['L'] 	= 30.0
	modeldata['f'] 	= 10.0
	modeldata['a'] 	= 22.86

	sigma['S11abs'] = 0.01
	sigma['d1']	= 0.015
	sigma['d2'] 	= 0.015
	sigma['L']	= 0.015

	epm = MC_two_slabs_guide(modeldata, sigma, M)

elif model == "two-slabs-free-space":
	#===========================================================================
	# two slabs in a free space
	#===========================================================================
	modeldata['ep1'] = 4.7
	modeldata['ep2'] = 12.5
	modeldata['d1'] = 10.0
	modeldata['d2'] = 6.0
	modeldata['L'] 	= 20.0
	modeldata['f'] 	= 9.0

	sigma['S11abs'] = 0.01
	sigma['d1']	= 0.015
	sigma['d2'] 	= 0.015
	sigma['L']	= 0.015

	epm = MC_two_slabs_free(modeldata, sigma, M)

elif model == "slab-and-rod":
	#===================================================================================
	# slab and rod model
	#===================================================================================
	modeldata['ep1'] = 4.2
	modeldata['ep2'] = 12.5
	modeldata['d1']  = 12.0
	modeldata['ro2'] = 4.7
	modeldata['L'] 	 = 19.5
	modeldata['f'] 	 = 10.0
	modeldata['a'] 	 = 22.86

	sigma['S11abs'] = 0.01
	sigma['d1']	= 0.0
	sigma['ro2'] 	= 0.0
	sigma['L']	= 0.0

	epm = MC_slab_and_rod(modeldata, sigma, M)

elif model == "rod-and-slab":
	#===================================================================================
	# rod and slab model
	#===================================================================================
	modeldata['ep1'] = 4.2
	modeldata['ep2'] = 12.5
	modeldata['ro1'] = 12.0
	modeldata['d2']  = 4.7
	modeldata['L'] 	 = 19.5
	modeldata['f'] 	 = 10.0
	modeldata['a'] 	 = 22.86

	sigma['S11abs'] = 0.01
	sigma['ro1']	= 0.015
	sigma['d2'] 	= 0.015
	sigma['L']	= 0.015

	epm = MC_rod_and_slab(modeldata, sigma, M)

elif model == "holed-and-solid":
	#===================================================================================
	# holed and solid rods in a waveguide
	#===================================================================================
	modeldata['ep1'] = 4.2
	modeldata['ep2'] = 12.5
	modeldata['ro1'] = 7.0
	modeldata['ro1in'] = 0.5
	modeldata['d1'] = 2.0
	modeldata['ph1'] = 0.0
	modeldata['ro2']  = 4.5
	modeldata['L'] 	 = 27.6
	modeldata['f'] 	 = 10.0
	modeldata['a'] 	 = 22.86

	sigma['S11abs'] = 0.01
	sigma['ro1']	= 0.015
	sigma['ro1in']	= 0.015
	sigma['d1']	= 0.015
	sigma['ph1']	= 0.05
	sigma['ro2'] 	= 0.015
	sigma['L']	= 0.015

	epm = MC_holed_and_solid_rods(modeldata, sigma, M)

elif model == "three-rod":
	#===================================================================================
	# three rods in a waveguide
	#===================================================================================

	modeldata['ep1'] = 4.2
	modeldata['ep2'] = 12.5
	modeldata['ep3'] = 4.2
	modeldata['ro1'] = 6.0
	modeldata['ro2'] = 3.0
	modeldata['ro3'] = 6.0
	modeldata['L1']	 = 16.0
	modeldata['L2']	 = 16.0
	modeldata['f'] 	 = 10.0
	modeldata['a'] 	 = 22.86

	sigma['S11abs'] = 0.01
	sigma['ro1']	= 0.015
	sigma['ro2'] 	= 0.015
	sigma['ro2'] 	= 0.015
	sigma['L1']	= 0.015
	sigma['L2']	= 0.015

	epm = MC_three_rods(modeldata, sigma, M)

#===============================================================
id_nan 	= np.isnan(epm)
epm 	= epm[np.logical_not(id_nan)]
print(epm.shape)

print(np.mean(epm))
print(np.sqrt(np.var(epm)))
