import os
from ctypes import *
import numpy as np

# define auxiliary data types and structures that will be used later
class ComplexDouble(Structure):
	_fields_ = [("real", c_double), ("imag", c_double)]

class DataB(Structure):
	_fields_ = [("ep1", ComplexDouble),\
	 ("ep2", ComplexDouble), \
	 ("la",  c_double), \
	 ("ro1", c_double), \
	 ("ro2", c_double), \
	 ("xo1", c_double), \
	 ("xo2", c_double), \
	 ("zo1", c_double), \
	 ("zo2", c_double)]

class DataC(Structure):
	_fields_ = [("ep1", ComplexDouble),\
	 ("ep2", ComplexDouble), \
	 ("ep3", ComplexDouble), \
	 ("la",  c_double), \
	 ("ro1", c_double), \
	 ("ro2", c_double), \
	 ("ro3", c_double), \
	 ("xo1", c_double), \
	 ("xo2", c_double), \
	 ("xo3", c_double), \
	 ("zo1", c_double), \
	 ("zo2", c_double), \
	 ("zo3", c_double) ]

class DataD(Structure):
	_fields_ = [("ep1", ComplexDouble),\
	("ep2", ComplexDouble), \
	("ep3", ComplexDouble), \
	("ep4", ComplexDouble), \
	("ep5", ComplexDouble), \
	("ep6", ComplexDouble), \
	("la",  c_double), \
	("ro1", c_double), \
	("ro2", c_double), \
	("ro3", c_double), \
	("ro4", c_double), \
	("ro5", c_double), \
	("ro6", c_double), \
	("xo1", c_double), \
	("xo2", c_double), \
	("xo3", c_double), \
	("xo4", c_double), \
	("xo5", c_double), \
	("xo6", c_double), \
	("zo1", c_double), \
	("zo2", c_double), \
	("zo3", c_double), \
	("zo4", c_double), \
	("zo5", c_double), \
	("zo6", c_double) ]


class MoMparam():
	def __init__(self):
		pass

#=========================================================
def MoMinit():

	global shlib

	CWD = os.getcwd()
	LIBPATH = os.environ.get('LD_LIBRARY_PATH', '')

	if LIBPATH.find(CWD + '/lib')  < 0:
		print('Please execute the following command first: "source libinit"')
		quit()

	shlib = CDLL("MoM.so")
#========================================================================================
def MoM_single_solid_rod_(param, MN, N=7):

	global shlib

	data = DataB()

	#-----------------------------------------------------------------------------
	# Assign variable values to the corresponding structure fields
	#-----------------------------------------------------------------------------
	data.ep1.real = param.ep.real
	data.ep1.imag = param.ep.imag

	data.la  = param.la
	data.ro1 = param.ro
	data.xo1 = param.xo
	data.zo1 = param.zo
	#=============================================================
	WN = 2*N+1

	DoubleArray = c_double * (2*MN*MN)

	PtDoubleArray = POINTER(DoubleArray)	# define a pointer to double array data type

	PTARRAY = PtDoubleArray * 4		# define a pointer array data type consisting of 4 pointers

	SMN = PTARRAY()
	#==============================================================
	SMN[0] = pointer(DoubleArray())
	SMN[1] = pointer(DoubleArray())
	SMN[2] = pointer(DoubleArray())
	SMN[3] = pointer(DoubleArray())
	#==============================================================

	shlib.funa(SMN, data, c_int(N), c_int(MN));

	S11 = np.asarray(list(SMN[0].contents))
	S12 = np.asarray(list(SMN[1].contents))
	S21 = np.asarray(list(SMN[2].contents))
	S22 = np.asarray(list(SMN[3].contents))

	S11 = (S11[::2] + 1.0j*S11[1::2]).reshape((MN, MN)).transpose()
	S12 = (S12[::2] + 1.0j*S12[1::2]).reshape((MN, MN)).transpose()
	S21 = (S21[::2] + 1.0j*S21[1::2]).reshape((MN, MN)).transpose()
	S22 = (S22[::2] + 1.0j*S22[1::2]).reshape((MN, MN)).transpose()

	return S11, S12, S21, S22
#========================================================================================
def MoM_two_solid_rods_(param, MN, N=7):

	global shlib

	data = DataB()

	#-----------------------------------------------------------------------------
	# Assign variable values to the corresponding structure fields
	#-----------------------------------------------------------------------------
	data.ep1.real = param.ep1.real
	data.ep1.imag = param.ep1.imag

	data.ep2.real = param.ep2.real
	data.ep2.imag = param.ep2.imag

	data.la = param.la

	data.ro1 = param.ro1
	data.ro2 = param.ro2

	data.xo1 = param.xo1
	data.xo2 = param.xo2

	data.zo1 = param.zo1
	data.zo2 = param.zo2
	#=============================================================
	WN = 2*N+1

	DoubleArray = c_double * (2*MN*MN)

	PtDoubleArray = POINTER(DoubleArray)	# define a pointer to double array data type

	PTARRAY = PtDoubleArray * 4		# define a pointer array data type consisting of 4 pointers

	SMN = PTARRAY()
	#==============================================================
	SMN[0] = pointer(DoubleArray())
	SMN[1] = pointer(DoubleArray())
	SMN[2] = pointer(DoubleArray())
	SMN[3] = pointer(DoubleArray())
	#==============================================================

	shlib.funb(SMN, data, c_int(N), c_int(MN));

	S11 = np.asarray(list(SMN[0].contents))
	S12 = np.asarray(list(SMN[1].contents))
	S21 = np.asarray(list(SMN[2].contents))
	S22 = np.asarray(list(SMN[3].contents))

	S11 = (S11[::2] + 1.0j*S11[1::2]).reshape((MN, MN)).transpose()
	S12 = (S12[::2] + 1.0j*S12[1::2]).reshape((MN, MN)).transpose()
	S21 = (S21[::2] + 1.0j*S21[1::2]).reshape((MN, MN)).transpose()
	S22 = (S22[::2] + 1.0j*S22[1::2]).reshape((MN, MN)).transpose()

	return S11, S12, S21, S22

#========================================================================================
def MoM_two_in_one_(param, MN, N=7):

	global shlib

	data = DataC()
	#-----------------------------------------------------------------------------
	# Assign variable values to the corresponding structure fields
	#-----------------------------------------------------------------------------
	data.ep1.real = param.ep1.real
	data.ep1.imag = param.ep1.imag

	data.ep2.real = param.ep2.real
	data.ep2.imag = param.ep2.imag

	data.ep3.real = param.ep3.real
	data.ep3.imag = param.ep3.imag

	data.la = param.la

	data.ro1 = param.ro1
	data.ro2 = param.ro2
	data.ro3 = param.ro3

	data.xo1 = param.xo1
	data.xo2 = param.xo2
	data.xo3 = param.xo3

	data.zo1 = param.zo1
	data.zo2 = param.zo2
	data.zo3 = param.zo3
	#=============================================================
	WN = 2*N+1

	DoubleArray = c_double * (2*MN*MN)

	PtDoubleArray = POINTER(DoubleArray)	# define a pointer to double array data type

	PTARRAY = PtDoubleArray * 4		# define a pointer array data type consisting of 4 pointers

	SMN = PTARRAY()
	#==============================================================
	SMN[0] = pointer(DoubleArray())
	SMN[1] = pointer(DoubleArray())
	SMN[2] = pointer(DoubleArray())
	SMN[3] = pointer(DoubleArray())
	#==============================================================

	shlib.func(SMN, data, c_int(N), c_int(MN));

	S11 = np.asarray(list(SMN[0].contents))
	S12 = np.asarray(list(SMN[1].contents))
	S21 = np.asarray(list(SMN[2].contents))
	S22 = np.asarray(list(SMN[3].contents))

	S11 = (S11[::2] + 1.0j*S11[1::2]).reshape((MN, MN)).transpose()
	S12 = (S12[::2] + 1.0j*S12[1::2]).reshape((MN, MN)).transpose()
	S21 = (S21[::2] + 1.0j*S21[1::2]).reshape((MN, MN)).transpose()
	S22 = (S22[::2] + 1.0j*S22[1::2]).reshape((MN, MN)).transpose()

	return S11, S12, S21, S22

def MoM_two_in_two_(param, MN, N=7):

	global shlib

	data = DataD()

	#-----------------------------------------------------------------------------
	# Assign variable values to the corresponding structure fields
	#-----------------------------------------------------------------------------
	data.ep1.real = param.ep1.real
	data.ep1.imag = param.ep1.imag

	data.ep2.real = param.ep2.real
	data.ep2.imag = param.ep2.imag

	data.ep3.real = param.ep3.real
	data.ep3.imag = param.ep3.imag

	data.ep4.real = param.ep4.real
	data.ep4.imag = param.ep4.imag

	data.ep5.real = param.ep5.real
	data.ep5.imag = param.ep5.imag

	data.ep6.real = param.ep6.real
	data.ep6.imag = param.ep6.imag

	data.la = param.la

	data.ro1 = param.ro1
	data.ro2 = param.ro2
	data.ro3 = param.ro3
	data.ro4 = param.ro4
	data.ro5 = param.ro5
	data.ro6 = param.ro6

	data.xo1 = param.xo1
	data.xo2 = param.xo2
	data.xo3 = param.xo3
	data.xo4 = param.xo4
	data.xo5 = param.xo5
	data.xo6 = param.xo6

	data.zo1 = param.zo1
	data.zo2 = param.zo2
	data.zo3 = param.zo3
	data.zo4 = param.zo4
	data.zo5 = param.zo5
	data.zo6 = param.zo6
	#=============================================================
	WN = 2*N+1

	DoubleArray = c_double * (2*MN*MN)

	PtDoubleArray = POINTER(DoubleArray)	# define a pointer to double array data type

	PTARRAY = PtDoubleArray * 4		# define a pointer array data type consisting of 4 pointers

	SMN = PTARRAY()
	#==============================================================
	SMN[0] = pointer(DoubleArray())
	SMN[1] = pointer(DoubleArray())
	SMN[2] = pointer(DoubleArray())
	SMN[3] = pointer(DoubleArray())
	#==============================================================

	shlib.fund(SMN, data, c_int(N), c_int(MN));

	S11 = np.asarray(list(SMN[0].contents))
	S12 = np.asarray(list(SMN[1].contents))
	S21 = np.asarray(list(SMN[2].contents))
	S22 = np.asarray(list(SMN[3].contents))

	S11 = (S11[::2] + 1.0j*S11[1::2]).reshape((MN, MN)).transpose()
	S12 = (S12[::2] + 1.0j*S12[1::2]).reshape((MN, MN)).transpose()
	S21 = (S21[::2] + 1.0j*S21[1::2]).reshape((MN, MN)).transpose()
	S22 = (S22[::2] + 1.0j*S22[1::2]).reshape((MN, MN)).transpose()

	return S11, S12, S21, S22

