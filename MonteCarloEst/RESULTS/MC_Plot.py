import os
import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *
from Derivatives import *

try:
        matplot = True
        from matplotlib import pyplot as plt
except ModuleNotFoundError:
        print("The matplotlib module is not installed.")
        matplot = False


from plot2pdf import *

from Uncertainty_vs_S11 import *

#=======================================================================================
#=======================================================================================
#model = 'two-slabs-guide'
#model = 'three-slabs-guide'
#model = 'three-slabs-free'
#model = 'two-slabs-free'
model = 'tworods'
#model = 'threerods'
#model = 'slab-rod'
#model = 'rod-slab'
#model = 'holed-solid'
#==================================================================
#task 		= 'sensitivity'
#task 		= 'standard'
task 		= 'total'

sensparam 	= 'S11abs'

isPDF 	= True
#isPRINT	= False
#===================================================================
#filename = model + '_var_' + var_param + '_standard_' + sensparam + '_EPM'
#np.save(filename, np.vstack([var_values, std_delta[var_param]]))

#filename = model + '_var_' + var_param + '_total_EPM'
#np.save(filename, np.vstack([var_values, tot_delta]))
#==================================================================================
# PLOTTING CALCULATED DATA
#==================================================================================
#filename = 'two_rods_var_L_sens_S11abs_MCnum_100000.npy'
MCfilename 	= './RESULTS/' + 'two_rods_var_L_sens_tan1_MCnum_100000.npy'
EPMfilename 	= './RESULTS/' + 'tworods_var_L_standard_tan1_EPM.npy'

MCdata 	= np.load(MCfilename)
EPMdata = np.load(EPMfilename)

print(MCdata.shape)
print(EPMdata.shape)

if isPDF:
	pass
#	labels = {'xlabel': xlabels[var_param], 'ylabel': ylabeldelta + ' ' + paramname[param]}
	plot2latex([MCdata[0,:], EPMdata[0,:]], [MCdata[1,:], EPMdata[1,:]], ylabel_vert=True, xDecNum=1)
elif matplot:
#	plt.ylabel(ylabeldelta + ' ' + paramname[param])
	plt.plot(MCdata[0,:], EPMdata[1,:])
	plt.grid()
	plt.show()
else:
	pass
#       print('Calculated data have been saved into a file.')

print('--------------------------------------------------------')
