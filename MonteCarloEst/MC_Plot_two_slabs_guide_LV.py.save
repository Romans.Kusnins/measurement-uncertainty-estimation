import os
import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *
from Derivatives import *

try:
        matplot = True
        from matplotlib import pyplot as plt
except ModuleNotFoundError:
        print("The matplotlib module is not installed.")
        matplot = False


from plot2pdf import *

from Uncertainty_vs_S11 import *
#==================================================================================
xMin 		= None
xMax 		= None
MaxVal 		= None

task 		= 'sensitivity'
#task 		= 'total'
#---------------------------------------
#sensparam 	= 'S11abs'
sensparam 	= 'f'
#---------------------------------------
var_param 	= 'L'
#---------------------------------------
quantity 	= 'var'


#PREFIX = '_29_'
PREFIX = '_4_5_'

MCNUM = 20000
#==================================================================================
isPDF 		= True
#isPRINT	= False
#==================================================================================
xlabels		= dict()

xlabels['ep1']  = 'Palīgslāņa dielektriskā caurlaidība'
xlabels['ep2']  = 'Mērāmā slāņa dielektriskā caurlaidība'
xlabels['tan1'] = 'oss tangent of the auxiliary slab'
xlabels['tan2'] = 'Loss tangent of MUT'
xlabels['d1']   = 'Thickness of the auxiliary slab, mm'
xlabels['d2']   = 'Thickness of the MUT slab, mm'
xlabels['a']    =  'Waveguide width, mm'
xlabels['L']    = 'Separation between the slabs, mm'
xlabels['f']    = 'Frequency, GHz'

paramname       = {'S11abs': '$S_{11}$', 'd1': '$d_{1}$', 'd2': '$d_{2}$', 'L': '$L$', 'ep1': '$\\varepsilon_{\\mathrm{r}}$', 'tan1': '$ \\tan{\\delta}$', 'tan2': '\\tan{\\delta}', 'a': 'a', 'f': 'f'}
#==================================================================================
# PLOTTING CALCULATED DATA
#==================================================================================
FOLDER = './RESULTS_2SL_GUIDE/'

MCSTR = '{:}'.format(MCNUM)

if task == 'sensitivity':
	MCfilename 	= FOLDER + 'two_slabs_guide_var_' + var_param + '_sens_' + sensparam + '_MCnum'+ PREFIX + '_' + MCSTR + '.npy'
	MCfilename_mean	= FOLDER + 'two_slabs_guide_var_' + var_param + '_sens_' + sensparam + '_MCnum_mean'+ PREFIX + '_' + MCSTR + '.npy'
	EPMfilename 	= FOLDER + 'two-slabs-guide_var_' + var_param + '_standard_' + sensparam + '_EPM' + PREFIX + '.npy'
else:
	MCfilename 	= FOLDER + 'two_slabs_guide_var_' + var_param + '_total_MCnum' + PREFIX + '_' + MCSTR + '.npy'
	MCfilename_mean	= FOLDER + 'two_slabs_guide_var_' + var_param + '_total_MCnum_mean' + PREFIX + '_' + MCSTR + '.npy'
	EPMfilename 	= FOLDER + 'two-slabs-guide_var_' + var_param + '_total_EPM' + PREFIX + '.npy'

MCdata 		= np.load(MCfilename)
MCdata_mean 	= np.load(MCfilename_mean)
EPMdata 	= np.load(EPMfilename)
#==================================================================================
if MaxVal != None:
	MCdata_mean[1, MCdata_mean[1,:] > MaxVal]	= np.nan
	MCdata[1, MCdata[1,:] > MaxVal] 		= np.nan
	EPMdata[1, EPMdata[1,:] > MaxVal] 		= np.nan

if xMin != None:
	ID 	= EPMdata[0,:] >= xMin
	EPMdata = EPMdata[:,ID]

	ID 	= MCdata[0,:] >= xMin
	MCdata 	= MCdata[:,ID]

if xMax != None:
	ID 	= EPMdata[0,:] <= xMax
	EPMdata = EPMdata[:,ID]

	ID 	= MCdata[0,:] <= xMax
	MCdata 	= MCdata[:,ID]
#==================================================================================
if isPDF:
	legend = ['EPM', 'MC']

	if task == 'sensitivity':
		filename  = 'TWO_SL_GUIDE_VAR_' + var_param + '_SENS_' + sensparam + PREFIX + '.npy'
		labels = {'xlabel': xlabels[var_param], 'ylabel': 'Sensitivity to ' + paramname[sensparam]}
	else:
		filename  = 'TWO_SL_GUIDE_VAR_' + var_param + '_TOTAL' + PREFIX + '.npy'
		labels = {'xlabel': xlabels[var_param], 'ylabel': 'Total uncertainty'}

	legendpos = [0.7, 0.8]

	if quantity == 'mean':
		filename  = 'TWO_SL_GUIDE_VAR_' + var_param + '_MEAN' + PREFIX + '.npy'
	#	labels = {'xlabel': xlabels[var_param], 'ylabel': ylabeldelta + ' ' + paramname[param]}
		plot2latex(MCdata_mean[0,:], MCdata_mean[1,:], ylabel_vert=True, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, LineType=['','dash'], YZero=False, LegendPosition=legendpos, FileName=filename)
	else:
		plot2latex([EPMdata[0,:], MCdata[0,:]], [EPMdata[1,:], MCdata[1,:]], ylabel_vert=True, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, LineType=['','dash'], LegendPosition=legendpos, FileName=filename)
#	plot2latex([EPMdata[0,:], MCdata[0,:]], [EPMdata[1,:], MCdata[1,:]], ylabel_vert=True, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels)
elif matplot:
#	plt.ylabel(ylabeldelta + ' ' + paramname[param])
	plt.plot(MCdata[0,:], EPMdata[1,:])
	plt.grid()
	plt.show()
else:
	pass
#       print('Calculated data have been saved into a file.')

print('--------------------------------------------------------')
