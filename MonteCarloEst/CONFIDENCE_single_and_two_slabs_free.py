import os
import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *
from Measurement_models import *
from PlottingRoutines import *
import GlobalVARS
#=======================================================================================
def plot2latex_MTTW2020_2():

	# specify the width of the coverage interval for S11
	S11_del	= 0.0025

	# specify the language for the figure legend (default is 'EN')

	# specify the name of the output pdf file

	GlobalVARS.LANG = 'LV'

	if GlobalVARS.LANG == 'LV':
		filename = 'SINGLE_AND_TWO_SLABS_FREE_COVER_LV'
	else:
		filename = 'SINGLE_AND_TWO_SLABS_FREE_COVER'

	fid = CreateLatexFile(filename)
	#========================================================================
	# MEASUREMENT MODEL PARAMETERS
	#========================================================================
	# slab model type: free space ('free') or waveguide model ('guide') (by default free space model is assumed)

	NM = 3
	N  = 5

	model 	= 'two-slabs-free'
	#========================================================================
	modeldata	= dict()
	#========================================================================
	modeldata['ep1']	= 10.2	# MUT complex permittivity
	modeldata['ep2']	= 30.0	# auxiliary slabs complex permittivity

	modeldata['d1']		= 2.6	# measurable slab thickness in mm
	modeldata['d2']		= 2.0	# auxiliary slab thickness in mm

	modeldata['L']		= 13.1	# interslab separation distance in mm
	modeldata['f']		= 10.0	# operating frequency in GHz
	#========================================================================
	epv = np.arange(1, 40, 0.01)

	if model == "tworods":
		yo = Measurement_model_eval_S11(model, epv, modeldata, NM=NM, N=N)
	else:
		yo = Measurement_model_eval_S11(model, epv, modeldata)
	#========================================================================
	ko = epv
	ko = ko/np.max(epv)

	LEN = yo.shape[0]
	#========================================================================
	epl = list();	epc = list();	epu = list(); 	S11 = list()
	for m in range(2):
		if m == 0:
			S11_, epl_, epc_, epu_ = Measurement_model_confidence_interval(model, modeldata, 'conventional', S11_del, NM=NM, N=N)
		else:
			S11_, epl_, epc_, epu_ = Measurement_model_confidence_interval(model, modeldata, 'extended', S11_del, NM=NM, N=N)

		epl.append(epl_); epc.append(epc_); epu.append(epu_); S11.append(S11_)
	#===========================================================================
	linewidth 	= 0.5
	#===========================================================================
	# LABEL PARAMETERS
	#===========================================================================
	xlabel_text 		=  '$\\varepsilon^{\\prime}_{\mathrm{r,mut}}$'
	ylabel_text 		=  '$|{S}_{11}|$'

	hor_delta_marker_text	= '\\varepsilon^{\\prime}_{\\mathrm{r,mut}}'
	hor_value_marker_text  	= '\\Delta\\varepsilon^{\\prime}_{\\mathrm{r,mut}}'

	ver_delta_marker_text	= '\\Delta|{S}_{11}|'
	ver_value_marker_text  	= '|{S}_{11}|'

	xlabelpos 		= 0.3 		# relative position of the horizontal axis label text
	ylabelpos 		= 0.5 		# relative position of the vertical axis label text

	xlabelsep 		= 0.1 		# spacing between the lower side of the graph and the horizontal axis label text
	ylabelsep 		= 0.12 		# spacing between the left side of the graph and the vertical axis label text

	labelx_scale 		= 2.6		# text scale for the x-axis label
	labely_scale 		= 2.4		# text scale for the y-axis label
	#===========================================================================
	# LEGEND PARAMETERS
	#===========================================================================
	legend_position 	= [ 0.2, 0.4 ]	# relative x and y coordinates of the legend
	tickscale 		= 2.2		# scaling coefficient for the ticks labels
	legend_scale 		= 2.0		# scaling coefficient for the legend text
	#===========================================================================
	# SPECIFY LEGEND TEXT
	#===========================================================================
	if GlobalVARS.LANG == 'LV':
		legend_text = ('KBTMM', 'BTDMM')
	else:
		legend_text = ('CSFSM', 'ETSFSM')
	#==============================================================================================================
	horizOpt1 			= dict()
	#==============================================================================================================
	horizOpt1['arrow_hor_sep'] 	= 0.25	# spacing between the right graph side and the marker arrow
	horizOpt1['upparrowlen']	= 0.01	# length of the upper arrow
	horizOpt1['lowarrlen']		= 0.01	# length of the lower arrow
	horizOpt1['side_line_ext']	= 0.02	# amount of the side line extensions
	horizOpt1['botsep']		= 0.01	# spacing between the central line of the bar and the text
	horizOpt1['rightsep']		= 0.30	# spacing between the text box and the graph left side
	horizOpt1['delarrsep']		= 0.05	# spacing between the arrow position and the delta marker text
	horizOpt1['lowtextsep']		= 0.01	# spacing between the lower delta marker line and delta marker text
	#==============================================================================================================
	horizOpt2 			= horizOpt1.copy()

	horizOpt2['arrow_hor_sep'] 	= 0.05	# spacing between the right graph side and the marker arrow
	horizOpt2['rightsep']		= 0.07	# spacing between the text box and the graph left side
	#==============================================================================================================
	vertOpt1 			= dict()
	vertOpt1['fontsize']		= 10.0	# marker font size in Pt
	vertOpt1['rightarrlen'] 	= 0.05	# right delta marker arrow length
	vertOpt1['delmarkersep']	= 0.2	# distance between the delta maker data line and the lower side of the graph
	vertOpt1['botsep'] 		= 0.01	# spacing between the marker lines and the text
	vertOpt1['dellinesep']		= 0.05	# spacing between the left delta marker line and the corresponding text
	vertOpt1['sline_ext']		= 0.02	# extension of the delta marker lines
	vertOpt1['deltaqntsep']		= 0.05	# spacing between the delta marker line and quantity marker line
	vertOpt1['qntlinsep']		= 0.05	# distance between the delta maker line and the marker text
	#==============================================================================================================
	vertOpt2 			= vertOpt1.copy()
	vertOpt2['delmarkersep']	= 0.1	# distance between the delta maker data line and the lower side of the graph
	#==============================================================================================================

	color = list()
	color.append('blue')
	color.append('red')
	#===========================================================================
	# PLOT CURVES
	#===========================================================================
	plotcurves(fid, ko, yo, color, linewidth, LEN)
	#===========================================================================
	# DRAW GRID LINES AND TICKS FOR THE GRAPH
	#===========================================================================
	x = np.linspace(0.0, 1.0, 6)
	y = np.arange(0.0, 1.0+0.1, 0.1)

	grid_line_V(fid, x, x, -0.01, x*np.max(epv), tickscale, [0,1])
	grid_line_H(fid, y, 0.0*np.ones((length(y),)), y, y, tickscale, [0,1])

	axis_new(fid, [0.0, 1.0, 0.0, 1.0], 0.5)
	#===========================================================================
	#===========================================================================
	for n in range(LEN):

		xval = (epl[n]/np.max(epv), epc[n]/np.max(epv), epu[n]/np.max(epv))
		yval = (S11[n] - S11_del, S11[n], S11[n] + S11_del)

		print(S11_del)

		rectangle_rot(fid, [xval[0],     0.0, xval[2]-xval[0],             1.0], 0, [0, 0], 'black', color[n], 0.25, 'corner')
		rectangle_rot(fid, [	0.0, yval[0],             1.0, yval[2]-yval[0]], 0, [0, 0], 'black', color[n], 0.25, 'corner')

		hor_qnty 	= hor_delta_marker_text + '=' + n2s(epc[n], 2, Delim=True)
		hor_delta  	= hor_value_marker_text + '=' + n2s(np.abs(epu[n]-epl[n]), 3, Delim=True)

		ver_qnty	= ver_value_marker_text + '=' + n2s(S11[n], 2, Delim=True)
		ver_delta 	= ver_delta_marker_text + '=' + n2s(2.0*S11_del, 3, Delim=True)

		if n == 0:
			horis_lines(fid, filename, yval, color[n], 0.25, color[n], 2, (ver_qnty, ver_delta), horizOpt=horizOpt1, index=n+1)
			vert_lines(fid, filename, xval, color[n], 0.25, color[n], 2, (hor_qnty, hor_delta), vertOpt=vertOpt1, index=n+1)
		else:
			horis_lines(fid, filename, yval, color[n], 0.25, color[n], 2, (ver_qnty, ver_delta), horizOpt=horizOpt2, index=n+1)
			vert_lines_no(fid, filename, xval, color[n], 0.25, color[n], 2, (hor_delta), vertnoOpt=vertOpt2, index=n+1)
	#===============================================================================
	fid.write('\\node[scale = ' + n2s(labelx_scale) + '](xlabel) at (' + n2s(xlabelpos) + ', ' + n2s(-xlabelsep) + '){' + xlabel_text + '};\n')
	fid.write('\\node[scale = ' + n2s(labely_scale) + '](ylabel) at (' + n2s(-ylabelsep) + ',' + n2s(ylabelpos)+ '){' + ylabel_text + '};\n')
	#===========================================================================
	# CREATE A LEGEND BOX
	#===========================================================================
#	legend_box(fid, filename, LEN, legend_position, color, legend_text, legend_scale, 'white', 1)
	legend_options = {'opacity': 1.0, 'colour': 'white', 'linecolour': 'white'}
	legend_box(fid, filename, LEN,legend_position, color, legend_text, legend_scale, **legend_options)
	#===========================================================================
	fid.write('\end{tikzpicture} \n')
	fid.write('\end{document} \n')

	fid.close()
	#===========================================================================
#	os.system('pdflatex MTTW_FIG_2.tex > /dev/null')
	os.system('pdflatex ' + filename + '.tex > /dev/null 2>&1')

	# open the generated PDF file with the default linux viewer
	os.system('xdg-open ' + filename + '.pdf')
#====================================================================================

plot2latex_MTTW2020_2()
