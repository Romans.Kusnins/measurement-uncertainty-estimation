import numpy as np

try:
	matplot = True
	from matplotlib import pyplot as plt
except ModuleNotFoundError:
	print("The matplotlib module is not installed.")
	matplot = False

from slab_res3md import (root_finding, POW2)
from MoM import *
#===========================================================================
#===========================================================================
ep1     = 7.2			# auxiliary holed rod dielectric constant
ep2     = 9.0			# MUT holed rod dielectric constant

ro1     = 5.0			# auxiliary rod radius in mm
ro2     = 5.0                   # MUT rod radius in mm

ro1in   = 0.5			# auxiliary rod hole radius in mm
ro2in   = 0.5			# MUT rod hole radius in mm

d1      = 2.0                   # auxiliary rod hole-to-hole distance in mm
d2      = 2.0                   # MUT rod hole-to-tole distance in mm

ph1     = 0.0                   # auxiliary rod hole line angle in radians
ph2     = 0.0                   # MUT rod hole line angle in radians

L       = 30.0			# holed-to-solid rod separation distance in mm
f       = 10.0			# operating frequency in GHz

a       = 22.86                 # waveguide width in mm

M   	= 10000			# Monte-Carlo algorithm trial number
MAXIT 	= 100			# max number of iterations for Newton's method
NTOL 	= 1e-4			# solution tolerance for Newton's method

NM	= 3			# number of higher order waveguide modes considered
N	= 5			# number of basis functions for each rod surface
#=========================================================================
MoMinit()

lam = 300.0/f
#=========================================================================
def evalparam(roddata):

	param           = MoMparam()

	param.ep1       = roddata['ep'] + 0.0j
	param.ep2       = 1.0 + 0.0j
	param.ep3       = 1.0 + 0.0j

	param.la        = a/lam

	param.ro1       = roddata['ro']/a
	param.ro2       = roddata['roin']/a
	param.ro3       = roddata['roin']/a

	param.xo1       = 0.5
	param.xo2       = 0.5 + 0.5*roddata['d']/a*np.cos(roddata['ph'])
	param.xo3       = 0.5 - 0.5*roddata['d']/a*np.cos(roddata['ph'])

	param.zo1       = 0.0
	param.zo2       = 0.5 - 0.5*roddata['d']/a*np.sin(roddata['ph'])
	param.zo3       = 0.5 + 0.5*roddata['d']/a*np.sin(roddata['ph'])

	return param
#=========================================================================
def MoM_calc_S_matrix(roddata):
	param = evalparam(roddata)
	S11a, S12a, S21a, S22a = MoM_two_in_one_(param, NM, N)
	return S11a, S12a, S21a, S22a
#=========================================================================
def calcS11(S11b):
	if NM == 1:
		S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
	else:
		S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

	return S11[0,0]
#==========================================================================
def calcT(L, lam):
	if NM == 1:
		T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
	else:
		T = np.eye(NM, dtype='complex')
		for n in range(NM):
			T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)

	return T
#==========================================================================
if matplot:

	epv = np.arange(1, 50, 0.01)

	S11abs = np.ndarray((epv.shape))
	S11ang = np.ndarray((epv.shape))

	T = calcT(L, lam)

	roddata1 = {'ep': ep1, 'ro': ro1, 'roin': ro1in, 'd': d1, 'ph': ph1}
	S11a, S12a, S21a, S22a = MoM_calc_S_matrix(roddata1)

	for n in range(epv.shape[0]):

		roddata2	= {'ep': epv[n], 'ro': ro2, 'roin': ro2in, 'd': d2, 'ph': ph2}
		param	 	= evalparam(roddata2)

		S11b, S12b, S21b, S22b = MoM_two_in_one_(param, NM, N)

		S11abs[n] = np.abs(calcS11(S11b))
		S11ang[n] = np.angle(calcS11(S11b))
	#------------------------------------------------------------------
	plt.plot(epv, S11abs)
	plt.grid()
	plt.show()

	plt.plot(epv, S11ang)
	plt.grid()
	plt.show()
#==========================================================================
# calculate S11meas - the total reflection coefficient
#==========================================================================
S11a, S12a, S21a, S22a = MoM_calc_S_matrix(roddata1)
T = calcT(L, lam)

roddata2 = {'ep': ep2, 'ro': ro2, 'roin': ro2in, 'd': d2, 'ph': ph2}
S11b, S12b, S21b, S22b = MoM_calc_S_matrix(roddata2)
S11meas = np.abs(calcS11(S11b))
# calculate the derivative of S11meas
roddata2 = {'ep': ep2+1e-3, 'ro': ro2, 'roin': ro2in, 'd': d2, 'ph': ph2}
S11b, S12b, S21b, S22b = MoM_calc_S_matrix(roddata2)
S11meas_p = np.abs(calcS11(S11b))

S11meas_der = (S11meas_p - S11meas)/1e-3
#--------------------------------------------------------------------------
# EVALUATING MEASUREMENT UNCERTAINTY
#--------------------------------------------------------------------------
sigma_S11abs 	= 0.010

sigma_ro1 	= 0.015
sigma_ro1in 	= 0.015
sigma_d1 	= 0.015
sigma_ph1 	= 0.03

sigma_ro2	= 0.015
sigma_ro2in 	= 0.015
sigma_d2 	= 0.015
sigma_ph2 	= 0.03

sigma_L 	= 0.015

epm 		= np.ndarray((M,))
#--------------------------------------------------------------------------
def func(epvar, **kwd):
	param.ep1 		= epvar
	S11b, S12b, S21b, S22b 	= MoM_two_in_one_(param, NM, N)
	return  np.abs(calcS11(S11b))
#--------------------------------------------------------------------------
for n in range(M):

	del_S11abs	= np.random.normal(0.0,	sigma_S11abs)
	del_ro1		= np.random.normal(0.0,	sigma_ro1)
	del_ro1in	= np.random.normal(0.0,	sigma_ro1in)
	del_d1		= np.random.normal(0.0,	sigma_d1)
	del_ph1		= np.random.normal(0.0,	sigma_ph1)

	del_ro2		= np.random.normal(0.0,	sigma_ro2)
	del_ro2in	= np.random.normal(0.0,	sigma_ro2in)
	del_d2		= np.random.normal(0.0,	sigma_d2)
	del_ph2		= np.random.normal(0.0,	sigma_ph2)

	del_L		= np.random.normal(0.0,	sigma_L)

	roddata1 	= {'ep': ep1, 'ro': ro1 + del_ro1, 'roin': ro1in + del_ro1in, 'd': d1 + del_d1, 'ph': ph1 + del_ph1}
	S11a, S12a, S21a, S22a  = MoM_calc_S_matrix(roddata1)

	T 		= calcT(L + del_L, lam)

	roddata2 	= {'ep': ep2, 'ro': ro2 + del_ro2, 'roin': ro2in + del_ro2in, 'd': d2 + del_d2, 'ph': ph2 + del_ph2}
	param 		= evalparam(roddata2)

	epm[n] 		= root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11meas_der, val=S11meas, delta=del_S11abs)
#==========================================================================
# DATA POSTPROCESSING
#==========================================================================
id_nan 	= np.isnan(epm)
epm 	= epm[np.logical_not(id_nan)]
print(epm.shape)

print(np.mean(epm))
print(np.sqrt(np.var(epm)))
