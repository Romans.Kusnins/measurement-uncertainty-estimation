import numpy as np

try:
	matplot = True
	from matplotlib import pyplot as plt
except ModuleNotFoundError:
	print("The matplotlib module is not installed.")
	matplot = False

from slab_res3md import (root_finding, POW2)
from MoM import *
#==========================================================================
#==========================================================================
ep1 	= 4.2		# auxiliary dielectric constant
ep2 	= 12.5		# MUT rod dielectric constant

ro1  	= 7.0 		# auxiliary rod radius in mm
ro2  	= 3.5		# MUT rod radius in mm

L   	= 30.0		# interrod separation distance in mm
f   	= 10.0		# operating frequency in GHz

a   	= 22.86		# waveguide width in mm

M   	= 10000		# Monte-Carlo algorithm trial number
MAXIT 	= 100		# max number of iterations for Newton's method
NTOL 	= 1e-4		# solution tolerance for Newton's method

NM	= 3		# number of higher order waveguide modes considered
N	= 5		# number of basis functions for each rod surface
#==========================================================================
MoMinit()		# C++ library initialization

lam = 300.0/f
#==========================================================================
def evalparam(ep, ro):
	param 		= MoMparam()

	param.ep 	= ep
	param.la 	= a/lam
	param.ro 	= ro/a
	param.xo 	= 0.5
	param.zo 	= 0.0

	return param
#==========================================================================
def MoM_calc_S_matrix(ep, ro):
	param = evalparam(ep, ro)
	S11a, S12a, S21a, S22a = MoM_single_solid_rod_(param, NM, N)
	return S11a, S12a, S21a, S22a
#==========================================================================
def calcS11(S11b):
	if NM == 1:
		S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
	else:
		S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

	return S11[0,0]
#==========================================================================
def calcT(L, lam):
	if NM == 1:
		T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
	else:
		T = np.eye(NM, dtype='complex')
		for n in range(NM):
			T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)

	return T
#==========================================================================
if matplot:

	epv = np.arange(1, 50, 0.01)

	S11abs = np.ndarray((epv.shape))
	S11ang = np.ndarray((epv.shape))

	T = calcT(L, lam)

	S11a, S12a, S21a, S22a = MoM_calc_S_matrix(ep1, ro1)

	for n in range(epv.shape[0]):

		param = evalparam(epv[n], ro2)
		S11b, S12b, S21b, S22b = MoM_single_solid_rod_(param, NM, N)
		S11abs[n] = np.abs(calcS11(S11b))
		S11ang[n] = np.angle(calcS11(S11b))
	#------------------------------------------------------------------
	plt.plot(epv, S11abs)
	plt.grid()
	plt.show()

	plt.plot(epv, S11ang)
	plt.grid()
	plt.show()
#==========================================================================
# calculate S11meas - the total reflection coefficient
#==========================================================================
S11a, S12a, S21a, S22a = MoM_calc_S_matrix(ep1, ro1)
T = calcT(L, lam)
S11b, S12b, S21b, S22b = MoM_calc_S_matrix(ep2, ro2)
S11meas = np.abs(calcS11(S11b))
#==========================================================================
# calculate the derivative of S11meas
#==========================================================================
S11b, S12b, S21b, S22b = MoM_calc_S_matrix(ep2+1e-3, ro2)
S11meas_p = np.abs(calcS11(S11b))
S11meas_der = (S11meas_p - S11meas)/1e-3
#--------------------------------------------------------------------------
# EVALUATING MEASUREMENT UNCERTAINTY
#--------------------------------------------------------------------------
sigma_S11abs 	= 0.01
sigma_ro1 	= 0.015
sigma_ro2	= 0.015
sigma_L 	= 0.015

epm 		= np.ndarray((M,))
#==========================================================================
def func(epvar, **kwd):
	param.ep 		= epvar
	S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
	return  np.abs(calcS11(S11b))
#==========================================================================
for n in range(M):

	del_S11abs 		= np.random.normal(S11meas, sigma_S11abs)
	del_ro1			= np.random.normal(ro1,	sigma_ro1)
	del_ro2	 		= np.random.normal(ro2,	sigma_ro2)
	del_L	 		= np.random.normal(L,	sigma_L)

	S11a, S12a, S21a, S22a  = MoM_calc_S_matrix(ep1, del_ro1)

	T 			= calcT(del_L, lam)

	param 			= evalparam(ep2, del_ro2)

	epm[n] 			= root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11meas_der, val=del_S11abs)
#==========================================================================
id_nan 	= np.isnan(epm)
epm 	= epm[np.logical_not(id_nan)]
print(epm.shape)

print(np.mean(epm))
print(np.sqrt(np.var(epm)))
