import numpy as np
from matplotlib import pyplot as plt
from slab_res3md import (three_slab_free_space, POW2)
from plot2pdf import *

#===========================================================================
ep1     = [10.2]		# auxiliary slab dielectric constant (Arlon AD1000 ep=10.2)
ep2     = [1.0, 40.0, 0.1]	# MUT slab dielectric constant (exxelia.com eps=30.0)
#ep2    = [30.0]		# MUT slab dielectric constant (exxelia.com eps=30.0)
#ep2     = [34.0]		# MUT slab dielectric constant (exxelia.com eps=30.0)

epc     = [2.2]			# middle slab dielectric constant (Duroid 5880 eps-2.2, tan-0.0009)

d1	= [5.7]			# auxiliary slab thickness in mm
#d1	= [5.05]		# auxiliary slab thickness in mm
#d1	= [5.7]			# auxiliary slab thickness in mm
#d2	= [1.5, 3.5, 0.05]	# MUT slab thickness in mm
d2	= [2.3]			# MUT slab thickness in mm

#L      = [1.0, 30.0, 0.1]	# interslab separation distance in mm
#L       = [29.0]		# interslab separation distance in mm (old value)
#L      = [3.6]			# interslab separation distance in mm
#L      = [8.5]			# interslab separation distance in mm
L       = [9.6]			# interslab separation distance in mm

f       = [10.0]		# operating frequency in GHz
#===========================================================================
isPDF   = False                 # generate pdf file with the plot
#===========================================================================

def calcS11(slabdata1, slabdata2, epc, L, lam):

	ep1 	= slabdata1['ep']
	ep2 	= slabdata2['ep']

	d1 	= slabdata1['d']
	d2 	= slabdata2['d']

	S11 	= three_slab_free_space(ep1, epc, ep2, d1/lam, d2/lam, L/lam)

	return S11
#===========================================================================
xlabels  = dict()
xlabels1 = dict()
xlabels2 = dict()

xlabels1['ep'] 	= 'MUT dielectric constant, mm'
xlabels2['ep'] 	= 'Auxiliary slab dielectric constant, mm'

xlabels1['d'] 	= 'MUT slab thickness, mm'
xlabels2['d'] 	= 'auxiliary slab thickness, mm'

xlabels['epc']	= 'Middle slab dielectric constant, mm'
xlabels['L'] 	= 'Rod separation distance , mm'
xlabels['f'] 	= 'Frequency, GHz'
#============================================================================
slabdata1 = {'ep': ep1, 'd': d1}
slabdata2 = {'ep': ep2, 'd': d2}
#============================================================================

list_found 	= False

isSlab1		= False
isSlab2		= False

for par in slabdata1.keys():
	if type(slabdata1[par]) == list:
		if len(slabdata1[par]) > 1 and not list_found:
			var_range	= slabdata1[par];
			parname 	= par
			isSlab1		= True
			list_found 	= True
		else:
			slabdata1[par] = slabdata1[par][0];
for par in slabdata2.keys():
	if type(slabdata2[par]) == list:
		if len(slabdata2[par]) > 1 and not list_found:
			var_range	= slabdata2[par];
			parname 	= par
			isSlab2		= True
			list_found 	= True
		else:
			slabdata2[par] = slabdata2[par][0];

if type(epc) == list:
	if len(epc) > 1 and not list_found:
		var_range 	=  epc
		parname 	= 'epc'
		list_found 	= True
	else:
		epc = epc[0]

if type(L) == list:
	if len(L) > 1 and not list_found:
		var_range 	=  L
		parname 	= 'L'
		list_found 	= True
	else:
		L = L[0]

if type(f) == list:
	if len(f) > 1 and not list_found:
		var_range 	=  f
		parname 	= 'f'
		list_found 	= True
	else:
		f = f[0]

if not list_found:
	print("There is nothing to plot.")
	quit()
#=====================================================================
var_values 	= np.arange(*var_range)
S11 		= np.ndarray((var_values.shape), dtype='complex')

if isSlab1:
	lam = 300.0/f
	for n in range(var_values.shape[0]):
		slabdata1[parname] = var_values[n]
		S11[n] = calcS11(slabdata1, slabdata2, epc, L, lam)
elif isSlab2:
	lam = 300.0/f
	for n in range(var_values.shape[0]):
		slabdata2[parname] = var_values[n]
		S11[n] = calcS11(slabdata1, slabdata2, epc, L, lam)
else:
	if parname == 'epc':
		lam = 300.0/f
		for n in range(var_values.shape[0]):
			S11[n] = calcS11(slabdata1, slabdata2, var_values[n], L, lam)
	elif parname == 'L':
		lam = 300.0/f
		for n in range(var_values.shape[0]):
			S11[n] = calcS11(slabdata1, slabdata2, epc, var_values[n], lam)
	else:
		for n in range(var_values.shape[0]):
			lam = 300.0/var_values[n]
			S11[n] = calcS11(slabdata1, slabdata2, epc, L, lam)
#====================================================================================
if isPDF:
        if isSlab1:
                labels ={'xlabel': xlabels1[parname], 'ylabel': '$|S_{11}|$'}
        elif isSlab2:
                labels ={'xlabel': xlabels2[parname], 'ylabel': '$|S_{11}|$'}
        else:
                labels ={'xlabel': xlabels[parname],  'ylabel': '$|S_{11}|$'}

        plot2latex(var_values, np.abs(S11), labels=labels, legend=['three slab model'])

else:
        plt.plot(var_values, np.abs(S11))

        if isSlab1:
                plt.xlabel(xlabels1[parname])
        elif isSlab2:
                plt.xlabel(xlabels2[parname])
        else:
                plt.xlabel(xlabels[parname])

        plt.ylabel('|S11|')
        plt.grid()
        plt.show()
