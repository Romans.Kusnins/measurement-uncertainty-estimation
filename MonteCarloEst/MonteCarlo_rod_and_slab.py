import numpy as np

try:
	matplot = True
	from matplotlib import pyplot as plt
except ModuleNotFoundError:
	print("The matplotlib module is not installed.")
	matplot = False

from slab_res3md import (root_finding, POW2, single_slab_guide_matrix)
from MoM import *
#===========================================================================
#===========================================================================
ro1  	= 12.0 		# auxiliary rod radius in mm
d2  	= 4.7		# MUT slab thickness in mm

L   	= 19.5		# rod-to-slab separation in mm
f   	= 10.0		# operating frequency in GHz

ep1 	= 4.2		# MUT slab dielectric constant
ep2 	= 12.5		# auxiliary slabs dielectric constant

a   	= 22.86		# waveguide width in mm

M   	= 10000		# Monte-Carlo algorithm trial number
MAXIT 	= 100		# max number of iterations for Newton's method
NTOL 	= 1e-4		# solution tolerance for Newton's method

NM	= 3		# number of higher order waveguide modes considered
N	= 5		# number of basis functions for each rod surface
#===========================================================================
MoMinit()

lam = 300.0/f
#===========================================================================
def evalparam(ep, ro):

	param 		= MoMparam()

	param.ep 	= ep
	param.la 	= a/lam
	param.ro 	= ro/a
	param.xo 	= 0.5
	param.zo 	= 0.0

	return param
#===========================================================================
def MoM_calc_S_matrix(ep, ro):
	param = evalparam(ep, ro)
	S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
	if NM == 1:
		return S11[0,0], S12[0,0], S21[0,0], S22[0,0]
	else:
		return S11, S12, S21, S22
#===========================================================================
def calcS11(S11b):
	if NM == 1:
		S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		return S11
	else:
		S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a
		return S11[0,0]
#===========================================================================
def calcT(L, lam):
	if NM == 1:
		T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
	else:
		T = np.eye(NM, dtype='complex')
		for n in range(NM):
			T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
	return T
#===========================================================================
if matplot:

	epv = np.arange(1, 20, 0.01)

	S11abs = np.ndarray((epv.shape))
	S11ang = np.ndarray((epv.shape))

	T = calcT(L, lam)

	S11a, S12a, S21a, S22a = MoM_calc_S_matrix(ep1, ro1)

	for n in range(epv.shape[0]):

		S11b, S12b, S21b, S22b = single_slab_guide_matrix(a/lam, epv[n], d2/a, NM)

		S11abs[n] = np.abs(calcS11(S11b))
		S11ang[n] = np.angle(calcS11(S11b))
	#-------------------------------------------------------------------
	plt.plot(epv, S11abs)
	plt.grid()
	plt.show()
#===========================================================================
# calculate S11meas - the total reflection coefficient
#===========================================================================
S11a, S12a, S21a, S22a = MoM_calc_S_matrix(ep1, ro1)
T = calcT(L, lam)
S11b, S12b, S21b, S22b = single_slab_guide_matrix(a/lam, ep2, d2/a, NM)
S11meas = np.abs(calcS11(S11b))
#===========================================================================
# calculate the derivative of S11meas
#===========================================================================
S11b, S12b, S21b, S22b = single_slab_guide_matrix(a/lam, ep2+1e-3, d2/a, NM)
S11meas_p = np.abs(calcS11(S11b))

S11meas_der = (S11meas_p - S11meas)/1e-3
#---------------------------------------------------------------------------
# EVALUATING MEASUREMENT UNCERTAINTY
#---------------------------------------------------------------------------
sigma_S11abs 	= 0.01
sigma_ro1 	= 0.015
sigma_d2	= 0.015
sigma_L 	= 0.015

epm 		= np.ndarray((M,))
#===========================================================================
def func(epvar, **kwd):

	S11b, S12b, S21b, S22b = single_slab_guide_matrix(a/lam, epvar, (d2+del_d2)/a, NM)

	if NM == 1:
		S11b = S11b[0,0]
		S12b = S12b[0,0]
		S21b = S21b[0,0]
		S22b = S22b[0,0]

	return  np.abs(calcS11(S11b))
#===========================================================================
for n in range(M):

	del_S11abs 		= np.random.normal(0.0,	sigma_S11abs)
	del_ro1			= np.random.normal(0.0,	sigma_ro1)
	del_d2	 		= np.random.normal(0.0,	sigma_d2)
	del_L	 		= np.random.normal(0.0,	sigma_L)

	S11a, S12a, S21a, S22a = MoM_calc_S_matrix(ep1, ro1+del_ro1)

	T 			= calcT(L + del_L, lam)

	epm[n] 			= root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11meas_der, val=S11meas, delta=del_S11abs)
#===========================================================================
id_nan 	= np.isnan(epm)
epm 	= epm[np.logical_not(id_nan)]
print(epm.shape)

print(np.mean(epm))
print(np.sqrt(np.var(epm)))
