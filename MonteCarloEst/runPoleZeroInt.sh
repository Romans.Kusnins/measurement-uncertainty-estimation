#!/bin/bash

MPATH=/home/student_etf/GitMonteCarlo/measurement-uncertainty-estimation/MonteCarloEst

cd $MPATH

rm -f ./PoleZeroInit

mpic++  PoleZeroInit.cpp -o PoleZeroInit -L./lib -lrod -lewald -lslatec -llapack -lrefblas -lgfortran -DMP

mpirun ./PoleZeroInit

cd ~
