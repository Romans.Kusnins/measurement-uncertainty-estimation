import numpy as np
from matplotlib import pyplot as plt
from slab_res3md import (three_slab_free_space, root_finding, POW2)

#------------------------------------------------------------------------
d1  	= 10.0 		# measurable slab thickness in mm
d2  	= 6.0		# auxiliary slab thickness in mm
L   	= 20.0		# interslab separation distance in mm
f   	= 9.0		# operating frequency in GHz
ep1 	= 4.7		# MUT complex permittivity
ep2 	= 12.5		# auxiliary slabs complex permittivity

M   	= 10000		# Monte-Carlo algorithm trial number
MAXIT 	= 1000		# max number of iterations for Newton's method
NTOL 	= 1e-4		# solution tolerance for Newton's method
#-------------------------------------------------------------------------

lam = 300.0/f;

epv = np.arange(1, 20, 0.01)

S11abs = np.ndarray((epv.shape))

for n in range(epv.shape[0]):
	S11abs[n] = np.abs(three_slab_free_space(ep1, 1.0, epv[n], d1/lam, d2/lam, L/lam))

plt.plot(epv, S11abs)
plt.grid()
plt.show()

S11mes = three_slab_free_space(ep1, 1.0, ep2, d1/lam, d2/lam, L/lam)
#-----------------------------------------------------------------
# EVALUATING MEASUREMENT UNCERTAINTY
#-----------------------------------------------------------------
sigma_S11abs 	= 0.010
sigma_d1 	= 0.015
sigma_d2	= 0.015
sigma_L 	= 0.015

vec_S11abs 	= np.random.normal(np.abs(S11mes), sigma_S11abs, M)
vec_d1 		= np.random.normal(d1, sigma_d1, M)
vec_d2	 	= np.random.normal(d2, sigma_d2, M)
vec_L	 	= np.random.normal(L,  sigma_L,  M)

epm 		= np.ndarray((M,))


for n in range(M):

	del_d1 		= vec_d1[n]
	del_d2		= vec_d2[n]
	del_L  		= vec_L[n]
	del_S11abs 	= vec_S11abs[n]

	def func(epm):
		S11 = three_slab_free_space(ep1, 1.0, epm, del_d1/lam, del_d2/lam, del_L/lam)
		return np.abs(S11) - del_S11abs

	epm[n] = root_finding(func, ep2, 1e-6, NTOL, MAXIT)
	#print(epm[n])

print(np.mean(epm))
print(np.sqrt(np.var(epm)))
