import numpy as np
from matplotlib import pyplot as plt
from slab_res3md import (POW2)
from MoM import *
from plot2pdf import *
#===========================================================================
ep1     = [4.2]			# auxiliary dielectric constant
ep2     = [1.0, 20.0, 0.01]	# MUT dielectric constant

ro1     = [5.0]		     	# auxiliary rod radius in mm
ro2     = 4.7			# MUT rod radius in mm

ro1in   = [1.5]           	# auxiliary rod hole radius in mm
d1  	= [4.0] 	   	# auxiliary rod hole-to-hole separation in mm
ph1  	= 0.0*np.pi/2.0        	# auxiliary hole line angle in radians

L       = [27.0]		# holed-to-solid rod separation distance in mm
f       = [10.0]		# operating frequency in GHz

a       = 22.86			# waveguide width in mm
#===========================================================================
NM      = 3			# number of higher order waveguide modes considered
N       = 5			# number of basis functions for each rod surface
#===========================================================================
isPDF	= True			# generatea a PDF file with curve plots
#===========================================================================
MoMinit()			# perform library initialization
#===========================================================================
def evalparam_holed(roddata, lam):

	param 		= MoMparam()

	param.ep1 	= roddata['ep'] + 0.0j
	param.ep2 	= 1.0 + 0.0j
	param.ep3 	= 1.0 + 0.0j

	param.la 	= a/lam

	param.ro1 	= roddata['ro']/a
	param.ro2 	= roddata['roin']/a
	param.ro3 	= roddata['roin']/a

	param.xo1 	= 0.5
	param.xo2 	= 0.5 + 0.5*roddata['d']/a*np.cos(roddata['ph'])
	param.xo3 	= 0.5 - 0.5*roddata['d']/a*np.cos(roddata['ph'])

	param.zo1 	= 0.0
	param.zo2 	= 0.5 - 0.5*roddata['d']/a*np.sin(roddata['ph'])
	param.zo3 	= 0.5 + 0.5*roddata['d']/a*np.sin(roddata['ph'])

	return param
#===========================================================================
def evalparam_solid(roddata, lam):

	param 		= MoMparam()

	param.ep 	= roddata['ep'] + 0.0j
	param.la 	= a/lam

	param.ro 	= roddata['ro']/a
	param.xo 	= 0.5
	param.zo 	= 0.0

	return param
#===========================================================================
def calcT(L, lam):
	if NM == 1:
		T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
	else:
		T = np.eye(NM, dtype='complex')
		for n in range(NM):
			T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)

	return T
#===========================================================================
def holed_calc_S_matrix(roddata, lam):

	param = evalparam_holed(roddata, lam)
	S11, S12, S21, S22 = MoM_two_in_one_(param, NM, N)

	return S11, S12, S21, S22
#===========================================================================
def solid_calc_S_matrix(roddata, lam):

	param = evalparam_solid(roddata, lam)
	S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)

	return S11, S12, S21, S22
#===========================================================================
def calcS11(roddata1, roddata2, L, lam):

	S11a, S12a, S21a, S22a = holed_calc_S_matrix(roddata1, lam)
	S11b, S12b, S21b, S22b = solid_calc_S_matrix(roddata2, lam)

	T = calcT(L, lam)

	if NM == 1:
		S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
	else:
		S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

	return S11[0,0]
#===========================================================================
xlabels  = dict()
xlabels1 = dict()
xlabels2 = dict()

xlabels1['ep'] 		= '1st rod dielectric constant, mm'
xlabels2['ep'] 		= '2nd rod dielectric constant, mm'

xlabels1['ro'] 		= '1st rod radius, mm'
xlabels2['ro'] 		= '2nd rod radius, mm'

xlabels1['roin'] 	= '1st rod hole radius, mm'
xlabels1['d']	 	= '1st rod interhole distance, mm'
xlabels1['ph']	 	= '1st rod hole angle, rad'

xlabels['L'] 		= 'Rod separation distance , mm'
xlabels['f'] 		= 'Frequency, GHz'
#============================================================================
roddata1 = {'ep': ep1, 'ro': ro1, 'roin': ro1in, 'd': d1, 'ph': ph1}
roddata2 = {'ep': ep2, 'ro': ro2}
#============================================================================

list_found 	= False

isRod1  	= False
isRod2  	= False

for par in roddata1.keys():
	if type(roddata1[par]) == list:
		if len(roddata1[par]) > 1 and not list_found:
			var_range	= roddata1[par];
			parname 	= par
			isRod1  	= True
			list_found 	= True
		else:
			roddata1[par] = roddata1[par][0];
for par in roddata2.keys():
	if type(roddata2[par]) == list:
		if len(roddata2[par]) > 1 and not list_found:
			var_range	= roddata2[par];
			parname 	= par
			isRod2  	= True
			list_found 	= True
		else:
			roddata2[par] = roddata2[par][0];


if type(L) == list:
	if len(L) > 1 and not list_found:
		var_range 	=  L
		parname 	= 'L'
		list_found 	= True
	else:
		L = L[0]

if type(f) == list:
	if len(f) > 1 and not list_found:
		var_range 	=  f
		parname 	= 'f'
		list_found 	= True
	else:
		f = f[0]

if not list_found:
	print("There is nothing to plot.")
	quit()
#=====================================================================
var_values 	= np.arange(*var_range)
S11 		= np.ndarray((var_values.shape), dtype='complex')

if isRod1:
	lam = 300.0/f
	for n in range(var_values.shape[0]):
		roddata1[parname] = var_values[n]
		S11[n] = calcS11(roddata1, roddata2, L, lam)
elif isRod2:
	lam = 300.0/f
	for n in range(var_values.shape[0]):
		roddata2[parname] = var_values[n]
		S11[n] = calcS11(roddata1, roddata2, L, lam)
else:
	if parname == 'L':
		lam = 300.0/f
		for n in range(var_values.shape[0]):
			S11[n] = calcS11(roddata1, roddata2, var_values[n], lam)
	else:
		for n in range(var_values.shape[0]):
			lam = 300.0/var_values[n]
			S11[n] = calcS11(roddata1, roddata2, L, lam)
#============================================================================
if isPDF:
	if isRod1:
		labels ={'xlabel': xlabels1[parname], 'ylabel': '$|S_{11}|$'}
	elif isRod2:
		labels ={'xlabel': xlabels2[parname], 'ylabel': '$|S_{11}|$'}
	else:
		labels ={'xlabel': xlabels[parname],  'ylabel': '$|S_{11}|$'}

	plot2latex(var_values, np.abs(S11), labels=labels, legend=['holed-solid rod model'])

else:

	plt.plot(var_values, np.abs(S11))

	if isRod1:
		plt.xlabel(xlabels1[parname])
	elif isRod2:
		plt.xlabel(xlabels2[parname])
	else:
		plt.xlabel(xlabels[parname])

	plt.ylabel('|S11|')
	plt.grid()
	plt.show()

