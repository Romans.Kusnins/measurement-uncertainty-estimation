import numpy as np
from MoM import *

N = 7

param = MoMparam()

param.ep  = 4.2

param.la  = 0.75

param.ro  = 0.2

param.xo  = 0.75

param.zo =  0.0

#=====================================================
# ENTERING USER DATA, SUHC AS A MODE NUMBER.
#=====================================================
MN = int(input("Enter the mode number: "))
#=====================================================
# INITIALIZE THE SHARED C FUNCTION LIBRARY
#=====================================================
MoMinit()
#=====================================================

S11, S12, S21, S22 = MoM_single_solid_rod_(param, MN, N)

#=====================================================
# POSTPROCESSING
#=====================================================
print("S11 matrix:")
for row in S11:
	for s11 in row:
        	print(s11)

print("\nS12 matrix:")
for row in S12:
	for s12 in row:
        	print(s12)

print("\nS21 matrix:")
for row in S21:
	for s21 in row:
	       	print(s21)

print("\nS22 matrix:")
for row in S22:
	for s22 in row:
        	print(s22)

print("\n\n")
