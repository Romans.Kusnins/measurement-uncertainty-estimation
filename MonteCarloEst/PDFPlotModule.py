import os
import numpy as np
from slab_res3md import (POW2)
from Confidence_interval_calculation import *

#=======================================================================================
def length(x):
	if type(x) is np.ndarray:
		return max(x.shape)
	else:
		return len(x)
#=======================================================================================
def n2s(x, *numdec):
	if abs(x-int(x)) < 1e-15:	# Check whether x is an integer or not.
		return '%d' % (int(x))	# If this is the case, omit the comma 					# and the fractional part
	else:
		if len(numdec)>0 and int(numdec[0])>0:# if the number of decimal places is 					# specified print, the number with the 
					# numdec decimal>0 places, else display 6 decimal places 					# decimal places
			fstr = "%%1.%df" % (int(numdec[0]))
			return fstr % (x)
		else:
			return '%1.6f' % (x)
#=======================================================================================
def line(fd, x1, y1, x2, y2, colour, lineopt):
	str  = '\\draw[' + colour + ',' + lineopt + '] ('
	str += n2s(x1) + ',' + n2s(y1) + ') -- (' + n2s(x2) + ',' + n2s(y2) + ');\n'
	fd.write(str)
#===================================================================================
def line3(fd, x1, y1, x2, y2, x3, y3, colour, lineopt):
	str  = '\\draw[' + colour + ',' + lineopt + '] ('
	str += n2s(x1) + ',' + n2s(y1) + ') -- (' + n2s(x2) + ',' + n2s(y2) + ') -- (' + n2s(x3) + ',' + n2s(y3) + ');\n'
	fd.write(str)
#===================================================================================
def dot(fd, xo, yo, radius, colour, dotopt):
	str  = '\\draw[' + colour + ', ' + dotopt + ', fill='
	str += colour + '] (' + n2s(xo) + ',' + n2s(yo)
	str += ') circle (' + n2s(radius) + 'mm); \n'
	fd.write(str)
#===================================================================================
def rectangle(fd, xo, yo, wd, ht, colour, opacity):
	str  = '\\fill[' + colour + ', fill=' + colour + ', opacity='
	str +=  n2s(opacity) + '] (' + n2s(xo) + ',' + n2s(yo)
	str += ') rectangle (' + n2s(xo+wd) + ',' + n2s(yo+ht) + ');\n'
	fd.write(str)
#===================================================================================
#===================================================================================
#===================================================================================
def legend_box(fd, filename, NUM, position, color_array, text_array, text_scale, colour, opacity):

	fontsize  	= 10.0
	#-------------------------------------------------------------------------
	# Legend box parameters
	#-------------------------------------------------------------------------
	line_len 	= 0.1		# legnths of the legend lines

	leftsep 	= 0.01		# spacing between the beginings of the lines and the left side of the legend box
	linetextsep 	= 0.01		# spacing between the ends of the lines and the respective items
	linebox 	= 0.01		# spacing between the text and the right side of outline

	topsep 		= 0.012		# spacing between the top side of the legend box and the text
	intersep	= 0.012		# spacing between the legend items
	botsep		= 0.012		# spacing between the bottom side of the legend box and the text
	#-------------------------------------------------------------------------
	# Determine legend box dimensions
	#-------------------------------------------------------------------------
	str = ""

	xo = position[0]
	yo = position[1]

	no_aux_file = False

	try:
		faux = open(filename + '.aux', 'r')
		fdata = faux.read()
		if fdata.find('\\boxwidth') < 0:
			no_aux_file = True
		else:
			widths	= list()
			heights	= list()
			fdata 	= fdata.split()
			for fline in fdata:
				if fline.find('\\boxwidth') > -1:
					widths.append(float(fline[(fline.find('{')+1):fline.find('pt')]))
				if fline.find('\\boxheight') > -1:
					heights.append(float(fline[(fline.find('{')+1):fline.find('pt')]))
		faux.close()
	except FileNotFoundError:
		no_aux_file = True
	#---------------------------------------------------------------------------
	if not no_aux_file:
		max_line_len 	= max(widths)*0.00175*text_scale
		item_height	= max(heights)*0.00175*text_scale	# single legend item height
	else:
		max_line_len 	= 0.0
		item_height 	= 0.0

	width	= max_line_len - 0.0125*text_scale + leftsep + line_len + linetextsep + linebox
	height	= NUM*item_height + (NUM-1)*intersep + topsep + botsep

	rectangle(fd, xo, yo, width, height, colour, opacity)
	#---------------------------------------------------------------------------
	for m in range(NUM):

		xn 	= xo - 0.0125*text_scale + leftsep + line_len + linetextsep
		shift_y	= 0.0062*text_scale*fontsize/10.0	# shift for 10pt font in the vertical direction
		yn 	= shift_y + yo + height - item_height - topsep - m*(intersep + item_height)

		lineopt = 'line width=0.25mm'
		line(fd, xo + leftsep, yn, xo + leftsep + line_len, yn, color_array[m], lineopt)

		item_text = '$ \\text{\\fontsize{' + n2s(fontsize) + '}{' + n2s(fontsize) + '}\selectfont ' + text_array[m] + '} $'

		if no_aux_file:
			fd.write('\\newbox\\bxa%\n')
			fd.write('\setbox\\bxa=\hbox{\pgfinterruptpicture $' + item_text + '$\endpgfinterruptpicture}\\relax%\n')
			fd.write('\makeatletter%\n\@bsphack \protected@write \@auxout {}{\string\def\string\\boxwidth{\\the\wd\\bxa}}\@esphack%\n\makeatother%\n')
			fd.write('\makeatletter%\n\@bsphack \protected@write \@auxout {}{\string\def\string\\boxheight{\\the\ht\\bxa}}\@esphack%\n\makeatother%\n')

		text_node(fd, [xn, yn], text_scale, 'black', item_text, orient='right')

	fd.write(str)
#======================================================================================
def axis_new(fd, dims, *varargin):

	str = ""

	x_min = dims[0]
	x_max = dims[1]

	y_min = dims[2]
	y_max = dims[3]

	if len(varargin) == 1:
		line_width = 'line width=' + n2s(varargin[0]) + 'mm'
	else:
		line_width = 'line width=0.25mm'	# default line width

	colour = 'black'

	line(fd, x_min, y_min, x_min, y_max, colour, line_width)
	line(fd, x_max, y_min, x_max, y_max, colour, line_width)
	line(fd, x_min, y_min, x_max, y_min, colour, line_width)
	line(fd, x_min, y_max, x_max, y_max, colour, line_width)

	fd.write(str)
#===========================================================================
def grid_line_V(fd, xvec, value_position, value_array, tickscale, dims, *varargin):

	str = ""

	LEN = length(xvec)

	if len(varargin) == 1:
		line_colour = varargin[0]
	else:
		line_colour = 'black'

	lineopt  = 'line width=0.25mm, dashed,'
	lineopt += 'dash pattern=on 1.0mm off 1.0mm'
	for n in range(LEN):
		if n > 0 and n < LEN-1:
			line(fd, xvec[n], dims[0], xvec[n], dims[1], line_colour, lineopt)

		text_node(fd, [value_position[0,n], value_position[1,n]], tickscale, 'black', n2s(value_array[n],1), orient='below')

	fd.write(str)
#=============================================================================
def grid_line_H(fd, yvec, value_position, value_array, tickscale, dims, varargin):

	str = ""

	LEN = length(yvec)

	if len(varargin) == 1:
		line_colour = varargin[0]
	else:
		line_colour = 'black'

	lineopt  = 'line width=0.25mm, dashed,'
	lineopt += 'dash pattern=on 1.0mm off 1.0mm'
	for n in range(LEN):
		if n > 0 and n < LEN-1:
			line(fd, dims[0], yvec[n], dims[1], yvec[n], line_colour, lineopt)

		text_node(fd, [value_position[0,n], value_position[1,n]], tickscale, 'black', n2s(value_array[n],1), orient='left')

	fd.write(str)
#============================================================================
def ellipse(fd, dims, rotang, colour, opacity):

	str = '\\draw[rotate around={' + n2s(rotang) + ':' + '(' + n2s(dims[0])
	str += ',' + n2s(dims[1]) + ')}, fill=' + colour + ', opacity='
	str += n2s(opacity) + '] (' + n2s(dims[0]) + ',' + n2s(dims[1])
	str += ') ellipse (' + n2s(dims[2]) + ' and ' + n2s(dims[3]) + ');\n'

	fd.write(str)
#=============================================================================
def rectangle_rot(fd, dims, rot_ang, rot_cnt, colour, fill_colour, opacity, origin):

	sn = np.sin(rot_ang/180.0*np.pi)
	cn = np.cos(rot_ang/180.0*np.pi)

	xo = dims[0]
	yo = dims[1]

	width  = dims[2]
	height = dims[3]

	if origin == 'center':
		u1 = (xo - 0.5*width)  - rot_cnt[0]
		v1 = (yo - 0.5*height) - rot_cnt[1]

		u2 = (xo - 0.5*width)  - rot_cnt[0]
		v2 = (yo + 0.5*height) - rot_cnt[1]

		u3 = (xo + 0.5*width)  - rot_cnt[0]
		v3 = (yo + 0.5*height) - rot_cnt[1]

		u4 = (xo + 0.5*width)  - rot_cnt[0]
		v4 = (yo - 0.5*height) - rot_cnt[1]
	else:
		u1 = (xo - 0.0)    - rot_cnt[0]
		v1 = (yo - 0.0)    - rot_cnt[1]

		u2 = (xo - 0.0)    - rot_cnt[0]
		v2 = (yo + height) - rot_cnt[1]

		u3 = (xo + width)  - rot_cnt[0]
		v3 = (yo + height) - rot_cnt[1]

		u4 = (xo + width)  - rot_cnt[0]
		v4 = (yo - 0.0)    - rot_cnt[1]
	#=====================================================
	x1 = rot_cnt[0] + u1*cn - v1*sn
	y1 = rot_cnt[1] + u1*sn + v1*cn

	x2 = rot_cnt[0] + u2*cn - v2*sn
	y2 = rot_cnt[1] + u2*sn + v2*cn

	x3 = rot_cnt[0] + u3*cn - v3*sn
	y3 = rot_cnt[1] + u3*sn + v3*cn

	x4 = rot_cnt[0] + u4*cn - v4*sn
	y4 = rot_cnt[1] + u4*sn + v4*cn
	#===========================================================================
	str  = '\\draw[' + colour + ', fill=' + fill_colour + ', opacity='
	str += n2s(opacity) + '](' + n2s(x1) + ',' + n2s(y1) + ')--('
	str += n2s(x2) + ',' + n2s(y2) + ')--(' + n2s(x3) + ',' + n2s(y3)
	str += ')--(' + n2s(x4) + ',' + n2s(y4) + ')--cycle;'

	fd.write(str)

#============================================================================
def dimensionsto2auxfile(fd, filename, text_string, name, **kwd):

	no_aux_file 	= False
	widths 		= list()
	heights 	= list()
	try:

		faux 	= open(filename + '.aux', 'r')
		fdata 	= faux.read()

		if 'index' in kwd.keys():
			if fdata.find(name + 'width' + n2s(kwd['index'])) < 0:
				no_aux_file = True
			else:
				fdata = fdata.split()
				for fline in fdata:
					if fline.find(name + 'width' + n2s(kwd['index'])) > -1:
						widths.append(float(fline[(fline.find('{')+1):fline.find('pt}')]))
					if fline.find(name + 'height' + n2s(kwd['index'])) > -1:
						heights.append(float(fline[(fline.find('{')+1):fline.find('pt}')]))
		else:
			if fdata.find(name + 'width') < 0:
				no_aux_file = True
			else:
				fdata = fdata.split()
				for fline in fdata:
					if fline.find(name + 'width') > -1:
						widths.append(float(fline[(fline.find('{')+1):fline.find('pt}')]))
					if fline.find(name + 'height') > -1:
						heights.append(float(fline[(fline.find('{')+1):fline.find('pt}')]))

		faux.close()
	except FileNotFoundError:
		no_aux_file = True

	if no_aux_file:

		fd.write('\\newbox\\bxa%\n')
		fd.write('\setbox\\bxa=\hbox{\pgfinterruptpicture $' + text_string + '$\endpgfinterruptpicture}\\relax%\n')

		if 'index' in kwd.keys():
			fd.write('\makeatletter%\n\@bsphack \protected@write \@auxout {}{\string\def\string\\' + name + 'width' + n2s(kwd['index']) + '{\\the\wd\\bxa}}\@esphack%\n\makeatother%\n')
			fd.write('\makeatletter%\n\@bsphack \protected@write \@auxout {}{\string\def\string\\' + name + 'height' + n2s(kwd['index']) + '{\\the\ht\\bxa}}\@esphack%\n\makeatother%\n')
		else:
			fd.write('\makeatletter%\n\@bsphack \protected@write \@auxout {}{\string\def\string\\' + name + 'width{\\the\wd\\bxa}}\@esphack%\n\makeatother%\n')
			fd.write('\makeatletter%\n\@bsphack \protected@write \@auxout {}{\string\def\string\\' + name + 'height{\\the\ht\\bxa}}\@esphack%\n\makeatother%\n')

	return no_aux_file, widths, heights
#============================================================================
def line_vert(fd, x_pos, y_ext, colour, line_width, *varargin):

	if len(varargin) == 1:
		dims = varargin[0]
		dashed = [', dashed, dash pattern=on ' + n2s(dims(1)) + 'mm off ' + n2s(dims(2)) + 'mm']
	else:
		dashed = ''

	lineopt  = 'line width=' + n2s(line_width) + 'mm ' + dashed

	x1 =  x_pos
	y1 = -y_ext
	x2 =  x1
	y2 =  1.0

	line(fd, x1, y1, x2, y2, colour, lineopt)
#============================================================================
def line_vert_folded(fd, x_pos, dims, colour, line_width, *varargin):

	if len(varargin) == 1:
		dims = varargin[0]
		dashed = ', dashed, dash pattern=on ' + n2s(dims[0]) + 'mm off ' + n2s(dims[1]) + 'mm'
	else:
		dashed = ''

	lineopt = 'line width=' + n2s(line_width) + 'mm ' + dashed

	x1 =  x_pos
	y1 =  1.0
	x2 =  x1
	y2 = -dims[0]
	x3 =  x1 + dims[1]
	y3 = -dims[0]

	line3(fd, x1, y1, x2, y2, x3, y3, colour, lineopt)
#============================================================================
def line_horis(fd, y_pos, x_ext, colour, line_width, *varargin):

	if len(varargin) == 1:
		dims = varargin[0]
		dashed = ', dashed, dash pattern=on ' + n2s(dims[0]) + 'mm off ' + n2s(dims[1]) + 'mm'
	else:
		dashed = ''

	lineopt = 'line width=' + n2s(line_width) + 'mm ' + dashed

	x1 =  1.0
	y1 =  y_pos
	x2 = -x_ext
	y2 =  y1

	line(fd, x1, y1, x2, y2, colour, lineopt)
#===============================================================================
def line_horis_folded(fd, y_pos, dims, colour, line_width, *varargin):

	if len(varargin) == 1:
		dims = varargin[0]
		dashed = ', dashed, dash pattern=on ' + n2s(dims[0]) + 'mm off ' + n2s(dims[1]) + 'mm'
	else:
		dashed = ''

	lineopt = 'line width=' + n2s(line_width) + 'mm ' + dashed

	x1 =  1.0
	y1 =  y_pos
	x2 = -dims[0]
	y2 =  y1
	x3 = -dims[1]
	y3 =  y1 + dims[1]

	line3(fd, x1, y1, x2, y2, x3, y3, colour, lineopt)
#================================================================================
def dimensions_H(fd, pos, dims, colour, line_width):

	lineopt = 'line width=' + n2s(line_width) + 'mm'

	x1 = pos[0] - dims[0]
	y1 = pos[1]
	x2 = pos[0]
	y2 = y1

	line(fd, x1, y1, x2, y2, colour, lineopt)
	#-----------------------------------------------------------------------
	lineopt = 'line width=' + n2s(line_width) + 'mm'

	x1 = pos[0] + dims[1]
	y1 = pos[1]
	x2 = x1 + dims[2]
	y2 = y2

	line(fd, x1, y1, x2, y2, colour, lineopt)
#============================================================================
def dimensions_V_inv(fd, pos, dims, colour, line_width):

	lineopt = 'line width=' + n2s(line_width) + 'mm,->'

	x1 = pos[0] - dims[3]
	y1 = pos[1] + dims[0]
	x2 = pos[0]
	y2 = y1
	x3 = x2
	y3 = pos[1]

	line3(fd, x1, y1, x2, y2, x3, y3, colour, lineopt)

	lineopt = 'line width=' + n2s(line_width) + 'mm,<-'
	#-------------------------------------------------------------------
	x1 = pos[0]
	y1 = pos[1] - dims[1]
	x2 = x1
	y2 = y1 - dims[2]

	line(fd, x1, y1, x2, y2, colour, lineopt)
#============================================================================
def dimensions_V(fd, pos, dims, colour, line_width):

	lineopt = 'line width=' + n2s(line_width) + 'mm,->'

	x1 = pos[0]
	y1 = pos[1] + dims[0]
	x2 = x1
	y2 = pos[1]

	line(fd, x1, y1, x2, y2, colour, lineopt)

	lineopt = 'line width=' + n2s(line_width) + 'mm,<-'

	x1 = pos[0]
	y1 = pos[1] - dims[1]
	x2 = pos[0]
	y2 = pos[1] - dims[1] - dims[2]
	x3 = pos[0] - dims[3]
	y3 = pos[1] - dims[1] - dims[2]

	line3(fd, x1, y1, x2, y2, x3, y3, colour, lineopt)
#=============================================================================
def text_node(fd, pos, scale, colour, textstring, **kwd):

	orient = kwd.get('orient', 'above')

	str  = '\\node[' + orient + ' ,scale=' + n2s(scale) + ',' + colour + '](a) at ('
	str += n2s(pos[0]) + ',' + n2s(pos[1]) + '){$' + textstring + '$};\n'

	fd.write(str)
#=============================================================================
def vert_lines(fd, filename, pos, line_colour, line_width, text_colour, text_scale, text_string, **kwd):
#def vert_lines(fd, filename, pos, dims, pos_arrow, dims_arrow, pos_text, line_colour, line_width, text_colour, text_scale, text_string, **kwd):

	fontsize 	= 10.0			# marker font size in Pt

	rightarrlen	= 0.05			# right delta marker arrow length
	delmarkersep	= 0.25			# distance between the delta maker data line and the lower side of the graph
	botsep 		= 0.01			# spacing between the marker lines and the text
	dellinesep	= 0.05			# spacing between the left delta marker line and the correspoding text
	sline_ext	= 0.05			# extension of the delta marker lines
	deltaqntsep	= 0.07			# spacing between the delta marker line and qunetity marker line
	qntlinsep	= 0.05			# distance between the maker data line and the marker text
	#----------------------------------------------------------------------
	no_aux_file_q, qwidths, qheights = dimensionsto2auxfile(fd, filename, text_string[0], 'verqnt', **kwd)
	no_aux_file_d, dwidths, dheights = dimensionsto2auxfile(fd, filename, text_string[1], 'verdel', **kwd)

	if not no_aux_file_q:
		qnt_label_wd	= max(qwidths)*0.00175*text_scale
		qnt_label_ht	= max(qheights)*0.00175*text_scale	# single legend item height
		del_label_wd	= max(dwidths)*0.00175*text_scale
		del_label_ht	= max(dheights)*0.00175*text_scale	# single legend item height
	else:
		qnt_label_wd	= 0.0
		qnt_label_ht	= 0.0
		del_label_wd	= 0.0
		del_label_ht	= 0.0
	#=======================================================================
	delta 		= pos[2]-pos[0]		# distance between the arrow tips (equals bar width)

	shift_y	= 0.0062*text_scale*fontsize/10.0

	xo = pos[0] - 0.0125/2.0*text_scale - del_label_wd - dellinesep
	yo = shift_y - delmarkersep + botsep

	xl = pos[1] - 0.0125/2.0*text_scale + qntlinsep
	yl = yo - qnt_label_ht - botsep - deltaqntsep
	#----------------------------------------------------------------------
	delta_line_len 	= del_label_wd + dellinesep

	sline_len 	= delmarkersep + sline_ext
	cline_len 	= delmarkersep + qnt_label_ht + botsep + deltaqntsep
	qntline_len	= qntlinsep + qnt_label_wd

	line_vert_folded(fd, pos[1], [cline_len, qntline_len], line_colour, line_width)

	line_vert(fd, pos[0], sline_len, line_colour, line_width)
	line_vert(fd, pos[2], sline_len, line_colour, line_width)

	dimensions_H(fd, (pos[0], -delmarkersep), (delta_line_len, delta, rightarrlen), line_colour, line_width)

	text_node(fd, [xl, yl], text_scale, text_colour, text_string[0], orient='right')
	text_node(fd, [xo, yo], text_scale, text_colour, text_string[1], orient='right')

#===============================================================================
def vert_lines_no(fd, filename, pos,line_colour, line_width, text_colour, text_scale, text_string, **kwd):

	fontsize 	= 10.0			# marker font size in Pt

	rightarrlen	= 0.05			# right delta marker arrow length
	delmarkersep	= 0.20			# distance between the delta maker data line and the lower side of the graph
	botsep 		= 0.01			# spacing between the marker lines and the text
	dellinesep	= 0.05			# spacing between the left delta marker line and the correspoding text
	sline_ext	= 0.025			# extension of the delta marker lines
	deltaqntsep	= 0.07			# spacing between the delta marker line and qunetity marker line
	qntlinsep	= 0.05			# distance between the delta maker line and the marker text
	#----------------------------------------------------------------------
	no_aux_file_d, dwidths, dheights = dimensionsto2auxfile(fd, filename, text_string, 'verdel', **kwd)

	if not no_aux_file_d:
		del_label_wd	= max(dwidths)*0.00175*text_scale
		del_label_ht	= max(dheights)*0.00175*text_scale	# single legend item height
	else:
		del_label_wd	= 0.0
		del_label_ht	= 0.0
	#=======================================================================
	delta 		= pos[2] - pos[0]	# distance between the arrow tips (equals bar width)

	shift_y	= 0.0062*text_scale*fontsize/10.0

	xo = pos[0] - 0.0125/2.0*text_scale - del_label_wd - dellinesep
	yo = shift_y - delmarkersep + botsep
	#----------------------------------------------------------------------
	delta_line_len 	= del_label_wd + dellinesep

	sline_len 	= delmarkersep + sline_ext

	line_vert(fd, pos[0], sline_len, line_colour, line_width)
	line_vert(fd, pos[2], sline_len, line_colour, line_width)

	dimensions_H(fd, (pos[0], -delmarkersep), (delta_line_len, delta, rightarrlen), line_colour, line_width)

	text_node(fd, [xo, yo], text_scale, text_colour, text_string, orient='right')

#===============================================================================
def horis_lines(fd, filename, pos, line_colour, line_width, text_colour, text_scale, text_string, **kwd):

#	text_scale 	= 2.0

	arrow_hor_sep	= 0.15			# spacing between the right graph side and the marker arrow
	upparrowlen 	= 0.05			# length of the upper arrow
	lowarrlen 	= 0.1			# length of the lower arrow
	side_line_ext	= 0.1			# amount of the side line extensions
	botsep 		= 0.01			# spacing between the central line of the bar and the text
	rightsep	= 0.20			# spacing between the text box and the graph left side
	delarrsep	= 0.05			# spacing between the arrow position and the delta marker text
	lowtextsep	= 0.02			# spacing between the lower delta marker line and delta marker text
	#------------------------------------------------------------------------
	no_aux_file_q, qwidths, qheights = dimensionsto2auxfile(fd, filename, text_string[0], 'horqnt', **kwd)
	no_aux_file_d, dwidths, dheights = dimensionsto2auxfile(fd, filename, text_string[1], 'hordel', **kwd)

	if not no_aux_file_q:
		qnt_label_wd	= max(qwidths)*0.00175*text_scale
		qnt_label_ht	= max(qheights)*0.00175*text_scale	# single legend item height
		del_label_wd	= max(dwidths)*0.00175*text_scale
		del_label_ht	= max(dheights)*0.00175*text_scale	# single legend item height
	else:
		qnt_label_wd	= 0.0
		qnt_label_ht	= 0.0
		del_label_wd	= 0.0
		del_label_ht	= 0.0
	#-----------------------------------------------------------------------
	fontsize = 10.0
	#-----------------------------------------------------------------------
	delta		= pos[2]-pos[0]					# distance between the tips of the arrows

	shift_y	= 0.0062*text_scale*fontsize/10.0

	xo = -0.0125/2.0*text_scale - qnt_label_wd - rightsep
	yo =  pos[1] + shift_y + botsep

	xl = -0.0125/2.0*text_scale - del_label_wd - arrow_hor_sep - delarrsep
	yl =  pos[0] + shift_y + botsep - 2.0*del_label_ht - lowtextsep

	cline_len = qnt_label_wd + rightsep
	sline_len = arrow_hor_sep + side_line_ext
	#----------------------------------------------------------------------
	line_horis(fd, pos[1], cline_len, line_colour, line_width)
	line_horis(fd, pos[0], sline_len, line_colour, line_width)
	line_horis(fd, pos[2], sline_len, line_colour, line_width)

	lowarrlen 	= 2.0*del_label_ht + lowtextsep
	del_line_len 	= del_label_wd + arrow_hor_sep - arrow_hor_sep + delarrsep

	dimensions_V(fd, (-arrow_hor_sep, pos[2]), (upparrowlen, delta, lowarrlen, del_line_len), line_colour, line_width)

	text_node(fd, [xo ,yo], text_scale, text_colour, text_string[0], orient='right')
	text_node(fd, [xl, yl], text_scale, text_colour, text_string[1], orient='right')
#===============================================================================

