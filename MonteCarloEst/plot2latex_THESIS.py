import os
import numpy as np

#=======================================================================================
def MTTW2020_two_solid_posts():
	pass
#=======================================================================================
def length(x):
	if type(x) is np.ndarray:
		return max(x.shape)
	else:
		return len(x)
#=======================================================================================
def n2s(x, *numdec):
	if abs(x-int(x)) < 1e-15:	# Check whether x is an integer or not.
		return '%d' % (int(x))	# If this is the case, omit the comma 					# and the fractional part
	else:
		if len(numdec)>0 and int(numdec[0])>0:# if the number of decimal places is 					# specified print, the number with the 
					# numdec decimal>0 places, else display 6 decimal places 					# decimal places
			fstr = "%%1.%df" % (int(numdec[0]))
			return fstr % (x)
		else:
			return '%1.6f' % (x)
#=======================================================================================
def lettertype(line):
	chartype = list()
	for char in line:
		charcode = ord(char)
		if charcode > 96 and charcode < 123:
			chartype.append('low')
		else:
			chartype.append('up')
	return chartype
#=======================================================================================
#=======================================================================================
def plot2latex_MTTW2020_2():

	filename = 'THESIS_FIG_1'

	ko = np.linspace(0.501, 0.999, 1000)

	ro1   = 0.14
	ro2   = 0.14
	ri1   = 0.02
	ri2   = 0.02
	d1    = 0.05
	d2    = 0.05
	xo1   = 0.5+0.3
	xo2   = 0.5-0.25
	phi1  = 90.0
	phi2  = 90.0
	#========================================================================
	yo = np.empty((3,length(ko)))

	yo[0,:] = np.abs(np.cos(2.0*np.pi*(ko-0.5)))
	yo[1,:] = np.abs(np.sin(2.0*np.pi*(ko-0.5)))
	yo[2,:] = np.abs(np.sin(1.0*np.pi*(ko-0.5)))

	# Ro_1 = np.abs(slabs_newton_value(1.0, 1.0, ep3o*(1+1j*tan3), d1, d2, d3))
	# Ro_2 = np.abs(slabs_newton_value(ep1*(1.0+1j*tan1), ep2*(1.0+1j*tan2), ep3o*(1.0+1j*tan3), d1, d2, d3))

	# ep3l_1 = np.abs(S11_finding(1.0, 1.0, ep3o*(1.0+1j*tan3), d1, d2, d3, Ro_1-Rl_1, ep3_min_1, ep3_max_1) - ep3o)

	# ep3u_1 = np.abs(S11_finding(1.0, 1.0, ep3o*(1.0+1j*tan3), d1, d2, d3, Ro_1+Ru_1, ep3_min_1, ep3_max_1) - ep3o)

	# ep3l_2 = np.abs(S11_finding(ep1*(1.0+1j*tan1), ep2*(1.0+1j*tan2), ep3o*(1.0+1j*tan3), d1, d2, d3, Ro_2-Rl_2, ep3_min_2, ep3_max_2) - ep3o)

	# ep3u_2 = np.abs(S11_finding(ep1*(1.0+1j*tan1), ep2*(1.0+1j*tan2), ep3o*(1.0+1j*tan3), d1, d2, d3, Ro_2+Ru_2, ep3_min_2, ep3_max_2) - ep3o)
	#===========================================================================
	# ko = ep3;
	#===========================================================================
	LEN = yo.shape[0]

	linewidth = 0.5
	#===========================================================================
	xlabel_dist =  0.1
	ylabel_dist =  0.12
	#===========================================================================
	#===========================================================================
	#pt2cm = 0.0352778

	legend_dims 		= [  0.0,   0.0 ]
	legend_line_dims 	= [ 0.01,  0.05 ]

	tickscale 		= 2.2

	legend_scale 		= 2.0

	labelx_scale 		= 2.6
	labely_scale 		= 2.4

	lwidth  		= 0.25
	lheight 		= 0.1

	legend_text 		= list()

	legend_text.append('ABC')
	legend_text.append('aaaaaa')
	legend_text.append('Abdsdsd')

	color = list()

	color.append('blue')
	color.append('red')
	color.append('black')
	color.append('magenta')
	color.append('green')

	fid = open(filename + '.tex', 'w')
	#===========================================================================
	komin = 0
	komax = max(ko)

	ko = 2.0*(ko - 0.5)

	len = 0.05

	N = 1

	ko = ko[::N]
	yo = yo[:,::N]

	#==========================================================================
	# fid.write('\\usepackage[hiresbb, dvips]{graphicx} \n');
	# fid.write('\\usepackage{color} \n');

	str = ""
	str += '\\documentclass[convert]{standalone} \n' 
	str += '\\usepackage{siunitx} \n'
	str += '\\usepackage[dvips]{color} \n'
	str += '\\usepackage{textgreek} \n'
	str += '\\usepackage{tikz} \n'
	str += '\\usetikzlibrary{calc} \n'
	str += '\\usetikzlibrary{arrows} \n'
	str += '\\begin{document} \n'
	str += '\\begin{tikzpicture}[scale=20] \n'

	fid.write(str)
	#===========================================================================
	str = ""

	strlinewidth =  n2s(linewidth)

	for m in range(LEN):
		if m==2:
			yl = yo[m,0]
			kl = ko[0]

			for n in range(length(ko)-1):
				if np.abs(yl-yo[m,n]) > 0.02 or np.abs(kl-ko[n]) > 0.02:
					yl = yo[m,n]
					kl = ko[n]

					dotopt = 'line width=' + strlinewidth + 'mm'
					dot(fid, ko[n], yo[m,n], 0.02, color[m], dotopt)
		else:
			for n in range(length(ko)-1):
				lineopt = 'line width=' + strlinewidth + 'mm'
				line(fid, ko[n], yo[m,n], ko[n+1], yo[m,n+1], color[m], lineopt)

	fid.write(str)
	#===========================================================================
	str = ""

	x = np.linspace(0.0, 1.0, 6)
	y = np.arange(0.0, 1.0+0.1, 0.1)

	grid_line_V(fid, x, np.vstack([x+0.0, -0.01*np.ones((1, length(x)))]), x/2.0+0.5, tickscale, [0,1], 'black')

	grid_line_H(fid, y, np.vstack([0.0*np.ones((1, length(y))), y]), y, tickscale, [0,1], 'black')

	axis_new(fid, [0.0, 1.0, 0.0, 1.0], 0.5)
	#===========================================================================
	# rectangle_rot(h, [xl_1, 0, xu_1-xl_1, 1], 0, [0,0], 'black', 'blue', 0.25, 'corner')

	# rectangle_rot(h, [0, yl_1, 1, yu_1-yl_1], 0, [0,0], 'black', 'blue', 0.25, 'corner')

	# rectangle_rot(h, [xl_2, 0, xu_2-xl_2, 1], 0, [0,0], 'black', 'red', 0.25, 'corner')

	# rectangle_rot(h, [0, yl_2, 1, yu_2-yl_2], 0, [0,0], 'black', 'red', 0.25, 'corner');

	# vert_lines(h, [xl_1, xo_1, xu_1], [0.23, 0.3, 0.2], 0.2, [0.25,0.05], [0.12,0.1], 'blue', 0.25, 'blue', ...
	      # 2, {['\\Delta \\varepsilon^{\\prime}_{\\mathrm{r3}}=', n2s(ep3u_1+ep3l_1),],...
	      # ['\\varepsilon^{\\prime}_{\\mathrm{r3}}=', n2s(ep3o)]})

	# horis_lines(h, [yl_1, yo_1, yu_1], [0.15, 0.4], 0.1, [0.02, 0.05, 0.3], [0.1,0.12], 'blue', 0.25, 'blue', ...
 	      # 2, {['R^{+}=', n2s(Ro_1,2)], ['\\Delta R^{+}=', n2s(Ru_1+Rl_1)]})

	# vert_lines_no(h, [xl_2, xu_2], [0.12], 0.1, [0.3, 0.05], [0.12], 'red', 0.25, 'red', ...
	      #2, {['\\Delta \\varepsilon^{\\prime}_{\\mathrm{r3}}=', n2s(ep3u_2+ep3l_2),],...
	      #['\\varepsilon^{\\prime}_{\\mathrm{r3}}=', n2s(ep3o)]})

	# horis_lines(h, [yl_2,yo_2,yu_2], [0.15,0.4], 0.1, [0.02,0.05,0.3], [0.1,0.12], 'red', 0.25, 'red', ...
 	      # 2, {['R^{+}=', n2s(Ro_2,2)], ['\\Delta R^{+}=', n2s(Ru_2+Rl_2)]})
	#===========================================================================
	fid.write('\\node[scale = ' + n2s(labelx_scale) + '](xlabel) at (0.5, ' + n2s(-xlabel_dist) + '){  $ a/\\lambda_{\\mathrm{o}} $  };\n')

	fid.write('\\node[scale = ' + n2s(labely_scale) + '](ylabel) at (' + n2s(-ylabel_dist) + ', 0.5){  $ S_{11}  $  };\n')
	#===========================================================================
	legend_box(fid, filename, LEN, color, legend_text, legend_scale, legend_line_dims, legend_dims, 'white', 1)

	fid.write('\end{tikzpicture} \n')
	fid.write('\end{document} \n')

	fid.close()
	#===========================================================================
#	os.system('pdflatex MTTW_FIG_2.tex > /dev/null')
	os.system('pdflatex ' + filename + '.tex > /dev/null 2>&1')

	# open the generated PDF file with the default linux viewer
	os.system('xdg-open ' + filename + '.pdf')
#===================================================================================
def line(fd, x1, y1, x2, y2, colour, lineopt):
	str  = '\\draw[' + colour + ',' + lineopt + '] ('
	str += n2s(x1) + ',' + n2s(y1) + ') -- (' + n2s(x2) + ',' + n2s(y2) + ');\n'
	fd.write(str)
#===================================================================================
def line3(fd, x1, y1, x2, y2, x3, y3, colour, lineopt):
	str  = '\\draw[' + colour + ',' + lineopt + '] ('
	str += n2s(x1) + ',' + n2s(y1) + ') -- (' + n2s(x2) + ',' + n2s(y2) + ') -- (' + n2s(x3) + ',' + n2s(y3) + ');\n'
	fd.write(str)
#===================================================================================
def dot(fd, xo, yo, radius, colour, dotopt):
	str  = '\\draw[' + colour + ', ' + dotopt + ', fill='
	str += colour + '] (' + n2s(xo) + ',' + n2s(yo)
	str += ') circle (' + n2s(radius) + 'mm); \n'
	fd.write(str)
#===================================================================================
def rectangle(fd, xo, yo, wd, ht, colour, opacity):
	str  = '\\fill[' + colour + ', fill=' + colour + ', opacity='
	str +=  n2s(opacity) + '] (' + n2s(xo) + ',' + n2s(yo)
	str += ') rectangle (' + n2s(xo+wd) + ',' + n2s(yo+ht) + ');\n'
	fd.write(str)
#===================================================================================
#===================================================================================
#===================================================================================
def legend_box(fd, filename, NUM, color_array, text_array, text_scale, line_dim, dim, colour, opacity):

	fontsize  	= 10.0
	#-------------------------------------------------------------------------
	# Legend box parameters
	#-------------------------------------------------------------------------
	leftsep 	= 0.01		# spacing between the beginings of the lines and the left side of the legend box
	linetextsep 	= 0.01		# spacing between the ends of the lines and the respective items
	linebox 	= 0.01		# spacing between the text and the right side of outline

	topsep 		= 0.012		# spacing between the top side of the legend box and the text
	intersep	= 0.012		# spacing between the legend items
	botsep		= 0.012		# spacing between the bottom side of the legend box and the text
	#-------------------------------------------------------------------------
	# Determine legend box dimensions
	#-------------------------------------------------------------------------
	str = ""

	xo		= dim[0]
	yo		= dim[1]

	line_xo  	= line_dim[0]
	line_len 	= line_dim[1]

	no_aux_file = False

	try:
		faux = open(filename + '.aux', 'r')
		fdata = faux.read()
		if fdata.find('\\boxwidth') < 0:
			no_aux_file = True
		else:
			widths	= list()
			heights	= list()
			fdata 	= fdata.split()
			for fline in fdata:
				if fline.find('\\boxwidth') > -1:
					widths.append(float(fline[(fline.find('{')+1):fline.find('pt')]))
				if fline.find('\\boxheight') > -1:
					heights.append(float(fline[(fline.find('{')+1):fline.find('pt')]))
		faux.close()
	except FileNotFoundError:
		no_aux_file = True
	#---------------------------------------------------------------------------
	if not no_aux_file:
		max_line_len 	= max(widths)*0.00175*text_scale
		item_height	= max(heights)*0.00175*text_scale	# single legend item height
	else:
		max_line_len 	= 0.0
		item_height 	= 0.0

	width	= max_line_len - 0.0125*text_scale + leftsep + line_len + linetextsep + linebox
	height	= NUM*item_height + (NUM-1)*intersep + topsep + botsep

	rectangle(fd, xo, yo, width, height, colour, opacity)
	#---------------------------------------------------------------------------
	for m in range(NUM):

		xn 	= xo - 0.0125*text_scale + leftsep + line_len + linetextsep
		shift_y	= 0.0062*text_scale*fontsize/10.0	# shift for 10pt font in the vertical direction
		yn 	= shift_y + yo + height - item_height - topsep - m*(intersep + item_height)

		lineopt = 'line width=0.25mm'
		line(fd, xo + leftsep, yn, xo + leftsep + line_len, yn, color_array[m], lineopt)

		item_text = '$ \\text{\\fontsize{' + n2s(fontsize) + '}{' + n2s(fontsize) + '}\selectfont ' + text_array[m] + '} $'

		if no_aux_file:
			fd.write('\\newbox\\bxa%\n')
			fd.write('\setbox\\bxa=\hbox{\pgfinterruptpicture $' + item_text + '$\endpgfinterruptpicture}\\relax%\n')
			fd.write('\makeatletter%\n\@bsphack \protected@write \@auxout {}{\string\def\string\\boxwidth{\\the\wd\\bxa}}\@esphack%\n\makeatother%\n')
			fd.write('\makeatletter%\n\@bsphack \protected@write \@auxout {}{\string\def\string\\boxheight{\\the\ht\\bxa}}\@esphack%\n\makeatother%\n')

		text_node(fd, [xn, yn], text_scale, 'black', item_text, orient='right')

	fd.write(str)
#======================================================================================
def axis_new(fd, dims, *varargin):

	str = ""

	x_min = dims[0]
	x_max = dims[1]

	y_min = dims[2]
	y_max = dims[3]

	if len(varargin) == 1:
		line_width = 'line width=' + n2s(varargin[0]) + 'mm'
	else:
		line_width = 'line width=0.25mm'	# default line width

	colour = 'black'

	line(fd, x_min, y_min, x_min, y_max, colour, line_width)
	line(fd, x_max, y_min, x_max, y_max, colour, line_width)
	line(fd, x_min, y_min, x_max, y_min, colour, line_width)
	line(fd, x_min, y_max, x_max, y_max, colour, line_width)

	fd.write(str)
#===========================================================================
def grid_line_V(fd, xvec, value_position, value_array, tickscale, dims, *varargin):

	str = ""

	LEN = length(xvec)

	if len(varargin) == 1:
		line_colour = varargin[0]
	else:
		line_colour = 'black'

	lineopt  = 'line width=0.25mm, dashed,'
	lineopt += 'dash pattern=on 1.0mm off 1.0mm'
	for n in range(LEN):
		if n > 0 and n < LEN-1:
			line(fd, xvec[n], dims[0], xvec[n], dims[1], line_colour, lineopt)

		text_node(fd, [value_position[0,n], value_position[1,n]], tickscale, 'black', n2s(value_array[n],1), orient='below')

	fd.write(str)
#=============================================================================
def grid_line_H(fd, yvec, value_position, value_array, tickscale, dims, varargin):

	str = ""

	LEN = length(yvec)

	if len(varargin) == 1:
		line_colour = varargin[0]
	else:
		line_colour = 'black'

	lineopt  = 'line width=0.25mm, dashed,'
	lineopt += 'dash pattern=on 1.0mm off 1.0mm'
	for n in range(LEN):
		if n > 0 and n < LEN-1:
			line(fd, dims[0], yvec[n], dims[1], yvec[n], line_colour, lineopt)

		text_node(fd, [value_position[0,n], value_position[1,n]], tickscale, 'black', n2s(value_array[n],1), orient='left')

	fd.write(str)
#============================================================================
def ellipse(fd, dims, rotang, colour, opacity):

	str = '\\draw[rotate around={' + n2s(rotang) + ':' + '(' + n2s(dims[0])
	str += ',' + n2s(dims[1]) + ')}, fill=' + colour + ', opacity='
	str += n2s(opacity) + '] (' + n2s(dims[0]) + ',' + n2s(dims[1])
	str += ') ellipse (' + n2s(dims[2]) + ' and ' + n2s(dims[3]) + ');\n'

	fd.write(str)
#=============================================================================
def rectangle_rot(fd, dims, rot_ang, rot_cnt, colour, fill_colour, opacity, origin):

	sn = np.sin(rot_ang/180.0*np.pi)
	cn = np.cos(rot_ang/180.0*np.pi)

	xo = dims[0]
	yo = dims[1]

	width  = dims[2]
	height = dims[3]

	if origin == 'center':
		u1 = (xo - 0.5*width)  - rot_cnt[0]
		v1 = (yo - 0.5*height) - rot_cnt[1]

		u2 = (xo - 0.5*width)  - rot_cnt[0]
		v2 = (yo + 0.5*height) - rot_cnt[1]

		u3 = (xo + 0.5*width)  - rot_cnt[0]
		v3 = (yo + 0.5*height) - rot_cnt[1]

		u4 = (xo + 0.5*width)  - rot_cnt[0]
		v4 = (yo - 0.5*height) - rot_cnt[1]
	else:
		u1 = (xo - 0.0)    - rot_cnt[0]
		v1 = (yo - 0.0)    - rot_cnt[1]

		u2 = (xo - 0.0)    - rot_cnt[0]
		v2 = (yo + height) - rot_cnt[1]

		u3 = (xo + width)  - rot_cnt[0]
		v3 = (yo + height) - rot_cnt[1]

		u4 = (xo + width)  - rot_cnt[0]
		v4 = (yo - 0.0)    - rot_cnt[1]
	#=====================================================
	x1 = rot_cnt[0] + u1*cn - v1*sn
	y1 = rot_cnt[1] + u1*sn + v1*cn

	x2 = rot_cnt[0] + u2*cn - v2*sn
	y2 = rot_cnt[1] + u2*sn + v2*cn

	x3 = rot_cnt[0] + u3*cn - v3*sn
	y3 = rot_cnt[1] + u3*sn + v3*cn

	x4 = rot_cnt[0] + u4*cn - v4*sn
	y4 = rot_cnt[1] + u4*sn + v4*cn

	#===========================================================================
	str += '\\draw[' + colour + ', fill=' + fill_colour + ', opacity='
	str += n2s(opacity) + '](' + n2s(x1) + ',' + n2s(y1) + ')--('
	str += n2s(x2) + ',' + n2s(y2) + ')--(' + n2s(x3) + ',' + n2s(y3)
	str += ')--(' + n2s(x4) + ',' + n2s(y4) + ')--cycle;'

	fd.write(str)
#============================================================================
def line_vert(fd, x_pos, y_ext, colour, line_width, *varargin):

	if len(varargin) == 1:
		dims = varargin[0]
		dashed = [', dashed, dash pattern=on ' + n2s(dims(1)) + 'mm off ' + n2s(dims(2)) + 'mm']
	else:
		dashed = ''

	lineopt  = 'line width=' + n2s(line_width) + 'mm ' + dashed

	x1 =  x_pos
	y1 = -y_ext
	x2 =  x1
	y2 =  1.0

	line(fd, x1, y1, x2, y2, colour, lineopt)

	fd.write(str)
#============================================================================
def line_vert_folded(fd, x_pos, dims, colour, line_width, *varargin):

	if len(varargin) == 1:
		dims = varargin[0]
		dashed = ', dashed, dash pattern=on ' + n2s(dims[0]) + 'mm off ' + n2s(dims[1]) + 'mm'
	else:
		dashed = ''

	lineopt = 'line width=' + n2s(line_width) + 'mm ' + dashed

	x1 =  x_pos
	y1 =  1.0
	x2 =  x1
	y2 = -dims[0]
	x3 =  x1 + dims[1]
	y3 = -dims[0]

	line3(fd, x1, y1, x2, y2, x3, y3, colour, lineopt)

	fd.write(str)
#============================================================================
def line_horis(fd, y_pos, x_ext, colour, line_width, *varargin):

	if len(varargin) == 1:
		dims = varargin[0]
		dashed = ', dashed, dash pattern=on ' + n2s(dims[0]) + 'mm off ' + n2s(dims[1]) + 'mm'
	else:
		dashed = ''

	lineopt = 'line width=' + n2s(line_width) + 'mm ' + dashed

	x1 =  1.0
	y1 =  y_pos
	x2 = -x_ext
	y2 =  y1

	line(fd, x1, y1, x2, y2, colour, lineopt)

	fd.write(str)
#===============================================================================
def line_horis_folded(fd, y_pos, dims, colour, line_width, *varargin):

	if len(varargin) == 1:
		dims = varargin[0]
		dashed = ', dashed, dash pattern=on ' + n2s(dims[0]) + 'mm off ' + n2s(dims[1]) + 'mm'
	else:
		dashed = ''

	lineopt = 'line width=' + n2s(line_width) + 'mm ' + dashed

	x1 =  1.0
	y1 =  y_pos
	x2 = -dims[0]
	y2 =  y1
	x3 = -dims[1]
	y3 =  y1 + dims[1]

	line3(fd, x1, y1, x2, y2, x3, y3, colour, lineopt)

	fd.write(str)
#================================================================================
def dimensions_H(fd, pos, dims, colour, line_width):

	lineopt = 'line width=' + n2s(line_width) + 'mm'

	x1 = pos[0] - dims[0]
	y1 = pos[1]
	x2 = pos[0]
	y2 = y1

	line(fd, x1, y1, x2, y2, colour, lineopt)
	#-----------------------------------------------------------------------
	lineopt = 'line width=' + n2s(line_width) + 'mm'

	x1 = pos[0] + dims[1]
	y1 = pos[1]
	x2 = x1 + dims[2]
	y2 = y2

	line(fd, x1, y1, x2, y2, colour, lineopt)

	fd.write(str)
#============================================================================
def dimensions_V_inv(fd, pos, dims, colour, line_width):

	lineopt = 'line width=' + n2s(line_width) + 'mm,->'

	x1 = pos[0] - dims[3]
	y1 = pos[1] + dims[0]
	x2 = pos[0]
	y2 = y1
	x3 = x2
	y3 = pos[1]

	line3(fd, x1, y1, x2, y2, x3, y3, colour, lineopt)

	lineopt = 'line width=' + n2s(line_width) + 'mm,<-'
	#-------------------------------------------------------------------
	x1 = pos[0]
	y1 = pos[1] - dims[1]
	x2 = x1
	y2 = y1 - dims[2]

	line(fd, x1, y1, x2, y2, colour, lineopt)

	fd.write(str)
#============================================================================
def dimensions_V(fd, pos, dims, colour, line_width):

	lineopt = 'line width=' + n2s(line_width) + 'mm,->'

	x1 = pos[0]
	y1 = pos[1] + dims[0]
	x2 = x1
	y2 = pos[1]

	line(fd, x1, y1, x2, y2, colour, lineopt)

	lineopt = 'line width=' + n2s(line_width) + 'mm,<-'

	x1 = pos[0]
	y1 = pos[1] - dims[1]
	x2 = pos[0]
	y2 = pos[1] - dims[1] - dims[2]
	x3 = pos[0] - dims[3]
	y3 = pos[1] - dims[1] - dims[2]

	line3(fd, x1, y1, x2, y2, x3, y3, colour, lineopt)

	fd.write(str)
#=============================================================================
def text_node(fd, pos, scale, colour, textstring, **kwd):

	orient = kwd.get('orient', 'above')

	str  = '\\node[' + orient + ' ,scale=' + n2s(scale) + ',' + colour + '](a) at ('
	str += n2s(pos[0]) + ',' + n2s(pos[1]) + '){$' + textstring + '$};\n'

	fd.write(str)
#=============================================================================
 # '\\Delta \\varepsilon^{\\prime}_{\\mathrm{r3}} '
def vert_lines(fd, pos, dims, pos_arrow, dims_arrow, pos_text, line_colour, line_width, text_colour, text_scale, text_string):

	line_vert(fd, pos[0], dims[0], line_colour, line_width)

	line_vert_folded(fd, pos[1], [dims[1], dims[2]], line_colour, line_width)

	line_vert(fd, pos[2], dims[0], line_colour, line_width)

	dimensions_H(fd, [pos[0], -pos_arrow], [dims_arrow[0], pos[2]-pos[0], dims_arrow[1]], line_colour, line_width)
	#----------------------------------------------------------------------
	xo =  pos[0] - dims_arrow[0] + pos_text[0]
	yo = -pos_arrow

	text_node(fd, [xo, yo], text_scale, text_colour, text_string[0])
	#----------------------------------------------------------------------
	xo =  pos[1] + pos_text[1]
	yo = -dims[1]

	text_node(fd, [xo, yo], text_scale, text_colour, text_string[1])
	#----------------------------------------------------------------------
	xo =  pos[1] + pos_text[1]
	yo = -dims[1] - 0.05

	text_node(fd, [xo, yo], text_scale, text_colour, '')
#===============================================================================
def vert_lines_no(fd, pos, dims, pos_arrow, dims_arrow, pos_text, line_colour, line_width, text_colour, text_scale, text_string):

	line_vert(fd, pos[0], dims[0], line_colour, line_width)

	line_vert(fd, pos[1], dims[0], line_colour, line_width)

	dimensions_H(fd, [pos[0], -pos_arrow], [dims_arrow[0], pos[1]-pos[0], dims_arrow[1]], line_colour, line_width)
	#-----------------------------------------------------------------------
	xo =  pos[0] - dims_arrow[0] + pos_text[0]
	yo = -pos_arrow

	text_node(fd, [xo, yo], text_scale, text_colour, text_string[0])
#===============================================================================
def horis_lines(fd, pos, dims, pos_arrow, dims_arrow, pos_text, line_colour, line_width, text_colour, text_scale, text_string):

	line_horis(fd, pos[0], dims[0], line_colour, line_width)

	line_horis(fd, pos[1], dims[1], line_colour, line_width)

	line_horis(fd, pos[2], dims[0], line_colour, line_width)

	dimensions_V(fd, [-pos_arrow, pos[2]], [dims_arrow[0], pos[2]-pos[0], dims_arrow[1], dims_arrow[2]], line_colour, line_width)
	#-----------------------------------------------------------------------
	xo = pos_text[0] - dims[1]
	yo = pos[1]

	text_node(fd, [xo ,yo], text_scale, text_colour, text_string[0])
	#-----------------------------------------------------------------------
	xo = pos_text[1] - pos_arrow - dims_arrow[2]
	yo = pos[0] - dims_arrow[1]

	text_node(fd, [xo, yo], text_scale, text_colour, text_string[1])
#===============================================================================

plot2latex_MTTW2020_2()
