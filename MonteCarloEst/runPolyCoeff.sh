#!/bin/bash

MPATH=/home/student_etf/GitMonteCarlo/measurement-uncertainty-estimation/MonteCarloEst

cd $MPATH

rm -f ./PolyCoeff

mpic++  PolyCoeff.cpp -o PolyCoeff -L./lib -lrod -lewald -lslatec -llapack -lrefblas -lgfortran  -DMP

mpirun ./PolyCoeff

cd ~
