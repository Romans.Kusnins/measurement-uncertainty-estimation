import numpy as np
from matplotlib import pyplot as plt
from slab_res3md import (three_slab_guide, root_finding, POW2)
#--------------------------------------------------------------------------------
ep1 	= 4.2		# MUT dielectric constant
ep2 	= 12.5		# auxiliary slabs dielectric constant

d1 	= 10.0 		# measurable slab thickness in mm
d2  	= 10.0		# auxiliary slab thickness in mm

L   	= 30.0		# interslab separation distance in mm
f   	= 10.0		# operating frequency in GHz

a   	= 22.86		# waveguide width in mm

M       = 10000         # Monte-Carlo algorithm trial number
MAXIT   = 1000          # max number of iterations for Newton's method
NTOL    = 1e-4          # solution tolerance for Newton's method
#---------------------------------------------------------------------------------
lam = 300.0/f;
#---------------------------------------------------------------------------------
epv = np.arange(1, 20, 0.01)

S11abs = np.ndarray((epv.shape))

for n in range(epv.shape[0]):
	S11abs[n] = np.abs(three_slab_guide(a/lam, ep1, 1.0, epv[n], d1/a, d2/a, L/a))

plt.plot(epv, S11abs)
plt.grid()
plt.show()

S11mes 		= three_slab_guide(a/lam, ep1, 1.0, ep2,      d1/a, d2/a, L/a)
S11mes_p 	= three_slab_guide(a/lam, ep1, 1.0, ep2+1e-3, d1/a, d2/a, L/a)
S11_der		= (np.abs(S11mes_p) - np.abs(S11mes))/1e-3
#---------------------------------------------------------------------------------
# EVALUATING MEASUREMENT UNCERTAINTY
#---------------------------------------------------------------------------------
sigma_S11abs 	= 0.01
sigma_d1 	= 0.015
sigma_d2	= 0.015
sigma_L 	= 0.015

epm 		= np.ndarray((M,))
#=================================================================================
def func(epm, **kwd):
	S11 = three_slab_guide(a/lam, ep1, 1.0, epm, del_d1/a, del_d2/a, del_L/a)
	return np.abs(S11)
#=================================================================================
for n in range(M):

	del_S11abs 	= np.random.normal(np.abs(S11mes), 	sigma_S11abs)
	del_d1 		= np.random.normal(d1,			sigma_d1)
	del_d2	 	= np.random.normal(d2, 			sigma_d2)
	del_L	 	= np.random.normal(L,  			sigma_L)

	epm[n] = root_finding(func, ep2, 1e-6, NTOL, MAXIT, val=del_S11abs, deriv=S11_der)
#=================================================================================
epm[np.logical_not(np.isnan(epm))]
print(epm.shape)

print(np.mean(epm))
print(np.sqrt(np.var(epm)))
