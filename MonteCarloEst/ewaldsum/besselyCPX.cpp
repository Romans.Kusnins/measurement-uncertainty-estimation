#include<complex.h>


#define cdouble _Complex double


extern "C" void zbesy_(double*,double*,double*,int*,int*,double*,double*,int*,double*,double*,int*);   


void besselyCPX(int n, cdouble z, cdouble *c, int na){

	int kode=1,nz,ierr;

	double cr[na], ci[na];
	double cwr[na], cwi[na];
	double zr, zi, nd = static_cast<double>(n);

	zr = creal(z);
	zi = cimag(z);

	zbesy_(&zr, &zi, &nd, &kode, &na, cr, ci, &nz, cwr, cwi, &ierr);

	for(int k=0; k<na; ++k){
		c[k] = cr[k] + I*ci[k];
	}

}
