#include<complex>
#include "Faddeeva.hh"

typedef std::complex<double> cdouble;


void Fadd_erfc(double* z){

	cdouble res;

	res = Faddeeva::erfc(std::complex<double>(z[0], z[1]));

	z[0] = res.real();
	z[1] = res.imag();

}


