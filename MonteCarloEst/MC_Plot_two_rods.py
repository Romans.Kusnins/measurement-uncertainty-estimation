import os
import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *
from Derivatives import *

try:
        matplot = True
        from matplotlib import pyplot as plt
except ModuleNotFoundError:
        print("The matplotlib module is not installed.")
        matplot = False


from plot2pdf import *

from Uncertainty_vs_S11 import *
#==================================================================================
xMin = None
xMax = None

task 		= 'sensitivity'
#task 		= 'standard'
task 		= 'total'

#sensparam 	= 'S11abs'
sensparam 	= 'S11abs'

var_param 	= 'ep2'
#var_param 	= 'L'
#var_param 	= 'ro1'

#PREFIX		= '_6_5_'
PREFIX		= ''

MCNUM		= 20000
#MCNUM		= 20000

isPDF 		= True
#isPRINT	= False
#==================================================================================
xlabels		= dict()

#xlabels['ep1']  = 'Dielectric constant of the auxiliary slab'
#xlabels['ep2']  = 'Dielectric constant of MUT'
#xlabels['tan1'] = 'Loss tangent of the auxiliary slab'
#xlabels['tan2'] = 'Loss tangent of MUT'
#xlabels['ro1']  = 'Radius of the auxiliary rod, mm'
#xlabels['ro2']  = 'Radius of the MUT rod, mm'
#xlabels['xo1']  = 'Offset of the auxiliary rod, mm'
#xlabels['xo2']  = 'Offset of the MUT rod, mm'
#xlabels['L']    = 'Separation between the slabs, mm'
#xlabels['a']    = 'Waveguide width, mm'
#xlabels['f']    = 'Frequency, GHz'

xlabels['ep1']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,aux}}$'
xlabels['ep2']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,mut}}$'
xlabels['tan1'] = '$\\tan_{\\delta^{\\mathrm{aux}}}$'
xlabels['tan2'] = '$\\tan_{\\delta^{\\mathrm{mut}}}$'
xlabels['d1']   = '$d_{\\mathrm{aux}}$ mm'
xlabels['d2']   = '$d_{\\mathrm{mut}}$ mm'
xlabels['a']    = '$a mm'
xlabels['L']    = '$d_{\\mathrm{int}}$ mm'
xlabels['f']    = 'f, GHz'

paramname       = {'S11abs': '$u_{|S_{11}|}$', 'ro1': '$u_{r_{\\mathrm{aux}}}$', 'ro2': '$u_{r_{\\mathrm{mut}}}$', 'L': '$u_{d_{\\mathrm{in}}}$', 'ep1': '$u_{\\varepsilon^{\\prime}_{\\mathrm{r,aux}}}$', 'tan1': '$ u_{\\tan{\\delta^{\\mathrm{aux}}}}$', 'tan2': 'u_{\\tan{\\delta^{\\mathrm{mut}}}}', 'a': 'u_{a}', 'f': 'u_{f}'}
#==================================================================================
# PLOTTING CALCULATED DATA
#==================================================================================
FOLDER 	= './RESULTS_2ROD_GUIDE/'

MCSTR 	= n2s(MCNUM)

if task == 'sensitivity':
	MCfilename 	= FOLDER + 'two_rods_var_' + var_param + '_sens_' + sensparam + '_MCnum' + PREFIX + '_' + MCSTR + '.npy'
	EPMfilename 	= FOLDER + 'tworods_var_' + var_param + '_standard_'+ sensparam + '_EPM' + PREFIX + '.npy'
else:
	MCfilename 	= FOLDER + 'two_rods_var_' + var_param + '_total_MCnum' + PREFIX + '_' + MCSTR + '.npy'
	EPMfilename 	= FOLDER + 'tworods_var_' + var_param + '_total_EPM' + PREFIX + '.npy'

#if var_param == 'L':
#	if task == 'sensitivity':
#		MCfilename 	= './RESULTS/' + 'two_rods_var_' + var_param + '_sens_' + sensparam + '_MCnum_50000.npy'
#		EPMfilename 	= './RESULTS/' + 'tworods_var_' + var_param + '_standard_'+ sensparam + '_EPM.npy'
#	else:
#		MCfilename 	= './RESULTS/' + 'two_rods_var_' + var_param + '_total_MCnum_50000.npy'
#		EPMfilename 	= './RESULTS/' + 'tworods_var_' + var_param + '_total_EPM.npy'
#else:
#	if ro1 == 5.8:
#		if task == 'sensitivity':
#			MCfilename 	= './RESULTS/' + 'two_rods_var_' + var_param + '_sens_' + sensparam + '_MCnum_50000.npy'
#			EPMfilename 	= './RESULTS/' + 'tworods_var_' + var_param + '_standard_'+ sensparam + '_EPM_5_8.npy'
#		else:
#			MCfilename 	= './RESULTS/' + 'two_rods_var_' + var_param + '_total_MCnum_50000.npy'
#			EPMfilename 	= './RESULTS/' + 'tworods_var_' + var_param + '_total_EPM_5_8.npy'
#	elif ro1 == 6.0:
#		if task == 'sensitivity':
#			MCfilename 	= './RESULTS/' + 'two_rods_var_' + var_param + '_sens_' + sensparam + '_MCnum_20000.npy'
#			EPMfilename 	= './RESULTS/' + 'tworods_var_' + var_param + '_standard_'+ sensparam + '_EPM_6_0_.npy'
#		else:
#			MCfilename 	= './RESULTS/' + 'two_rods_var_' + var_param + '_total_MCnum_20000.npy'
#			EPMfilename 	= './RESULTS/' + 'tworods_var_' + var_param + '_total_EPM_6_0_.npy'
#	else:
#		if task == 'sensitivity':
#			MCfilename 	= './RESULTS/' + 'two_rods_var_' + var_param + '_sens_' + sensparam + '_MCnum_25000.npy'
#			EPMfilename 	= './RESULTS/' + 'tworods_var_' + var_param + '_standard_'+ sensparam + '_EPM.npy'
#		else:
#			MCfilename 	= './RESULTS/' + 'two_rods_var_' + var_param + '_total_MCnum_25000.npy'
#			EPMfilename 	= './RESULTS/' + 'tworods_var_' + var_param + '_total_EPM.npy'

MCdata 	= np.load(MCfilename)
EPMdata = np.load(EPMfilename)
#==================================================================================
EPMdata[1, EPMdata[1,:] > 1.0] = np.nan

if xMin != None:
	ID 	= EPMdata[0,:] >= xMin
	EPMdata = EPMdata[:,ID]

	ID 	= MCdata[0,:] >= xMin
	MCdata 	= MCdata[:,ID]

if xMax != None:
	ID 	= EPMdata[0,:] <= xMax
	EPMdata = EPMdata[:,ID]

	ID 	= MCdata[0,:] <= xMax
	MCdata 	= MCdata[:,ID]
#==================================================================================
if isPDF:
	legend = ['EPM', 'MC']

	LEGPOS = [0.1, 0.5]

	if task == 'sensitivity':
		labels = {'xlabel': xlabels[var_param], 'ylabel': 'Sensitivity to ' + sensparam}
	else:
		labels = {'xlabel': xlabels[var_param], 'ylabel': 'Total uncertainty'}

#	labels = {'xlabel': xlabels[var_param], 'ylabel': ylabeldelta + ' ' + paramname[param]}
	plot2latex([EPMdata[0,:], MCdata[0,:]], [EPMdata[1,:], MCdata[1,:]], ylabel_vert=True, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, LineType=['','dash'], LegendPosition=LEGPOS)
elif matplot:
#	plt.ylabel(ylabeldelta + ' ' + paramname[param])
	plt.plot(MCdata[0,:], EPMdata[1,:])
	plt.grid()
	plt.show()
else:
	pass
#       print('Calculated data have been saved into a file.')

print('--------------------------------------------------------')
