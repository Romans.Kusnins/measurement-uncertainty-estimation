import os
import numpy as np
from slab_res3md import (three_slab_free_space, root_finding, POW2)
from Confidence_interval_calculation import *
#from matplotlib import pyplot as plt

#=======================================================================================
def MTTW2020_two_solid_posts():
	pass
#=======================================================================================
def length(x):
	if type(x) is np.ndarray:
		return max(x.shape)
	else:
		return len(x)
#=======================================================================================
def n2s(x, *numdec):
	if abs(x-int(x)) < 1e-15:	# Check whether x is an integer or not.
		return '%d' % (int(x))	# If this is the case, omit the comma 					# and the fractional part
	else:
		if len(numdec)>0 and int(numdec[0])>0:# if the number of decimal places is 					# specified print, the number with the 
					# numdec decimal>0 places, else display 6 decimal places 					# decimal places
			fstr = "%%1.%df" % (int(numdec[0]))
			return fstr % (x)
		else:
			return '%1.6f' % (x)
#=======================================================================================
def lettertype(line):
	chartype = list()
	for char in line:
		charcode = ord(char)
		if charcode > 96 and charcode < 123:
			chartype.append('low')
		else:
			chartype.append('up')
	return chartype
#=======================================================================================
#=======================================================================================
def plot2latex_MTTW2020_2():

	filename = 'THESIS_FIG_1'

	#ko = np.linspace(1.0, 20.0, 1000)

	ep1 	= 4.7
	ep2 	= 12.5
	d1 	= 10.0
	d2 	= 6.0
	L 	= 20.0
	f	= 9.0
	#========================================================================
	lam = 300.0/f;

	epv = np.arange(1, 20, 0.01)

	S11abs_1 = np.ndarray(epv.shape)
	S11abs_2 = np.ndarray(epv.shape)

	for n in range(epv.shape[0]):
        	S11abs_1[n] = np.abs(three_slab_free_space(ep1, 1.0, epv[n], d1/lam, d2/lam, L/lam))
        	S11abs_2[n] = np.abs(three_slab_free_space(1.0, 1.0, epv[n], d1/lam, d2/lam, L/lam))
	#========================================================================
	ko = epv

	yo = np.empty((2,length(ko)))

	ko = ko/np.max(ko)

	yo[0,:] = S11abs_1
	yo[1,:] = S11abs_2

	epl = [0.0, 0.0]
	epc = [0.0, 0.0]
	epu = [0.0, 0.0]

	S11 = [0.0, 0.0]

	S11_del 		= 0.05

	S11_			= np.abs(three_slab_free_space(ep1, 1.0, ep2, d1/lam, d2/lam, L/lam))
	epl_, epc_, epu_	= Confidence_interval_two_slabs_free(ep1, ep2, d1, d2, L, f, S11_del, 1e-4, 100)

	epl[0] 	= epl_
	epc[0] 	= epc_
	epu[0] 	= epu_

	S11[0]	= S11_

	S11_			= np.abs(three_slab_free_space(1.0, 1.0, ep2, d1/lam, d2/lam, L/lam))
	epl_, epc_, epu_	= Confidence_interval_two_slabs_free(1.0, ep2, d1, d2, L, f, S11_del, 1e-4, 100)

	epl[1] 	= epl_
	epc[1] 	= epc_
	epu[1] 	= epu_

	S11[1]	= S11_

	print(epl[0])
	print(epc[0])
	print(epu[0])

	print(S11[0])

	print(epl[1])
	print(epc[1])
	print(epu[1])

	print(S11[1])

	#===========================================================================
	LEN = yo.shape[0]

	linewidth = 0.5
	#===========================================================================
	xlabel_dist =  0.1
	ylabel_dist =  0.12
	#===========================================================================
	# LEGEND PARAMETERS
	#===========================================================================
	legend_position 	= [ 0.0, 0.0 ]

#	legend_line_dims 	= [ 0.01,  0.05 ]

	tickscale 		= 2.2		# scaling coefficient for the ticks labels

	legend_scale 		= 2.0		# scaling coefficient for the legend text

	labelx_scale 		= 2.6		# text scale for the x-axis label
	labely_scale 		= 2.4		# text scale for the y-axis label

	lwidth  		= 0.25
	lheight 		= 0.1

	legend_text 		= list()

	legend_text.append('ABC')
	legend_text.append('aaaaaa')
	legend_text.append('Abdsdsd')

	color = list()

	color.append('blue')
	color.append('red')
	color.append('black')
	color.append('magenta')
	color.append('green')

	fid = open(filename + '.tex', 'w')
	#===========================================================================
	komin = 0
	komax = max(epv)

	len = 0.05

	N = 1

	ko = ko[::N]
	yo = yo[:,::N]

	#==========================================================================
	# fid.write('\\usepackage[hiresbb, dvips]{graphicx} \n');
	# fid.write('\\usepackage{color} \n');

	str = ""
	str += '\\documentclass[convert]{standalone} \n' 
	str += '\\usepackage{siunitx} \n'
	str += '\\usepackage[dvips]{color} \n'
	str += '\\usepackage{textgreek} \n'
	str += '\\usepackage{tikz} \n'
	str += '\\usetikzlibrary{calc} \n'
	str += '\\usetikzlibrary{arrows} \n'
	str += '\\begin{document} \n'
	str += '\\begin{tikzpicture}[scale=20] \n'

	fid.write(str)
	#===========================================================================
	str = ""

	strlinewidth =  n2s(linewidth)

	for m in range(LEN):
		if m==2:
			yl = yo[m,0]
			kl = ko[0]

			for n in range(length(ko)-1):
				if np.abs(yl-yo[m,n]) > 0.02 or np.abs(kl-ko[n]) > 0.02:
					yl = yo[m,n]
					kl = ko[n]

					dotopt = 'line width=' + strlinewidth + 'mm'
					dot(fid, ko[n], yo[m,n], 0.02, color[m], dotopt)
		else:
			for n in range(length(ko)-1):
				lineopt = 'line width=' + strlinewidth + 'mm'
				line(fid, ko[n], yo[m,n], ko[n+1], yo[m,n+1], color[m], lineopt)

	fid.write(str)
	#===========================================================================
	# DRAW GRID LINES AND TICKS FOR THE GRAPH
	#===========================================================================
	str = ""

	x = np.linspace(0.0, 1.0, 6)
	y = np.arange(0.0, 1.0+0.1, 0.1)

	grid_line_V(fid, x, np.vstack([x, -0.01*np.ones((1, length(x)))]), x*np.max(epv), tickscale, [0,1], 'black')
	grid_line_H(fid, y, np.vstack([0.0*np.ones((1, length(y))), y]), y, tickscale, [0,1], 'black')

	axis_new(fid, [0.0, 1.0, 0.0, 1.0], 0.5)
	#===========================================================================
	for n in range(LEN):

		xval = (epl[n]/np.max(epv), epc[n]/np.max(epv), epu[n]/np.max(epv))
		yval = (S11[n] - S11_del, S11[n], S11[n] + S11_del)

		rectangle_rot(fid, [xval[0],     0.0, xval[2]-xval[0],             1.0], 0, [0, 0], 'black', color[n], 0.25, 'corner')
		rectangle_rot(fid, [	0.0, yval[0],             1.0, yval[2]-yval[0]], 0, [0, 0], 'black', color[n], 0.25, 'corner')

		hor_qnty 	= '\\varepsilon^{\\prime}_{\\mathrm{r3}}=' + n2s(epc[n],2)
		hor_delta  	= '\\Delta \\varepsilon^{\\prime}_{\\mathrm{r3}}=' + n2s(epu[n]-epl[n],2)

		ver_qnty	= 'S_{11}=' + n2s(S11[n], 2)
		ver_delta 	= '\\Delta S_{11}=' + n2s(2.0*S11_del,2)

		if n == 0:
			vert_lines(fid, filename, xval, color[n], 0.25, color[n], 2, (hor_qnty, hor_delta), index=n+1)
		else:
			vert_lines_no(fid, filename, xval, color[n], 0.25, color[n], 2, (hor_delta), index=n+1)

		horis_lines(fid, filename, yval, color[n], 0.25, color[n], 2, (ver_qnty, ver_delta), index=n+1)

	#===============================================================================
	fid.write('\\node[scale = ' + n2s(labelx_scale) + '](xlabel) at (0.5, ' + n2s(-xlabel_dist) + '){  $ a/\\lambda_{\\mathrm{o}} $  };\n')

	fid.write('\\node[scale = ' + n2s(labely_scale) + '](ylabel) at (' + n2s(-ylabel_dist) + ', 0.5){  $ S_{11}  $  };\n')
	#===========================================================================
	legend_box(fid, filename, LEN, legend_position, color, legend_text, legend_scale, 'white', 1)

	fid.write('\end{tikzpicture} \n')
	fid.write('\end{document} \n')

	fid.close()
	#===========================================================================
#	os.system('pdflatex MTTW_FIG_2.tex > /dev/null')
	os.system('pdflatex ' + filename + '.tex > /dev/null 2>&1')

	# open the generated PDF file with the default linux viewer
	os.system('xdg-open ' + filename + '.pdf')
#===================================================================================
def line(fd, x1, y1, x2, y2, colour, lineopt):
	str  = '\\draw[' + colour + ',' + lineopt + '] ('
	str += n2s(x1) + ',' + n2s(y1) + ') -- (' + n2s(x2) + ',' + n2s(y2) + ');\n'
	fd.write(str)
#===================================================================================
def line3(fd, x1, y1, x2, y2, x3, y3, colour, lineopt):
	str  = '\\draw[' + colour + ',' + lineopt + '] ('
	str += n2s(x1) + ',' + n2s(y1) + ') -- (' + n2s(x2) + ',' + n2s(y2) + ') -- (' + n2s(x3) + ',' + n2s(y3) + ');\n'
	fd.write(str)
#===================================================================================
def dot(fd, xo, yo, radius, colour, dotopt):
	str  = '\\draw[' + colour + ', ' + dotopt + ', fill='
	str += colour + '] (' + n2s(xo) + ',' + n2s(yo)
	str += ') circle (' + n2s(radius) + 'mm); \n'
	fd.write(str)
#===================================================================================
def rectangle(fd, xo, yo, wd, ht, colour, opacity):
	str  = '\\fill[' + colour + ', fill=' + colour + ', opacity='
	str +=  n2s(opacity) + '] (' + n2s(xo) + ',' + n2s(yo)
	str += ') rectangle (' + n2s(xo+wd) + ',' + n2s(yo+ht) + ');\n'
	fd.write(str)
#===================================================================================
#===================================================================================
#===================================================================================
def legend_box(fd, filename, NUM, position, color_array, text_array, text_scale, colour, opacity):

	fontsize  	= 10.0
	#-------------------------------------------------------------------------
	# Legend box parameters
	#-------------------------------------------------------------------------
	line_len 	= 0.1		# legnths of the legend lines

	leftsep 	= 0.01		# spacing between the beginings of the lines and the left side of the legend box
	linetextsep 	= 0.01		# spacing between the ends of the lines and the respective items
	linebox 	= 0.01		# spacing between the text and the right side of outline

	topsep 		= 0.012		# spacing between the top side of the legend box and the text
	intersep	= 0.012		# spacing between the legend items
	botsep		= 0.012		# spacing between the bottom side of the legend box and the text
	#-------------------------------------------------------------------------
	# Determine legend box dimensions
	#-------------------------------------------------------------------------
	str = ""

	xo = position[0]
	yo = position[1]

	no_aux_file = False

	try:
		faux = open(filename + '.aux', 'r')
		fdata = faux.read()
		if fdata.find('\\boxwidth') < 0:
			no_aux_file = True
		else:
			widths	= list()
			heights	= list()
			fdata 	= fdata.split()
			for fline in fdata:
				if fline.find('\\boxwidth') > -1:
					widths.append(float(fline[(fline.find('{')+1):fline.find('pt')]))
				if fline.find('\\boxheight') > -1:
					heights.append(float(fline[(fline.find('{')+1):fline.find('pt')]))
		faux.close()
	except FileNotFoundError:
		no_aux_file = True
	#---------------------------------------------------------------------------
	if not no_aux_file:
		max_line_len 	= max(widths)*0.00175*text_scale
		item_height	= max(heights)*0.00175*text_scale	# single legend item height
	else:
		max_line_len 	= 0.0
		item_height 	= 0.0

	width	= max_line_len - 0.0125*text_scale + leftsep + line_len + linetextsep + linebox
	height	= NUM*item_height + (NUM-1)*intersep + topsep + botsep

	rectangle(fd, xo, yo, width, height, colour, opacity)
	#---------------------------------------------------------------------------
	for m in range(NUM):

		xn 	= xo - 0.0125*text_scale + leftsep + line_len + linetextsep
		shift_y	= 0.0062*text_scale*fontsize/10.0	# shift for 10pt font in the vertical direction
		yn 	= shift_y + yo + height - item_height - topsep - m*(intersep + item_height)

		lineopt = 'line width=0.25mm'
		line(fd, xo + leftsep, yn, xo + leftsep + line_len, yn, color_array[m], lineopt)

		item_text = '$ \\text{\\fontsize{' + n2s(fontsize) + '}{' + n2s(fontsize) + '}\selectfont ' + text_array[m] + '} $'

		if no_aux_file:
			fd.write('\\newbox\\bxa%\n')
			fd.write('\setbox\\bxa=\hbox{\pgfinterruptpicture $' + item_text + '$\endpgfinterruptpicture}\\relax%\n')
			fd.write('\makeatletter%\n\@bsphack \protected@write \@auxout {}{\string\def\string\\boxwidth{\\the\wd\\bxa}}\@esphack%\n\makeatother%\n')
			fd.write('\makeatletter%\n\@bsphack \protected@write \@auxout {}{\string\def\string\\boxheight{\\the\ht\\bxa}}\@esphack%\n\makeatother%\n')

		text_node(fd, [xn, yn], text_scale, 'black', item_text, orient='right')

	fd.write(str)
#======================================================================================
def axis_new(fd, dims, *varargin):

	str = ""

	x_min = dims[0]
	x_max = dims[1]

	y_min = dims[2]
	y_max = dims[3]

	if len(varargin) == 1:
		line_width = 'line width=' + n2s(varargin[0]) + 'mm'
	else:
		line_width = 'line width=0.25mm'	# default line width

	colour = 'black'

	line(fd, x_min, y_min, x_min, y_max, colour, line_width)
	line(fd, x_max, y_min, x_max, y_max, colour, line_width)
	line(fd, x_min, y_min, x_max, y_min, colour, line_width)
	line(fd, x_min, y_max, x_max, y_max, colour, line_width)

	fd.write(str)
#===========================================================================
def grid_line_V(fd, xvec, value_position, value_array, tickscale, dims, *varargin):

	str = ""

	LEN = length(xvec)

	if len(varargin) == 1:
		line_colour = varargin[0]
	else:
		line_colour = 'black'

	lineopt  = 'line width=0.25mm, dashed,'
	lineopt += 'dash pattern=on 1.0mm off 1.0mm'
	for n in range(LEN):
		if n > 0 and n < LEN-1:
			line(fd, xvec[n], dims[0], xvec[n], dims[1], line_colour, lineopt)

		text_node(fd, [value_position[0,n], value_position[1,n]], tickscale, 'black', n2s(value_array[n],1), orient='below')

	fd.write(str)
#=============================================================================
def grid_line_H(fd, yvec, value_position, value_array, tickscale, dims, varargin):

	str = ""

	LEN = length(yvec)

	if len(varargin) == 1:
		line_colour = varargin[0]
	else:
		line_colour = 'black'

	lineopt  = 'line width=0.25mm, dashed,'
	lineopt += 'dash pattern=on 1.0mm off 1.0mm'
	for n in range(LEN):
		if n > 0 and n < LEN-1:
			line(fd, dims[0], yvec[n], dims[1], yvec[n], line_colour, lineopt)

		text_node(fd, [value_position[0,n], value_position[1,n]], tickscale, 'black', n2s(value_array[n],1), orient='left')

	fd.write(str)
#============================================================================
def ellipse(fd, dims, rotang, colour, opacity):

	str = '\\draw[rotate around={' + n2s(rotang) + ':' + '(' + n2s(dims[0])
	str += ',' + n2s(dims[1]) + ')}, fill=' + colour + ', opacity='
	str += n2s(opacity) + '] (' + n2s(dims[0]) + ',' + n2s(dims[1])
	str += ') ellipse (' + n2s(dims[2]) + ' and ' + n2s(dims[3]) + ');\n'

	fd.write(str)
#=============================================================================
def rectangle_rot(fd, dims, rot_ang, rot_cnt, colour, fill_colour, opacity, origin):

	sn = np.sin(rot_ang/180.0*np.pi)
	cn = np.cos(rot_ang/180.0*np.pi)

	xo = dims[0]
	yo = dims[1]

	width  = dims[2]
	height = dims[3]

	if origin == 'center':
		u1 = (xo - 0.5*width)  - rot_cnt[0]
		v1 = (yo - 0.5*height) - rot_cnt[1]

		u2 = (xo - 0.5*width)  - rot_cnt[0]
		v2 = (yo + 0.5*height) - rot_cnt[1]

		u3 = (xo + 0.5*width)  - rot_cnt[0]
		v3 = (yo + 0.5*height) - rot_cnt[1]

		u4 = (xo + 0.5*width)  - rot_cnt[0]
		v4 = (yo - 0.5*height) - rot_cnt[1]
	else:
		u1 = (xo - 0.0)    - rot_cnt[0]
		v1 = (yo - 0.0)    - rot_cnt[1]

		u2 = (xo - 0.0)    - rot_cnt[0]
		v2 = (yo + height) - rot_cnt[1]

		u3 = (xo + width)  - rot_cnt[0]
		v3 = (yo + height) - rot_cnt[1]

		u4 = (xo + width)  - rot_cnt[0]
		v4 = (yo - 0.0)    - rot_cnt[1]
	#=====================================================
	x1 = rot_cnt[0] + u1*cn - v1*sn
	y1 = rot_cnt[1] + u1*sn + v1*cn

	x2 = rot_cnt[0] + u2*cn - v2*sn
	y2 = rot_cnt[1] + u2*sn + v2*cn

	x3 = rot_cnt[0] + u3*cn - v3*sn
	y3 = rot_cnt[1] + u3*sn + v3*cn

	x4 = rot_cnt[0] + u4*cn - v4*sn
	y4 = rot_cnt[1] + u4*sn + v4*cn
	#===========================================================================
	str  = '\\draw[' + colour + ', fill=' + fill_colour + ', opacity='
	str += n2s(opacity) + '](' + n2s(x1) + ',' + n2s(y1) + ')--('
	str += n2s(x2) + ',' + n2s(y2) + ')--(' + n2s(x3) + ',' + n2s(y3)
	str += ')--(' + n2s(x4) + ',' + n2s(y4) + ')--cycle;'

	fd.write(str)

#============================================================================
def dimensionsto2auxfile(fd, filename, text_string, name, **kwd):

	no_aux_file 	= False
	widths 		= list()
	heights 	= list()
	try:

		faux 	= open(filename + '.aux', 'r')
		fdata 	= faux.read()

		if 'index' in kwd.keys():
			if fdata.find(name + 'width' + n2s(kwd['index'])) < 0:
				no_aux_file = True
			else:
				fdata = fdata.split()
				for fline in fdata:
					if fline.find(name + 'width' + n2s(kwd['index'])) > -1:
						widths.append(float(fline[(fline.find('{')+1):fline.find('pt}')]))
					if fline.find(name + 'height' + n2s(kwd['index'])) > -1:
						heights.append(float(fline[(fline.find('{')+1):fline.find('pt}')]))
		else:
			if fdata.find(name + 'width') < 0:
				no_aux_file = True
			else:
				fdata = fdata.split()
				for fline in fdata:
					if fline.find(name + 'width') > -1:
						widths.append(float(fline[(fline.find('{')+1):fline.find('pt}')]))
					if fline.find(name + 'height') > -1:
						heights.append(float(fline[(fline.find('{')+1):fline.find('pt}')]))

		faux.close()
	except FileNotFoundError:
		no_aux_file = True

	if no_aux_file:

		fd.write('\\newbox\\bxa%\n')
		fd.write('\setbox\\bxa=\hbox{\pgfinterruptpicture $' + text_string + '$\endpgfinterruptpicture}\\relax%\n')

		if 'index' in kwd.keys():
			fd.write('\makeatletter%\n\@bsphack \protected@write \@auxout {}{\string\def\string\\' + name + 'width' + n2s(kwd['index']) + '{\\the\wd\\bxa}}\@esphack%\n\makeatother%\n')
			fd.write('\makeatletter%\n\@bsphack \protected@write \@auxout {}{\string\def\string\\' + name + 'height' + n2s(kwd['index']) + '{\\the\ht\\bxa}}\@esphack%\n\makeatother%\n')
		else:
			fd.write('\makeatletter%\n\@bsphack \protected@write \@auxout {}{\string\def\string\\' + name + 'width{\\the\wd\\bxa}}\@esphack%\n\makeatother%\n')
			fd.write('\makeatletter%\n\@bsphack \protected@write \@auxout {}{\string\def\string\\' + name + 'height{\\the\ht\\bxa}}\@esphack%\n\makeatother%\n')

	return no_aux_file, widths, heights
#============================================================================
def line_vert(fd, x_pos, y_ext, colour, line_width, *varargin):

	if len(varargin) == 1:
		dims = varargin[0]
		dashed = [', dashed, dash pattern=on ' + n2s(dims(1)) + 'mm off ' + n2s(dims(2)) + 'mm']
	else:
		dashed = ''

	lineopt  = 'line width=' + n2s(line_width) + 'mm ' + dashed

	x1 =  x_pos
	y1 = -y_ext
	x2 =  x1
	y2 =  1.0

	line(fd, x1, y1, x2, y2, colour, lineopt)
#============================================================================
def line_vert_folded(fd, x_pos, dims, colour, line_width, *varargin):

	if len(varargin) == 1:
		dims = varargin[0]
		dashed = ', dashed, dash pattern=on ' + n2s(dims[0]) + 'mm off ' + n2s(dims[1]) + 'mm'
	else:
		dashed = ''

	lineopt = 'line width=' + n2s(line_width) + 'mm ' + dashed

	x1 =  x_pos
	y1 =  1.0
	x2 =  x1
	y2 = -dims[0]
	x3 =  x1 + dims[1]
	y3 = -dims[0]

	line3(fd, x1, y1, x2, y2, x3, y3, colour, lineopt)
#============================================================================
def line_horis(fd, y_pos, x_ext, colour, line_width, *varargin):

	if len(varargin) == 1:
		dims = varargin[0]
		dashed = ', dashed, dash pattern=on ' + n2s(dims[0]) + 'mm off ' + n2s(dims[1]) + 'mm'
	else:
		dashed = ''

	lineopt = 'line width=' + n2s(line_width) + 'mm ' + dashed

	x1 =  1.0
	y1 =  y_pos
	x2 = -x_ext
	y2 =  y1

	line(fd, x1, y1, x2, y2, colour, lineopt)
#===============================================================================
def line_horis_folded(fd, y_pos, dims, colour, line_width, *varargin):

	if len(varargin) == 1:
		dims = varargin[0]
		dashed = ', dashed, dash pattern=on ' + n2s(dims[0]) + 'mm off ' + n2s(dims[1]) + 'mm'
	else:
		dashed = ''

	lineopt = 'line width=' + n2s(line_width) + 'mm ' + dashed

	x1 =  1.0
	y1 =  y_pos
	x2 = -dims[0]
	y2 =  y1
	x3 = -dims[1]
	y3 =  y1 + dims[1]

	line3(fd, x1, y1, x2, y2, x3, y3, colour, lineopt)
#================================================================================
def dimensions_H(fd, pos, dims, colour, line_width):

	lineopt = 'line width=' + n2s(line_width) + 'mm'

	x1 = pos[0] - dims[0]
	y1 = pos[1]
	x2 = pos[0]
	y2 = y1

	line(fd, x1, y1, x2, y2, colour, lineopt)
	#-----------------------------------------------------------------------
	lineopt = 'line width=' + n2s(line_width) + 'mm'

	x1 = pos[0] + dims[1]
	y1 = pos[1]
	x2 = x1 + dims[2]
	y2 = y2

	line(fd, x1, y1, x2, y2, colour, lineopt)
#============================================================================
def dimensions_V_inv(fd, pos, dims, colour, line_width):

	lineopt = 'line width=' + n2s(line_width) + 'mm,->'

	x1 = pos[0] - dims[3]
	y1 = pos[1] + dims[0]
	x2 = pos[0]
	y2 = y1
	x3 = x2
	y3 = pos[1]

	line3(fd, x1, y1, x2, y2, x3, y3, colour, lineopt)

	lineopt = 'line width=' + n2s(line_width) + 'mm,<-'
	#-------------------------------------------------------------------
	x1 = pos[0]
	y1 = pos[1] - dims[1]
	x2 = x1
	y2 = y1 - dims[2]

	line(fd, x1, y1, x2, y2, colour, lineopt)
#============================================================================
def dimensions_V(fd, pos, dims, colour, line_width):

	lineopt = 'line width=' + n2s(line_width) + 'mm,->'

	x1 = pos[0]
	y1 = pos[1] + dims[0]
	x2 = x1
	y2 = pos[1]

	line(fd, x1, y1, x2, y2, colour, lineopt)

	lineopt = 'line width=' + n2s(line_width) + 'mm,<-'

	x1 = pos[0]
	y1 = pos[1] - dims[1]
	x2 = pos[0]
	y2 = pos[1] - dims[1] - dims[2]
	x3 = pos[0] - dims[3]
	y3 = pos[1] - dims[1] - dims[2]

	line3(fd, x1, y1, x2, y2, x3, y3, colour, lineopt)
#=============================================================================
def text_node(fd, pos, scale, colour, textstring, **kwd):

	orient = kwd.get('orient', 'above')

	str  = '\\node[' + orient + ' ,scale=' + n2s(scale) + ',' + colour + '](a) at ('
	str += n2s(pos[0]) + ',' + n2s(pos[1]) + '){$' + textstring + '$};\n'

	fd.write(str)
#=============================================================================
def vert_lines(fd, filename, pos, line_colour, line_width, text_colour, text_scale, text_string, **kwd):
#def vert_lines(fd, filename, pos, dims, pos_arrow, dims_arrow, pos_text, line_colour, line_width, text_colour, text_scale, text_string, **kwd):

	fontsize 	= 10.0			# marker font size in Pt

	rightarrlen	= 0.05			# right delta marker arrow length
	delmarkersep	= 0.25			# distance between the delta maker data line and the lower side of the graph
	botsep 		= 0.01			# spacing between the marker lines and the text
	dellinesep	= 0.05			# spacing between the left delta marker line and the correspoding text
	sline_ext	= 0.05			# extension of the delta marker lines
	deltaqntsep	= 0.07			# spacing between the delta marker line and qunetity marker line
	qntlinsep	= 0.05			# distance between the maker data line and the marker text
	#----------------------------------------------------------------------
	no_aux_file_q, qwidths, qheights = dimensionsto2auxfile(fd, filename, text_string[0], 'verqnt', **kwd)
	no_aux_file_d, dwidths, dheights = dimensionsto2auxfile(fd, filename, text_string[1], 'verdel', **kwd)

	if not no_aux_file_q:
		qnt_label_wd	= max(qwidths)*0.00175*text_scale
		qnt_label_ht	= max(qheights)*0.00175*text_scale	# single legend item height
		del_label_wd	= max(dwidths)*0.00175*text_scale
		del_label_ht	= max(dheights)*0.00175*text_scale	# single legend item height
	else:
		qnt_label_wd	= 0.0
		qnt_label_ht	= 0.0
		del_label_wd	= 0.0
		del_label_ht	= 0.0
	#=======================================================================
	delta 		= pos[2]-pos[0]		# distance between the arrow tips (equals bar width)

	shift_y	= 0.0062*text_scale*fontsize/10.0

	xo = pos[0] - 0.0125/2.0*text_scale - del_label_wd - dellinesep
	yo = shift_y - delmarkersep + botsep

	xl = pos[1] - 0.0125/2.0*text_scale + qntlinsep
	yl = yo - qnt_label_ht - botsep - deltaqntsep
	#----------------------------------------------------------------------
	delta_line_len 	= del_label_wd + dellinesep

	sline_len 	= delmarkersep + sline_ext
	cline_len 	= delmarkersep + qnt_label_ht + botsep + deltaqntsep
	qntline_len	= qntlinsep + qnt_label_wd

	line_vert_folded(fd, pos[1], [cline_len, qntline_len], line_colour, line_width)

	line_vert(fd, pos[0], sline_len, line_colour, line_width)
	line_vert(fd, pos[2], sline_len, line_colour, line_width)

	dimensions_H(fd, (pos[0], -delmarkersep), (delta_line_len, delta, rightarrlen), line_colour, line_width)

	text_node(fd, [xl, yl], text_scale, text_colour, text_string[0], orient='right')
	text_node(fd, [xo, yo], text_scale, text_colour, text_string[1], orient='right')

#===============================================================================
def vert_lines_no(fd, filename, pos,line_colour, line_width, text_colour, text_scale, text_string, **kwd):

	fontsize 	= 10.0			# marker font size in Pt

	rightarrlen	= 0.05			# right delta marker arrow length
	delmarkersep	= 0.20			# distance between the delta maker data line and the lower side of the graph
	botsep 		= 0.01			# spacing between the marker lines and the text
	dellinesep	= 0.05			# spacing between the left delta marker line and the correspoding text
	sline_ext	= 0.05			# extension of the delta marker lines
	deltaqntsep	= 0.07			# spacing between the delta marker line and qunetity marker line
	qntlinsep	= 0.05			# distance between the maker data line and the marker text
	#----------------------------------------------------------------------
	no_aux_file_d, dwidths, dheights = dimensionsto2auxfile(fd, filename, text_string, 'verdel', **kwd)

	if not no_aux_file_d:
		del_label_wd	= max(dwidths)*0.00175*text_scale
		del_label_ht	= max(dheights)*0.00175*text_scale	# single legend item height
	else:
		del_label_wd	= 0.0
		del_label_ht	= 0.0
	#=======================================================================
	delta 		= pos[2] - pos[0]	# distance between the arrow tips (equals bar width)

	shift_y	= 0.0062*text_scale*fontsize/10.0

	xo = pos[0] - 0.0125/2.0*text_scale - del_label_wd - dellinesep
	yo = shift_y - delmarkersep + botsep
	#----------------------------------------------------------------------
	delta_line_len 	= del_label_wd + dellinesep

	sline_len 	= delmarkersep + sline_ext

	line_vert(fd, pos[0], sline_len, line_colour, line_width)
	line_vert(fd, pos[2], sline_len, line_colour, line_width)

	dimensions_H(fd, (pos[0], -delmarkersep), (delta_line_len, delta, rightarrlen), line_colour, line_width)

	text_node(fd, [xo, yo], text_scale, text_colour, text_string, orient='right')

#===============================================================================
def horis_lines(fd, filename, pos, line_colour, line_width, text_colour, text_scale, text_string, **kwd):

#	text_scale 	= 2.0

	arrow_hor_sep	= 0.15			# spacing between the right graph side and the marker arrow
	upparrowlen 	= 0.05			# length of the upper arrow
	lowarrlen 	= 0.1			# length of the lower arrow
	side_line_ext	= 0.1			# amount of the side line extensions
	botsep 		= 0.01			# spacing between the central line of the bar and the text
	rightsep	= 0.20			# spacing between the text box and the graph left side
	delarrsep	= 0.05			# spacing between the arrow position and the delta marker text
	lowtextsep	= 0.05			# spacing between the lower marker line and delta marker text
	#------------------------------------------------------------------------
	no_aux_file_q, qwidths, qheights = dimensionsto2auxfile(fd, filename, text_string[0], 'horqnt', **kwd)
	no_aux_file_d, dwidths, dheights = dimensionsto2auxfile(fd, filename, text_string[1], 'hordel', **kwd)

	if not no_aux_file_q:
		qnt_label_wd	= max(qwidths)*0.00175*text_scale
		qnt_label_ht	= max(qheights)*0.00175*text_scale	# single legend item height
		del_label_wd	= max(dwidths)*0.00175*text_scale
		del_label_ht	= max(dheights)*0.00175*text_scale	# single legend item height
	else:
		qnt_label_wd	= 0.0
		qnt_label_ht	= 0.0
		del_label_wd	= 0.0
		del_label_ht	= 0.0
	#-----------------------------------------------------------------------
	fontsize = 10.0
	#-----------------------------------------------------------------------
	delta		= pos[2]-pos[0]					# distance between the tips of the arrows

	shift_y	= 0.0062*text_scale*fontsize/10.0

	xo = -0.0125/2.0*text_scale - qnt_label_wd - rightsep
	yo =  pos[1] + shift_y + botsep

	xl = -0.0125/2.0*text_scale - del_label_wd - arrow_hor_sep - delarrsep
	yl =  pos[0] + shift_y + botsep - 2.0*del_label_ht - lowtextsep

	cline_len = qnt_label_wd + rightsep
	sline_len = arrow_hor_sep + side_line_ext
	#----------------------------------------------------------------------
	line_horis(fd, pos[1], cline_len, line_colour, line_width)
	line_horis(fd, pos[0], sline_len, line_colour, line_width)
	line_horis(fd, pos[2], sline_len, line_colour, line_width)

	lowarrlen 	= 2.0*del_label_ht + lowtextsep
	del_line_len 	= del_label_wd + arrow_hor_sep - arrow_hor_sep + delarrsep

	dimensions_V(fd, (-arrow_hor_sep, pos[2]), (upparrowlen, delta, lowarrlen, del_line_len), line_colour, line_width)

	text_node(fd, [xo ,yo], text_scale, text_colour, text_string[0], orient='right')
	text_node(fd, [xl, yl], text_scale, text_colour, text_string[1], orient='right')
#===============================================================================

plot2latex_MTTW2020_2()
