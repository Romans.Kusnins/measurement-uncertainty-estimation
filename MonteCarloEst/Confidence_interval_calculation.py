import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, root_finding, single_slab_guide_matrix, POW2)
from MoM import *

#=================================================================================
#=================================================================================

def Confidence_interval_two_slabs_free(ep1, ep2, d1, d2, L, f, del_S11abs, NTOL, MAXIT):
	#-------------------------------------------------------------------------
	# ep1 	= 4.7		# auxiliary slab dielectric constant
	# ep2 	= 12.5		# MUT slab dielectric constant
	#
	# d1  	= 10.0 		# auxiliary slab thickness in mm
	# d2  	= 6.0		# MUT slab thickness in mm
	#
	# L   	= 20.0		# interslab separation distance in mm
	# f   	= 9.0		# operating frequency in GHz
	#
	# MAXIT = 100		# max number of iterations for Newton's method
	# NTOL 	= 1e-4		# solution tolerance for Newton's method
	#-------------------------------------------------------------------------
	lam = 300.0/f;
	#-------------------------------------------------------------------------
	# EVALUATING COVERAGE INTERVAL WIDTH
	#-------------------------------------------------------------------------
	# del_S11abs = 0.010

	S11 	= np.abs(three_slab_free_space(ep1, 1.0, ep2, d1/lam, d2/lam, L/lam))
	S11p 	= np.abs(three_slab_free_space(ep1, 1.0, ep2+1e-3, d1/lam, d2/lam, L/lam))

	S11abs_der = (S11p - S11)/1e-3

	def func(epvar, **kwd):
		S11 = three_slab_free_space(ep1, 1.0, epvar, d1/lam, d2/lam, L/lam)
		return np.abs(S11)
	#==========================================================================
	epc = root_finding(func, ep2, 1e-8, NTOL, MAXIT, val=S11, deriv=S11abs_der)

	epu = root_finding(func, ep2, 1e-8, NTOL, MAXIT, val=S11, delta=del_S11abs, deriv=S11abs_der)

	epl = root_finding(func, ep2, 1e-8, NTOL, MAXIT, val=S11, delta=-del_S11abs, deriv=S11abs_der)
	#==========================================================================
	return S11, epl, epc, epu

def Confidence_interval_two_slabs_guide(a, ep1, ep2, d1, d2, L, f, del_S11abs, NTOL, MAXIT):
	#--------------------------------------------------------------------------
	# ep1 	= 4.7		# MUT dielectric constant
	# ep2 	= 12.5		# auxiliary slabs dielectric constant
	#
	# d1  	= 10.0 		# measurable slab thickness in mm
	# d2  	= 6.0		# auxiliary slab thickness in mm
	#
	# L   	= 20.0		# interslab separation distance in mm
	# f   	= 9.0		# operating frequency in GHz
	#
	# a	= 22.86		# width of the waveguide broader wall
	#
	# MAXIT = 100		# max number of iterations for Newton's method
	# NTOL 	= 1e-4		# solution tolerance for Newton's method
	#--------------------------------------------------------------------------
	lam = 300.0/f;
	#--------------------------------------------------------------------------
	# EVALUATING COVERAGE INTERVAL WIDTH
	#--------------------------------------------------------------------------
	# del_S11abs = 0.010

	S11 	= np.abs(three_slab_guide(a/lam, ep1, 1.0,      ep2, d1/a, d2/a, L/a))
	S11p 	= np.abs(three_slab_guide(a/lam, ep1, 1.0, ep2+1e-3, d1/a, d2/a, L/a))

	S11abs_der = (S11p - S11)/1e-3

	def func(epvar, **kwd):
		S11 = three_slab_guide(a/lam, ep1, 1.0, epvar, d1/a, d2/a, L/a)
		return np.abs(S11)
	#==========================================================================
	epc = root_finding(func, ep2, 1e-6, NTOL, MAXIT, val=S11, deriv=S11abs_der)

	epu = root_finding(func, ep2, 1e-6, NTOL, MAXIT, val=S11, delta=del_S11abs, deriv=S11abs_der)

	epl = root_finding(func, ep2, 1e-6, NTOL, MAXIT, val=S11, delta=-del_S11abs, deriv=S11abs_der)
	#==========================================================================

	return S11, epl, epc, epu

def Confidence_interval_three_slabs_guide(a, ep1, epm, ep2, d1, d2, L, f, del_S11abs, NTOL, MAXIT):
	#--------------------------------------------------------------------------
	# ep1 	= 4.2		# auxiliary slab dielectric constant
	# ep2 	= 12.5		# MUT slab dielectric constant
	# epm 	= 2.2		# middle slab dielectric constant
	#
	# d1  	= 10.0 		# auxiliary slab thickness in mm
	# d2  	= 6.0		# MUT slab thickness in mm
	#
	# L   	= 20.0		# interslab separation distance in mm
	# f   	= 9.0		# operating frequency in GHz
	#
	# a	= 22.86		# width of the waveguide broader wall
	#
	# MAXIT = 100		# max number of iterations for Newton's method
	# NTOL 	= 1e-4		# solution tolerance for Newton's method
	#--------------------------------------------------------------------------
	lam = 300.0/f;
	#--------------------------------------------------------------------------
	# EVALUATING COVERAGE INTERVAL WIDTH
	#--------------------------------------------------------------------------
	# del_S11abs = 0.010

	S11 	= np.abs(three_slab_guide(a/lam, ep1, epm,      ep2, d1/a, d2/a, L/a))
	S11p 	= np.abs(three_slab_guide(a/lam, ep1, epm, ep2+1e-3, d1/a, d2/a, L/a))

	S11abs_der = (S11p - S11)/1e-3

	def func(epvar, **kwd):
		S11 = three_slab_guide(a/lam, ep1, epm, epvar, d1/a, d2/a, L/a)
		return np.abs(S11)
	#==========================================================================
	epc = root_finding(func, ep2, 1e-8, NTOL, MAXIT, val=S11, deriv=S11abs_der)

	epu = root_finding(func, ep2, 1e-8, NTOL, MAXIT, val=S11, delta=del_S11abs, deriv=S11abs_der)

	epl = root_finding(func, ep2, 1e-8, NTOL, MAXIT, val=S11, delta=-del_S11abs, deriv=S11abs_der)
	#==========================================================================
	return S11, epl, epc, epu

def Confidence_interval_three_slabs_free(ep1, epm, ep2, d1, d2, L, f, del_S11abs, NTOL, MAXIT):
	#-------------------------------------------------------------------------
	# ep1 	= 4.2		# auxiliary slab dielectric constant
	# ep2 	= 12.5		# MUT slab dielectric constant
	# epm 	= 2.2		# middle slab dielectric constant
	#
	# d1  	= 10.0 		# auxiliary slab thickness in mm
	# d2  	= 10.0		# MUT slab thickness in mm
	#
	# L   	= 23.0		# interslab separation distance in mm
	# f   	= 10.0		# operating frequency in GHz
	#
	# MAXIT = 100		# max number of iterations for Newton's method
	# NTOL 	= 1e-4		# solution tolerance for Newton's method
	#-------------------------------------------------------------------------
	lam = 300.0/f;
	#-------------------------------------------------------------------------
	# EVALUATING COVERAGE INTERVAL WIDTH
	#-------------------------------------------------------------------------
	# del_S11abs = 0.010

	S11 	= np.abs(three_slab_free_space(ep1, epm, ep2, d1/lam, d2/lam, L/lam))
	S11p 	= np.abs(three_slab_free_space(ep1, epm, ep2+1e-3, d1/lam, d2/lam, L/lam))

	S11abs_der = (S11p - S11)/1e-3

	def func(epvar, **kwd):
		S11 = three_slab_free_space(ep1, epm, epvar, d1/lam, d2/lam, L/lam)
		return np.abs(S11)
	#==========================================================================
	epc = root_finding(func, ep2, 1e-8, NTOL, MAXIT, val=S11, deriv=S11abs_der)

	epu = root_finding(func, ep2, 1e-8, NTOL, MAXIT, val=S11, delta=del_S11abs, deriv=S11abs_der)

	epl = root_finding(func, ep2, 1e-8, NTOL, MAXIT, val=S11, delta=-del_S11abs, deriv=S11abs_der)
	#==========================================================================
	return S11, epl, epc, epu

def Confidence_interval_two_rods(a, ep1, ep2, ro1, ro2, L, f, del_S11abs, NTOL, MAXIT, NM, N):

	#--------------------------------------------------------------------------
	# ep1 	= 4.2		# auxiliary rod dielectric constant
	# ep2 	= 12.2		# MUT rod dielectric constant
	#
	# ro1  	= 7.0 		# auxiliary rod radius in mm
	# ro2  	= 3.0		# MUT rod radius in mm
	#
	# L   	= 32.0		# interrod separation distance in mm
	# f   	= 10.0		# operating frequency in GHz
	#
	# a   	= 22.86		# waveguide width in mm
	#
	# MAXIT	= 100		# max number of iterations for Newton's method
	# NTOL 	= 1e-4		# solution tolerance for Newton's method
	#
	# NM	= 3		# number of higher order waveguide modes considered
	# N	= 5		# number of basis functions for each rod surface
	#--------------------------------------------------------------------------
	#--------------------------------------------------------------------------
	MoMinit()		# C++ library initialization

	lam = 300.0/f
	#==========================================================================
	def evalparam(ep, ro):
		param 		= MoMparam()
		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5
		param.zo 	= 0.0

		return param
	#==========================================================================
	def MoM_calc_S_matrix(ep, ro):
		param = evalparam(ep, ro)
		S11a, S12a, S21a, S22a = MoM_single_solid_rod_(param, NM, N)
		return S11a, S12a, S21a, S22a
	#==========================================================================
	def calcS11(S11b):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#==========================================================================
	def calcT(L, lam):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#==========================================================================
	# calculate S11meas - the total reflection coefficient
	S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1)
	T 			= calcT(L, lam)
	#--------------------------------------------------------------------------
	S11b, S12b, S21b, S22b 	= MoM_calc_S_matrix(ep2, ro2)
	S11 			= np.abs(calcS11(S11b))
	# calculate the derivative of S11meas
	S11b, S12b, S21b, S22b	= MoM_calc_S_matrix(ep2+1e-3, ro2)
	S11p 			= np.abs(calcS11(S11b))

	S11_der 		= (S11p - S11)/1e-3
	#--------------------------------------------------------------------------
	# EVALUATING MEASUREMENT UNCERTAINTY
	#--------------------------------------------------------------------------
	def func(epvar, **kwd):
		param.ep 		= epvar
		S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
		return  np.abs(calcS11(S11b))

	param = evalparam(ep2, ro2)

	epc = root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=S11)
	epu = root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=S11, delta= del_S11abs)
	epl = root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=S11, delta=-del_S11abs)

	return S11, epl, epc, epu


def Confidence_interval_three_rods(a, ep1, ep2, ep3, ro1, ro2, ro3, L1, L2, f, del_S11abs, NTOL, MAXIT, NM, N):

	#--------------------------------------------------------------------------
	# ep1 	= 12.2		# 1st auxiliary rod dielectric constant
	# ep2 	= 4.2		# MUT rod dielectric constant
	# ep3 	= 12.2		# 2nd auxiliary rod dielectric constant
	#
	# ro1  	= 3.0		# 1st auxiliary rod radius in mm
	# ro2  	= 7.0 		# MUT rod radius in mm
	# ro3  	= 3.0		# 2nd auxiliary rod radius in mm
	#
	# L1   	= 15.0		# distance between the 1st and the 2nd rods in mm
	# L2   	= 15.0		# distance between the 2nd and the 3rd rods in mm
	#
	# f   	= 10.0		# operating frequency in GHz
	#
	# a   	= 22.86		# waveguide width in mm
	#
	# MAXIT	= 100		# max number of iterations for Newton's method
	# NTOL 	= 1e-4		# solution tolerance for Newton's method
	#
	# NM	= 3		# number of higher order waveguide modes considered
	# N	= 5		# number of basis functions for each rod surface
	#--------------------------------------------------------------------------
	#--------------------------------------------------------------------------
	MoMinit()		# perform C++ library initialization

	lam = 300.0/f
	#==========================================================================
	def evalparam(ep, ro):
		param 		= MoMparam()
		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5
		param.zo 	= 0.0
		return param
	#==========================================================================
	def MoM_calc_S_matrix(ep, ro):
		param = evalparam(ep, ro)
		S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
		return S11, S12, S21, S22
	#==========================================================================
	def calcS(S11b, S12b, S21b, S22b, T):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
			S21 = S21a*T/(1.0 - S11b*T*S22a*T)*S21b
			S12 = S12b*T/(1.0 - S22a*T*S11b*T)*S12a
			S22 = S22b + S12b*T*S22a*T/(1.0 - S11b*T*S22a*T)*S21b
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a
			S21 = S21a*T*np.linalg.inv(np.eye(NM) - S11b*T*S22a*T)*S21b
			S12 = S12b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a
			S22 = S22b + S12b*T*S22a*T*np.linalg.inv(np.eye(NM) - S11b*T*S22a*T)*S21b

		return S11, S12, S21, S22
	#==========================================================================
	def calcS11(S11a, S12a, S21a, S22a, S11b, T):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#==========================================================================
	def calcT(L, lam):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#==========================================================================
	# calculate S11meas - the total reflection coefficient
	S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1)
	T1 			= calcT(L1, lam)
	T2 			= calcT(L2, lam)
	S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3, ro3)
	#--------------------------------------------------------------------------
	S11b, S12b, S21b, S22b 	= MoM_calc_S_matrix(ep2, ro2)
	S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
	S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
	#--------------------------------------------------------------------------
	S11b, S12b, S21b, S22b 	= MoM_calc_S_matrix(ep2+1e-3, ro2)
	S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
	S11abs_p		= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
	#--------------------------------------------------------------------------
	S11_der 		= (S11abs_p - S11abs)/1e-3
	#--------------------------------------------------------------------------
	def func(epvar, **kwd):
		param.ep 		= epvar
		S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
		S11, S12, S21, S22 	= calcS(S11b, S12b, S21b, S22b, T1)
		S11abs			= np.abs(calcS11(S11, S12, S21, S22, S11c, T2))
		return S11abs
	#--------------------------------------------------------------------------
	param = evalparam(ep2, ro2)

	epc = root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=S11abs)
	epu = root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=S11abs, delta= del_S11abs)
	epl = root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=S11abs, delta=-del_S11abs)

	return S11abs, epl, epc, epu

def Confidence_interval_slab_and_rod(a, ep1, ep2, d1, ro2, L, f, del_S11abs, NTOL, MAXIT, NM, N):

	#--------------------------------------------------------------------------
	# ep1 	= 12.2		# auxiliary slab dielectric constant
	# ep2 	= 4.2		# MUT rod dielectric constant
	#
	# d1  	= 3.0		# auxiliary slab thickness in mm
	# ro2  	= 7.0 		# MUT rod radius in mm
	#
	# L   	= 15.0		# slab-to-rod separation in mm
	# f   	= 10.0		# operating frequency in GHz
	#
	# a   	= 22.86		# waveguide width in mm
	#
	# MAXIT = 100		# max number of iterations for Newton's method
	# NTOL 	= 1e-4		# solution tolerance for Newton's method
	#
	# NM	= 3		# number of higher order waveguide modes considered
	# N	= 5		# number of basis functions for each rod surface
	#--------------------------------------------------------------------------
	#--------------------------------------------------------------------------
	MoMinit()		# C++ library initialization

	lam = 300.0/f
	#==========================================================================
	def evalparam(ep, ro):
		param 		= MoMparam()
		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5
		param.zo 	= 0.0

		return param
	#===========================================================================
	def MoM_calc_S_matrix(ep, ro):
		param = evalparam(ep, ro)
		S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
		return S11, S12, S21, S22
	#===========================================================================
	def calcS11(S11b, T):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#===========================================================================
	def calcT(L, lam):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#===========================================================================
	# calculate S11meas - the total reflection coefficient
	S11a, S12a, S21a, S22a 	= single_slab_guide_matrix(a/lam, ep1, d1/a, NM)
	T 			= calcT(L, lam)
	#---------------------------------------------------------------------------
	S11b, S12b, S21b, S22b 	= MoM_calc_S_matrix(ep2, ro2)
	S11abs			= np.abs(calcS11(S11b, T))
	#---------------------------------------------------------------------------
	S11b, S12b, S21b, S22b 	= MoM_calc_S_matrix(ep2+1e-3, ro2)
	S11abs_p		= np.abs(calcS11(S11b, T))
	#---------------------------------------------------------------------------
	S11_der 		= (S11abs_p - S11abs)/1e-3
	#---------------------------------------------------------------------------
	def func(epvar, **kwd):
		param.ep 		= epvar
		S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
		S11abs			= np.abs(calcS11(S11b, T))
		return S11abs
	#---------------------------------------------------------------------------
	param = evalparam(ep2, ro2)

	epc = root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=S11abs)
	epu = root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=S11abs, delta=del_S11abs)
	epl = root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=S11abs, delta=-del_S11abs)

	return S11abs, epl, epc, epu

def Confidence_interval_rod_and_slab(a, ep1, ep2, ro1, d2, L, f, del_S11abs, NTOL, MAXIT, NM, N):

	#---------------------------------------------------------------------------
	# ep1 	= 12.2		# auxiliary rod dielectric constant
	# ep2 	= 4.2		# MUT slab dielectric constant
	#
	# ro1  	= 7.0 		# auxiliary rod radius in mm
	# d2  	= 3.0		# MUT slab thickness in mm
	#
	# L   	= 15.0		# rod-to-slab separarion in mm
	# f   	= 10.0		# operating frequency in GHz
	#
	# a   	= 22.86		# waveguide width in mm
	#
	# MAXIT	= 100		# max number of iterations for Newton's method
	# NTOL 	= 1e-4		# solution tolerance for Newton's method
	#
	# NM	= 3		# number of higher order waveguide modes considered
	# N	= 5		# number of basis functions for each rod surface
	#---------------------------------------------------------------------------
	#---------------------------------------------------------------------------
	MoMinit()		# C++ library initialization
	#
	lam = 300.0/f
	#===========================================================================
	def evalparam(ep, ro):
		param 		= MoMparam()
		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5
		param.zo 	= 0.0

		return param
	#===========================================================================
	def MoM_calc_S_matrix(ep, ro):
		param = evalparam(ep, ro)
		S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
		return S11, S12, S21, S22
	#===========================================================================
	def calcS11(S11b):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#===========================================================================
	def calcT(L, lam):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#===========================================================================
	# calculate S11meas - the total reflection coefficient
	#===========================================================================
	S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1)
	T 			= calcT(L, lam)
	#---------------------------------------------------------------------------
	S11b, S12b, S21b, S22b 	= single_slab_guide_matrix(a/lam, ep2, d2/a, NM)
	S11abs			= np.abs(calcS11(S11b))
	#---------------------------------------------------------------------------
	S11b, S12b, S21b, S22b 	= single_slab_guide_matrix(a/lam, ep2+1e-3, d2/a, NM)
	S11abs_p		= np.abs(calcS11(S11b))
	#---------------------------------------------------------------------------
	S11_der 		= (S11abs_p - S11abs)/1e-3
	#---------------------------------------------------------------------------
	def func(epvar, **kwd):
		S11b, S12b, S21b, S22b 	= single_slab_guide_matrix(a/lam, epvar, d2/a, NM)
		S11abs			= np.abs(calcS11(S11b))
		return S11abs
	#---------------------------------------------------------------------------
	epc = root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=S11abs)
	epu = root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=S11abs, delta=del_S11abs)
	epl = root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=S11abs, delta=-del_S11abs)

	return S11abs, epl, epc, epu

def Confidence_interval_holed_and_solid_rods(a, ep1, ep2, ro1, ro1in, d1, ph1, ro2, L, f, del_S11abs, NTOL, MAXIT, NM, N):

	#===========================================================================
	# ep1	= 7.2			# auxiliary holed rod dielectric constant
	# ep2	= 12.0			# MUT solid rod dielectric constant

	# ro1	= 5.0           	# auxiliary rod radius in mm
	# ro2	= 5.0			# MUT rod radius in mm

	# ro1in	= 0.5           	# auxiliary rod hole radius in mm
	# d1	= 2.0           	# auxiliary rod hole-to-hole separation in mm
	# ph1  	= 0.0           	# auxiliary rod hole line angle in mm

	# L	= 30.0			# interrod separation distance in mm
	# f	= 10.0			# operating frequency in GHz

	# a	= 22.86			# waveguide width in mm
	#===========================================================================
	# NM	= 3			# number of higher order waveguide modes considered
	# N	= 5			# number of basis functions for each rod surface
	#===========================================================================
	MoMinit()			# perform C++ library initialization

	lam = 300.0/f
	#===========================================================================
	def evalparam_holed(roddata, lam):

		param 		= MoMparam()

		param.ep1 	= roddata['ep'] + 0.0j
		param.ep2 	= 1.0 + 0.0j
		param.ep3 	= 1.0 + 0.0j

		param.la 	= a/lam

		param.ro1 	= roddata['ro']/a
		param.ro2 	= roddata['roin']/a
		param.ro3 	= roddata['roin']/a

		param.xo1 	= 0.5
		param.xo2 	= 0.5 + 0.5*roddata['d']/a*np.cos(roddata['ph'])
		param.xo3 	= 0.5 - 0.5*roddata['d']/a*np.cos(roddata['ph'])

		param.zo1 	= 0.0
		param.zo2 	= 0.5 - 0.5*roddata['d']/a*np.sin(roddata['ph'])
		param.zo3 	= 0.5 + 0.5*roddata['d']/a*np.sin(roddata['ph'])

		return param
	#===========================================================================
	def evalparam_solid(roddata, lam):

		param 		= MoMparam()

		param.ep 	= roddata['ep'] + 0.0j

		param.la 	= a/lam

		param.ro 	= roddata['ro']/a
		param.xo 	= 0.5
		param.zo 	= 0.0

		return param
	#===========================================================================
	def calcT(L, lam):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)

		return T
	#===========================================================================
	def MoM_holed_rod_S_matrix(roddata, lam):
		param = evalparam_holed(roddata, lam)
		S11, S12, S21, S22 = MoM_two_in_one_(param, NM, N)
		return S11, S12, S21, S22
	#===========================================================================
	def MoM_solid_rod_S_matrix(roddata, lam):
		param = evalparam_solid(roddata, lam)
		S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
		return S11, S12, S21, S22
	#===========================================================================
	def calcS11(S11b):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#===========================================================================
	roddata1 		= {'ep': ep1, 'ro': ro1, 'roin': ro1in, 'd': d1, 'ph': ph1}
	S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
	T 			= calcT(L, lam)
	#===========================================================================
	roddata2 		= {'ep': ep2, 'ro': ro2 }
	S11b, S12b, S21b, S22b 	= MoM_solid_rod_S_matrix(roddata2, lam)
	S11abs 			= np.abs(calcS11(S11b))

	roddata2 		= {'ep': ep2+1e-3, 'ro': ro2}
	S11b, S12b, S21b, S22b 	= MoM_solid_rod_S_matrix(roddata2, lam)
	S11abs_p		= np.abs(calcS11(S11b))

	S11_der			= (S11abs_p - S11abs)/1e-3
	#---------------------------------------------------------------------------
	def func(epvar, **kwd):
		param.ep 		= epvar
		S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)
		S11abs			= np.abs(calcS11(S11b))
		return S11abs
	#---------------------------------------------------------------------------
	roddata2	= {'ep': ep2, 'ro': ro2}
	param 		= evalparam_solid(roddata2, lam)

	epc = root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=S11abs)
	epu = root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=S11abs, delta=del_S11abs)
	epl = root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11_der, val=S11abs, delta=-del_S11abs)

	return S11abs, epl, epc, epu

