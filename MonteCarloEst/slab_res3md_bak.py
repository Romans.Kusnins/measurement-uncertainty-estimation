import numpy as np
from MoM import *

#====================================================================================
def POW2(x):
	return x*x

def three_slab(l, ep1, ep2, ep3, d1, d2, L):

	PI = np.pi

	kzo  = np.sqrt(l*l     - 0.25);
	kzp1 = np.sqrt(l*l*ep1 - 0.25);
	kzp2 = np.sqrt(l*l*ep2 - 0.25);
	kzp3 = np.sqrt(l*l*ep3 - 0.25);

	R1_l = (kzo  - kzp1)/(kzo  + kzp1);
	R1_r = (kzp2 - kzp1)/(kzp2 + kzp1);

	R3_l = (kzp2 - kzp3)/(kzp2 + kzp3);
	R3_r = (kzo  - kzp3)/(kzo  + kzp3);

	R1 = (kzo - kzp1)/(kzo + kzp1);
	R2 = (kzo - kzp2)/(kzo + kzp2);
	R3 = (kzo - kzp3)/(kzo + kzp3);

	T1 = np.exp(2.0j*PI*kzp1*d1);
	T2 = np.exp(2.0j*PI*kzp2*L);
	T3 = np.exp(2.0j*PI*kzp3*d2);

	Sr11_1 = (R1_l - R1_r*POW2(T1))/(1.0 - R1_l*R1_r*POW2(T1));
	Sr12_1 = T1*(1.0 + R1_l)*(1.0 - R1_r)/(1.0 - R1_l*R1_r*POW2(T1));
	Sr21_1 = T1*(1.0 - R1_l)*(1.0 + R1_r)/(1.0 - R1_l*R1_r*POW2(T1));
	Sr22_1 = (R1_r - R1_l*POW2(T1))/(1.0 - R1_l*R1_r*POW2(T1));

	Sr11_3 = (R3_l - R3_r*POW2(T3))/(1.0 - R3_l*R3_r*POW2(T3));
	Sr12_3 = T3*(1.0 + R3_l)*(1.0 - R3_r)/(1.0 - R3_l*R3_r*POW2(T3));
	Sr21_3 = T3*(1.0 - R3_l)*(1.0 + R3_r)/(1.0 - R3_l*R3_r*POW2(T3));
	Sr22_3 = (R3_r - R3_l*POW2(T3))/(1.0 - R3_l*R3_r*POW2(T3));

	S11_1 = R1*(1.0 - POW2(T1))/(1.0 - POW2(R1*T1));
	S12_1 = T1*(1.0 - POW2(R1))/(1.0 - POW2(R1*T1));

	S11_2 = R2*(1.0 - POW2(T2))/(1.0 - POW2(R2*T2));
	S12_2 = T2*(1.0 - POW2(R2))/(1.0 - POW2(R2*T2));

	S11_3 = R3*(1.0 - POW2(T3))/(1.0 - POW2(R3*T3));
	S12_3 = T3*(1.0 - POW2(R3))/(1.0 - POW2(R3*T3));

	A = np.exp(2.0j*PI*kzp2*L);

	Ac = 1.0 - POW2(np.abs(S11_1)) - POW2(np.abs(S12_1));

	print(Ac)

	#-----------------------------------------------------------------

	Sr11 = Sr11_1 + Sr12_1*Sr11_3*POW2(A)*Sr21_1/(1.0 - Sr22_1*Sr11_3*POW2(A));

	P = np.exp(1j*(np.angle(Sr22_1) + np.angle(Sr11_3)))*POW2(A);

	Sr11a = np.abs((np.abs(Sr22_1) - np.abs(Sr11_3)*P)/(1.0 - np.abs(Sr22_1)*np.abs(Sr11_3)*P));

	# std::max(abs((Sr11_1*Sr22_1 - Sr12_1*Sr21_1)*A*A));

	S11e = S11_1 + POW2(S12_1)*S11_2/(1.0 - S11_1*S11_2);
	S22e = S11_2 + POW2(S12_2)*S11_1/(1.0 - S11_1*S11_2);
	S12e = S12_1*S12_2/(1.0 - S11_1*S11_2);

	S11 = S11e + S12e*S12e*S11_3/(1.0 - S22e*S11_3);

	# P = std::exp(1j*(angle(S22e) + angle(S11_3)));
	# S11a = (abs(S22e) - abs(S11_3)*P)/(1.0 - abs(S22e)*abs(S11_3)*P);

	return S11

def single_slab_guide_matrix(l, ep, d, NM):

	PI = np.pi

	epo = 1.0;

	epo = np.cdouble(epo)
	ep  = np.cdouble(ep)

	if NM == 1:
		kzo = np.sqrt(POW2(l)*epo - 0.25)
		kzp = np.sqrt(POW2(l)*ep  - 0.25)

		R = (kzo - kzp)/(kzo + kzp)
		T = np.exp(2.0j*PI*kzp*d)

		S11  = R*(1.0 - POW2(T))/(1.0 - POW2(R)*POW2(T))
		S21  = T*(1.0 - POW2(R))/(1.0 - POW2(R)*POW2(T))
		S12  = T*(1.0 - POW2(R))/(1.0 - POW2(R)*POW2(T))
		S22  = R*(1.0 - POW2(T))/(1.0 - POW2(R)*POW2(T))

		return S11, S12, S21, S22
	else:
		S11 = np.eye(NM, dtype = 'complex')
		S21 = np.eye(NM, dtype = 'complex')
		S12 = np.eye(NM, dtype = 'complex')
		S22 = np.eye(NM, dtype = 'complex')

		for n in range(NM):
			kzo = np.sqrt(POW2(l)*epo - POW2(n+1)*0.25)
			kzp = np.sqrt(POW2(l)*ep  - POW2(n+1)*0.25)

			R = (kzo  - kzp)/(kzo  + kzp)
			T = np.exp(2.0j*PI*kzp*d)

			S11[n,n] = R*(1.0 - POW2(T))/(1.0 - POW2(R)*POW2(T))
			S21[n,n] = T*(1.0 - POW2(R))/(1.0 - POW2(R)*POW2(T))
			S12[n,n] = T*(1.0 - POW2(R))/(1.0 - POW2(R)*POW2(T))
			S22[n,n] = R*(1.0 - POW2(T))/(1.0 - POW2(R)*POW2(T))

		return S11, S12, S21, S22
#============================================================
def three_slab_guide(l, ep1, ep2, ep3, d1, d2, L):

	PI = np.pi

	epo = 1.0;

	epo = np.cdouble(epo)
	ep1 = np.cdouble(ep1)
	ep2 = np.cdouble(ep2)
	ep3 = np.cdouble(ep3)

	kzo  = np.sqrt(POW2(l)*epo - 0.25)
	kzp1 = np.sqrt(POW2(l)*ep1 - 0.25)
	kzp2 = np.sqrt(POW2(l)*ep2 - 0.25)
	kzp3 = np.sqrt(POW2(l)*ep3 - 0.25)

	R1_l = (kzo  - kzp1)/(kzo  + kzp1)
	R1_r = (kzp2 - kzp1)/(kzp2 + kzp1)

	R3_l = (kzp2 - kzp3)/(kzp2 + kzp3)
	R3_r = (kzo  - kzp3)/(kzo  + kzp3)

	T1 = np.exp(2.0j*PI*kzp1*d1)
	T2 = np.exp(2.0j*PI*kzp2*L)
	T3 = np.exp(2.0j*PI*kzp3*d2)

	Sr11_1 = (R1_l - R1_r*POW2(T1))/(1.0 - R1_l*R1_r*POW2(T1))
	Sr12_1 = T1*(1.0 + R1_l)*(1.0 - R1_r)/(1.0- R1_l*R1_r*POW2(T1))
	Sr21_1 = T1*(1.0 - R1_l)*(1.0 + R1_r)/(1.0 - R1_l*R1_r*POW2(T1))
	Sr22_1 = (R1_r - R1_l*POW2(T1))/(1.0 - R1_l*R1_r*POW2(T1))

	Sr11_3 = (R3_l - R3_r*POW2(T3))/(1.0 - R3_l*R3_r*POW2(T3))
	Sr12_3 = T3*(1.0 + R3_l)*(1.0 - R3_r)/(1.0 - R3_l*R3_r*POW2(T3))
	Sr21_3 = T3*(1.0 - R3_l)*(1.0 + R3_r)/(1.0 - R3_l*R3_r*POW2(T3))
	Sr22_3 = (R3_r - R3_l*POW2(T3))/(1.0 - R3_l*R3_r*POW2(T3))
	#---------------------------------------------------------------------
	A = np.exp(2.0j*PI*kzp2*L)

	Sr11 = Sr11_1 + Sr12_1*Sr11_3*POW2(A)*Sr21_1/(1.0 - Sr22_1*Sr11_3*POW2(A))

	# P = np.exp(1j*(np.angle(Sr22_1) + np.angle(Sr11_3)))*POW2(A);
	# Sr11a = np.abs((np.abs(Sr22_1) - np.abs(Sr11_3)*P)/(1.0 - np.abs(Sr22_1)*np.abs(Sr11_3)*P));

	return Sr11

def three_slab_free_space(ep1, ep2, ep3, d1, d2, L):

	PI = np.pi

	epo = 1.0;

	epo = np.cdouble(epo)
	ep1 = np.cdouble(ep1)
	ep2 = np.cdouble(ep2)
	ep3 = np.cdouble(ep3)

	kzo  = np.sqrt(epo)
	kzp1 = np.sqrt(ep1)
	kzp2 = np.sqrt(ep2)
	kzp3 = np.sqrt(ep3)

	R1_l = (kzo  - kzp1)/(kzo  + kzp1)
	R1_r = (kzp2 - kzp1)/(kzp2 + kzp1)

	R3_l = (kzp2 - kzp3)/(kzp2 + kzp3)
	R3_r = (kzo  - kzp3)/(kzo  + kzp3)

	T1 = np.exp(2.0j*PI*kzp1*d1)
	T2 = np.exp(2.0j*PI*kzp2*L)
	T3 = np.exp(2.0j*PI*kzp3*d2)

	Sr11_1 = (R1_l - R1_r*POW2(T1))/(1.0 - R1_l*R1_r*POW2(T1))
	Sr12_1 = T1*(1.0 + R1_l)*(1.0 - R1_r)/(1.0- R1_l*R1_r*POW2(T1))
	Sr21_1 = T1*(1.0 - R1_l)*(1.0 + R1_r)/(1.0 - R1_l*R1_r*POW2(T1))
	Sr22_1 = (R1_r - R1_l*POW2(T1))/(1.0 - R1_l*R1_r*POW2(T1))

	Sr11_3 = (R3_l - R3_r*POW2(T3))/(1.0 - R3_l*R3_r*POW2(T3))
	Sr12_3 = T3*(1.0 + R3_l)*(1.0 - R3_r)/(1.0 - R3_l*R3_r*POW2(T3))
	Sr21_3 = T3*(1.0 - R3_l)*(1.0 + R3_r)/(1.0 - R3_l*R3_r*POW2(T3))
	Sr22_3 = (R3_r - R3_l*POW2(T3))/(1.0 - R3_l*R3_r*POW2(T3))
	#---------------------------------------------------------------------
	A = np.exp(2.0j*PI*kzp2*L)

	Sr11 = Sr11_1 + Sr12_1*Sr11_3*POW2(A)*Sr21_1/(1.0 - Sr22_1*Sr11_3*POW2(A))

	# P = np.exp(1j*(np.angle(Sr22_1) + np.angle(Sr11_3)))*POW2(A);
	# Sr11a = np.abs((np.abs(Sr22_1) - np.abs(Sr11_3)*P)/(1.0 - np.abs(Sr22_1)*np.abs(Sr11_3)*P));

	return Sr11

def root_finding(fun, xo, dx, TOL, MAXIT, **kwd):

	it 	= 0
	xi 	= xo

	deriv  	= kwd.get('deriv', 0.0)
	val   	= kwd.get('val', 0.0) + kwd.get('delta', 0.0)

	x_sol 	= np.nan

	if deriv != None:
		if deriv > 0.0:
			Dsign =  1.0
		else:
			Dsign = -1.0

	while(it < MAXIT):
		fun_val = fun(xo,**kwd)
		fun_der = (fun(xo+dx,**kwd) - fun_val)/dx
		xn = xo - (fun_val - val)/fun_der
		if(abs(xn/xi-1.0) > 0.3):
#			print('Incorrect value of |S11|.')
			break
		if(abs(xn-xo) < TOL):
			x_sol = xn
			break
		if(it == MAXIT-1):
#			print('Max it. num. reached.')
			pass

		xo = xn
		print(xo)
		it = it + 1

	fun_val = fun(xo,**kwd)
	fun_der = (fun(xo+dx,**kwd) - fun_val)/dx

	if deriv != None:
		if (-Dsign)*fun_der > -0.00001*(-Dsign):
			x_sol = np.nan
	#		print('Incorrect value of |S11|.')

#	print('Converged at the %d-th iteration' % (it))
	return x_sol
#===================================================================================
def two_solid_rods(ep1, ep2, ro1, ro2, L, f, a, NM, N):

	MoMinit()
	#===========================================================================
	lam = 300.0/f
	#===========================================================================
	def evalparam(ep, ro):
		param 		= MoMparam()
		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5
		param.zo 	= 0.0

		return param
	#===========================================================================
	param = evalparam(ep1, ro1)
	S11a, S12a, S21a, S22a = MoM_single_solid_rod_(param, NM, N)

	param = evalparam(ep2, ro2)
	S11b, S12b, S21b, S22b = MoM_single_solid_rod_(param, NM, N)

	if NM == 1:
		T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
	else:
		T = np.eye(NM, dtype='complex')
		for n in range(NM):
			T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)

		S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

	return S11[0,0]
#================================================================================
def three_solid_rods(ep1, ep2, ep3, ro1, ro2, ro3, L1, L2, f, a, NM, N):

	#------------------------------------------------------------------------
	# ep1	- 1st auxiliary rod dielectric constant
	# ep2	- MUT rod dielectric constant
	# ep3	- 2nd auxiliary rod dielectric constant
	#
	# ro1	- 1st auxiliary rod radius in mm
	# ro2	- MUT rod radius in mm
	# ro3	- 2nd auxiliary rod radius in mm
	#
	# L1 	- distance between the 1st and the 2nd rods in mm
	# L2 	- distance between the 2nd and the 3rd rods in mm
	#
	# f  	- operating frequency in GHz
	#
	# a 	- waveguide width in mm
	#
	# MAXIT	- max number of iterations for Newton's method
	# NTOL 	- solution tolerance for Newton's method
	#
	# NM	- number of higher order waveguide modes considered
	# N	- number of basis functions for each rod surface
	#---------------------------------------------------------------------------
	#---------------------------------------------------------------------------
	MoMinit()		# C++ library initialization

	lam = 300.0/f
	#===========================================================================
	def evalparam(ep, ro):
		param 		= MoMparam()
		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5
		param.zo 	= 0.0

		return param
	#===========================================================================
	def MoM_calc_S_matrix(ep, ro):
		param = evalparam(ep, ro)
		S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
		return S11, S12, S21, S22
	#===========================================================================
	def calcS(S11a, S12a, S21a, S22a, S11b, S12b, S21b, S22b, T):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
			S21 = S21a*T/(1.0 - S11b*T*S22a*T)*S21b
			S12 = S12b*T/(1.0 - S22a*T*S11b*T)*S12a
			S22 = S22b + S12b*T*S22a*T/(1.0 - S11b*T*S22a*T)*S21b
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a
			S21 = S21a*T*np.linalg.inv(np.eye(NM) - S11b*T*S22a*T)*S21b
			S12 = S12b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a
			S22 = S22b + S12b*T*S22a*T*np.linalg.inv(np.eye(NM) - S11b*T*S22a*T)*S21b

		return S11, S12, S21, S22
	#===========================================================================
	def calcS11(S11a, S12a, S21a, S22a, S11b, T):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#===========================================================================
	def calcT(L, lam):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#===========================================================================
	# calculate S11meas - the total reflection coefficient
	S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1)
	T1 			= calcT(L1, lam)
	T2 			= calcT(L2, lam)
	S11c, S12c, S21c, S22c 	= MoM_calc_S_matrix(ep3, ro3)
	#----------------------------------------------------------------
	S11b, S12b, S21b, S22b 	= MoM_calc_S_matrix(ep2, ro2)
	S11, S12, S21, S22 	= calcS(S11a, S12a, S21a, S22a, S11b, S12b, S21b, S22b, T1)
	S11			= calcS11(S11, S12, S21, S22, S11c, T2)

	return S11

def slab_and_rod(ep1, ep2, d1, ro2, L, f, a, NM, N):

	#------------------------------------------------------------------------
	# ep1	- auxiliary slab dielectric constant
	# ep2	- MUT rod dielectric constant
	#
	# d1	- auxiliary slab radius in mm
	# ro2	- MUT rod radius in mm
	#
	# L 	- distance between slab and the rod in mm
	#
	# f  	- operating frequency in GHz
	#
	# a  	- waveguide width in mm
	#
	# MAXIT	- max number of iterations for Newton's method
	# NTOL 	- solution tolerance for Newton's method
	#
	# NM	- number of higher order waveguide modes considered
	# N	- number of basis functions for each rod surface
	#---------------------------------------------------------------------------
	#---------------------------------------------------------------------------
	MoMinit()		# C++ library initialization

	lam = 300.0/f
	#===========================================================================
	def evalparam(ep, ro):
		param 		= MoMparam()
		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5
		param.zo 	= 0.0

		return param
	#===========================================================================
	def MoM_calc_S_matrix(ep, ro):
		param = evalparam(ep, ro)
		S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
		return S11, S12, S21, S22
	#===========================================================================
	def calcS11(S11a, S12a, S21a, S22a, S11b, T):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#===========================================================================
	def calcT(L, lam):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#===========================================================================
	# calculate S11abs - the total reflection coefficient
	S11a, S12a, S21a, S22a 	= single_slab_guide_matrix(a/lam, ep1, d1/a, NM)
	T 			= calcT(L, lam)
	#----------------------------------------------------------------
	S11b, S12b, S21b, S22b 	= MoM_calc_S_matrix(ep2, ro2)
	S11			= calcS11(S11a, S12a, S21a, S22a, S11b, T)

	return S11

def rod_and_slab(ep1, ep2, ro1, d2, L, f, a, NM, N):

	#------------------------------------------------------------------------
	# ep1 	- auxiliary rod dielectric constant
	# ep2 	- SUT dielectric constant
	#
	# ro1  	- auxiliary rod radius in mm
	# d2  	- SUT (slab under test) radius in mm
	#
	# L   	- distance between the 1st and the 2nd rods in mm
	#
	# f   	- operating frequency in GHz
	#
	# a   	- waveguide width in mm
	#
	# NM	- number of higher order waveguide modes considered
	# N	- number of basis functions for each rod surface
	#---------------------------------------------------------------------------
	#---------------------------------------------------------------------------
	MoMinit()		# C++ library initialization

	lam = 300.0/f
	#===========================================================================
	def evalparam(ep, ro):
		param 		= MoMparam()
		param.ep 	= ep
		param.la 	= a/lam
		param.ro 	= ro/a
		param.xo 	= 0.5
		param.zo 	= 0.0

		return param
	#===========================================================================
	def MoM_calc_S_matrix(ep, ro):
		param = evalparam(ep, ro)
		S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
		return S11, S12, S21, S22
	#===========================================================================
	def calcS11(S11a, S12a, S21a, S22a, S11b, T):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#===========================================================================
	def calcT(L, lam):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)
		return T
	#===========================================================================
	# calculate S11abs - the total reflection coefficient
	S11a, S12a, S21a, S22a 	= MoM_calc_S_matrix(ep1, ro1)
	T 			= calcT(L, lam)
	#----------------------------------------------------------------
	S11b, S12b, S21b, S22b 	= single_slab_guide_matrix(a/lam, ep2, d2/a, NM)
	S11			= calcS11(S11a, S12a, S21a, S22a, S11b, T)

	return S11

def holed_and_solid_rods(a, ep1, ep2, ro1, ro1in, d1, ph1, ro2, L, f, NM, N):

	#===========================================================================
	# ep1	- auxiliary dielectric constant
	# ep2	- MUT rod dielectric constant
	#
	# ro1	- auxiliary rod radius in mm
	# ro2	- MUT rod radius in mm
	#
	# ro1in	- auxiliary rod hole radius in mm
	# d1	- axuiliary rod hole-to-hole separation in mm
	# ph1  	- auxiliary rod hole line angle in radians
	#
	# L	- interrod separation distance in mm
	# f	- operating frequency in GHz
	#
	# a	- waveguide width in mm
	#===========================================================================
	# NM	# number of higher order waveguide modes considered
	# N	# number of basis functions for each rod surface
	#===========================================================================
	MoMinit()	# perform C+= library initialization

	lam = 300.0/f
	#===========================================================================
	def evalparam_holed(roddata, lam):

		param 		= MoMparam()

		param.ep1 	= roddata['ep'] + 0.0j
		param.ep2 	= 1.0 + 0.0j
		param.ep3 	= 1.0 + 0.0j

		param.la 	= a/lam

		param.ro1 	= roddata['ro']/a
		param.ro2 	= roddata['roin']/a
		param.ro3 	= roddata['roin']/a

		param.xo1 	= 0.5
		param.xo2 	= 0.5 + 0.5*roddata['d']/a*np.cos(roddata['ph'])
		param.xo3 	= 0.5 - 0.5*roddata['d']/a*np.cos(roddata['ph'])

		param.zo1 	= 0.0
		param.zo2 	= 0.5 - 0.5*roddata['d']/a*np.sin(roddata['ph'])
		param.zo3 	= 0.5 + 0.5*roddata['d']/a*np.sin(roddata['ph'])

		return param
	#===========================================================================
	def evalparam_solid(roddata, lam):

		param 		= MoMparam()

		param.ep 	= roddata['ep'] + 0.0j

		param.la 	= a/lam

		param.ro 	= roddata['ro']/a
		param.xo 	= 0.5
		param.zo 	= 0.0

		return param
	#===========================================================================
	def calcT(L, lam):
		if NM == 1:
			T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
		else:
			T = np.eye(NM, dtype='complex')
			for n in range(NM):
				T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)

		return T
	#===========================================================================
	def MoM_holed_rod_S_matrix(roddata, lam):
		param = evalparam_holed(roddata, lam)
		S11, S12, S21, S22 = MoM_two_in_one_(param, NM, N)
		return S11, S12, S21, S22
	#===========================================================================
	def MoM_solid_rod_S_matrix(roddata, lam):
		param = evalparam_solid(roddata, lam)
		S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
		return S11, S12, S21, S22
	#===========================================================================
	def calcS11(S11b):
		if NM == 1:
			S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		else:
			S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

		return S11[0,0]
	#===========================================================================
	roddata1 		= {'ep': ep1, 'ro': ro1, 'roin': ro1in, 'd': d1, 'ph': ph1}
	S11a, S12a, S21a, S22a 	= MoM_holed_rod_S_matrix(roddata1, lam)
	T			= calcT(L, lam)
	roddata2 		= {'ep': ep2, 'ro': ro2 }
	S11b, S12b, S21b, S22b 	= MoM_solid_rod_S_matrix(roddata2, lam)
	S11 			= calcS11(S11b)

	return S11
