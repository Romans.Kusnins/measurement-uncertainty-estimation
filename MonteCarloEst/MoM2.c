#include<stdio.h>
#include<math.h>
#include<complex.h>

typedef _Complex double cdouble;


void MoM_two_solid_rods(cdouble, cdouble, double, double, double, double, double, double, double, cdouble*, cdouble*, cdouble*, cdouble*, const int, const int);

//void func(cdouble * S11, cdouble * S12, cdouble * S21, cdouble * S22, const int N, const int MN){
void func(cdouble * SMN[], const int N, const int MN){

//	const int WN = 2*N+1;

	MoM_two_solid_rods(4.2, 4.2, 0.75, 0.1, 0.1, 0.2, 0.2, 0.0, 0.5, SMN[0], SMN[1], SMN[2], SMN[3], N, MN);
//	MoM_two_solid_rods(4.2, 4.2, 0.75, 0.1, 0.1, 0.2, 0.2, 0.0, 0.5, S11, S12, S21, S22, N, MN);

}

