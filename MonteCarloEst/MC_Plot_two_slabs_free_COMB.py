import os
import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *
from Derivatives import *

try:
        matplot = True
        from matplotlib import pyplot as plt
except ModuleNotFoundError:
        print("The matplotlib module is not installed.")
        matplot = False


from plot2pdf import *

from Uncertainty_vs_S11 import *
#==================================================================================
xMin 		= None
xMax 		= None
MaxVal 		= None

task 		= 'sensitivity'
task 		= 'total'
#---------------------------------------
#sensparam 	= 'S11abs'
sensparam 	= 'S11abs'
#---------------------------------------
var_param 	= 'L'
#---------------------------------------
quantity 	= 'var'


#PREFIX = '_29_'

PREFIX = ['_6_0_', '_6_5_', '', '_7_5_']


if var_param == 'L':
	MCNUM = 100000
else:
	MCNUM = 25000
#==================================================================================
isPDF 		= True
#isPRINT	= False
#==================================================================================
xlabels		= dict()

xlabels['ep1']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,aux}}$'
xlabels['ep2']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,mut}}$'
xlabels['tan1'] = '$\\tan_{\\delta^{\\mathrm{aux}}}$'
xlabels['tan2'] = '$\\tan_{\\delta^{\\mathrm{mut}}}$'
xlabels['d1']   = '$d_{\\mathrm{aux}}$ mm'
xlabels['d2']   = '$d_{\\mathrm{mut}}$ mm'
xlabels['L']    = '$d_{\\mathrm{int}}$ mm'
xlabels['f']    = 'f, GHz'

paramname       = {'S11abs': '$u_{|S_{11}|}$', 'd1': '$u_{d_{\\mathrm{aux}}}$', 'd2': '$u_{d_{\\mathrm{mut}}}$', 'L': '$u_{d_{\\mathrm{in}}}$', 'ep1': '$u_{\\varepsilon^{\\prime}_{\\mathrm{r,aux}}}$', 'tan1': '$ u_{\\tan{\\delta^{\\mathrm{aux}}}}$', 'tan2': 'u_{\\tan{\\delta^{\\mathrm{mut}}}}', 'f': 'u_{f}'}
#==================================================================================
# PLOTTING CALCULATED DATA
#==================================================================================
FOLDER = './RESULTS_2SL_FREE/'

MCSTR = '{:}'.format(MCNUM)


MCval = list()
MCarg = list()

for n in range(len(PREFIX)):

	if task == 'sensitivity':
		MCfilename 	= FOLDER + 'two_slabs_free_var_' + var_param + '_sens_' + sensparam + '_MCnum'+ PREFIX[n] + '_' + MCSTR + '.npy'
		MCfilename_mean	= FOLDER + 'two_slabs_free_var_' + var_param + '_sens_' + sensparam + '_MCnum_mean'+ PREFIX[n] + '_' + MCSTR + '.npy'
		EPMfilename 	= FOLDER + 'two-slabs-free_var_' + var_param + '_standard_' + sensparam + '_EPM' + PREFIX[n] + '.npy'
	else:
		MCfilename 	= FOLDER + 'two_slabs_free_var_' + var_param + '_total_MCnum' + PREFIX[n] + '_' + MCSTR + '.npy'
		MCfilename_mean	= FOLDER + 'two_slabs_free_var_' + var_param + '_total_MCnum_mean' + PREFIX[n] + '_' + MCSTR + '.npy'
		EPMfilename 	= FOLDER + 'two-slabs-free_var_' + var_param + '_total_EPM' + PREFIX[n] + '.npy'

	MCvar 	= np.load(MCfilename)
	MCmean 	= np.load(MCfilename_mean)
#==================================================================================
	if MaxVal != None:
		Mmean[1, MCmean[1,:] > MaxVal]	= np.nan
		MCvar[1, MCvar[1,:]  > MaxVal]	= np.nan

	if xMin != None:
		ID 	= MCvar[0,:] >= xMin
		MCvar 	= MCvar[:,ID]

	if xMax != None:
		ID 	= MCvar[0,:] <= xMax
		MCvar 	= MCvar[:,ID]

	MCarg.append(MCvar[0,:])
	MCval.append(MCvar[1,:])
#==================================================================================
if isPDF:
	legend = ['$d_{\\mathrm{aux}} = 6.0$', '$d_{\\mathrm{aux}} = 6.5$', '$d_{\\mathrm{aux}} = 7.0$', '$d_{\\mathrm{aux}} = 7.5$']


	if task == 'sensitivity':
		filename  = 'TWO_SL_FREE_VAR_' + var_param + '_SENS_' + sensparam + 'COMB'
		labels = {'xlabel': xlabels[var_param], 'ylabel': paramname[sensparam]}
	else:
		filename  = 'TWO_SL_FREE_VAR_' + var_param + '_TOTAL_COMB'
		labels = {'xlabel': xlabels[var_param], 'ylabel': '$u(\\varepsilon_{\\mathrm{r,mut}})$'}

	LEGPOS = [0.3, 0.7]
	if quantity == 'mean':
		filename  = 'TWO_SL_FREE_VAR_' + var_param + 'MEAN_COMB'
		labels = {'xlabel': xlabels[var_param], 'ylabel': '$\\bar{u}(\\varepsilon_{\\mathrm{r,mut}})$'}
	#	labels = {'xlabel': xlabels[var_param], 'ylabel': ylabeldelta + ' ' + paramname[param]}
		plot2latex(MCarg, MCval, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, YZero=False, LegendPosition=LEGPOS, FileName=filename)
	else:
		plot2latex(MCarg, MCval, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, LegendPosition=LEGPOS, FileName=filename)
#	plot2latex([EPMdata[0,:], MCdata[0,:]], [EPMdata[1,:], MCdata[1,:]], ylabel_vert=True, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels)
elif matplot:
#	plt.ylabel(ylabeldelta + ' ' + paramname[param])
	plt.plot(MCdata[0,:], EPMdata[1,:])
	plt.grid()
	plt.show()
else:
	pass
#       print('Calculated data have been saved into a file.')

print('--------------------------------------------------------')
