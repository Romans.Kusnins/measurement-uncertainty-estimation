#include<stdio.h>
#include<complex.h>

#define cdouble _Complex double

#define eps_o(i) eps_o[i-1]


cdouble fapp(cdouble eps, cdouble * eps_o, const int M){

int m;

cdouble res=1.0;


for(m=1;m<=M;++m){

    res *= (eps - conj(eps_o(m)))/(eps - eps_o(m));   

}

return res;

} 
