#include<stdio.h>
#include<math.h>
#include<complex.h>

typedef _Complex double cdouble;


void MoMoff_GENS(cdouble, cdouble, double, double, double, double, double, double, double, cdouble*, const int, const int);


//void func(cdouble * S11, cdouble * S12, cdouble * S21, cdouble * S22, const int N, const int NM){
void func(cdouble * SMN, const int N, const int MN){

//	const int WN = 2*N+1;

	MoMoff_GENS(4.2, 4.2, 0.75, 0.1, 0.1, 0.2, 0.2, 0.0, 0.5, SMN, N, MN);

}

