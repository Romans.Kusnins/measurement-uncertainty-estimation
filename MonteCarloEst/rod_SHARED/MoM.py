import os
from ctypes import *
import numpy as np


class ComplexDouble(Structure):
	_fields_ = [("real", c_double), ("imag", c_double)]

N = 10

MN = 1

#===========================================
WN = 2*N+1

#ComplexDoubleArray = ComplexDouble * 10
DoubleArray = c_double * (4*2*MN)

# S11 = ComplexDoubleArray()
SMN = DoubleArray()

shlib = CDLL("./MoM.so")

shlib.func(SMN, c_int(N), c_int(MN));

SMN = np.asarray(list(SMN))

SMN = SMN[::2] + 1.0j*SMN[1::2]

for z in SMN:
	print(z)
