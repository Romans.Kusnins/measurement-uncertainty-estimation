//#include<complex>
#include<complex.h>
#include<math.h>
#include<stdio.h>
#include<iostream>

#define PI 3.1415926535897931

#define RT(m,n,l,o)   RT[m-1 + (n-1)*NM + (l-1)*NM*NM + (o-1)*NM*NM*2]

#define cdouble _Complex double

//typedef std::complex<double> cdouble;
//cdouble I = std::complex<double>(0.0,1.0);


//void MoMoff_GENS(cdouble epo, double la, double ro, double xo, cdouble * RT, const int N, const int NM);
void MoMoff_GENS(cdouble ep1, cdouble ep2, double la, double ro1, double ro2, double xo1, double xo2, double zo1, double zo2, cdouble * RT, const int N, const int NM);

int main(int argc, char ** argv){

	const int N = 7;
	const int NM = 3;
	cdouble RT[NM*NM*4];

//	MoMoff_GENS(4.2, 0.75, 0.1, 0.2, RT, N, NM);
	MoMoff_GENS(4.2, 4.2, 0.75, 0.1, 0.1, 0.2, 0.2, 0.0, 0.5, RT, N, NM);

	for(int n=1; n<=NM; ++n){
		for(int m=1; m<=NM; ++m){
			printf("S11 = %5.12f, %5.12f\n", creal(RT(m,n,1,1)), cimag(RT(m,n,1,1)));
		}
	}
	printf("\n\n");
	for(int n=1; n<=NM; ++n){
		for(int m=1; m<=NM; ++m){
			printf("S12 = %5.12f, %5.12f\n", creal(RT(m,n,1,2)), cimag(RT(m,n,1,2)));
		}
	}
	printf("\n\n");
	for(int n=1; n<=NM; ++n){
		for(int m=1; m<=NM; ++m){
			printf("S21 = %5.12f, %5.12f\n", creal(RT(m,n,2,1)), cimag(RT(m,n,2,1)));
		}
	}
	printf("\n\n");
	for(int n=1; n<=NM; ++n){
		for(int m=1; m<=NM; ++m){
			printf("S22 = %5.12f, %5.12f\n", creal(RT(m,n,2,2)), cimag(RT(m,n,2,2)));
		}
	}

	return 0;

}

