#include<stdio.h>
#include<malloc.h>
#include<math.h>
#include<complex.h>


#define PI 3.141592653589793238462

#define cdouble  _Complex double


void cosin(double * wph[], double * cosn[], const int M, const int IP);

void besselj(int,cdouble,cdouble*,int);
void bessely(int,cdouble,cdouble*,int);


#define P1(i,j) PQ[0][i-1+(j-1)*M]
#define P2(i,j) PQ[1][i-1+(j-1)*M]
#define Q1(i,j) PQ[2][i-1+(j-1)*M]
#define Q2(i,j) PQ[3][i-1+(j-1)*M]

#define cosn1(i,j) cosn1[i-1+(j-1)*IP]
#define cosn2(i,j) cosn2[i-1+(j-1)*IP]

#define gm(i) gm[i-1]
#define kz(i) kz[i-1]

#define sm(i) sm[i-1]
#define cm(i) cm[i-1]

#define sn(i) sn[i-1]
#define cn(i) cn[i-1]
#define ph(i) ph[i-1]
#define ww(i) ww[i-1]


#define BJa1(i)  BJ[2*i-1]
#define BJDa1(i) 0.5*(BJ[2*i-2]-BJ[2*i])

#define BJa2(i)  BJ[2*i]
#define BJDa2(i) 0.5*(BJ[2*i-1]-BJ[2*i+1])

#define BYa1(i)  BY[2*i-1]
#define BYDa1(i) 0.5*(BY[2*i-2]-BY[2*i])

#define BYa2(i)  BY[2*i]
#define BYDa2(i) 0.5*(BY[2*i-1]-BY[2*i+1])




void besscyl_freq(double al, cdouble * PQ[], const int M,const int IP){


	int m,n,k;

	double  gm[M];
	cdouble kz[M];

	cdouble koa = PI*al;

	cdouble BJ[2*(M+1)],BY[2*(M+1)];

	double ph[IP], ww[IP], sn[IP], cn[IP];
	double cosn1[IP*M], cosn2[IP*M];

	double * wphPt[]  = {ph,ww,sn,cn};
	double * cosnPt[] = {cosn1,cosn2};


	cdouble tmpexp;
	cdouble tmpr;
	double  tmpsn;
	cdouble costm, sintm, costmx;

	double  sm[M], cm[M];

	cosin(wphPt,cosnPt,M,IP);

	gm(1) = 0.5/al;
	kz(1) = sqrt(1.0 - gm(1)*gm(1));

	for(m=2;m<=M;++m){
		gm(m) = m/al - gm(1);
		kz(m) = I*sqrt(gm(m)*gm(m) - 1.0);
	}

	besselj(0,koa,&BJ[1],2*M+1);
	bessely(0,koa,&BY[1],2*M+1);

	BJ[0] = -1.0*BJ[2];
	BY[0] = -1.0*BY[2];

	for(m=1;m<=M;++m){
        	for(k=1;k<=IP;++k){
                	tmpexp   =   ww(k)*cexp(I*kz(m)*PI*al*cn(k));
	                tmpsn    =   PI*(m-0.5)*sn(k);
        	        sintm    =  -gm(m)*sn(k)*sin(tmpsn)*tmpexp;
                	costmx   =   cos(tmpsn)*tmpexp;
			sintm   +=   I*kz(m)*cn(k)*costmx;
			for(n=1; n<=M; ++n){
				if(k==1){
					P1(n,m) = 0.0;
					P2(n,m) = 0.0;
					Q1(n,m) = 0.0;
					Q2(n,m) = 0.0;
				}

                		P1(n,m) += costmx*cosn1(k,n);
		                Q1(n,m) += sintm*cosn1(k,n);

        		        P2(n,m) += costmx*cosn2(k,n);
                		Q2(n,m) += sintm*cosn2(k,n);

			}
		}
	}

	for(n=1;n<=M;++n){
		for(m=1;m<=M;++m){

			tmpr    = BJDa1(m)*P1(m,n) - BJa1(m)*Q1(m,n);
			Q1(m,n) = BYDa1(m)*P1(m,n) - BYa1(m)*Q1(m,n);
			P1(m,n) = tmpr;

			tmpr    = BJDa2(m)*P2(m,n) - BJa2(m)*Q2(m,n);
			Q2(m,n) = BYDa2(m)*P2(m,n) - BYa2(m)*Q2(m,n);
			P2(m,n) = tmpr;

		}
	}

}

