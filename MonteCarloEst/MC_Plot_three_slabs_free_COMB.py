import os
import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *
from Derivatives import *

try:
        matplot = True
        from matplotlib import pyplot as plt
except ModuleNotFoundError:
        print("The matplotlib module is not installed.")
        matplot = False


from plot2pdf import *

from Uncertainty_vs_S11 import *
#==================================================================================
xMin 		= None
xMax 		= None
MaxVal 		= 100.0

#task 		= 'sensitivity'
task 		= 'total'
#---------------------------------------
sensparam 	= 'S11abs'
#---------------------------------------
var_param 	= 'L'
#---------------------------------------
quantity 	= 'var'


#PREFIX = '_29_'
PREFIX = ['_5_75_', '_6_0_', '_6_25_', '_6_5_']

MCNUM = 20000
#==================================================================================
isPDF 		= True
#isPRINT	= False
#==================================================================================
xlabels		= dict()

#xlabels['ep1']  = 'Dielectric constant of the auxiliary slab'
#xlabels['epm']  = 'Dielectric constant of the middle slab'
#xlabels['ep2']  = 'Dielectric constant of MUT'

#xlabels['tan1'] = 'Loss tangent of the auxiliary slab'
#xlabels['tanm'] = 'Loss tangent of the middle slab'
#xlabels['tan2'] = 'Loss tangent of MUT'

#xlabels['d1']   = 'Thickness of the auxiliary slab, mm'
#xlabels['d2']   = 'Thickness of the MUT slab, mm'
#xlabels['L']    = 'Thickness of the middle slab, mm'
#xlabels['f']    = 'Frequency, GHz'

xlabels['ep1']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,aux}}$'
xlabels['ep2']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,mut}}$'
xlabels['epm']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,mut}}$'
xlabels['tan1'] = '$\\tan_{\\delta^{\\mathrm{aux}}}$'
xlabels['tan2'] = '$\\tan_{\\delta^{\\mathrm{mut}}}$'
xlabels['tanm'] = '$\\tan_{\\delta^{\\mathrm{mut}}}$'
xlabels['d1']   = '$d_{\\mathrm{aux}}$ mm'
xlabels['d2']   = '$d_{\\mathrm{mut}}$ mm'
xlabels['L']    = '$d_{\\mathrm{int}}$ mm'
xlabels['f']    = 'f, GHz'

paramname       = {'S11abs': '$u_{|S_{11}|}$', 'd1': '$u_{d_{\\mathrm{aux}}}$', 'd2': '$u_{d_{\\mathrm{mut}}}$','L': '$u_{d_{\\mathrm{in}}}$', 'ep1': '$u_{\\varepsilon^{\\prime}_{\\mathrm{r,aux}}}$',  'tan1': '$ u_{\\tan{\\delta^{\\mathrm{aux}}}}$', 'tan2': 'u_{\\tan{\\delta^{\\mathrm{mut}}}}', 'a': 'u_{a}', 'f': 'u_{f}'}
#==================================================================================
# PLOTTING CALCULATED DATA
#==================================================================================
FOLDER = './RESULTS_3SL_FREE/'

MCSTR = '{:}'.format(MCNUM)

MCarg = list()
MCval = list()

for n in range(len(PREFIX)):

	if task == 'sensitivity':
		MCfilename 	= FOLDER + 'three_slabs_free_var_' + var_param + '_sens_' + sensparam + '_MCnum'+ PREFIX[n] + '_' + MCSTR + '.npy'
		MCfilename_mean	= FOLDER + 'three_slabs_free_var_' + var_param + '_sens_' + sensparam + '_MCnum_mean'+ PREFIX[n] + '_' + MCSTR + '.npy'
		EPMfilename 	= FOLDER + 'three-slabs-free_var_' + var_param + '_standard_' + sensparam + '_EPM' + PREFIX[n] + '.npy'
	else:
		MCfilename 	= FOLDER + 'three_slabs_free_var_' + var_param + '_total_MCnum' + PREFIX[n] + '_' + MCSTR + '.npy'
		MCfilename_mean	= FOLDER + 'three_slabs_free_var_' + var_param + '_total_MCnum_mean' + PREFIX[n] + '_' + MCSTR + '.npy'
		EPMfilename 	= FOLDER + 'three-slabs-free_var_' + var_param + '_total_EPM' + PREFIX[n] + '.npy'

	MCvar 	= np.load(MCfilename)
	MCmean 	= np.load(MCfilename_mean)

	MCarg.append(MCvar[0,:])
	MCval.append(MCvar[1,:])
#==================================================================================
	if MaxVal != None:
		MCmean[1, MCmean[1,:] > MaxVal]	= np.nan
		MCvar[1, MCvar[1,:] > MaxVal]	= np.nan

	if xMin != None:
		ID 	= MCvar[0,:] >= xMin
		MCvar 	= MCvar[:,ID]

	if xMax != None:
		ID 	= MCvar[0,:] <= xMax
		MCvar 	= MCvar[:,ID]
#==================================================================================
if isPDF:

	legend = ['$d_{\\mathrm{aux}}$ = 5.75', '$d_{\\mathrm{aux}} = 6.0$', '$d_{\\mathrm{aux}} = 6.25$', '$d_{\\mathrm{aux}} = 6.5$']

	if task == 'sensitivity':
		filename  = 'THREE_SL_FREE_VAR_' + var_param + '_SENS_' + sensparam + 'COMB'
		labels = {'xlabel': xlabels[var_param], 'ylabel': sensparam}
	else:
		filename  = 'THREE_SL_FREE_VAR_' + var_param + '_TOTAL_COMB'
		labels = {'xlabel': xlabels[var_param], 'ylabel': '$u(\\varepsilon^{\\prime}_{\\mathrm{r,mut}})$'}

	LegendPos = [0.1, 0.8]

	if quantity == 'mean':
	#	labels = {'xlabel': xlabels[var_param], 'ylabel': ylabeldelta + ' ' + paramname[param]}
		filename  = 'THREE_SL_FREE_VAR_' + var_param + '_MEAN_COMB'
		plot2latex(MCarg, MCval, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, YZero=False, LegendPosition=LegendPos, FileName=filename)
	else:
		plot2latex(MCarg, MCval, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, YZero=False, LegendPosition=LegendPos, FileName=filename)
#	plot2latex([EPMdata[0,:], MCdata[0,:]], [EPMdata[1,:], MCdata[1,:]], ylabel_vert=True, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels)
elif matplot:
#	plt.ylabel(ylabeldelta + ' ' + paramname[param])
	plt.plot(MCdata[0,:], EPMdata[1,:])
	plt.grid()
	plt.show()
else:
	pass
#       print('Calculated data have been saved into a file.')

print('--------------------------------------------------------')
