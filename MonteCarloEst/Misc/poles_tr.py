import numpy as np

def poles_tr(r, al, num, mod):

	if num == 1:
	   ri = np.round((r - 0.05)/0.001 + 1)
	elif num == 2:
	   ri = np.round((r - 0.3)/0.001 + 1)
	else:
	   ri = np.round((r - 0.44)/0.001 + 1)

	ri = int(ri)


	str = 'poles_new_tr_%d_%d_%s' % (int(100*al), int(num), mod)

	dat = np.fromfile(str, dtype='double')

	y = dat[::2] + 1j*dat[1::2]

	y = y[25*(ri-1):25*ri]

	return y


dat = poles_tr(0.3, 0.75, 1, 'p')

print(dat)
