#include<complex>
#include<stdio.h>
#include<iostream>

#define POW2(x) ((x)*(x))

#define PI 3.1415926


typedef std::complex<double> cdouble;
cdouble I(0.0, 1.0);

void slab_res3(double l, cdouble ep1, cdouble ep2, cdouble ep3, double d1, double d2, double L);


int main(int argc, char ** argv){

	slab_res3(0.65, 3.0, 2.0, 5.0, 0.1, 0.2, 0.6);

	return 0;
}


void slab_res3(double l, cdouble ep1, cdouble ep2, cdouble ep3, double d1, double d2, double L){

	cdouble kzo, kzp1, kzp2, kzp3;
	cdouble R1_l, R1_r, R3_l, R3_r;

	cdouble R1, R2, R3;
	cdouble T1, T2, T3;

	cdouble Sr11_1, Sr21_1, Sr12_1, Sr22_1;
	cdouble Sr11_3, Sr21_3, Sr12_3, Sr22_3;

	cdouble S11_1, S12_1;
	cdouble S11_2, S12_2;
	cdouble S11_3, S12_3;

	cdouble A, Ac;
	cdouble Sr11, P;
	double  Sr11a;
	cdouble S11e, S12e, S21e, S22e;
	cdouble S11;

	//----------------------------------------------

	kzo  = sqrt(l*l     - 0.25);
	kzp1 = sqrt(l*l*ep1 - 0.25);
	kzp2 = sqrt(l*l*ep2 - 0.25);
	kzp3 = sqrt(l*l*ep3 - 0.25);

	R1_l = (kzo  - kzp1)/(kzo  + kzp1);
	R1_r = (kzp2 - kzp1)/(kzp2 + kzp1);

	R3_l = (kzp2 - kzp3)/(kzp2 + kzp3);
	R3_r = (kzo  - kzp3)/(kzo  + kzp3);

	R1 = (kzo - kzp1)/(kzo + kzp1);
	R2 = (kzo - kzp2)/(kzo + kzp2);
	R3 = (kzo - kzp3)/(kzo + kzp3);

	T1 = std::exp(I*2.0*PI*kzp1*d1);
	T2 = std::exp(I*2.0*PI*kzp2*L);
	T3 = std::exp(I*2.0*PI*kzp3*d2);


	Sr11_1 = (R1_l - R1_r*POW2(T1))/(1.0 - R1_l*R1_r*POW2(T1));
	Sr12_1 = T1*(1.0 + R1_l)*(1.0 - R1_r)/(1.0- R1_l*R1_r*POW2(T1));
	Sr21_1 = T1*(1.0 - R1_l)*(1.0 + R1_r)/(1.0 - R1_l*R1_r*POW2(T1));
	Sr22_1 = (R1_r - R1_l*POW2(T1))/(1.0 - R1_l*R1_r*POW2(T1));

	Sr11_3 = (R3_l - R3_r*POW2(T3))/(1.0 - R3_l*R3_r*POW2(T3));
	Sr12_3 = T3*(1.0 + R3_l)*(1.0 - R3_r)/(1.0 - R3_l*R3_r*POW2(T3));
	Sr21_3 = T3*(1.0 - R3_l)*(1.0 + R3_r)/(1.0 - R3_l*R3_r*POW2(T3));
	Sr22_3 = (R3_r - R3_l*POW2(T3))/(1.0 - R3_l*R3_r*POW2(T3));

	S11_1 = R1*(1.0 - POW2(T1))/(1.0 - POW2(R1*T1));
	S12_1 = T1*(1.0 - POW2(R1))/(1.0 - POW2(R1*T1));

	S11_2 = R2*(1.0 - POW2(T2))/(1.0 - POW2(R2*T2));
	S12_2 = T2*(1.0 - POW2(R2))/(1.0 - POW2(R2*T2));

	S11_3 = R3*(1.0 - POW2(T3))/(1.0 - POW2(R3*T3));
	S12_3 = T3*(1.0 - POW2(R3))/(1.0 - POW2(R3*T3));

	A = exp(I*2.0*PI*kzp2*L);

	Ac = 1.0 - POW2(std::abs(S11_1)) - POW2(std::abs(S12_1));

	std::cout << Ac << std::endl;

	//-----------------------------------------------------------------

	Sr11 = Sr11_1 + Sr12_1*Sr11_3*POW2(A)*Sr21_1/(1.0 - Sr22_1*Sr11_3*POW2(A));

	P = exp(I*(std::arg(Sr22_1) + std::arg(Sr11_3)))*POW2(A);

	Sr11a = std::abs((std::abs(Sr22_1) - std::abs(Sr11_3)*P)/(1.0 - std::abs(Sr22_1)*std::abs(Sr11_3)*P));


	//std::max(abs((Sr11_1*Sr22_1 - Sr12_1*Sr21_1)*A*A));

	S11e = S11_1 + POW2(S12_1)*S11_2/(1.0 - S11_1*S11_2);
	S22e = S11_2 + POW2(S12_2)*S11_1/(1.0 - S11_1*S11_2);
	S12e = S12_1*S12_2/(1.0 - S11_1*S11_2);

	S11 = S11e + S12e*S12e*S11_3/(1.0 - S22e*S11_3);

	std::cout << Sr11 << std::endl;
	std::cout << S11  << std::endl;

	std::cout << std::abs(S11) << std::endl;
	std::cout << Sr11a  << std::endl;

	// P = std::exp(I*(std::angle(S22e) + std::angle(S11_3)));

	// S11a = (std::abs(S22e) - std::abs(S11_3)*P)./(1.0 - std::abs(S22e)*std::abs(S11_3)*P);


}
