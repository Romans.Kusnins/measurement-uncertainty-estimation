
slab_2(cdouble ep1, cdouble ep2, cdouble ep3, double d1, double d2, double d3, double L1, double L2, cdouble epa, cdouble * res)

l = 0.55:0.0001:0.95;

double kzo, kzp1, kzp2, kzp3;
double R1, R2, R3;
double T1, T2, T3;

double S11_1, S12_1;
double S11_2, S12_2;
double S11_3, S12_3;

double A1, S11, A2, S11E;

//===========================================================

kzo  = std::sqrt(1.0 - 0.25);
kzp1 = std::sqrt(ep1 - 0.25);
kzp2 = std::sqrt(ep2 - 0.25);
kzp3 = std::sqrt(ep3 - 0.25);

R1 = (kzo - kzp1)/(kzo + kzp1);
R2 = (kzo - kzp2)/(kzo + kzp2);
R3 = (kzo - kzp3)/(kzo + kzp3);

T1 = std::exp(I*2.0*PI*kzp1*d1);
T2 = std::exp(I*2.0*PI*kzp2*d2);
T3 = std::exp(I*2.0*PI*kzp3*d3);

S11_1 = R1*(1.0 - T1*T1)/(1.0 - R1*R1*T1*T1);
S12_1 = T1*(1.0 - R1*R1)/(1.0 - R1*R1*T1*T1);

S11_2 = R2*(1.0 - T2*T2)/(1.0 - R2*R2*T2*T2);
S12_2 = T2*(1.0 - R2*R2)/(1.0 - R2*R2*T2*T2);

S11_3 = R3*(1.0 - T3*T3)/(1.0 - R3*R3*T3*T3);
S12_3 = T3*(1.0 - R3*R3)/(1.0 - R3*R3*T3*T3);

A1 = exp(I*2.0*PI*kzo*L1);

S11 = S11_2 + S12_2*S12_2*A1*A1*S11_3/(1.0-S11_2*S11_3*A1*A1);

A2 = exp(I*2.0*PI*kzo*L2);

S11E = S11_1 + S12_1*S12_1*A2*S11/(1.0-S11_1*S11*A2*A2);

//plot(l, abs(S11E),'r')

res = abs(S11E);
  

