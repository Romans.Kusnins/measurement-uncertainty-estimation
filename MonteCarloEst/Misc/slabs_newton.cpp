
slabs_newton_value(cdouble ep1, cdouble ep2, cdouble ep3, double d1, double d2, double d3, cdouble * S11, const N){

	cdouble R1_l, R1_r, R3_l, R3_r;
	cdouble T1, T2, T3;
	cdouble Sr11_1, Sr12_1, Sr21_1, Sr22_1;
	cdouble Sr11_3;
	//---------------------------------------------------------------------

	for(int id = 0, id < N; ++id){
		R1_l = (1.0 - std::sqrt(ep1))/(1.0  + std::sqrt(ep1));
		R1_r = (std::sqrt(ep2) - std::sqrt(ep1))/(std::sqrt(ep2) + std::sqrt(ep1));

		R3_l = (std::sqrt(ep2) - std::sqrt(ep3))/(std::sqrt(ep2) + std::sqrt(ep3));
		R3_r = (1.0 - std::sqrt(ep3))/(1.0 + std::sqrt(ep3));

		T1 = exp(I*2.0*PI*std::sqrt(ep1)*d1);
		T2 = exp(I*2.0*PI*std::sqrt(ep2)*d2);
		T3 = exp(I*2.0*PI*std::sqrt(ep3)*d3);

		Sr11_1 = (R1_l - R1_r*T1*T1)/(1.0 - R1_l*R1_r*T1*T1);
		Sr12_1 = T1*(1.0 + R1_l)*(1.0 - R1_r)/(1.0 - R1_l*R1_r*T1*T1);
		Sr21_1 = T1*(1.0 - R1_l)*(1.0 + R1_r)/(1.0 - R1_l*R1_r*T1*T1);
		Sr22_1 = (R1_r - R1_l*T1*T1)/(1.0 - R1_l*R1_r*T1*T1);

		Sr11_3 = (R3_l - R3_r*T3*T3)/(1.0 - R3_l*R3_r*T3*T3);

		S11[id] = Sr11_1 + Sr12_1*T2*T2*Sr11_3*Sr21_1/(1.0 - Sr22_1*Sr11_3*T2*T2);
	}

}
