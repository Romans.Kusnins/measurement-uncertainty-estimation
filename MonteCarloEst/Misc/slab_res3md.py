import numpy as np


def POW2(x):
	return x*x


def three_slab(l, ep1, ep2, ep3, d1, d2, L):

	PI = np.pi

	kzo  = np.sqrt(l*l     - 0.25);
	kzp1 = np.sqrt(l*l*ep1 - 0.25);
	kzp2 = np.sqrt(l*l*ep2 - 0.25);
	kzp3 = np.sqrt(l*l*ep3 - 0.25);

	R1_l = (kzo  - kzp1)/(kzo  + kzp1);
	R1_r = (kzp2 - kzp1)/(kzp2 + kzp1);

	R3_l = (kzp2 - kzp3)/(kzp2 + kzp3);
	R3_r = (kzo  - kzp3)/(kzo  + kzp3);

	R1 = (kzo - kzp1)/(kzo + kzp1);
	R2 = (kzo - kzp2)/(kzo + kzp2);
	R3 = (kzo - kzp3)/(kzo + kzp3);

	T1 = np.exp(2.0j*PI*kzp1*d1);
	T2 = np.exp(2.0j*PI*kzp2*L);
	T3 = np.exp(2.0j*PI*kzp3*d2);

	Sr11_1 = (R1_l - R1_r*POW2(T1))/(1.0 - R1_l*R1_r*POW2(T1));
	Sr12_1 = T1*(1.0 + R1_l)*(1.0 - R1_r)/(1.0- R1_l*R1_r*POW2(T1));
	Sr21_1 = T1*(1.0 - R1_l)*(1.0 + R1_r)/(1.0 - R1_l*R1_r*POW2(T1));
	Sr22_1 = (R1_r - R1_l*POW2(T1))/(1.0 - R1_l*R1_r*POW2(T1));

	Sr11_3 = (R3_l - R3_r*POW2(T3))/(1.0 - R3_l*R3_r*POW2(T3));
	Sr12_3 = T3*(1.0 + R3_l)*(1.0 - R3_r)/(1.0 - R3_l*R3_r*POW2(T3));
	Sr21_3 = T3*(1.0 - R3_l)*(1.0 + R3_r)/(1.0 - R3_l*R3_r*POW2(T3));
	Sr22_3 = (R3_r - R3_l*POW2(T3))/(1.0 - R3_l*R3_r*POW2(T3));

	S11_1 = R1*(1.0 - POW2(T1))/(1.0 - POW2(R1*T1));
	S12_1 = T1*(1.0 - POW2(R1))/(1.0 - POW2(R1*T1));

	S11_2 = R2*(1.0 - POW2(T2))/(1.0 - POW2(R2*T2));
	S12_2 = T2*(1.0 - POW2(R2))/(1.0 - POW2(R2*T2));

	S11_3 = R3*(1.0 - POW2(T3))/(1.0 - POW2(R3*T3));
	S12_3 = T3*(1.0 - POW2(R3))/(1.0 - POW2(R3*T3));

	A = np.exp(2.0j*PI*kzp2*L);

	Ac = 1.0 - POW2(np.abs(S11_1)) - POW2(np.abs(S12_1));

	print(Ac)

	#-----------------------------------------------------------------

	Sr11 = Sr11_1 + Sr12_1*Sr11_3*POW2(A)*Sr21_1/(1.0 - Sr22_1*Sr11_3*POW2(A));

	P = np.exp(1j*(np.angle(Sr22_1) + np.angle(Sr11_3)))*POW2(A);

	Sr11a = np.abs((np.abs(Sr22_1) - np.abs(Sr11_3)*P)/(1.0 - np.abs(Sr22_1)*np.abs(Sr11_3)*P));

	# std::max(abs((Sr11_1*Sr22_1 - Sr12_1*Sr21_1)*A*A));

	S11e = S11_1 + POW2(S12_1)*S11_2/(1.0 - S11_1*S11_2);
	S22e = S11_2 + POW2(S12_2)*S11_1/(1.0 - S11_1*S11_2);
	S12e = S12_1*S12_2/(1.0 - S11_1*S11_2);

	S11 = S11e + S12e*S12e*S11_3/(1.0 - S22e*S11_3);

	print(Sr11);
	print(S11);

	print(abs(S11));
	print(Sr11a);

	# P = std::exp(1j*(angle(S22e) + angle(S11_3)));

	# S11a = (abs(S22e) - abs(S11_3)*P)/(1.0 - abs(S22e)*abs(S11_3)*P);

	return S11

def three_slab_guide(l, ep1, ep2, ep3, d1, d2, L):

	PI = np.pi

	epo = 1.0;

	epo = np.cdouble(epo)
	ep1 = np.cdouble(ep1)
	ep2 = np.cdouble(ep2)
	ep3 = np.cdouble(ep3)

	kzo  = np.sqrt(POW2(l)*epo - 0.25)
	kzp1 = np.sqrt(POW2(l)*ep1 - 0.25)
	kzp2 = np.sqrt(POW2(l)*ep2 - 0.25)
	kzp3 = np.sqrt(POW2(l)*ep3 - 0.25)

	R1_l = (kzo  - kzp1)/(kzo  + kzp1)
	R1_r = (kzp2 - kzp1)/(kzp2 + kzp1)

	R3_l = (kzp2 - kzp3)/(kzp2 + kzp3)
	R3_r = (kzo  - kzp3)/(kzo  + kzp3)

	T1 = np.exp(2.0j*PI*kzp1*d1)
	T2 = np.exp(2.0j*PI*kzp2*L)
	T3 = np.exp(2.0j*PI*kzp3*d2)

	Sr11_1 = (R1_l - R1_r*POW2(T1))/(1.0 - R1_l*R1_r*POW2(T1))
	Sr12_1 = T1*(1.0 + R1_l)*(1.0 - R1_r)/(1.0- R1_l*R1_r*POW2(T1))
	Sr21_1 = T1*(1.0 - R1_l)*(1.0 + R1_r)/(1.0 - R1_l*R1_r*POW2(T1))
	Sr22_1 = (R1_r - R1_l*POW2(T1))/(1.0 - R1_l*R1_r*POW2(T1))

	Sr11_3 = (R3_l - R3_r*POW2(T3))/(1.0 - R3_l*R3_r*POW2(T3))
	Sr12_3 = T3*(1.0 + R3_l)*(1.0 - R3_r)/(1.0 - R3_l*R3_r*POW2(T3))
	Sr21_3 = T3*(1.0 - R3_l)*(1.0 + R3_r)/(1.0 - R3_l*R3_r*POW2(T3))
	Sr22_3 = (R3_r - R3_l*POW2(T3))/(1.0 - R3_l*R3_r*POW2(T3))
	#---------------------------------------------------------------------
	A = np.exp(2.0j*PI*kzp2*L)

	Sr11 = Sr11_1 + Sr12_1*Sr11_3*POW2(A)*Sr21_1/(1.0 - Sr22_1*Sr11_3*POW2(A))

	# P = np.exp(1j*(np.angle(Sr22_1) + np.angle(Sr11_3)))*POW2(A);
	# Sr11a = np.abs((np.abs(Sr22_1) - np.abs(Sr11_3)*P)/(1.0 - np.abs(Sr22_1)*np.abs(Sr11_3)*P));

	return Sr11

def three_slab_free_space(ep1, ep2, ep3, d1, d2, L):

	PI = np.pi

	epo = 1.0;

	epo = np.cdouble(epo)
	ep1 = np.cdouble(ep1)
	ep2 = np.cdouble(ep2)
	ep3 = np.cdouble(ep3)

	kzo  = np.sqrt(epo)
	kzp1 = np.sqrt(ep1)
	kzp2 = np.sqrt(ep2)
	kzp3 = np.sqrt(ep3)

	R1_l = (kzo  - kzp1)/(kzo  + kzp1)
	R1_r = (kzp2 - kzp1)/(kzp2 + kzp1)

	R3_l = (kzp2 - kzp3)/(kzp2 + kzp3)
	R3_r = (kzo  - kzp3)/(kzo  + kzp3)

	T1 = np.exp(2.0j*PI*kzp1*d1)
	T2 = np.exp(2.0j*PI*kzp2*L)
	T3 = np.exp(2.0j*PI*kzp3*d2)

	Sr11_1 = (R1_l - R1_r*POW2(T1))/(1.0 - R1_l*R1_r*POW2(T1))
	Sr12_1 = T1*(1.0 + R1_l)*(1.0 - R1_r)/(1.0- R1_l*R1_r*POW2(T1))
	Sr21_1 = T1*(1.0 - R1_l)*(1.0 + R1_r)/(1.0 - R1_l*R1_r*POW2(T1))
	Sr22_1 = (R1_r - R1_l*POW2(T1))/(1.0 - R1_l*R1_r*POW2(T1))

	Sr11_3 = (R3_l - R3_r*POW2(T3))/(1.0 - R3_l*R3_r*POW2(T3))
	Sr12_3 = T3*(1.0 + R3_l)*(1.0 - R3_r)/(1.0 - R3_l*R3_r*POW2(T3))
	Sr21_3 = T3*(1.0 - R3_l)*(1.0 + R3_r)/(1.0 - R3_l*R3_r*POW2(T3))
	Sr22_3 = (R3_r - R3_l*POW2(T3))/(1.0 - R3_l*R3_r*POW2(T3))
	#---------------------------------------------------------------------
	A = np.exp(2.0j*PI*kzp2*L)

	Sr11 = Sr11_1 + Sr12_1*Sr11_3*POW2(A)*Sr21_1/(1.0 - Sr22_1*Sr11_3*POW2(A))

	# P = np.exp(1j*(np.angle(Sr22_1) + np.angle(Sr11_3)))*POW2(A);
	# Sr11a = np.abs((np.abs(Sr22_1) - np.abs(Sr11_3)*P)/(1.0 - np.abs(Sr22_1)*np.abs(Sr11_3)*P));

	return Sr11

def root_finding(fun, xo, dx, TOL, MAXIT):
	it = 0
	while(it < MAXIT):
		fun_val = fun(xo)
		fun_der = (fun(xo+dx) - fun_val)/dx
		xn = xo - fun_val/fun_der
		if(abs(xn-xo) < TOL):
			break
		xo = xn
		it = it + 1
	# print('Converged at the %d-th iteration' % (it))
	return xo
