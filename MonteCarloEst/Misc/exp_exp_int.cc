#include<math.h>
#include<stdio.h>
#include<complex>
#include<cmath>
#include "Faddeeva.hh"

double expint(double x, int n, char mode);

typedef std::complex<double> cdouble;

#define PI  3.14159265358979
#define POW2(x) ((x)*(x))
#define I  cdouble(0.0, 1.0)



//int main(int argc, char ** argv){

void exp_exp_int(double la[], double tau, double yp, double len, const int M, const int LAS, std::complex<double> resc[]){


const int N = 14;
//const M = 10;

const int MP = 10;

const int MN = (M < N) ? M : N;

//double len = 2.0; 

double  kp;

//double yp   = 0.1;
//double tau  = 0.3;

double  alpha = 2.0;

cdouble ko;
double  kp2;
double  kn[M+1];
double  sqrt2 = sqrt(2.0);


const int K = (M > N) ? M : N;

double  xo = 0.5;
double  x;
double  xa;

double  ka[M][M];
double  ka2[N-1][N-1];

double  mat1[M], mat2[N];

double  rr[M];
double  rr2[N];

double   res[M+1];
cdouble  res1[M+1], res2[M+1];

cdouble p_cf_p[M+1], p_cf_n[M+1];

double  p[M+1][M+1];
double  Cmn[M+1][M+1];

double  cf[N];
long    tm1[N], tm2[N];
double  factor[K+2];
double  leng = -xo;
double  tmp;

double  expxo = exp(xo);
double  expxa;

cdouble  coef_p[M+1],   coef_n[M+1];
cdouble  exp_cf_p[M+1], exp_cf_n[M+1];
cdouble  kx_cf[M+1],    der_cf[M+1];
cdouble  der_cf_p,      der_cf_n;

double   sqrta[M+1];
cdouble  argum_p, argum_n;

double  powa[K+1], powxa[K+1];


// ===================================

powa[0]  = 1.0;
for(int k = 1; k <= K; ++k){
   powa[k]  = powa[k-1]*2.0/alpha; 
}

factor[0] = 1.0;
factor[1] = 1.0;

for(int n = 2; n <= K+1; ++n){
   factor[n] = n*factor[n-1];
}
for(int n = 0; n <= M; ++n){
   for(int m = 0; m <= M; ++m){
       p[n][m] = 0.0;        
       Cmn[n][m] = 0.0;        
   }
}
for(int n = 0; n <= M; ++n){
   for(int m = 0; m <= n; ++m){
       Cmn[n][m] = factor[n]/factor[m]/factor[n-m];        
   }
}

p[0][0] =  1.0;
p[1][1] = -1.0;

for(int n = 2; n < M; ++n){
   for(int m = 0; m < n; ++m){
       p[n][n-m] = -((n-1)*p[n-2][n-m] + p[n-1][n-m-1]);
   }
   p[n][0] = -(n-1)*p[n-2][0];
}


tmp = 1.0;
cf[0] = 0.0;
for(int m = 0; m < N; ++m){
   tm1[m] = 1;
   cf[0] += tm1[m]*tmp/factor[m];
   tmp *= leng;     
}
if(N > 1){
  for(int n = 1; n < N; ++n){
     tmp    = 1.0;
     cf[n]  = 1.0/factor[n]; 
     for(int m = 1; m < N-n; ++m){
        tmp *= leng;     
        tm1[m] += tm1[m-1];
        cf[n] += tm1[m]*tmp/factor[m+n];
     }
  }  
}
for(int k = 0; k < M; ++k){
   for(int n = 0; n < M; ++n){
      ka[k][n] = 0.0; 
   }
}
for(int k = 0; k < N-1; ++k){
   for(int n = 0; n < N-1; ++n){
      ka2[k][n] = 0.0; 
   }
}

for(int n = 0; n < M; ++n){
   for(int m = 0; m < (M-n); ++m){      
      mat1[m] = powa[m+n+1]*factor[m+n]/factor[m];
//    printf("mat1 = %5.12f \n", mat1[m]); 
   } 
   for(int k = 0; k < (M-n); ++k){
      for(int m = 0; m < k+1 && m < N; ++m){
         ka[k+n][n] += cf[m]*mat1[k-m];
//       printf("mat1 = %5.12f \n", mat1[k-m]); 
      }
//    printf("\n");
   } 
//  printf("\n");
//  printf("\n");
  
}

/*
for(int k = 0; k < M; ++k){
   for(int n = 0; n < M; ++n){
      printf("ka[%d][%d] = %5.12f \n", k, n, ka[k][n]);  
   }
    printf("\n");
}
*/

for(int n = 0; n < N-1; ++n){
   for(int m = 0; m < N-1-n; ++m){
      mat2[m] = pow(-1.0,n)/powa[m+n+1]/factor[m+n+1]*factor[m];
   } 
   for(int k = 0; k <= M && k < N-1; ++k){
      for(int m = 0; m < N-1-k-n; ++m){
          ka2[k][n] += cf[m+k+n+1]*mat2[m];
      }
   }   
}
/*
for(int k = 0; k < N-1; ++k){
   for(int n = 0; n < N-1; ++n){
      printf("ka2[%d][%d] = %5.15e \n", k, n, ka2[k][n]);  
   }
    printf("\n");
}
*/


cdouble exptau[MP], exptaun[MP];

exptau[0] = std::exp(I*2.0*PI*tau/len);
exptaun[0] = 1.0/exptau[0];

for(int m = 1; m < MP; ++m){
   exptau[m] = exptau[m-1]*exptau[0];
   exptaun[m] = 1.0/exptau[m];
}


//cdouble resc[1000];

// printf("%d \n", LAS);

//for(int it = 1; it <= 1000; ++it){


// ======================================================

for(int it = 0; it < LAS; ++it){

kp = 2.0*PI*la[it];
kp2 = kp*kp;

sqrta[0] = 1.0;
double alkp = sqrt(alpha/kp2); 
for(int k = 1; k <= M; ++k){
   sqrta[k] = sqrta[k-1]*alkp; 
}
kn[0] = 1.0;
for(int k = 1; k <= M; ++k){
   kn[k] = kn[k-1]*kp;
}

for(int n = 0; n <= M; ++n){
   res1[n] = 0.0;
   res2[n] = 0.0;
}


// ===================================================

for(int m = -1; m <= 1; ++m){

   x  = 0.25*(yp*yp + (m*len + tau)*(m*len + tau))*kp2;
   xa = 2.0/alpha*x;
   expxa = exp(-xa);

   powxa[0] = 1.0;
   for(int k = 1; k <= K; ++k){
      powxa[k] = powxa[k-1]*xa; 
   }

   double R, S[N];

   R = expxo*expint(xa, 1, 'r');


   for(int k = 0; k < M; ++k){
       rr[k] = 0.0;
       for(int n = 0; n <= k; ++n){
          rr[k] += ka[k][n]*1.0/powxa[n+1];
       }
       rr[k] = expxo*expxa*rr[k];  
   }
   for(int k = 0; k < N; ++k){
       rr2[k] = 0.0;
       S[k]   = 0.0;
       for(int n = 0; n < N-k; ++n){
          S[k] += cf[n+k]/factor[n]*pow(-x,n);
       }
       for(int n = 0; n < N-1-k; ++n){
          rr2[k] += ka2[k][n]*powxa[n];
       }
       rr2[k] = expxo*expxa*rr2[k] + R*S[k];
   }

   res[0] = rr2[0];
   for(int k = 1; k <= M+1 && k < N; ++k){
      res[k]  = rr2[k];
      res[k] += rr[k-1];
   }
   for(int k = N; k <= M; ++k){
      res[k] = rr[k-1];
   }

   cdouble mulp(1.0, 0.0);
   cdouble muln(1.0, 0.0);

   cdouble imulp =  0.5*(yp + I*static_cast<double>(m*len + tau))*kp;   
   cdouble imuln = -0.5*(yp - I*static_cast<double>(m*len + tau))*kp;   

   for(int n = 1; n <= M+1; ++n){
      res1[n-1] -= I*res[n-1]/PI*mulp;
      res2[n-1] -= I*res[n-1]/PI*muln;
      mulp *= imulp;   
      muln *= imuln;   
   }

}

// =================================================

for(int m = 0; m <= MP; ++m){

   ko = std::sqrt(static_cast<cdouble>(kp2 - POW2(2.0*PI*m/len)));

   argum_p = ( yp*kp - I*ko*alpha/kp)/sqrt(alpha);
   argum_n = (-yp*kp - I*ko*alpha/kp)/sqrt(alpha);

// printf("argum_p = %5.15g, %5.15g\n", argum_n.real(), argum_n.imag()); 

   coef_p[0] = 1.0;
   coef_n[0] = 1.0;
   exp_cf_p[0] = 1.0;
   exp_cf_n[0] = 1.0;
   kx_cf[0] = 1.0;

   for(int k = 1; k <= M; ++k){
      coef_p[k]   = coef_p[k-1]*argum_p; 
      coef_n[k]   = coef_n[k-1]*argum_n; 
      exp_cf_p[k] = exp_cf_p[k-1]*(-I*ko);
      exp_cf_n[k] = exp_cf_n[k-1]*(-I*ko);
      kx_cf[k]    = kx_cf[k-1]*(-2.0*PI*m/len);
   }

   cdouble exp_tm_p = std::exp(-0.5*coef_p[2]);
   cdouble exp_tm_n = std::exp(-0.5*coef_n[2]);

   p_cf_p[0] =  Faddeeva::erfc(argum_p/sqrt2);
   p_cf_n[0] =  Faddeeva::erfc(argum_n/sqrt2);

   for(int n = 1; n <= M; ++n){
      p_cf_p[n] = 0.0; 
      p_cf_n[n] = 0.0; 
      for(int k = 0; k < n; ++k){
         p_cf_p[n] += p[n-1][k]*coef_p[k]; 
         p_cf_n[n] += p[n-1][k]*coef_n[k]; 
      }
      p_cf_p[n] *= -exp_tm_p/sqrta[n]/sqrt(PI/2.0);
      p_cf_n[n] *= -exp_tm_n/sqrta[n]/sqrt(PI/2.0);
   }

/*
   for(int n = 0; n <= M; ++n){   
      printf("p_cf_p = %5.15g, %5.15g\n", p_cf_p[n].real(), p_cf_p[n].imag()); 
   }
   printf("\n");
*/

   for(int n = 1; n <= M+1; ++n){
      der_cf_p = 0.0;
      der_cf_n = 0.0;
      for(int k = 0; k < n; ++k){
          der_cf_p += Cmn[n-1][k]*exp_cf_p[n-1-k]*p_cf_p[k];
          der_cf_n += Cmn[n-1][k]*exp_cf_n[n-1-k]*p_cf_n[k];
      }
      der_cf[n-1] = (der_cf_p*exp(-I*ko*yp) - pow(-1,n)*der_cf_n*exp(I*ko*yp))/ko;

//    printf("der = %5.15g, %5.15g\n", der_cf[n-1].real(), der_cf[n-1].imag()); 
   }
// printf("\n");

   cdouble tmpres;

   if(m==0){
      for(int n = 1; n <= M+1; ++n){
         for(int k = 0; k < n; ++k){
            tmpres = Cmn[n-1][k]*kx_cf[n-1-k]*der_cf[k]/kn[n-1]/len;
            res1[n-1] -= std::pow(-1,n)*tmpres;
            res2[n-1] += tmpres;
         }   
      }
   }else{

      for(int n = 1; n <= M+1; ++n){
         for(int k = 0; k < n; ++k){
            tmpres = Cmn[n-1][k]*kx_cf[n-1-k]*der_cf[k]/kn[n-1]/len;
            res1[n-1] -= std::pow(-1,n)*(exptau[m-1] + pow(-1,n-1-k)*exptaun[m-1])*tmpres;
            res2[n-1] += (exptaun[m-1] + pow(-1,n-1-k)*exptau[m-1])*tmpres;
         }   
      }
   }
}

//resc[it-1] = res1[M];

//}

// =======================================================================


for(int n = 0; n < M; ++n){
//  printf("res2 = %5.15g, %5.15g \n", res2[M-n].real(), res2[M-n].imag()); 
   *(resc++) = res2[M-n];
}
//printf("\n");
for(int n = 0; n <= M; ++n){
//  printf("res1 = %5.15g, %5.15g \n", res1[n].real(), res1[n].imag()); 
   *(resc++) = res1[n];
}

}


//for(int n = 0; n < 100; ++n){
//  printf("resc = %5.15g, %5.15g \n", resc[99].real(), resc[99].imag()); 
//}

// return 1;

}




