
import os

filename = 'overleaf_test'

fp = open(filename + '.tex', 'w')

str = ""


str += '\\documentclass[fleqn,a4paper,12pt]{article}%%\n'
str += '\\usepackage[left=1in, right=1in, top=1in, bottom=1in]{geometry}%%\n'

str += '\\usepackage{amsmath}%%\n'
str += '\\usepackage{tabulary}%%\n'
str += '\\usepackage{polyglossia}%%\n'
str += '\\usepackage{upgreek}%%\n'
str += '\\usepackage{siunitx}%%\n'
str += '\\usepackage{titlesec}%%\n'
str += '\\usepackage{tikz}%%\n'
str += '\\usepackage{caption}%%\n'
str += '\\usepackage{graphicx}%%\n'
str += '\\usepackage{listings}%%\n'
str += '\\usepackage{color}%%\n'
str += '\\usepackage[american,siunitx]{circuitikz}%%\n'
str += '\\usetikzlibrary{calc,math}%%\n\n'

str += '\\begin{document}%%\n\n'
#======================================================
str +='\\def\\ccdot{\\mskip-3mu\\cdot\\mskip-3mu}%%\n\n'
#======================================================
str += '\\makeatletter%%\n'
str += '''\\def\\mklist #1#2{%%
	\\newcount\cnt%%
	\\cnt=1\\relax%%
	\\expandafter\\edef\\csname#1\\endcsname{}%%
	\\def\\arradd ##1,##2\\@Del{%%
		\\edef\\cmpi{E}%%
		\\edef\\cmpii{##2}%%
		\\expandafter\\edef\\csname#1\\endcsname{%%
			\\csname#1\\endcsname\\the\\cnt:##1;%%
		}%%
		\\advance\\cnt by 1\\relax%%
		\\ifx\\cmpi\\cmpii\\relax%%
		\\else%%
			\\arradd ##2\\@Del%%
		\\fi%%
	}%%
	\\expandafter\\arradd #2,E\\@Del%%\n}%%
	%%\n\n'''

str +=	'''\\def\\listgeta #1#2#3{%%
	\\def\\extract ##1#1:##2;##3\\@Del{%%
		##2pt%%
	}%%
	#3=\\expandafter\\extract #2\\@Del\\relax%%\n}%%\n\n'''
	
str +=	'''\\def\\listget #1#2[#3]{%%
	\\expandafter\\listgeta\\expandafter{#3}{#2}{#1}%%\n}%%\n\n'''
	
#======================================================
# define dimension and counter registers
str += '\\newdimen\\chlnwd%%\n'
str += '\\newdimen\\cvlnwd%%\n'
str += '\\newdimen\\cvlnwdp%%\n'
str += '\\newdimen\\cvlnwdm%%\n'
str += '\\newdimen\\bvlnwd%%\n'
str += '\\newdimen\\cellwd%%\n'
str += '\\newdimen\\tablewidth%%\n'
str += '\\newdimen\\firstcellwd%%\n'
str += '\\newdimen\\cellht%%\n'

str += '\\newdimen\\tmpdimi%%\n'
str += '\\newdimen\\tmpdimii%%\n'
str += '\\newdimen\\tmpdimiii%%\n'
str += '\\newcount\\tmpcount%%\n'
str += '\\newcount\\colnum%%\n'
str += '\\newcount\\loopcnt%%\n'

# intialize registers
str += '\\chlnwd=0.5pt%%\n'
str += '\\cvlnwd=0.5pt%%\n'
str += '\\bvlnwd=1.5pt%%\n'
str += '\\firstcellwd=145pt%%\n'
str += '\\cellwd=80pt%%\n'

# define some macro
str += '\\def\\rhti{18}%%\n'
str += '%%\\def\\cwdi{135}%%\n'
str += '%%\\def\\firstcwdi{135}%%\n'

str += '%%===================================================\n\n'

str += '\\makeatletter%%\n\n'

str += '''\\def\\addtablineleft #1||#2\\@Del#3{%%'
	\\edef\\cmpa{E}%%
	\\def\\cmpb{#2}%%
	\\hbox to #3 {%%
		\\kern3pt {\\fontsize{11}{11} \\selectfont #1} \\kern3pt \\hfil%%
	}%%
	\\ifx\\cmpa\\cmpb%%
	\\else%%
		\\hrule%%
	\\addtablineleft #2\\@Del{#3}%%
	\\fi%\n}%%\n\n'''

str += '''\\def\\addtablinecent #1||#2\\@Del#3{%%
	\\edef\\cmpa{E}%%
	\\def\\cmpb{#2}%%
	\\hbox to #3 {%%
		\\hfil{\\fontsize{11}{11}\\selectfont #1}\\hfil%%
	}%%
	\\ifx\\cmpa\\cmpb%%
	\\else%%
		\\vfil%%
		\\hrule%%
		\\vfil%%
		\\addtablinecent #2\\@Del{#3}%%
	\\fi%%\n}%%\n\n'''

str += '''\\def\\tabcell [#1,#2]#3#4#5{%%
	\\edef\\cmpi{c}%%
	\\edef\\cmpii{#4}%%
	\\cellht=#2pt\\relax%%
	\\multiply\\cellht by #5\\relax%%
	\\vbox to \\cellht {%%
		\\vfil%%
		\\ifx\\cmpi\\cmpii%%
			\\addtablinecent #3||E\\@Del{#1}%%
		\\else%%
			\\addtablineleft #3||E\\@Del{#1}%%
		\\fi%%
		\\vfil%%
	}%%\n}%%\n\n'''

str += '''\def\\ctabcell [#1,#2]#3#4#5{%%
	\\tabvline{\\cvlnwd}%%
	\\tabcell [#1,#2]{#3}{#4}{#5}%%\n}%%\n\n'''

str += '''\def\\firstcell #1,#2\\@Dela#3,#4\\@Delb#5{%%
	\\colnum=1\\relax%%
	\\listget{\\cellwd}{\\colwd}[\\the\\colnum]%%
	\\tabcell[\\cellwd,\\rhti]{#1}{#3}{#5}%%
	\\sepitems #2,E\\@Dela#4,E\\@Delb{#5}%%\n}%%\n\n'''

str += '''\def\\sepitems #1,#2\\@Dela#3,#4\\@Delb#5{%%
	\\advance\\colnum by 1\\relax%%
	\\listget{\\cellwd}{\\colwd}[\\the\\colnum]%%
	\\edef\\Del{E}%%
	\\def\\cmp{#2}%%
		\\ctabcell[\\cellwd,\\rhti]{#1}{#3}{#5}%%
		\\ifx\\cmp\\Del%%
		\\else%%
			\\sepitems #2\@Dela#4\@Delb{#5}%%
		\\fi%%
	}%%\n\n'''

str += '''\\def\\geblist #1#2#3{%%
	\\firstcell #1\\@Dela#2\\@Delb{#3}%%\n}%%\n\n'''

str += '\\makeatother%%\n\n'

str += '''\\def\\tabvline #1{%%
	\\vrule width #1%%\n}%%\n\n'''

str += '''\\def\\tabrow #1#2#3{%%
	\\hbox{%%
		\\tabvline{\\bvlnwd}%%
		\\geblist{#1}{#2}{#3}%%
		\\tabvline{\\bvlnwd}%%
	}%%
	\\tabhline{\\the\\chlnwd}%%
	\\kern -1pt%%\n}%%\n\n'''

str += '''\\def\\tablastrow #1{%%
	\\hbox{%%
		\\tabvline{\\bvlnwd}%%
		\\geblist{#1}%%
		\\tabvline{\\bvlnwd}%%
	}%%\n}%%\n\n'''
	
str += '''\\def\\cellhline [#1,#2,#3]#4{%%
	\\tmpdimi = #1\\relax%%
	\\tmpdimii = #2\\relax%%
	\\tmpcount = #3\\relax%%
	\\multiply\tmpdimi\\tmpcount\\relax%%
	\\advance\\tmpcount-1\\relax%%
	\\multiply\\tmpdimii\\tmpcount\\relax%%
	\\advance\\tmpdimi\\tmpdimii\\relax%%
	\\hrule width \\the\\tmpdimi height #4\\relax%%\n}%%\n\n'''

str += '''\\def\\tablewd #1#2{%%
	\\tablewidth=0pt\\relax%%
	\\loopcnt=0\\relax%%
	\\loop%%
	\\ifnum\\loopcnt < #1%%
		\\advance\\loopcnt by 1\\relax%%
		\\listget{\\cellwd}{#2}[\\the\\loopcnt]%%
      		\\advance\\tablewidth\\cvlnwd\\relax%%
		\\advance\\tablewidth\\cellwd\\relax%%
	\\repeat%%
	\\advance\\tablewidth-\\cvlnwd\\relax%%
	\\advance\\tablewidth\\bvlnwd%%
	\\advance\\tablewidth\\bvlnwd%%\n}%%\n\n'''

str += '''\\def\multicolwd #1#2[#3-#4]{%%
	\\loopcnt=#3\\relax%%
	\\listget{\\tmpdimii}{#2}[\\the\\loopcnt]%%
	\\tmpdimi=\\tmpdimii\\relax%%
	\\loop%%
	\\ifnum\\loopcnt < #4%%
		\\advance\\loopcnt by 1\\relax%%
		\\listget{\\tmpdimii}{#2}[\\the\loopcnt]%%
		\\advance\\tmpdimi\\tmpdimii\\relax%%
		\\advance\\tmpdimi\\cvlnwd\\relax%%
	\\repeat%%
	#1=\\tmpdimi\\relax%%\n}%%\n\n'''
#===================================================================
str += '''\\def\\textsize [#1,#2]#3{%%
	{\\fontsize{#1}{#2} \\selectfont#3}%%\n}%%\n\n'''

str += '\\def\\tabhline #1{\\hrule height #1 width \\tablewidth}%%\n\n'
str += '%%========================================================\n\n'

str += '\\mklist{colwd}{100,100,100}%%\n\n'

str += '\\tablewd{3}{\\colwd}%%\n\n'

str += '''\\hbox to \\textwidth {%%
	\\hfil\\vbox{%%
		\\hbox to \\tablewidth {\\hfil 1. Tabula: Variantu tabula\\hfil}%%
		\\kern8pt%%
		\\tabhline{1.5pt}%%
		\\hbox{%%
			\\vrule width \\bvlnwd%%\n'''
	
str +=	'''\t\t\t\\listget{\\cellwd}{\\colwd}[1]% Sets the register \\cellwd to the first element in the list \\colwd (3rd argument may be a counter)%%
			\\vbox to 46pt {%%
				\\vfil%%
				\\hbox to \\cellwd {\\hfil \\textsize[11,11]{Varianta}\\hfil}%%			\\hbox to \\cellwd {\\hfil \\textsize[11,11]{Nr.}\\hfil}%%
				\\vfil%%
			}%%
			\\vrule width \\cvlnwd%%
			\\listget{\\cellwd}{\\colwd}[2]%%
			%% Sets the register \\cellwd to the second element in the list \\colwd 
			\\vbox to 46pt {%%
				\\vfil%%
				\\hbox to \\cellwd {\\hfil \\textsize[11,11]{2rd column}\\hfil}%%
				\\vfil%%
			}%%\n'''
	
str +=	'\t\t\t\\vrule width \\cvlnwd%%\n'

str +=	'''\t\t\t\\listget{\\cellwd}{\\colwd}[3]%%
			%% Set the register \\cellwd to the second element in the list \\colwd%%\n'''
			
str += '''		\t\\vbox to 46pt {%%
				\\vfil%%
				\\hbox to \\cellwd {\\hfil\\textsize[11,11]{3rd column}\\hfil}%%
				\\vfil%%
			}%%\n'''
	
str +=	'''\t\t\t\\vrule width \\bvlnwd%%
		}%%\n'''
		
str +='''\t\t\\tabhline{1.5pt}%%
		\\tabrow{1.,181REB346||181RDB181,1}{c,c,c}{3}%%
		\\tabrow{2.,171REC048||161REB071,2}{c,c,c}{3}%%
		\\tabrow{3.,181RMB003||\\phantom{ABC},3}{c,c,c}{3}%%
		\\tabrow{4.,181RDB210||\\phantom{ABC},4}{c,c,c}{3}%%
		\\tabhline{1.5pt}%%
	}%%
	\\hfil%%
}%%\n\n'''

str += '\\end{document}%%\n\n'

#===========================================================
fp.write(str)
fp.close()
#===========================================================
os.system('xelatex ' + filename + '.tex > /dev/null 2>&1')

# open the generated PDF file with the default linux viewer
os.system('xdg-open ' + filename + '.pdf')

