#include<octave/oct.h>
#include<complex>

double expint(double x, int n, char mode);

void exp_exp_int_ss(double * la, double x, double len, const int M, const int LAS, std::complex<double> * res);


DEFUN_DLD (schlom_s, args, , "Evaluate expint(x,1)")
{
  int nargin = args.length();

  if(nargin != 4)
     print_usage ();
  else
  {

     NDArray LA  = args(0).array_value();
     NDArray X   = args(1).array_value();
     NDArray LEN = args(2).array_value();
     NDArray M   = args(3).array_value();

     dim_vector dv(2*M(0)+1,  LA.dims()(1));
     ComplexNDArray  RES(dv,0);

//     printf("%d \n", A.ndims());
//     printf("%d \n", A.dims()(0));
//     printf("%d \n", A.dims()(1));

//   A(0) = expint(A(0), 1, 'r');

     exp_exp_int_ss(LA.fortran_vec(), X(0), LEN(0), M(0), LA.dims()(1), RES.fortran_vec());

     if(!error_state)
        return octave_value(RES);
  }

  return octave_value_list();
}

