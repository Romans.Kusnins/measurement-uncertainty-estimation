import os
import numpy as np


def MTTW2020_two_solid_posts():
	pass



def length(x):
	if type(x) is np.ndarray:
		return max(x.shape)
	else:
		return len(x)

def n2s(x, *numdec):
	if abs(x-int(x)) < 1e-15:	# Check whether x is an integer or not.
		return '%d' % (int(x))	# If this is the case, omit the comma 					# and the fractional part
	else:
		if len(numdec)>0 and int(numdec[0])>0:# if the number of decimal places is 					# specified print, the number with the 
					# numdec decimal>0 places, else display 6 decimal places 					# decimal places
			fstr = "%%1.%df" % (int(numdec[0]))
			return fstr % (x)
		else:
			return '%1.6f' % (x)


def plot2latex_MTTW2020_2():

	filename = 'THESIS_FIG_1'

	# ======================================================================
	# bar dimensions and other properties
	# ======================================================================
	# d1=0.2; d2=0.56; d3=0.15; ep=1:0.01:70; plot(ep, abs(slabs_newton_value(12.85*(1+j*0.1), d1, d2, d3)),'r', ep, abs(slabs_newton_value(1, 1, ep*(1+j*0.001), d1, d2, d3)))
	# ======================================================================
	# 12.85*(1.0+1j*0.0019), 2.94*(1.0+1j*0.0012), ep3*(1.0+1j*0.0001), 0.18, 0.645, 0.3

	# epo = 10

	ko = np.linspace(0.501, 0.999, 1000)

	ro1   = 0.14
	ro2   = 0.14
	ri1   = 0.02
	ri2   = 0.02
	d1    = 0.05
	d2    = 0.05
	xo1   = 0.5+0.3
	xo2   = 0.5-0.25
	phi1  = 90.0
	phi2  = 90.0

	#========================================================================
#	MTTW2020_two_solid_posts()
#	S11 = S[:,0,0]

#	R1, T1 = MoM_two_in_two_d(12.6, 23.7, ko, 0.14, 0.12, 0.02, 0.02, 0.05, 0.04, 0.5+0.3, 0.5-0.25, np.pi/4.0, np.pi/4.0)

#	yo[1,:] = np.abs(S11)
#	yo[0,:] = np.abs(R1)

	yo = np.empty((2,length(ko)))

	yo[0,:] = np.abs(np.cos(2.0*np.pi*(ko-0.5)))
	yo[1,:] = np.abs(np.sin(2.0*np.pi*(ko-0.5)))

	# Ro_1 = np.abs(slabs_newton_value(1.0, 1.0, ep3o*(1+1j*tan3), d1, d2, d3))
	# Ro_2 = np.abs(slabs_newton_value(ep1*(1.0+1j*tan1), ep2*(1.0+1j*tan2), ep3o*(1.0+1j*tan3), d1, d2, d3))

	# ep3l_1 = np.abs(S11_finding(1.0, 1.0, ep3o*(1.0+1j*tan3), d1, d2, d3, Ro_1-Rl_1, ep3_min_1, ep3_max_1) - ep3o)

	# ep3u_1 = np.abs(S11_finding(1.0, 1.0, ep3o*(1.0+1j*tan3), d1, d2, d3, Ro_1+Ru_1, ep3_min_1, ep3_max_1) - ep3o)

	# ep3l_2 = np.abs(S11_finding(ep1*(1.0+1j*tan1), ep2*(1.0+1j*tan2), ep3o*(1.0+1j*tan3), d1, d2, d3, Ro_2-Rl_2, ep3_min_2, ep3_max_2) - ep3o)

	# ep3u_2 = np.abs(S11_finding(ep1*(1.0+1j*tan1), ep2*(1.0+1j*tan2), ep3o*(1.0+1j*tan3), d1, d2, d3, Ro_2+Ru_2, ep3_min_2, ep3_max_2) - ep3o)
	#===========================================================================
	# ko = ep3;
	#===========================================================================
	LEN = yo.shape[0]

	linewidth = 0.5
	#===========================================================================
	xlabel_dist =  0.1
	ylabel_dist =  0.12
	#===========================================================================
	#===========================================================================
	legend_dims = [0.6, 0.1, 0.25, 0.16]
	legend_line_dims = [0.01, 0.05]

	tickscale = 2.2

	legend_scale = 2.0

	labelx_scale = 2.6
	labely_scale = 2.4

	lwidth  = 0.25
	lheight = 0.1

	legend_text = list()
	legend_text.append('$ \\text{IBIM} $')
	legend_text.append('$ \\text{HFSS} $')

	color = list()
	color.append('blue')
	color.append('red')
	color.append('black')
	color.append('magenta')
	color.append('green')

	fid = open(filename + '.tex', 'w')
	#===========================================================================
	komin = 0
	# komin = min(ko)
	komax = max(ko)

	ko = 2.0*(ko - 0.5)

	# xl_1 = (ep3o - ep3l_1)/komax
	# xo_1 = ep3o/komax
	# xu_1 = (ep3o + ep3u_1)/komax

	# yl_1 = Ro_1 - Rl_1
	# yo_1 = Ro_1
	# yu_1 = Ro_1 + Ru_1

	# xl_2 = (ep3o - ep3l_2)/komax
	# xo_2 = ep3o/komax
	# xu_2 = (ep3o + ep3u_2)/komax

	# yl_2 = Ro_2 - Rl_2
	# yo_2 = Ro_2
	# yu_2 = Ro_2 + Ru_2

	# ko = (ko-komin)/(komax-komin)
	# ko = ko/komax

	len = 0.05

	N = 1

	ko = ko[::N]
	yo = yo[:,::N]

	#==========================================================================
	# fid.write('\\usepackage[hiresbb, dvips]{graphicx} \n');
	# fid.write('\\usepackage{color} \n');

	str = ""
	str += '\\documentclass[convert]{standalone} \n' 
	str += '\\usepackage{siunitx} \n'
	str += '\\usepackage[dvips]{color} \n'
	str += '\\usepackage{textgreek} \n'
	str += '\\usepackage{tikz} \n'
	str += '\\usetikzlibrary{calc} \n'
	str += '\\usetikzlibrary{arrows} \n'
	str += '\\begin{document} \n'
	str += '\\begin{tikzpicture}[scale=20] \n'

	fid.write(str)
	#===========================================================================
	str = ""

	strlinewidth =  n2s(linewidth)

	print(LEN)
	print(length(ko))

	for m in range(LEN):
		if m==2:
			yl = yo[m,0]
			kl = ko[0]

			for n in range(length(ko)-1):
				# sp = np.round(n/5.0)
				# if mod(sp,2) == 0:
				# else:
					# str += '\\draw'+color[m] + ', line width=' + strlinewidth +'mm] ('
					# str += n2s(ko(n))  + ',' + n2s(yo(m,n)) + ') -- ('
					# str += n2s(ko[n+1] + ',' + n2s(yo[m,n+1]) + '); \n'

				if np.abs(yl-yo[m,n]) > 0.02 or np.abs(kl-ko[n]) > 0.02:
					yl = yo[m,n]
					kl = ko[n]
					str += '\\draw[' + color[m] + ',line width='
					str += strlinewidth + 'mm, fill='+color[m] + '] (' + n2s(ko[n])
					str += ',' + n2s(yo[m,n]) + ') circle (0.02mm); \n'
		else:
			for n in range(length(ko)-1):
				str += '\\draw[' + color[m] + ', line width='
				str += strlinewidth + 'mm] ('  + n2s(ko[n])   + ','
				str += n2s(yo[m,n]) + ') -- (' + n2s(ko[n+1]) + ','
				str += n2s(yo[m,n+1]) + '); \n'

	fid.write(str)
	#===========================================================================
	str = ""

	x = np.linspace(0.0, 1.0, 6)
	y = np.arange(0.0, 1.0+0.1, 0.1)

	grid_line_V(fid, x, np.vstack([x+0.0, -0.01*np.ones((1, length(x)))]), x/2.0+0.5, tickscale, [0,1],'black')

	grid_line_H(fid, y, np.vstack([0.0*np.ones((1, length(y))), y]), y, tickscale, [0,1], 'black')

	axis_new(fid, [0.0, 1.0, 0.0, 1.0], 0.5)
	#===========================================================================
	# rectangle_rot(h, [xl_1, 0, xu_1-xl_1, 1], 0, [0,0], 'black', 'blue', 0.25, 'corner')

	# rectangle_rot(h, [0, yl_1, 1, yu_1-yl_1], 0, [0,0], 'black', 'blue', 0.25, 'corner')

	# rectangle_rot(h, [xl_2, 0, xu_2-xl_2, 1], 0, [0,0], 'black', 'red', 0.25, 'corner')

	# rectangle_rot(h, [0, yl_2, 1, yu_2-yl_2], 0, [0,0], 'black', 'red', 0.25, 'corner');

	# vert_lines(h, [xl_1, xo_1, xu_1], [0.23, 0.3, 0.2], 0.2, [0.25,0.05], [0.12,0.1], 'blue', 0.25, 'blue', ...
	      # 2, {['\\Delta \\varepsilon^{\\prime}_{\\mathrm{r3}}=', n2s(ep3u_1+ep3l_1),],...
	      # ['\\varepsilon^{\\prime}_{\\mathrm{r3}}=', n2s(ep3o)]})

	# horis_lines(h, [yl_1, yo_1, yu_1], [0.15, 0.4], 0.1, [0.02, 0.05, 0.3], [0.1,0.12], 'blue', 0.25, 'blue', ...
 	      # 2, {['R^{+}=', n2s(Ro_1,2)], ['\\Delta R^{+}=', n2s(Ru_1+Rl_1)]})

	# vert_lines_no(h, [xl_2, xu_2], [0.12], 0.1, [0.3, 0.05], [0.12], 'red', 0.25, 'red', ...
	      #2, {['\\Delta \\varepsilon^{\\prime}_{\\mathrm{r3}}=', n2s(ep3u_2+ep3l_2),],...
	      #['\\varepsilon^{\\prime}_{\\mathrm{r3}}=', n2s(ep3o)]})

	# horis_lines(h, [yl_2,yo_2,yu_2], [0.15,0.4], 0.1, [0.02,0.05,0.3], [0.1,0.12], 'red', 0.25, 'red', ...
 	      # 2, {['R^{+}=', n2s(Ro_2,2)], ['\\Delta R^{+}=', n2s(Ru_2+Rl_2)]})
	#===========================================================================
	fid.write('\\node[scale = ' + n2s(labelx_scale) + '](xlabel) at (0.5, ' + n2s(-xlabel_dist) + '){  $ a/\\lambda_{\\mathrm{o}} $  };\n')

	fid.write('\\node[scale = ' + n2s(labely_scale) + '](ylabel) at (' + n2s(-ylabel_dist) + ', 0.5){  $ S_{11}  $  };\n')
	#===========================================================================
	fid.write('\\newcommand{\\newarrow}[8]{\\draw[red,fill=red] (#1,#2)--(#1+#4*#8,#2+#4*#7)--(#1+#3*#7+#4*#8,#2-#3*#8+#4*#7)--(#1+#3*#7,#2-#3*#8)--(#1,#2); \\draw[red,fill=red] (#1-#5*#7+#4*#8,#2+#5*#8+#4*#7)--(#1+#3/2*#7+#4*#8+#6*#8,#2-#3/2*#8+#4*#7+#6*#7)--(#1+#3*#7+#5*#7+#4*#8,#2-#3*#8-#5*#8+#4*#7)--(#1-#5*#7+#4*#8,#2+#5*#8+#4*#7)};\n')

	fid.write('\\newcommand{\\rectanglewhite}[4]{\\draw[white,fill=white] (#1,#2)--(#1,#2+#4)--(#1+#3,#2+#4)--(#1+#3,#2)--(#1,#2)};\n')

	fid.write('\\newcommand{\\vertlinea}[3]{\\draw[black] (#1,#2)--(#1,#2+#3)};\n')

	# fid.write(['\\newarrow{0.7}{0.7}{0.02}{0.2}{0.01}{0.02}{1}{0};'])
	# fid.write(['\\newarrow{0.55}{0.7}{0.02}{0.25}{0.01}{0.02}{0.60182}{-0.79864};'])
	# fid.write(['\\newarrow{0.51}{0.61}{0.02}{0.4}{0.01}{0.02}{-0.17365}{-0.98481};'])
	# fid.write(['\\rectanglecentrvert{',n2s(ep3_o/komax),'}{0.0}{',n2s(ep3_delta_low/komax),'}{',n2s(ep3_delta_up/komax),'}{1};'])
	# fid.write(['\\rectanglewhite{0.52}{0.59}{0.36}{0.1};'])
	# fid.write(['\\node[below,scale=3](low) at (0.7,0.7){$ \\text{\\textcolor{magenta}{low sensitivity}} $ };'])
	# fid.write(['\\node[below,scale=3](low) at (0.7,0.65){$ \\text{\\textcolor{magenta}{regions}} $ };'])

	# fid.write('\\draw[red](0.5,0.5)--(0.5,0.8*))')
	# fid.write(['\\rectanglewhite{0.43}{0.8}{0.45}{0.18};'])
	# fid.write(['\\newarrow{0.6}{0.8}{0.02}{0.2}{0.01}{0.02}{-0.17365}{-0.98481};'])

	# fid.write(['\\node[scale=2](low) at (0.65,0.95){$ \\text{\\textcolor{blue}{a region of high sensitivity}} $ };'])
	# fid.write(['\\node[scale=2](low) at (0.65,0.9){$ \\text{\\textcolor{blue}{and uniqueness of solution}} $ };'])
	# fid.write(['\\node[scale=2](low) at (0.65,0.85){$ \\text{\\textcolor{blue}{for extended model}} $ };'])

	legend_box(fid, LEN, color, legend_text, legend_scale, legend_line_dims, legend_dims, 'white', 1)

	fid.write('\end{tikzpicture} \n')
	fid.write('\end{document} \n')

	fid.close()
	#===========================================================================
#	os.system('pdflatex MTTW_FIG_2.tex > /dev/null')
	os.system('pdflatex ' + filename + '.tex > /dev/null 2>&1')

	# open the generated PDF file with the default linux viewer
	os.system('xdg-open ' + filename + '.pdf')

#===================================================================================
def legend_box(fd, NUM, color_array, text_array, text_scale, line_dim, dim, colour, opacity):

	str = ""

	# legend's background
	str += '\\fill[' + colour + ', fill=' + colour + ', opacity='
	str +=  n2s(opacity) + '] (' + n2s(dim[0]) + ',' + n2s(dim[1])
	str += ') rectangle (' + n2s(dim[0]+dim[2]) + ',' + n2s(dim[1]+dim[3]) + ');\n'

	for m in range(NUM):
		if m == 2:
			str += '\\draw[' + color_array[m] + ',line width=0.5mm] ('
			str += n2s(dim[0]+line_dim[0]) +  ','
			str += n2s(dim[1]+dim[3]/(NUM+1.0)*(NUM+1-m)) + ') -- ('
			str += n2s(dim[0]+line_dim[0]+line_dim[1])  + ','
			str += n2s(dim[1]+dim[3]/(NUM+1.0)*(NUM+1-m)) + ');\n'

			x = np.linspace(dim[0]+line_dim[0], dim[0]+line_dim[0]+line_dim[1], 4);

			for n in range(length(x)):
				str += '\\draw[' + color_array[m] + ',line width=0.5mm, fill='
				str += color_array[m] + '] (' + n2s(x(n)) + ','
				str += n2s(dim[1]+dim[3]/(NUM+1.0)*(NUM+1-m))
				str += ') circle (0.02mm); \n'

		# legend's text
			str += '\\node[black, right, scale=' + n2s(text_scale) + '] at ('
			str += n2s(dim[0]+line_dim[0]+line_dim[1]) + ','
			str += n2s(dim[1]+dim[3]/(NUM+1.0)*(NUM+1-m)) + ') {'
			str += text_array[m], '}; \n'
		else:
		# line length
			str += '\\draw[' + color_array[m] + ',line width=0.5mm] ('
			str += n2s(dim[0]+line_dim[0]) + ','
			str += n2s(dim[1]+dim[3]/(NUM+1.0)*(NUM+1-m)) + ') -- ('
			str += n2s(dim[0]+line_dim[0]+line_dim[1]) + ','
			str += n2s(dim[1]+dim[3]/(NUM+1.0)*(NUM+1-m)) + '); \n'
		# legend's text
			str += '\\node[black, right, scale=' + n2s(text_scale) + '] at ('
			str += n2s(dim[0]+line_dim[0]+line_dim[1]) + ','
			str += n2s(dim[1]+dim[3]/(NUM+1.0)*(NUM+1-m)) + ') {'
			str += text_array[m] + '}; \n'

	fd.write(str)
#=========================================================================
def axis_new(fd, dims, *varargin):

	x_min = n2s(dims[0])
	x_max = n2s(dims[1])

	y_min = n2s(dims[2])
	y_max = n2s(dims[3])

	if len(varargin) == 1:
		line_width = 'line width=' + n2s(varargin[0]) + 'mm'
	else:
		line_width = 'line width=0.25mm'	# default line width

	str  = '\\draw[' + line_width + '] (' + x_min + ',' + y_min
	str += ')--(' + x_min + ',' + y_max + '); \n'

	str += '\\draw[' + line_width + '] (' + x_max + ',' + y_min
	str += ')--(' + x_max + ',' + y_max + '); \n'

	str += '\\draw[' + line_width + '] (' + x_min + ',' + y_min
	str += ')--(' + x_max + ',' + y_min + '); \n'

	str += '\\draw[' + line_width + '] (' + x_min + ',' + y_max
	str += ')--(' + x_max + ',' + y_max + '); \n'

	fd.write(str)
#===========================================================================
def grid_line_V(fd, xvec, value_position, value_array, tickscale, dims, *varargin):

	str = ""

	LEN = length(xvec)

	if len(varargin) == 1:
		line_colour = varargin[0]
	else:
		line_colour = 'black'

	for n in range(LEN):
		if n > 0 and n < LEN-1:
			str += '\\draw[' + line_colour
			str += ', line width=0.25mm, dashed,'
			str += 'dash pattern=on 1.0mm off 1.0mm] ('
			str += n2s(xvec[n]) +  ',' + n2s(dims[0]) + ')--('
			str += n2s(xvec[n]) + ',' + n2s(dims[1]) + '); \n'

		str += '\\node[below, scale=' + n2s(tickscale) + '] at ('
		str += n2s(value_position[0,n]) + ',' + n2s(value_position[1,n])
		str += ') {$' + n2s(value_array[n], 1) + '$}; \n'

	fd.write(str)
#=============================================================================
def grid_line_H(fd, yvec, value_position, value_array, tickscale, dims, varargin):

	str = ""

	LEN = length(yvec)

	if len(varargin) == 1:
		line_colour = varargin[0]
	else:
		line_colour = 'black'

	for n in range(LEN):
		if n > 0 and n < LEN-1:
			str += '\\draw[' + line_colour
			str += ', line width=0.25mm, dashed,'
			str += 'dash pattern=on 1.0mm off 1.0mm] ('
			str += n2s(dims[0]) + ',' + n2s(yvec[n]) + ')--('
			str += n2s(dims[1]) + ',' + n2s(yvec[n]) + ');\n'

		str += '\\node[left, scale=' + n2s(tickscale) + '] at ('
		str += n2s(value_position[0,n]) + ',' + n2s(value_position[1,n])
		str += ') {$' + n2s(value_array[n], 1) + '$};\n'

	fd.write(str)
#============================================================================
def ellipse(fd, dims, rotang, colour, opacity):

	str = '\\draw[rotate around={' + n2s(rotang) + ':' + '(' + n2s(dims[0])
	str += ',' + n2s(dims[1]) + ')}, fill=' + colour + ', opacity='
	str += n2s(opacity) + '] (' + n2s(dims[0]) + ',' + n2s(dims[1])
	str += ') ellipse (' + n2s(dims[2]) + ' and ' + n2s(dims[3]) + ');\n'

	fd.write(str)
#=============================================================================
def rectangle_rot(fd, dims, rot_ang, rot_cnt, colour, fill_colour, opacity, origin):

	sn = np.sin(rot_ang/180.0*np.pi)
	cn = np.cos(rot_ang/180.0*np.pi)

	if strcmp(origin, 'center'):
		u1 = (dims[0] - dims[2]/2.0) - rot_cnt[0]
		v1 = (dims[1] - dims[3]/2.0) - rot_cnt[1]

		u2 = (dims[0] - dims[2]/2.0) - rot_cnt[0]
		v2 = (dims[1] + dims[3]/2.0) - rot_cnt[1]

		u3 = (dims[0] + dims[2]/2.0) - rot_cnt[0]
		v3 = (dims[1] + dims[3]/2.0) - rot_cnt[1]

		u4 = (dims[0] + dims[2]/2.0) - rot_cnt[0]
		v4 = (dims[1] - dims[3]/2.0) - rot_cnt[1]
	else:
		u1 = (dims[0] - 0.0) - rot_cnt[0]
		v1 = (dims[1] - 0.0) - rot_cnt[1]

		u2 = (dims[0] - 0.0) - rot_cnt[0]
		v2 = (dims[1] + dims[3]) - rot_cnt[1]

		u3 = (dims[0] + dims[2]) - rot_cnt[0]
		v3 = (dims[1] + dims[3]) - rot_cnt[1]

		u4 = (dims[0] + dims[2]) - rot_cnt[0]
		v4 = (dims[1] - 0.0) - rot_cnt[1]

	x1 = rot_cnt[0] + u1*cn - v1*sn
	y1 = rot_cnt[1] + u1*sn + v1*cn

	x2 = rot_cnt[0] + u2*cn - v2*sn
	y2 = rot_cnt[1] + u2*sn + v2*cn

	x3 = rot_cnt[0] + u3*cn - v3*sn
	y3 = rot_cnt[1] + u3*sn + v3*cn

	x4 = rot_cnt[0] + u4*cn - v4*sn
	y4 = rot_cnt[1] + u4*sn + v4*cn

	#===========================================================================
	str += '\\draw[' + colour + ', fill=' + fill_colour + ', opacity='
	str += n2s(opacity) + '](' + n2s(x1) + ',' + n2s(y1) + ')--('
	str += n2s(x2) + ',' + n2s(y2) + ')--(' + n2s(x3) + ',' + n2s(y3)
	str += ')--(' + n2s(x4) + ',' + n2s(y4) + ')--cycle;'

	fd.write(str)
#============================================================================
def line_vert(fd, x_pos, y_ext, colour, line_width, *varargin):

	if len(varargin) == 1:
		dims = varargin[0]
		dashed = [', dashed, dash pattern=on ' + n2s(dims(1)) + 'mm off ' + n2s(dims(2)) + 'mm']
	else:
		dashed = ''

	str  = '\\draw[' + colour + ', line width=' + n2s(line_width) + 'mm '
	str += dashed + '](' + n2s(x_pos) + ',' + n2s(-y_ext) + ') -- ('
	str += n2s(x_pos) + ', 1.0);'

	fd.write(str)
#============================================================================
def line_vert_folded(fd, x_pos, dims, colour, line_width, *varargin):

	if len(varargin) == 1:
		dims = varargin[0]
		dashed = ', dashed, dash pattern=on ' + n2s(dims[0]) + 'mm off ' + n2s(dims[1]) + 'mm'
	else:
		dashed = ''

	str  = '\\draw[' + colour + ', line width=' + n2s(line_width) + 'mm '
	str += dashed + '](' + n2s(x_pos) + ', 1.0) -- ('
	str += n2s(x_pos) + ',' + n2s(-dims[0]) + ') -- ('
	str += n2s(x_pos+dims[1]) + ',' + n2s(-dims[0]) + ');'

	fd.write(str)
#============================================================================
def line_horis(fd, y_pos, x_ext, colour, line_width, *varargin):

	if len(varargin) == 1:
		dims = varargin[0]
		dashed = ', dashed, dash pattern=on ' + n2s(dims[0]) + 'mm off ' + n2s(dims[1]) + 'mm'
	else:
		dashed = ''

	str  = '\\draw[' + colour + ', line width=' + n2s(line_width) + 'mm '
	str += dashed + '](1.0, ' + n2s(y_pos) + ') -- ('
	str += n2s(-x_ext)  + ',' + n2s(y_pos) + ');\n'

	fd.write(str)
#===============================================================================
def line_horis_folded(fd, y_pos, dims, colour, line_width, *varargin):

	if len(varargin) == 1:
		dims = varargin[0]
		dashed = ', dashed, dash pattern=on ' + n2s(dims[0]) + 'mm off ' + n2s(dims[1]) + 'mm'
	else:
		dashed = ''

	str  = '\\draw[' + colour  + ', line width=' + n2s(line_width) + 'mm '
	str += dashed + '](1.0, '  + n2s(y_pos) + ') -- ('
	str += n2s(-dims[0]) + ',' + n2s(y_pos) + ') -- ('
	str += n2s(-dims[1]) + ',' + n2s(y_pos+dims[1]) + ');\n'

	fd.write(str)
#================================================================================
def dimensions_H(fd, pos, dims, colour, line_width):

	str  = '\\draw[' + colour + ', line width=' + n2s(line_width) + 'mm,->]('
	str += n2s(pos[0]-dims[0]) + ',' + n2s(pos[1]) + ') -- ('
	str += n2s(pos[0]) + ',' + n2s(pos[1]) + ');\n'

	str += '\\draw[' + colour + ', line width=' + n2s(line_width) + 'mm,<-]('
	str += n2s(pos[0]+dims[1]) + ',' + n2s(pos[1]) + ') -- ('
	str += n2s(pos[0]+dims[1]+dims[2]) + ',' + n2s(pos[1]) + ');\n'

	fd.write(str)
#============================================================================
def dimensions_V_inv(fd, pos, dims, colour, line_width):

	str  = '\\draw[' + colour + ', line width=' + n2s(line_width) + 'mm,->]('
	str += n2s(pos[0]-dims[3]) + ',' + n2s(pos[1]+dims[0]) + ')--('
	str += n2s(pos[0]) + ',' + n2s(pos[1]+dims[0]) + ')--('
	str += n2s(pos[0]) + ',' + n2s(pos[1]) + ');\n'

	str += '\\draw[' + colour + ', line width=' + n2s(line_width) + 'mm,<-]('
	str += n2s(pos[0]) + ',' + n2s(pos[1]-dims[1]) + ')--('
	str += n2s(pos[0]) + ',' + n2s(pos[1]-dims[1]-dims[2]) + ');\n'

	fd.write(str)
#============================================================================
def dimensions_V(fd, pos, dims, colour, line_width):

	str  = '\\draw[' + colour + ', line width=' + n2s(line_width) + 'mm,->]('
	str += n2s(pos[0]) + ',' + n2s(pos[1]+dims[0]) + ')--('
	str += n2s(pos[0]) + ',' + n2s(pos[1]) + ');\n'

	str += '\\draw[' + colour + ', line width=' + n2s(line_width) + 'mm,<-]('
	str += n2s(pos[0]) + ',' + n2s(pos[1]-dims[1]) + ')--('
	str += n2s(pos[0]) + ',' + n2s(pos[1]-dims[1]-dims[2]) + ')--('
	str += n2s(pos[0]-dims[3]) + ',' + n2s(pos[1]-dims[1]-dims[2]) + ');\n'

	fd.write(str)
#=============================================================================
def text_node(fd, pos, scale, colour, textstring):

	str  = '\\node[above ,scale=' + n2s(scale) + ',' + colour + '](a) at ('
	str += n2s(pos[0]) + ',' + n2s(pos[1]) + '){$' + textstring,'$};\n'

	fd.write(str)
#=============================================================================
 # '\\Delta \\varepsilon^{\\prime}_{\\mathrm{r3}} '
def vert_lines(fd, pos, dims, pos_arrow, dims_arrow, pos_text, line_colour, line_width, text_colour, text_scale, text_string):

	line_vert(fd, pos[0], dims[0], line_colour, line_width)

	line_vert_folded(fd, pos[1], [dims[1], dims[2]], line_colour, line_width)

	line_vert(fd, pos[2], dims[0], line_colour, line_width)

	dimensions_H(fd, [pos[0], -pos_arrow], [dims_arrow[0], pos[2]-pos[0], dims_arrow[1]], line_colour, line_width)

	text_node(fd, [pos[0]-dims_arrow[0]+pos_text[0], -pos_arrow], text_scale, text_colour, text_string[0])

	text_node(fd, [pos[1]+pos_text[1], -dims[1]], text_scale, text_colour, text_string[1])

	text_node(fd, [pos[1]+pos_text[1], -dims[1]-0.05], text_scale, text_colour, '')
#==============================================================================
def vert_lines_no(fd, pos, dims, pos_arrow, dims_arrow, pos_text, line_colour, line_width, text_colour, text_scale, text_string):

	line_vert(fd, pos[0], dims[0], line_colour, line_width)

	line_vert(fd, pos[1], dims[0], line_colour, line_width)

	dimensions_H(fd, [pos[0], -pos_arrow], [dims_arrow[0], pos[1]-pos[0], dims_arrow[1]], line_colour, line_width)

	text_node(fd, [pos[0]-dims_arrow[0]+pos_text[0], -pos_arrow], text_scale, text_colour, text_string[0])
#==============================================================================
def horis_lines(fd, pos, dims, pos_arrow, dims_arrow, pos_text, line_colour, line_width, text_colour, text_scale, text_string):

	line_horis(fd, pos[0], dims[0], line_colour, line_width)

	line_horis(fd, pos[1], dims[1], line_colour, line_width)

	line_horis(fd, pos[2], dims[0], line_colour, line_width)

	dimensions_V(fd, [-pos_arrow, pos[2]], [dims_arrow[0], pos[2]-pos[0], dims_arrow[1], dims_arrow[2]], line_colour, line_width)

	text_node(fd, [pos_text[0]-dims[1], pos[1]], text_scale, text_colour, text_string[0])

	text_node(fd, [pos_text[1]-pos_arrow-dims_arrow[2], pos[0]-dims_arrow[1]], text_scale, text_colour, text_string[1])

#===============================================================================

plot2latex_MTTW2020_2()
