import numpy as np

try:
	matplot = True
	from matplotlib import pyplot as plt
except ModuleNotFoundError:
	print("The matplotlib module is not installed.")
	matplot = False

from slab_res3md import (root_finding, POW2)
from MoM import *
#=============================================================================
#=============================================================================
ep1 	= 4.2		# 1st auxiliary rod dielectric constant
ep2 	= 12.5		# MUT rod dielectric constant
ep3 	= 4.2		# 2nd auxiliary rod dielectric constant

ro1  	= 6.0 		# 1st auxiliary rod radius in mm
ro2  	= 3.0		# auxiliary slab thickness in mm
ro3  	= 6.0		# 2nd auxiliary rod radius in mm

L1   	= 16.0		# interslab separation distance in mm
L2   	= 16.0		# interslab separation distance in mm

f   	= 10.0		# operating frequency in GHz

a   	= 22.86		# waveguide width in mm

M   	= 10000		# Monte-Carlo algorithm trial number
MAXIT 	= 100		# max number of iterations for Newton's method
NTOL 	= 1e-4		# solution tolerance for Newton's method

NM	= 3		# number of higher order waveguide modes considered
N	= 5		# number of basis functions for each rod surface
#=============================================================================
MoMinit()		# initialize C++ function library

lam = 300.0/f
#=============================================================================
def evalparam(ep, ro):

	param 		= MoMparam()
	param.ep 	= ep
	param.la 	= a/lam
	param.ro 	= ro/a
	param.xo 	= 0.5
	param.zo 	= 0.0

	return param
#=============================================================================
def MoM_calc_S_matrix(ep, ro):

	param = evalparam(ep, ro)

	S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)

	if NM == 1:
		return S11[0,0], S12[0,0], S21[0,0], S22[0,0]
	else:
		return S11, S12, S21, S22
#=============================================================================
def calcSab(S11b, S12b, S21b, S22b, T):
	if NM == 1:
		S11ab = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
		S22ab = S22b + S11b*T*S22a*T/(1.0 - S11b*T*S22a*T)*S21b
		S21ab = S21a*T/(1.0 - S11b*T*S22a*T)*S21b
		S12ab = S12b*T/(1.0 - S22a*T*S11b*T)*S12a

	else:
		S11ab = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a
		S22ab = S22b + S12b*T*S22a*T*np.linalg.inv(np.eye(NM) - S11b*T*S22a*T)*S21b
		S21ab = S21a*T*np.linalg.inv(np.eye(NM) - S11b*T*S22a*T)*S21b
		S12ab = S12b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

	return S11ab, S12ab, S21ab, S22ab
#=============================================================================
def calcS11(S11ab, S12ab, S21ab, S22ab, T):
	if NM == 1:
		S11 = S11ab + S21ab*T*S11c*T/(1.0 - S22a*T*S11c*T)*S12ab
		return S11
	else:
		S11 = S11ab + S21ab*T*S11c*T*np.linalg.inv(np.eye(NM) - S22ab*T*S11c*T)*S12ab
		return S11[0,0]
#=============================================================================
def calcT(L, lam):
	if NM == 1:
		T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
	else:
		T = np.eye(NM, dtype='complex')
		for n in range(NM):
			T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)

	return T
#=============================================================================
if matplot:

	epv = np.arange(1, 20, 0.01)

	S11abs = np.ndarray((epv.shape))
	S11ang = np.ndarray((epv.shape))

	T1 = calcT(L1, lam)
	T2 = calcT(L2, lam)

	S11a, S12a, S21a, S22a = MoM_calc_S_matrix(ep1, ro1)

	for n in range(epv.shape[0]):

		param = evalparam(epv[n], ro2)
		S11b, S12b, S21b, S22b = MoM_single_solid_rod_(param, NM, N)

		param = evalparam(ep3, ro3)
		S11c, S12c, S21c, S22c = MoM_single_solid_rod_(param, NM, N)

		S11ab, S12ab, S21ab, S22ab = calcSab(S11b, S12b, S21b, S22b, T1)

		S11abs[n] = np.abs(calcS11(S11ab, S12ab, S21ab, S22ab, T2))
		S11ang[n] = np.angle(calcS11(S11ab, S12ab, S21ab, S22ab, T2))
	#---------------------------------------------------------------------
	plt.plot(epv, S11abs)
	plt.grid()
	plt.show()

	plt.plot(epv, S11ang)
	plt.grid()
	plt.show()
#=============================================================================
# calculate S11meas - the total reflection coefficient
#=============================================================================
S11a, S12a, S21a, S22a = MoM_calc_S_matrix(ep1, ro1)
T1 = calcT(L1, lam)
T2 = calcT(L2, lam)
S11b, S12b, S21b, S22b = MoM_calc_S_matrix(ep2, ro2)
S11c, S12c, S21c, S22c = MoM_calc_S_matrix(ep3, ro3)

S11ab, S12ab, S21ab, S22ab = calcSab(S11b, S12b, S21b, S22b, T1)
S11meas = np.abs(calcS11(S11ab, S12ab, S21ab, S22ab, T2))
# calculate the derivative of S11meas
S11b, S12b, S21b, S22b = MoM_calc_S_matrix(ep2+1e-2, ro2)
S11ab, S12ab, S21ab, S22ab = calcSab(S11b, S12b, S21b, S22b, T1)

S11meas_p = np.abs(calcS11(S11ab, S12ab, S21ab, S22ab, T2))
S11meas_der = (S11meas_p - S11meas)/1e-2
#=============================================================================
# EVALUATING MEASUREMENT UNCERTAINTY
#=============================================================================
sigma_S11meas 	= 0.01
sigma_ro1 	= 0.015
sigma_ro2	= 0.015
sigma_ro3	= 0.015
sigma_L1 	= 0.015
sigma_L2 	= 0.015

epm 		= np.ndarray((M,))
#=============================================================================
def func(epvar, **kwd):
	param.ep 		= epvar
	S11b, S12b, S21b, S22b 	= MoM_single_solid_rod_(param, NM, N)

	if NM == 1:
		S11b = S11b[0,0]
		S12b = S12b[0,0]
		S21b = S21b[0,0]
		S22b = S22b[0,0]

	S11ab, S12ab, S21ab, S22ab = calcSab(S11b, S12b, S21b, S22b, T1)
	return np.abs(calcS11(S11ab, S12ab, S21ab, S22ab, T2))
#=============================================================================
for n in range(M):

	del_S11meas 		= np.random.normal(0.0,	sigma_S11meas)
	del_ro1			= np.random.normal(0.0,	sigma_ro1)
	del_ro2	 		= np.random.normal(0.0,	sigma_ro2)
	del_ro3	 		= np.random.normal(0.0,	sigma_ro3)
	del_L1	 		= np.random.normal(0.0,	sigma_L1)
	del_L2	 		= np.random.normal(0.0,	sigma_L2)

	S11a, S12a, S21a, S22a  = MoM_calc_S_matrix(ep1, ro1 + del_ro1)
	S11c, S12c, S21c, S22c  = MoM_calc_S_matrix(ep3, ro3 + del_ro3)

	T1 			= calcT(L1 + del_L1, lam)
	T2 			= calcT(L2 + del_L2, lam)

	param 			= evalparam(ep2, ro2 + del_ro2)

	epm[n] 			= root_finding(func, ep2, 1e-4, NTOL, MAXIT, deriv=S11meas_der, val=S11meas, delta=del_S11meas)
#=============================================================================
id_nan 	= np.isnan(epm)
epm 	= epm[np.logical_not(id_nan)]
print(epm.shape)

print(np.mean(epm))
print(np.sqrt(np.var(epm)))
