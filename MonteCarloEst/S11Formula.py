import os
import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *
from Derivatives import *

try:
        matplot = True
        from matplotlib import pyplot as plt
except ModuleNotFoundError:
        print("The matplotlib module is not installed.")
        matplot = False


from plot2pdf import *

from Uncertainty_vs_S11 import *
#==================================================================================
xMin 		= 0.0
xMax 		= 10.0
MaxVal 		= 10.0
#==================================================================================
isPDF 		= True
#isPRINT	= False
#==================================================================================

S11a 		= 0.9
S11b 		= [0.1, 0.2, 0.3]

if type(S11a) is list:
	S11abs 	= list()
	legend 	= list()
	phi 	= list()
	for n in range(len(S11a)):
		phi.append(np.arange(-np.pi, np.pi, 0.01))
		S11abs.append(np.abs((S11a[n] - S11b*np.exp(1.0j*phi[n]))/(1.0 - S11a[n]*S11b*np.exp(2.0j*phi[n]))))
		legend.append('$|S_{11}| = ' + n2s(S11a[n], 2) + '$')
elif type(S11b) is list:
	S11abs 	= list()
	legend 	= list()
	phi 	= list()
	for n in range(len(S11b)):
		phi.append(np.arange(-np.pi, np.pi, 0.01))
		S11abs.append(np.abs((S11a - S11b[n]*np.exp(1.0j*phi[n]))/(1.0 - S11a*S11b[n]*np.exp(2.0j*phi[n]))))
		legend.append('$|S_{11}| = ' + n2s(S11b[n], 2) + '$')
else:
	phi 	= np.arange(-np.pi, np.pi, 0.01)
	S11abs	= np.abs((S11a - S11b*np.exp(1.0j*phi))/(1.0 - S11a*S11b*np.exp(2.0j*phi)))
	legend = ['']


#for n in range(S11abs.shape[0]):
#	S11[n] 	= S11fun(S11abs[n])
#==================================================================================
# PLOTTING CALCULATED DATA
#==================================================================================
xlabel		= '$\\varphi$'
ylabel		= '$|S_{11}|$'

if isPDF:

	labels = {'xlabel': xlabel, 'ylabel': ylabel}

	filename = 'Uncert_S11abs'

	LegendPos = [0.1, 0.1]

#	plot2latex(S11abs, Uncert_S11, xDecNum=1, LineWidth=0.75, labels=labels, LegendPosition=legendpos, FileName=filename)
	plot2latex(phi, S11abs, xDecNum=1, LineWidth=0.75, labels=labels, legend=legend, LegendPosition=LegendPos, FileName=filename, YZero=True)

elif matplot:

	plt.xlabel(xlabel)
	plt.ylabel(ylabel)

	plt.plot(phi, S11abs)

	plt.grid()
	plt.show()

else:
	pass
#       print('Calculated data have been saved into a file.')

print('--------------------------------------------------------')
