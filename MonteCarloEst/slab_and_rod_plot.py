import numpy as np
from matplotlib import pyplot as plt
from slab_res3md import (single_slab_guide_matrix, POW2)
from MoM import *
from plot2pdf import *

#===========================================================================
ep1     = [4.3]			# auxiliary slab dielectric constant
ep2     = [1.0, 40.0, 0.01]		# MUT rod dielectric constant (24 at 10 GHz exxelia.com)
#ep2     = [28.0]			# MUT rod dielectric constant (24 at 10 GHz exxelia.com)

d1	= [9.25]		# auxiliary slab thickness in mm
ro2     = [3.3]			# MUT rod radius in mm

L       = [22.0]		# slab-to-rod separation distance in mm
f       = [10.0]		# operating frequency in GHz

a       = 22.86			# waveguide width in mm
#===========================================================================
NM      = 3			# number of higher order waveguide modes considered
N       = 5			# number of basis functions for each rod surface
#===========================================================================
isPDF	= False			# generate a PDF file with curve plots
#===========================================================================
MoMinit()			# perform library initialization
#===========================================================================
def evalparam(roddata, lam):

	param 		= MoMparam()

	param.ep 	= roddata['ep'] + 0.0j
	param.la 	= a/lam
	param.ro 	= roddata['ro']/a
	param.xo 	= 0.5
	param.zo 	= 0.0

	return param
#===========================================================================
def calcT(L, lam):
	if NM == 1:
		T = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-0.25)*L/a)
	else:
		T = np.eye(NM, dtype='complex')
		for n in range(NM):
			T[n,n] = np.exp(2.0j*np.pi*np.sqrt(POW2(a/lam)-POW2(n+1)*0.25 + 0.0j)*L/a)

	return T
#===========================================================================
def Calc_slab_S_matrix(slabdata, lam):
	S11, S12, S21, S22 = single_slab_guide_matrix(a/lam, slabdata['ep'], slabdata['d']/a, NM)
	return S11, S12, S21, S22
#===========================================================================
def Calc_rod_S_matrix(roddata, lam):
	param = evalparam(roddata, lam)
	S11, S12, S21, S22 = MoM_single_solid_rod_(param, NM, N)
	return S11, S12, S21, S22
#===========================================================================
def calcS11(slabdata1, roddata2, L, lam):

	S11a, S12a, S21a, S22a = Calc_slab_S_matrix(slabdata1, lam)
	S11b, S12b, S21b, S22b = Calc_rod_S_matrix(roddata2, lam)
	T = calcT(L, lam)

	if NM == 1:
		S11 = S11a + S21a*T*S11b*T/(1.0 - S22a*T*S11b*T)*S12a
	else:
		S11 = S11a + S21a*T*S11b*T*np.linalg.inv(np.eye(NM) - S22a*T*S11b*T)*S12a

	return S11[0,0]
#===========================================================================
xlabels  = dict()
xlabels1 = dict()
xlabels2 = dict()

xlabels1['ep'] 	= 'slab dielectric constant, mm'
xlabels2['ep'] 	= 'rod dielectric constant, mm'

xlabels1['d'] 	= 'slab thickness, mm'
xlabels2['ro'] 	= 'rod radius, mm'

xlabels['L'] 	= 'Rod separation distance , mm'
xlabels['f'] 	= 'Frequency, GHz'
#============================================================================
slabdata1 = {'ep': ep1,  'd': d1}
roddata2  = {'ep': ep2, 'ro': ro2}
#============================================================================

list_found 	= False

isSlab		= False
isRod		= False

for par in slabdata1.keys():
	if type(slabdata1[par]) == list:
		if len(slabdata1[par]) > 1 and not list_found:
			var_range	= slabdata1[par];
			parname 	= par
			isSlab		= True
			list_found 	= True
		else:
			slabdata1[par] = slabdata1[par][0];
for par in roddata2.keys():
	if type(roddata2[par]) == list:
		if len(roddata2[par]) > 1 and not list_found:
			var_range	= roddata2[par];
			parname 	= par
			isRod 	 	= True
			list_found 	= True
		else:
			roddata2[par] = roddata2[par][0];

if type(L) == list:
	if len(L) > 1 and not list_found:
		var_range 	=  L
		parname 	= 'L'
		list_found 	= True
	else:
		L = L[0]

if type(f) == list:
	if len(f) > 1 and not list_found:
		var_range 	=  f
		parname 	= 'f'
		list_found 	= True
	else:
		f = f[0]

if not list_found:
	print("There is nothing to plot.")
	quit()
#=====================================================================
var_values 	= np.arange(*var_range)
S11 		= np.ndarray((var_values.shape), dtype='complex')

if isSlab:
	lam = 300.0/f
	for n in range(var_values.shape[0]):
		slabdata1[parname] = var_values[n]
		S11[n] = calcS11(slabdata1, roddata2, L, lam)
elif isRod:
	lam = 300.0/f
	for n in range(var_values.shape[0]):
		roddata2[parname] = var_values[n]
		S11[n] = calcS11(slabdata1, roddata2, L, lam)
else:
	if parname == 'L':
		lam = 300.0/f
		for n in range(var_values.shape[0]):
			S11[n] = calcS11(slabdata1, roddata2, var_values[n], lam)
	else:
		for n in range(var_values.shape[0]):
			lam = 300.0/var_values[n]
			S11[n] = calcS11(slabdata1, roddata2, L, lam)
#================================================================================
if isPDF:
	if isSlab:
		labels ={'xlabel': xlabels1[parname], 'ylabel': '$|S_{11}|$'}
	elif isRod:
		labels ={'xlabel': xlabels2[parname], 'ylabel': '$|S_{11}|$'}
	else:
		labels ={'xlabel': xlabels[parname],  'ylabel': '$|S_{11}|$'}

	plot2latex(var_values, np.abs(S11), labels=labels, legend=['slab-rod model'])

else:
	plt.plot(var_values, np.abs(S11))

	if isSlab:
		plt.xlabel(xlabels1[parname])
	elif isRod:
		plt.xlabel(xlabels2[parname])
	else:
		plt.xlabel(xlabels[parname])

	plt.ylabel('|S11|')
	plt.grid()
	plt.show()

