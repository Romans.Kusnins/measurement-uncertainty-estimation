//#include<complex>
#include<complex.h>
#include<math.h>
#include<stdio.h>


#define PI 3.1415926535897931

#define POW2(x)   ((x)*(x))

#define BJo1(m)   BJo1[m+N+1]
#define BJ1e1(m)  BJ1e1[m+N+1]
#define BJ2e1(m)  BJ2e1[m+N+1]
#define BJ3e1(m)  BJ3e1[m+N+1]

#define BHo1(m)   BHo1[m+N+1]
#define BH1e1(m)  BH1e1[m+N+1]
#define BH2e1(m)  BH2e1[m+N+1]
#define BH3e1(m)  BH3e1[m+N+1]

#define BJDo1(m)   BJDo1[m+N]
#define BJD1e1(m)  BJD1e1[m+N]
#define BJD2e1(m)  BJD2e1[m+N]
#define BJD3e1(m)  BJD3e1[m+N]

#define BHDo1(m)   BHDo1[m+N]
#define BHD1e1(m)  BHD1e1[m+N]
#define BHD2e1(m)  BHD2e1[m+N]
#define BHD3e1(m)  BHD3e1[m+N]

#define BJ12(m)    BJ12[m+N+1]
#define BJ13(m)    BJ13[m+N+1]
#define BJ23(m)    BJ23[m+N+1]
#define BH23(m)    BH23[m+N+1]

#define BJ1p2(m)   BJ1p2[m+N+1]
#define BJ1p3(m)   BJ1p3[m+N+1]

#define BJD1p2(m)   BJD1p2[m+N]
#define BJD1p3(m)   BJD1p3[m+N]

#define BJ12u(m)    BJ12u[m+N]
#define BJ13u(m)    BJ13u[m+N]
#define BH23u(m)    BH23u[m+N]

#define BJ12l(m)    BJ12l[m+N]
#define BJ13l(m)    BJ13l[m+N]
#define BH23l(m)    BH23l[m+N]


#define EH2(m)  EH2[m+N]
#define EH3(m)  EH3[m+N]


#define E1(m,n,l)   EX[m+N + 0*WN + (n-1)*4*WN + (l-1)*4*WN*NM]
#define H1(m,n,l)   EX[m+N + 1*WN + (n-1)*4*WN + (l-1)*4*WN*NM]
#define E12(m,n,l)  EX[m+N + 2*WN + (n-1)*4*WN + (l-1)*4*WN*NM]
#define E13(m,n,l)  EX[m+N + 3*WN + (n-1)*4*WN + (l-1)*4*WN*NM]

#define kw(m)   kw[m-1]
#define phk(m)  phk[m-1]

#define  XO1(m)   XO1[m-1]
#define XOI1(m)  XOI1[m-1]

#define  XO2(m)   XO2[m-1]
#define XOI2(m)  XOI2[m-1]

#define  ZO1(m)   ZO1[m-1]
#define ZOI1(m)  ZOI1[m-1]

#define  ZO2(m)   ZO2[m-1]
#define ZOI2(m)  ZOI2[m-1]

#define  PH(m,n)   PH[m+N + (n-1)*WN]
#define PHI(m,n)  PHI[m+N + (n-1)*WN]

#define S11(m,n)  S11[m-1 + (n-1)*NM]
#define S21(m,n)  S21[m-1 + (n-1)*NM]
#define S12(m,n)  S12[m-1 + (n-1)*NM]
#define S22(m,n)  S22[m-1 + (n-1)*NM]

#define res11_p(m)  res11_p[2*N+m]
#define res11_n(m)  res11_n[2*N+m]

#define SM11(m,n)  SM[m+N + 0*WN + (n+N)*4*WN + 0*4*WN*WN]
#define SM21(m,n)  SM[m+N + 1*WN + (n+N)*4*WN + 0*4*WN*WN]
#define SM31(m,n)  SM[m+N + 2*WN + (n+N)*4*WN + 0*4*WN*WN]
#define SM41(m,n)  SM[m+N + 3*WN + (n+N)*4*WN + 0*4*WN*WN]

#define SM12(m,n)  SM[m+N + 0*WN + (n+N)*4*WN + 1*4*WN*WN]
#define SM22(m,n)  SM[m+N + 1*WN + (n+N)*4*WN + 1*4*WN*WN]
#define SM32(m,n)  SM[m+N + 2*WN + (n+N)*4*WN + 1*4*WN*WN]
#define SM42(m,n)  SM[m+N + 3*WN + (n+N)*4*WN + 1*4*WN*WN]

#define SM13(m,n)  SM[m+N + 0*WN + (n+N)*4*WN + 2*4*WN*WN]
#define SM23(m,n)  SM[m+N + 1*WN + (n+N)*4*WN + 2*4*WN*WN]
#define SM33(m,n)  SM[m+N + 2*WN + (n+N)*4*WN + 2*4*WN*WN]
#define SM43(m,n)  SM[m+N + 3*WN + (n+N)*4*WN + 2*4*WN*WN]

#define SM14(m,n)  SM[m+N + 0*WN + (n+N)*4*WN + 3*4*WN*WN]
#define SM24(m,n)  SM[m+N + 1*WN + (n+N)*4*WN + 3*4*WN*WN]
#define SM34(m,n)  SM[m+N + 2*WN + (n+N)*4*WN + 3*4*WN*WN]
#define SM44(m,n)  SM[m+N + 3*WN + (n+N)*4*WN + 3*4*WN*WN]
//------------------------------------------------------------

#define cdouble _Complex double

//typedef std::complex<double> cdouble;
//cdouble I = std::complex<double>(0.0,1.0);

extern "C" void MoM_two_in_one(cdouble ep1, cdouble ep2, cdouble ep3, double la, double ro1, double ro2, double ro3, double xo1, double xo2, double xo3, double zo1, double zo2, double zo3, cdouble * S11, cdouble * S12, cdouble * S21, cdouble * S22, const int N, const int NM);


void besseljCPX(int, cdouble, cdouble *, int);
void besselyCPX(int, cdouble, cdouble *, int);


double expint(double x, int n, char mode);

void  exp_exp_int_ss(double * la, double x, double len, const int M, const int LAS, cdouble * res);
void  exp_exp_int(double * la, double x, double y, double len, const int M, const int LAS, cdouble * res);

extern "C" void zgesv_(int*,int*,cdouble*,int*,int*,cdouble*,int*,int*);


void MoM_two_in_one(cdouble ep1, cdouble ep2, cdouble ep3, double la, double ro1, double ro2, double ro3, double xo1, double xo2, double xo3, double zo1, double zo2, double zo3, cdouble * S11, cdouble * S12, cdouble * S21, cdouble * S22, const int N, const int NM){ //default value of N is 11

	const int L = 2*N+1;
	int nrhs = NM*2, info, pivot[4*(4*N+2)];
	const int WN = 2*N+1;
	int M = 4*WN;

	double Z = 120.0*PI;
	double k;

	cdouble  kw[NM], phk[NM];

	cdouble  ZO1[NM], ZOI1[NM];
	cdouble  XO1[NM], XOI1[NM];

	cdouble  ZO2[NM], ZOI2[NM];
	cdouble  XO2[NM], XOI2[NM];

 	cdouble  BJo1[WN+2], BJ1e1[WN+2], BJ2e1[WN+2], BJ3e1[WN+2];
 	cdouble  BHo1[WN+2], BH1e1[WN+2], BH2e1[WN+2], BH3e1[WN+2];

 	cdouble  BJDo1[WN], BJD1e1[WN], BJD2e1[WN], BJD3e1[WN];
 	cdouble  BHDo1[WN], BHD1e1[WN], BHD2e1[WN], BHD3e1[WN];

	cdouble  BJ12[WN+2], BJ13[WN+2], BJ23[WN+2], BH23[WN+2];

	cdouble  BJ1p2[WN+2], BJ1p3[WN+2];
	cdouble  BJD1p2[WN],  BJD1p3[WN];

       //-------------------------------------------------------
	cdouble BJ12u[WN], BJ13u[WN], BH23u[WN];
	cdouble BJ12l[WN], BJ13l[WN], BH23l[WN];
       //-------------------------------------------------------

	cdouble  SM[4*4*WN*WN];

	cdouble  res11_p[4*N+1], res11_n[4*N+1];

	cdouble   PH[WN*NM];
	cdouble  PHI[WN*NM];

	cdouble  EX[2*4*WN*NM];

	cdouble  EH2[WN];
	cdouble  EH3[WN];

	cdouble  E11o, H11o;

	cdouble CFP, CFN;

	double xo1u, xo2u, xo3u;
	double xo1l, xo2l, xo3l;

	double zo1u, zo2u, zo3u;
	double zo1l, zo2l, zo3l;

	double ro12, ph12u, ph12l;
	double ro13, ph13u, ph13l;
	double ro23, ph23u, ph23l;

	cdouble EH21u, EH12u, EH31u, EH13u, EH32u, EH23u;

	cdouble ED21u, ED12u, ED31u, ED13u, ED32u, ED23u;

	cdouble HD21u, HD12u, HD31u, HD13u, HD32u, HD23u;

	cdouble HD11, ED11;
	cdouble HD22, ED22;
	cdouble HD33, ED33;

	cdouble HDo, EDo;

  	//----------------------------------------------------------------------------
	// xo1 += 0.5;	// change the coordinae system
	// xo2 += 0.5;
	//----------------------------------------------------------------------------

	ep1 = csqrt(ep1);
	ep2 = csqrt(ep2);
	ep3 = csqrt(ep3);

	xo1u = 1.0 - xo1;
	xo2u = 1.0 - xo3;
	xo3u = 1.0 - xo2;

	xo1l = xo1;
	xo2l = xo2;
	xo3l = xo3;

	zo1u = zo1;
	zo2u = zo3;
	zo3u = zo2;

	zo1l = zo1;
	zo2l = zo2;
	zo3l = zo3;

	for(int m=1; m<=NM; ++m){
		XO1(m)  = cexp(I*static_cast<double>(m)*PI*xo1u);
		XOI1(m) = 1.0/XO1(m);
	}

	exp_exp_int_ss(&la,      0.0, 2.0, 2*N, 1, res11_n);
	exp_exp_int_ss(&la, 2.0*xo1u, 2.0, 2*N, 1, res11_p);

	//============================================================

	k = 2.0*PI*la;

	besseljCPX(0, 	  k*ro1, &BJo1[N+1],  N+2);
	besseljCPX(0, ep1*k*ro1, &BJ1e1[N+1], N+2);
	besseljCPX(0, ep1*k*ro2, &BJ2e1[N+1], N+2);
	besseljCPX(0, ep1*k*ro3, &BJ3e1[N+1], N+2);

	besselyCPX(0, 	  k*ro1, &BHo1[N+1],  N+2);
	besselyCPX(0, ep1*k*ro1, &BH1e1[N+1], N+2);
	besselyCPX(0, ep1*k*ro2, &BH2e1[N+1], N+2);
	besselyCPX(0, ep1*k*ro3, &BH3e1[N+1], N+2);

	besseljCPX(0, ep1*k*ro12, &BJ12[N+1],  N+2);
	besseljCPX(0, ep1*k*ro13, &BJ13[N+1],  N+2);
	besseljCPX(0, ep1*k*ro23, &BJ23[N+1],  N+2);
	besselyCPX(0, ep1*k*ro23, &BH23[N+1],  N+2);

	besseljCPX(0, ep2*k*ro2, &BJ1p2[N+1], N+2);
	besseljCPX(0, ep3*k*ro3, &BJ1p3[N+1], N+2);

	//----------------------------------------------
	// obtain Hankel functions of the 1st order
	for(int n=0;n<=N+1;++n){

		BHo1(n)  = BJo1(n)  + I*BHo1(n);
		BH1e1(n) = BJ1e1(n) + I*BH1e1(n);
		BH2e1(n) = BJ2e1(n) + I*BH2e1(n);
		BH3e1(n) = BJ3e1(n) + I*BH3e1(n);

		BH23(n) = BJ23(n) + I*BH23(n);
	}
	// calculate cylindrical functions of negative order
	for(int n=1;n<=N+1;++n){

		BJo1(-n)  = pow(-1.0,n)*BJo1(n);
		BJ1e1(-n) = pow(-1.0,n)*BJ1e1(n);
		BJ2e1(-n) = pow(-1.0,n)*BJ2e1(n);
		BJ3e1(-n) = pow(-1.0,n)*BJ3e1(n);

		BHo1(-n)  = pow(-1.0,n)*BHo1(n);
		BH1e1(-n) = pow(-1.0,n)*BH1e1(n);
		BH2e1(-n) = pow(-1.0,n)*BH2e1(n);
		BH3e1(-n) = pow(-1.0,n)*BH3e1(n);

		BJ12(-n)  = pow(-1.0,n)*BJ12(n);
		BJ13(-n)  = pow(-1.0,n)*BJ13(n);
		BJ23(-n)  = pow(-1.0,n)*BJ23(n);
		BH23(-n)  = pow(-1.0,n)*BH23(n);

		BJ1p2(-n)  = pow(-1.0,n)*BJ1p2(n);
		BJ1p3(-n)  = pow(-1.0,n)*BJ1p3(n);

	}
	// calculate derivatives of the cylindrical functions
	for(int n=-N;n<=N;++n){

		BJDo1(n)  = 0.5*(BJo1(n-1)  - BJo1(n+1));
		BJD1e1(n) = 0.5*(BJ1e1(n-1) - BJ1e1(n+1));
		BJD2e1(n) = 0.5*(BJ2e1(n-1) - BJ2e1(n+1));
		BJD3e1(n) = 0.5*(BJ3e1(n-1) - BJ3e1(n+1));

		BHDo1(n)  = 0.5*(BHo1(n-1)  - BHo1(n+1));
		BHD1e1(n) = 0.5*(BH1e1(n-1) - BH1e1(n+1));
		BHD2e1(n) = 0.5*(BH2e1(n-1) - BH2e1(n+1));
		BHD3e1(n) = 0.5*(BH3e1(n-1) - BH3e1(n+1));

		BJD1p2(n)  = 0.5*(BJ1p2(n-1) - BJ1p2(n+1));
		BJD1p3(n)  = 0.5*(BJ1p3(n-1) - BJ1p3(n+1));
	}
	//---------------------------------------------------------

	ro12    = sqrt(POW2(xo1-xo2) + POW2(zo1-zo2));
	ph12u   = atan2(xo1u-xo2u, zo1u-zo2u);

	ro13    = sqrt(POW2(xo1-xo3) + POW2(zo1-zo3));
	ph13u   = atan2(xo1u-xo3u, zo1u-zo3u);

	ro23    = sqrt(POW2(xo2-xo3) + POW2(zo2-zo3));
	ph23u   = atan2(xo2u-xo3u, zo2u-zo3u);

	for(int n=-N; n<=N; ++n){
		BJ12u(n) = BJ12(n)*cexp(-I*static_cast<double>(n)*ph12u);
		BJ13u(n) = BJ13(n)*cexp(-I*static_cast<double>(n)*ph13u);
		BH23u(n) = BH23(n)*cexp(-I*static_cast<double>(n)*ph23u);
	}
	//---------------------------------------------------------

	for(int m=1; m<=NM; ++m){
		if(m==1){
			kw(1)  = sqrt(k*k - PI*PI);
			phk(1) = atan2(PI, creal(kw(1)));
		}else{
			kw(m) = I*sqrt(PI*PI*m*m - k*k);
			double kxz = cabs(static_cast<double>(m)*PI/kw(m));
			phk(m) = 0.5*I*log(fabs((1.0-kxz)/(1.0+kxz)));
			if(kxz > 1.0){
				phk(m) += PI/2.0;
			}
		}

		ZO1(m)  = cexp(I*kw(m)*zo1u);
		ZOI1(m) = 1.0/ZO1(m);

	}

	for(int n=-N; n<=N; ++n){
		for(int m=1; m<=NM; ++m){
			PH(n,m)  = cexp(I*static_cast<double>(n)*phk(m));
			PHI(n,m) = 1.0/PH(n,m);

			E1(n,m,1) = -I*(cpow( I,n)*0.5*BJo1(n)*ZO1(m)*(XO1(m)*PHI(n,m) - XOI1(m)*PH(n,m)));
			E1(n,m,2) = -I*(cpow(-I,n)*0.5*BJo1(n)*ZOI1(m)*(XO1(m)*PH(n,m) - XOI1(m)*PHI(n,m)));

			H1(n,m,1) = 0.0;
			H1(n,m,2) = 0.0;

			E12(n,m,1) = 0.0;
			E12(n,m,2) = 0.0;
			E13(n,m,1) = 0.0;
			E13(n,m,2) = 0.0;

		}
		EH2(n) = BJD1p2(n)/BJ1p2(n);
		EH3(n) = BJD1p3(n)/BJ1p3(n);
	}

	for(int m=-N;m<=N;++m){
		for(int n=-N;n<=N;++n){
			if(n%2==0){
				H11o = BJo1(m)*(res11_n(m-n) - res11_p(n+m))*BJo1(n);
				E11o = BJo1(m)*(res11_n(m-n) - res11_p(n+m))*BJDo1(n);

			}else{
				H11o = BJo1(m)*(res11_n(m-n) + res11_p(n+m))*BJo1(n);
				E11o = BJo1(m)*(res11_n(m-n) + res11_p(n+m))*BJDo1(n);
			}
			//---------------------------------------------------------------------------------
			if(m==n){

				HDo  = BHo1(n)*BJo1(n);
				EDo  = BHo1(n)*BJDo1(n);

				HD11 = BH1e1(n)*BJ1e1(n);
				ED11 = BH1e1(n)*BJD1e1(n);

				HD22 = BJ2e1(n)*BH2e1(n);
				ED22 = BJ2e1(n)*BHD2e1(n);

				HD33 = BJ3e1(n)*BH3e1(n);
				ED33 = BJ3e1(n)*BHD3e1(n);

			}
			//---------------------------------------------------------------------------------

			EH21u = pow(-1.0, m-n)*BJ12u(m-n);
			EH12u = BJ12u(m-n);

			EH31u = pow(-1.0, m-n)*BJ13u(m-n);
			EH13u = BJ13u(m-n);

			EH32u = pow(-1.0, m-n)*BH23u(m-n);
			EH23u = BH23u(m-n);

			//---------------------------------------------------------------------------------

			HD21u = BH1e1(m)*EH21u*BJ2e1(n);
			ED21u = BH1e1(m)*EH21u*BJD2e1(n);

			HD12u = BJ2e1(m)*EH12u*BH1e1(n);
			ED12u = BJ2e1(m)*EH12u*BHD1e1(n);

			HD31u = BH1e1(m)*EH31u*BJ3e1(n);
			ED31u = BH1e1(m)*EH31u*BJD3e1(n);

			HD13u = BJ3e1(m)*EH13u*BH1e1(n);
			ED13u = BJ3e1(m)*EH13u*BHD1e1(n);

			HD32u = BJ2e1(m)*EH32u*BJ3e1(n);
			ED32u = BJ2e1(m)*EH32u*BJD3e1(n);

			HD23u = BJ3e1(m)*EH23u*BJ2e1(n);
			ED23u = BJ3e1(m)*EH23u*BJD2e1(n);

			//-------------------------------------------------------------------------
			SM11(m,n) = 0.0; SM12(m,n) = 0.0; SM13(m,n) = 0.0; SM14(m,n) = 0.0;
			SM21(m,n) = 0.0; SM22(m,n) = 0.0; SM23(m,n) = 0.0; SM24(m,n) = 0.0;
			SM31(m,n) = 0.0; SM32(m,n) = 0.0; SM33(m,n) = 0.0; SM34(m,n) = 0.0;
			SM41(m,n) = 0.0; SM42(m,n) = 0.0; SM43(m,n) = 0.0; SM44(m,n) = 0.0;
			//--------------------------------------------------------------------------
			if(m==n){
				SM11(m,n) += 1.0;
			}
   		      //----------------------------------------------------------
			if(m==n){
				SM11(m,n) +=   PI/2.0/I*ro1*k*(E11o + EDo);
				SM12(m,n) +=  -PI/2.0/I*ro1*I*k*Z*(H11o + HDo);
			}else{
				SM11(m,n) +=   PI/2.0/I*ro1*k*E11o;
				SM12(m,n) +=  -PI/2.0/I*ro1*I*k*Z*H11o;
			}

			if(m==n){
				SM21(m,n) +=   ro1*ep1*ED11;
				SM22(m,n) +=  -ro1*I*Z*HD11;
			}
			SM23(m,n) +=   ro2*(ep1*ED21u - ep2*HD21u*EH2(n));
			SM24(m,n) +=  -ro3*(ep1*ED31u - ep3*HD31u*EH3(n));

			SM31(m,n) +=   ro1*ep1*ED12u;
			SM32(m,n) +=  -ro1*I*Z*HD12u;
			if(m==n){
				SM33(m,n) +=   ro2*(ep1*ED22  - ep2*HD22*EH2(n));
			}
			SM34(m,n) +=  -ro3*(ep1*ED32u - ep3*HD32u*EH3(n));

			SM41(m,n) +=   ro1*ep1*ED13u;
			SM42(m,n) +=  -ro1*I*Z*HD13u;
			SM43(m,n) +=   ro2*(ep1*ED23u - ep2*HD23u*EH2(n));
			if(m==n){
				SM44(m,n) +=  -ro3*(ep1*ED33  - ep3*HD33*EH3(n));
			}
   		      //-----------------------------------------------------------

		}
	}

//	for(int n=0; n<8*2*WN*NM; ++n){
//		printf("n=%d, m=%d: %5.6f, %5.6f\n", n, m, creal(E1(n,m,1)), cimag(E1(n,m,1)));
//		printf("n=%d: %5.6f, %5.6f\n", n, creal(EX[n]), cimag(EX[n]));
//	}

	zgesv_(&M, &nrhs, SM, &M, pivot, EX, &M, &info);

	for(int m=1; m<=NM; ++m){
		for(int n=1; n<=NM; ++n){
			if(m==n){
				S11(m,n) = 0.0;
				S21(m,n) = 1.0;
				S12(m,n) = 1.0;
				S22(m,n) = 0.0;
			}else{
				S11(m,n) = 0.0;
				S21(m,n) = 0.0;
				S12(m,n) = 0.0;
				S22(m,n) = 0.0;
			}
			for(int p=-N; p<=N; ++p){

				// Expressions for 1st rod
				CFP = PI*k*ro1*cpow(I,p)*(I*Z*BJo1(p)*H1(p,n,1) - BJDo1(p)*E1(p,n,1))/kw(m);
				CFN = PI*k*ro1*cpow(I,p)*(I*Z*BJo1(p)*H1(p,n,2) - BJDo1(p)*E1(p,n,2))/kw(m);

				//==============================================================

				S11(m,n) -= (ZO1(m)*(XO1(m)*PH(p,m) - XOI1(m)*PHI(p,m)))*CFP;

				S12(m,n) -= (ZO1(m)*(XO1(m)*PH(p,m) - XOI1(m)*PHI(p,m)))*CFN;

				if(p%2==0){
					S21(m,n) -= (ZOI1(m)*(XO1(m)*PHI(p,m) - XOI1(m)*PH(p,m)))*CFP;
					S22(m,n) -= (ZOI1(m)*(XO1(m)*PHI(p,m) - XOI1(m)*PH(p,m)))*CFN;
				}else{
					S21(m,n) += (ZOI1(m)*(XO1(m)*PHI(p,m) - XOI1(m)*PH(p,m)))*CFP;
					S22(m,n) += (ZOI1(m)*(XO1(m)*PHI(p,m) - XOI1(m)*PH(p,m)))*CFN;
				}

			}
		}
	}


}
