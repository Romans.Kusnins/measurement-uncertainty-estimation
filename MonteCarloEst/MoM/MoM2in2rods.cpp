//#include<complex>
#include<complex.h>
#include<math.h>
#include<stdio.h>


#define PI 3.1415926535897931

#define POW2(x)   ((x)*(x))

#define BJo1(m)   BJo1[m+N+1]
#define BJ1e1(m)  BJ1e1[m+N+1]
#define BJ2e1(m)  BJ2e1[m+N+1]
#define BJ3e1(m)  BJ3e1[m+N+1]

#define BJo2(m)   BJo2[m+N+1]
#define BJ1e2(m)  BJ1e2[m+N+1]
#define BJ2e2(m)  BJ2e2[m+N+1]
#define BJ3e2(m)  BJ3e2[m+N+1]

#define BHo1(m)   BHo1[m+N+1]
#define BH1e1(m)  BH1e1[m+N+1]
#define BH2e1(m)  BH2e1[m+N+1]
#define BH3e1(m)  BH3e1[m+N+1]

#define BHo2(m)   BHo2[m+N+1]
#define BH1e2(m)  BH1e2[m+N+1]
#define BH2e2(m)  BH2e2[m+N+1]
#define BH3e2(m)  BH3e2[m+N+1]

//-----------------------------------------

#define BJDo1(m)   BJDo1[m+N]
#define BJD1e1(m)  BJD1e1[m+N]
#define BJD2e1(m)  BJD2e1[m+N]
#define BJD3e1(m)  BJD3e1[m+N]

#define BHDo1(m)   BHDo1[m+N]
#define BHD1e1(m)  BHD1e1[m+N]
#define BHD2e1(m)  BHD2e1[m+N]
#define BHD3e1(m)  BHD3e1[m+N]

#define BJDo2(m)   BJDo2[m+N]
#define BJD1e2(m)  BJD1e2[m+N]
#define BJD2e2(m)  BJD2e2[m+N]
#define BJD3e2(m)  BJD3e2[m+N]

#define BHDo2(m)   BHDo2[m+N]
#define BHD1e2(m)  BHD1e2[m+N]
#define BHD2e2(m)  BHD2e2[m+N]
#define BHD3e2(m)  BHD3e2[m+N]

//-----------------------------------------

#define BJ1p2(m)   BJ1p2[m+N+1]
#define BJ1p3(m)   BJ1p3[m+N+1]

#define BJD1p2(m)  BJD1p2[m+N]
#define BJD1p3(m)  BJD1p3[m+N]

#define BJ2p2(m)   BJ2p2[m+N+1]
#define BJ2p3(m)   BJ2p3[m+N+1]

#define BJD2p2(m)  BJD2p2[m+N]
#define BJD2p3(m)  BJD2p3[m+N]

//-----------------------------------------

#define BJ12u(m)    BJ12u[m+2*N]
#define BJ13u(m)    BJ13u[m+2*N]
#define BJ23u(m)    BJ23u[m+2*N]
#define BH23u(m)    BH23u[m+2*N]

#define BJ12l(m)    BJ12l[m+2*N]
#define BJ13l(m)    BJ13l[m+2*N]
#define BJ23l(m)    BJ23l[m+2*N]
#define BH23l(m)    BH23l[m+2*N]

//-----------------------------------------

#define EH2u(m)  EH2u[m+N]
#define EH3u(m)  EH3u[m+N]

#define EH2l(m)  EH2l[m+N]
#define EH3l(m)  EH3l[m+N]


#define E1(m,n,l)   EX[m+N + 0*WN + (n-1)*8*WN + (l-1)*8*WN*NM]
#define H1(m,n,l)   EX[m+N + 1*WN + (n-1)*8*WN + (l-1)*8*WN*NM]
#define E12(m,n,l)  EX[m+N + 2*WN + (n-1)*8*WN + (l-1)*8*WN*NM]
#define E13(m,n,l)  EX[m+N + 3*WN + (n-1)*8*WN + (l-1)*8*WN*NM]

#define E2(m,n,l)   EX[m+N + 4*WN + (n-1)*8*WN + (l-1)*8*WN*NM]
#define H2(m,n,l)   EX[m+N + 5*WN + (n-1)*8*WN + (l-1)*8*WN*NM]
#define E22(m,n,l)  EX[m+N + 6*WN + (n-1)*8*WN + (l-1)*8*WN*NM]
#define E23(m,n,l)  EX[m+N + 7*WN + (n-1)*8*WN + (l-1)*8*WN*NM]


#define kw(m)   kw[m-1]
#define phk(m)  phk[m-1]

#define  XO1(m)   XO1[m-1]
#define XOI1(m)  XOI1[m-1]

#define  XO2(m)   XO2[m-1]
#define XOI2(m)  XOI2[m-1]

#define  ZO1(m)   ZO1[m-1]
#define ZOI1(m)  ZOI1[m-1]

#define  ZO2(m)   ZO2[m-1]
#define ZOI2(m)  ZOI2[m-1]

#define  PH(m,n)   PH[m+N + (n-1)*WN]
#define PHI(m,n)  PHI[m+N + (n-1)*WN]

#define S11(m,n)  S11[m-1 + (n-1)*NM]
#define S21(m,n)  S21[m-1 + (n-1)*NM]
#define S12(m,n)  S12[m-1 + (n-1)*NM]
#define S22(m,n)  S22[m-1 + (n-1)*NM]

#define res11_p(m)  res11_p[2*N+m]
#define res11_n(m)  res11_n[2*N+m]

#define res12_p(m)  res12_p[2*N+m]
#define res12_n(m)  res12_n[2*N+m]

#define res21_p(m)  res21_p[2*N+m]
#define res21_n(m)  res21_n[2*N+m]

#define res22_p(m)  res22_p[2*N+m]
#define res22_n(m)  res22_n[2*N+m]

#define SM11_11(m,n)  SM[m+N + 0*WN + (n+N)*8*WN + 0*8*WN*WN]
#define SM11_21(m,n)  SM[m+N + 1*WN + (n+N)*8*WN + 0*8*WN*WN]
#define SM11_31(m,n)  SM[m+N + 2*WN + (n+N)*8*WN + 0*8*WN*WN]
#define SM11_41(m,n)  SM[m+N + 3*WN + (n+N)*8*WN + 0*8*WN*WN]
#define SM21_11(m,n)  SM[m+N + 4*WN + (n+N)*8*WN + 0*8*WN*WN]
#define SM21_21(m,n)  SM[m+N + 5*WN + (n+N)*8*WN + 0*8*WN*WN]
#define SM21_31(m,n)  SM[m+N + 6*WN + (n+N)*8*WN + 0*8*WN*WN]
#define SM21_41(m,n)  SM[m+N + 7*WN + (n+N)*8*WN + 0*8*WN*WN]

#define SM11_12(m,n)  SM[m+N + 0*WN + (n+N)*8*WN + 1*8*WN*WN]
#define SM11_22(m,n)  SM[m+N + 1*WN + (n+N)*8*WN + 1*8*WN*WN]
#define SM11_32(m,n)  SM[m+N + 2*WN + (n+N)*8*WN + 1*8*WN*WN]
#define SM11_42(m,n)  SM[m+N + 3*WN + (n+N)*8*WN + 1*8*WN*WN]
#define SM21_12(m,n)  SM[m+N + 4*WN + (n+N)*8*WN + 1*8*WN*WN]
#define SM21_22(m,n)  SM[m+N + 5*WN + (n+N)*8*WN + 1*8*WN*WN]
#define SM21_32(m,n)  SM[m+N + 6*WN + (n+N)*8*WN + 1*8*WN*WN]
#define SM21_42(m,n)  SM[m+N + 7*WN + (n+N)*8*WN + 1*8*WN*WN]

#define SM11_13(m,n)  SM[m+N + 0*WN + (n+N)*8*WN + 2*8*WN*WN]
#define SM11_23(m,n)  SM[m+N + 1*WN + (n+N)*8*WN + 2*8*WN*WN]
#define SM11_33(m,n)  SM[m+N + 2*WN + (n+N)*8*WN + 2*8*WN*WN]
#define SM11_43(m,n)  SM[m+N + 3*WN + (n+N)*8*WN + 2*8*WN*WN]
#define SM21_13(m,n)  SM[m+N + 4*WN + (n+N)*8*WN + 2*8*WN*WN]
#define SM21_23(m,n)  SM[m+N + 5*WN + (n+N)*8*WN + 2*8*WN*WN]
#define SM21_33(m,n)  SM[m+N + 6*WN + (n+N)*8*WN + 2*8*WN*WN]
#define SM21_43(m,n)  SM[m+N + 7*WN + (n+N)*8*WN + 2*8*WN*WN]

#define SM11_14(m,n)  SM[m+N + 0*WN + (n+N)*8*WN + 3*8*WN*WN]
#define SM11_24(m,n)  SM[m+N + 1*WN + (n+N)*8*WN + 3*8*WN*WN]
#define SM11_34(m,n)  SM[m+N + 2*WN + (n+N)*8*WN + 3*8*WN*WN]
#define SM11_44(m,n)  SM[m+N + 3*WN + (n+N)*8*WN + 3*8*WN*WN]
#define SM21_14(m,n)  SM[m+N + 4*WN + (n+N)*8*WN + 3*8*WN*WN]
#define SM21_24(m,n)  SM[m+N + 5*WN + (n+N)*8*WN + 3*8*WN*WN]
#define SM21_34(m,n)  SM[m+N + 6*WN + (n+N)*8*WN + 3*8*WN*WN]
#define SM21_44(m,n)  SM[m+N + 7*WN + (n+N)*8*WN + 3*8*WN*WN]
//------------------------------------------------------------
#define SM12_11(m,n)  SM[m+N + 0*WN + (n+N)*8*WN + 4*8*WN*WN]
#define SM12_21(m,n)  SM[m+N + 1*WN + (n+N)*8*WN + 4*8*WN*WN]
#define SM12_31(m,n)  SM[m+N + 2*WN + (n+N)*8*WN + 4*8*WN*WN]
#define SM12_41(m,n)  SM[m+N + 3*WN + (n+N)*8*WN + 4*8*WN*WN]
#define SM22_11(m,n)  SM[m+N + 4*WN + (n+N)*8*WN + 4*8*WN*WN]
#define SM22_21(m,n)  SM[m+N + 5*WN + (n+N)*8*WN + 4*8*WN*WN]
#define SM22_31(m,n)  SM[m+N + 6*WN + (n+N)*8*WN + 4*8*WN*WN]
#define SM22_41(m,n)  SM[m+N + 7*WN + (n+N)*8*WN + 4*8*WN*WN]

#define SM12_12(m,n)  SM[m+N + 0*WN + (n+N)*8*WN + 5*8*WN*WN]
#define SM12_22(m,n)  SM[m+N + 1*WN + (n+N)*8*WN + 5*8*WN*WN]
#define SM12_32(m,n)  SM[m+N + 2*WN + (n+N)*8*WN + 5*8*WN*WN]
#define SM12_42(m,n)  SM[m+N + 3*WN + (n+N)*8*WN + 5*8*WN*WN]
#define SM22_12(m,n)  SM[m+N + 4*WN + (n+N)*8*WN + 5*8*WN*WN]
#define SM22_22(m,n)  SM[m+N + 5*WN + (n+N)*8*WN + 5*8*WN*WN]
#define SM22_32(m,n)  SM[m+N + 6*WN + (n+N)*8*WN + 5*8*WN*WN]
#define SM22_42(m,n)  SM[m+N + 7*WN + (n+N)*8*WN + 5*8*WN*WN]

#define SM12_13(m,n)  SM[m+N + 0*WN + (n+N)*8*WN + 6*8*WN*WN]
#define SM12_23(m,n)  SM[m+N + 1*WN + (n+N)*8*WN + 6*8*WN*WN]
#define SM12_33(m,n)  SM[m+N + 2*WN + (n+N)*8*WN + 6*8*WN*WN]
#define SM12_43(m,n)  SM[m+N + 3*WN + (n+N)*8*WN + 6*8*WN*WN]
#define SM22_13(m,n)  SM[m+N + 4*WN + (n+N)*8*WN + 6*8*WN*WN]
#define SM22_23(m,n)  SM[m+N + 5*WN + (n+N)*8*WN + 6*8*WN*WN]
#define SM22_33(m,n)  SM[m+N + 6*WN + (n+N)*8*WN + 6*8*WN*WN]
#define SM22_43(m,n)  SM[m+N + 7*WN + (n+N)*8*WN + 6*8*WN*WN]

#define SM12_14(m,n)  SM[m+N + 0*WN + (n+N)*8*WN + 7*8*WN*WN]
#define SM12_24(m,n)  SM[m+N + 1*WN + (n+N)*8*WN + 7*8*WN*WN]
#define SM12_34(m,n)  SM[m+N + 2*WN + (n+N)*8*WN + 7*8*WN*WN]
#define SM12_44(m,n)  SM[m+N + 3*WN + (n+N)*8*WN + 7*8*WN*WN]
#define SM22_14(m,n)  SM[m+N + 4*WN + (n+N)*8*WN + 7*8*WN*WN]
#define SM22_24(m,n)  SM[m+N + 5*WN + (n+N)*8*WN + 7*8*WN*WN]
#define SM22_34(m,n)  SM[m+N + 6*WN + (n+N)*8*WN + 7*8*WN*WN]
#define SM22_44(m,n)  SM[m+N + 7*WN + (n+N)*8*WN + 7*8*WN*WN]
//-------------------------------------------------------------

#define cdouble _Complex double

//typedef std::complex<double> cdouble;
//cdouble I = std::complex<double>(0.0,1.0);

extern "C" void MoM_two_in_two(cdouble *epv, double la, double *rov, double *xov, double *zov, cdouble * S11, cdouble * S12, cdouble * S21, cdouble * S22, const int N, const int NM);


void besseljCPX(int, cdouble, cdouble *, int);
void besselyCPX(int, cdouble, cdouble *, int);


double expint(double x, int n, char mode);

void  exp_exp_int_ss(double * la, double x, double len, const int M, const int LAS, cdouble * res);
void  exp_exp_int(double * la, double x, double y, double len, const int M, const int LAS, cdouble * res);

extern "C" void zgesv_(int*,int*,cdouble*,int*,int*,cdouble*,int*,int*);


void MoM_two_in_two(cdouble *epv, double la, double *rov, double *xov, double *zov, cdouble * S11, cdouble * S12, cdouble * S21, cdouble * S22, const int N, const int NM){ //default value of N is 11

	cdouble ep1, ep2, ep3, ep4, ep5, ep6;
	double  ro1, ro2, ro3, ro4, ro5, ro6;
	double  xo1, xo2, xo3, xo4, xo5, xo6;
	double  zo1, zo2, zo3, zo4, zo5, zo6;

	ep1 = epv[0]; ep2 = epv[1]; ep3 = epv[2]; ep4 = epv[3]; ep5 = epv[4]; ep6 = epv[5];
	ro1 = rov[0]; ro2 = rov[1]; ro3 = rov[2]; ro4 = rov[3]; ro5 = rov[4]; ro6 = rov[5];
	xo1 = xov[0]; xo2 = xov[1]; xo3 = xov[2]; xo4 = xov[3]; xo5 = xov[4]; xo6 = xov[5];
	zo1 = zov[0]; zo2 = zov[1]; zo3 = zov[2]; zo4 = zov[3]; zo5 = zov[4]; zo6 = zov[5];

	const int L = 2*N+1;
	int nrhs = NM*2, info, pivot[8*(4*N+2)];
	const int WN = 2*N+1;
	int M = 8*WN;

	double Z = 120.0*PI;
	double k;

	cdouble  kw[NM], phk[NM];

	cdouble  ZO1[NM], ZOI1[NM];
	cdouble  XO1[NM], XOI1[NM];

	cdouble  ZO2[NM], ZOI2[NM];
	cdouble  XO2[NM], XOI2[NM];
//---------------------------------------------------------------------
 	cdouble  BJo1[WN+2], BJ1e1[WN+2], BJ2e1[WN+2], BJ3e1[WN+2];
 	cdouble  BHo1[WN+2], BH1e1[WN+2], BH2e1[WN+2], BH3e1[WN+2];

 	cdouble  BJo2[WN+2], BJ1e2[WN+2], BJ2e2[WN+2], BJ3e2[WN+2];
 	cdouble  BHo2[WN+2], BH1e2[WN+2], BH2e2[WN+2], BH3e2[WN+2];
//---------------------------------------------------------------------
 	cdouble  BJDo1[WN], BJD1e1[WN], BJD2e1[WN], BJD3e1[WN];
 	cdouble  BHDo1[WN], BHD1e1[WN], BHD2e1[WN], BHD3e1[WN];

 	cdouble  BJDo2[WN], BJD1e2[WN], BJD2e2[WN], BJD3e2[WN];
 	cdouble  BHDo2[WN], BHD1e2[WN], BHD2e2[WN], BHD3e2[WN];
//---------------------------------------------------------------------
	cdouble  BJ1p2[WN+2], BJ1p3[WN+2];
	cdouble  BJD1p2[WN],  BJD1p3[WN];

	cdouble  BJ2p2[WN+2], BJ2p3[WN+2];
	cdouble  BJD2p2[WN],  BJD2p3[WN];
//---------------------------------------------------------------------
	cdouble BJ12u[4*N+1], BJ13u[4*N+1], BJ23u[4*N+1], BH23u[4*N+1];
	cdouble BJ12l[4*N+1], BJ13l[4*N+1], BJ23l[4*N+1], BH23l[4*N+1];
       //-------------------------------------------------------

	cdouble  SM[8*8*WN*WN];

	cdouble  res11_p[4*N+1], res11_n[4*N+1];
	cdouble  res12_p[4*N+1], res12_n[4*N+1];
	cdouble  res21_p[4*N+1], res21_n[4*N+1];
	cdouble  res22_p[4*N+1], res22_n[4*N+1];

	cdouble   PH[WN*NM];
	cdouble  PHI[WN*NM];

	cdouble  EX[2*8*WN*NM];

	cdouble  EH2u[WN];
	cdouble  EH3u[WN];
	cdouble  EH2l[WN];
	cdouble  EH3l[WN];

	cdouble  E11o, H11o;
	cdouble  E12o, H12o;
	cdouble  E21o, H21o;
	cdouble  E22o, H22o;

	cdouble CFP1, CFN1;
	cdouble CFP2, CFN2;

	double xo1u, xo2u, xo3u;
	double xo1l, xo2l, xo3l;

	double zo1u, zo2u, zo3u;
	double zo1l, zo2l, zo3l;

	double ro12u, ro12l, ph12u, ph12l;
	double ro13u, ro13l, ph13u, ph13l;
	double ro23u, ro23l, ph23u, ph23l;

	cdouble EH21u, EH12u, EH31u, EH13u, EH32u, EH23u;
	cdouble EH21l, EH12l, EH31l, EH13l, EH32l, EH23l;

	cdouble ED21u, ED12u, ED31u, ED13u, ED32u, ED23u;
	cdouble ED21l, ED12l, ED31l, ED13l, ED32l, ED23l;

	cdouble HD21u, HD12u, HD31u, HD13u, HD32u, HD23u;
	cdouble HD21l, HD12l, HD31l, HD13l, HD32l, HD23l;
//----------------------------------------------------------------------------
	cdouble HD11u, ED11u;
	cdouble HD22u, ED22u;
	cdouble HD33u, ED33u;

	cdouble HD11l, ED11l;
	cdouble HD22l, ED22l;
	cdouble HD33l, ED33l;

	cdouble HDo1, EDo1;
	cdouble HDo2, EDo2;
//----------------------------------------------------------------------------
	// xo1 += 0.5;	// change the coordinae system
	// xo2 += 0.5;
//----------------------------------------------------------------------------

	ep1 = csqrt(ep1);
	ep2 = csqrt(ep2);
	ep3 = csqrt(ep3);

	ep4 = csqrt(ep4);
	ep5 = csqrt(ep5);
	ep6 = csqrt(ep6);

	xo1u = xo1;
	xo2u = xo3;
	xo3u = xo2;

	xo1l = xo4;
	xo2l = xo5;
	xo3l = xo6;

	zo1u = zo1;
	zo2u = zo3;
	zo3u = zo2;

	zo1l = zo4;
	zo2l = zo5;
	zo3l = zo6;

	ro12u   = sqrt(POW2(xo1u-xo2u) + POW2(zo1u-zo2u));
	ro12l   = sqrt(POW2(xo1l-xo2l) + POW2(zo1l-zo2l));
	ph12u   = atan2(xo1u-xo2u, zo1u-zo2u);
	ph12l   = atan2(xo1l-xo2l, zo1l-zo2l);

	ro13u   = sqrt(POW2(xo1u-xo3u) + POW2(zo1u-zo3u));
	ro13l   = sqrt(POW2(xo1l-xo3l) + POW2(zo1l-zo3l));
	ph13u   = atan2(xo1u-xo3u, zo1u-zo3u);
	ph13l   = atan2(xo1l-xo3l, zo1l-zo3l);

	ro23u   = sqrt(POW2(xo2u-xo3u) + POW2(zo2u-zo3u));
	ro23l   = sqrt(POW2(xo2l-xo3l) + POW2(zo2l-zo3l));
	ph23u   = atan2(xo2u-xo3u, zo2u-zo3u);
	ph23l   = atan2(xo2l-xo3l, zo2l-zo3l);


	for(int m=1; m<=NM; ++m){
		XO1(m)  = cexp(I*static_cast<double>(m)*PI*xo1u);
		XOI1(m) = 1.0/XO1(m);

		XO2(m)  = cexp(I*static_cast<double>(m)*PI*xo1l);
		XOI2(m) = 1.0/XO2(m);
	}

	exp_exp_int_ss(&la,      0.0, 2.0, 2*N, 1, res11_n);
	exp_exp_int_ss(&la, 2.0*xo1u, 2.0, 2*N, 1, res11_p);

	exp_exp_int_ss(&la,     0.0,  2.0, 2*N, 1, res22_n);
	exp_exp_int_ss(&la, 2.0*xo1l, 2.0, 2*N, 1, res22_p);

	exp_exp_int(&la, xo1u-xo1l, zo1l-zo1u, 2.0, 2*N, 1, res12_n);
	exp_exp_int(&la, xo1u+xo1l, zo1l-zo1u, 2.0, 2*N, 1, res12_p);

	exp_exp_int(&la, xo1l-xo1u, zo1u-zo1l, 2.0, 2*N, 1, res21_n);
	exp_exp_int(&la, xo1l+xo1u, zo1u-zo1l, 2.0, 2*N, 1, res21_p);

	//============================================================

	k = 2.0*PI*la;

	besseljCPX(0, 	  k*ro1, &BJo1[N+1],  N+2);
	besseljCPX(0, ep1*k*ro1, &BJ1e1[N+1], N+2);
	besseljCPX(0, ep1*k*ro2, &BJ2e1[N+1], N+2);
	besseljCPX(0, ep1*k*ro3, &BJ3e1[N+1], N+2);

	besseljCPX(0, 	  k*ro4, &BJo2[N+1],  N+2);
	besseljCPX(0, ep4*k*ro4, &BJ1e2[N+1], N+2);
	besseljCPX(0, ep4*k*ro5, &BJ2e2[N+1], N+2);
	besseljCPX(0, ep4*k*ro6, &BJ3e2[N+1], N+2);

	besselyCPX(0, 	  k*ro1, &BHo1[N+1],  N+2);
	besselyCPX(0, ep1*k*ro1, &BH1e1[N+1], N+2);
	besselyCPX(0, ep1*k*ro2, &BH2e1[N+1], N+2);
	besselyCPX(0, ep1*k*ro3, &BH3e1[N+1], N+2);

	besselyCPX(0, 	  k*ro4, &BHo2[N+1],  N+2);
	besselyCPX(0, ep4*k*ro4, &BH1e2[N+1], N+2);
	besselyCPX(0, ep4*k*ro5, &BH2e2[N+1], N+2);
	besselyCPX(0, ep4*k*ro6, &BH3e2[N+1], N+2);
//------------------------------------------------------------
	besseljCPX(0, ep1*k*ro12u, &BJ12u[2*N],  2*N+1);
	besseljCPX(0, ep1*k*ro13u, &BJ13u[2*N],  2*N+1);
	besseljCPX(0, ep1*k*ro23u, &BJ23u[2*N],  2*N+1);
	besselyCPX(0, ep1*k*ro23u, &BH23u[2*N],  2*N+1);

	besseljCPX(0, ep4*k*ro12l, &BJ12l[2*N],  2*N+1);
	besseljCPX(0, ep4*k*ro13l, &BJ13l[2*N],  2*N+1);
	besseljCPX(0, ep4*k*ro23l, &BJ23l[2*N],  2*N+1);
	besselyCPX(0, ep4*k*ro23l, &BH23l[2*N],  2*N+1);
//------------------------------------------------------------
	besseljCPX(0, ep2*k*ro2, &BJ1p2[N+1], N+2);
	besseljCPX(0, ep3*k*ro3, &BJ1p3[N+1], N+2);

	besseljCPX(0, ep5*k*ro2, &BJ2p2[N+1], N+2);
	besseljCPX(0, ep6*k*ro3, &BJ2p3[N+1], N+2);
//------------------------------------------------------------
	// obtain Hankel functions of the 1st order
//------------------------------------------------------------
	for(int n=0;n<=N+1;++n){
		BHo1(n)  = BJo1(n)  + I*BHo1(n);
		BH1e1(n) = BJ1e1(n) + I*BH1e1(n);
		BH2e1(n) = BJ2e1(n) + I*BH2e1(n);
		BH3e1(n) = BJ3e1(n) + I*BH3e1(n);

		BHo2(n)  = BJo2(n)  + I*BHo2(n);
		BH1e2(n) = BJ1e2(n) + I*BH1e2(n);
		BH2e2(n) = BJ2e2(n) + I*BH2e2(n);
		BH3e2(n) = BJ3e2(n) + I*BH3e2(n);
	}
	for(int n=0;n<=2*N;++n){
		BH23u(n) = BJ23u(n) + I*BH23u(n);
		BH23l(n) = BJ23l(n) + I*BH23l(n);
	}

	// calculate cylindrical functions of negative order
	for(int n=1;n<=N+1;++n){
		BJo1(-n)  = pow(-1.0,n)*BJo1(n);
		BJ1e1(-n) = pow(-1.0,n)*BJ1e1(n);
		BJ2e1(-n) = pow(-1.0,n)*BJ2e1(n);
		BJ3e1(-n) = pow(-1.0,n)*BJ3e1(n);

		BHo1(-n)  = pow(-1.0,n)*BHo1(n);
		BH1e1(-n) = pow(-1.0,n)*BH1e1(n);
		BH2e1(-n) = pow(-1.0,n)*BH2e1(n);
		BH3e1(-n) = pow(-1.0,n)*BH3e1(n);
  	//----------------------------------------------------
		BJo2(-n)  = pow(-1.0,n)*BJo2(n);
		BJ1e2(-n) = pow(-1.0,n)*BJ1e2(n);
		BJ2e2(-n) = pow(-1.0,n)*BJ2e2(n);
		BJ3e2(-n) = pow(-1.0,n)*BJ3e2(n);

		BHo2(-n)  = pow(-1.0,n)*BHo2(n);
		BH1e2(-n) = pow(-1.0,n)*BH1e2(n);
		BH2e2(-n) = pow(-1.0,n)*BH2e2(n);
		BH3e2(-n) = pow(-1.0,n)*BH3e2(n);
	//----------------------------------------------------
		BJ1p2(-n)  = pow(-1.0,n)*BJ1p2(n);
		BJ1p3(-n)  = pow(-1.0,n)*BJ1p3(n);

		BJ2p2(-n)  = pow(-1.0,n)*BJ2p2(n);
		BJ2p3(-n)  = pow(-1.0,n)*BJ2p3(n);
	}
	//----------------------------------------------------
	for(int n=1;n<=2*N;++n){
		BJ12u(-n)  = pow(-1.0,n)*BJ12u(n);
		BJ13u(-n)  = pow(-1.0,n)*BJ13u(n);
		BJ23u(-n)  = pow(-1.0,n)*BJ23u(n);
		BH23u(-n)  = pow(-1.0,n)*BH23u(n);

		BJ12l(-n)  = pow(-1.0,n)*BJ12l(n);
		BJ13l(-n)  = pow(-1.0,n)*BJ13l(n);
		BJ23l(-n)  = pow(-1.0,n)*BJ23l(n);
		BH23l(-n)  = pow(-1.0,n)*BH23l(n);
	}
	// calculate derivatives of the cylindrical functions
	for(int n=-N;n<=N;++n){

		BJDo1(n)  = 0.5*(BJo1(n-1)  - BJo1(n+1));
		BJD1e1(n) = 0.5*(BJ1e1(n-1) - BJ1e1(n+1));
		BJD2e1(n) = 0.5*(BJ2e1(n-1) - BJ2e1(n+1));
		BJD3e1(n) = 0.5*(BJ3e1(n-1) - BJ3e1(n+1));

		BHDo1(n)  = 0.5*(BHo1(n-1)  - BHo1(n+1));
		BHD1e1(n) = 0.5*(BH1e1(n-1) - BH1e1(n+1));
		BHD2e1(n) = 0.5*(BH2e1(n-1) - BH2e1(n+1));
		BHD3e1(n) = 0.5*(BH3e1(n-1) - BH3e1(n+1));
	//----------------------------------------------------
		BJDo2(n)  = 0.5*(BJo2(n-1)  - BJo2(n+1));
		BJD1e2(n) = 0.5*(BJ1e2(n-1) - BJ1e2(n+1));
		BJD2e2(n) = 0.5*(BJ2e2(n-1) - BJ2e2(n+1));
		BJD3e2(n) = 0.5*(BJ3e2(n-1) - BJ3e2(n+1));

		BHDo2(n)  = 0.5*(BHo2(n-1)  - BHo2(n+1));
		BHD1e2(n) = 0.5*(BH1e2(n-1) - BH1e2(n+1));
		BHD2e2(n) = 0.5*(BH2e2(n-1) - BH2e2(n+1));
		BHD3e2(n) = 0.5*(BH3e2(n-1) - BH3e2(n+1));
	//----------------------------------------------------
		BJD1p2(n)  = 0.5*(BJ1p2(n-1) - BJ1p2(n+1));
		BJD1p3(n)  = 0.5*(BJ1p3(n-1) - BJ1p3(n+1));

		BJD2p2(n)  = 0.5*(BJ2p2(n-1) - BJ2p2(n+1));
		BJD2p3(n)  = 0.5*(BJ2p3(n-1) - BJ2p3(n+1));
	}
	//---------------------------------------------------------

	for(int n=-N; n<=N; ++n){

		BJ12u(n) = BJ12u(n)*cexp(-I*static_cast<double>(n)*ph12u);
		BJ13u(n) = BJ13u(n)*cexp(-I*static_cast<double>(n)*ph13u);
		BH23u(n) = BH23u(n)*cexp(-I*static_cast<double>(n)*ph23u);

		BJ12l(n) = BJ12l(n)*cexp(-I*static_cast<double>(n)*ph12l);
		BJ13l(n) = BJ13l(n)*cexp(-I*static_cast<double>(n)*ph13l);
		BH23l(n) = BH23l(n)*cexp(-I*static_cast<double>(n)*ph23l);

	}
	//---------------------------------------------------------

	for(int m=1; m<=NM; ++m){
		if(m==1){
			kw(1)  = sqrt(k*k - PI*PI);
			phk(1) = atan2(PI, creal(kw(1)));
		}else{
			kw(m) = I*sqrt(PI*PI*m*m - k*k);
			double kxz = cabs(static_cast<double>(m)*PI/kw(m));
			phk(m) = 0.5*I*log(fabs((1.0-kxz)/(1.0+kxz)));
			if(kxz > 1.0){
				phk(m) += PI/2.0;
			}
		}

		ZO1(m)  = cexp(I*kw(m)*zo1u);
		ZOI1(m) = 1.0/ZO1(m);

		ZO2(m)  = cexp(I*kw(m)*zo1l);
		ZOI2(m) = 1.0/ZO2(m);

	}

	for(int n=-N; n<=N; ++n){
		for(int m=1; m<=NM; ++m){
			PH(n,m)  = cexp(I*static_cast<double>(n)*phk(m));
			PHI(n,m) = 1.0/PH(n,m);

			E1(n,m,1) = -I*(cpow( I,n)*0.5*BJo1(n)*ZO1(m)*(XO1(m)*PHI(n,m) - XOI1(m)*PH(n,m)));
			E1(n,m,2) = -I*(cpow(-I,n)*0.5*BJo1(n)*ZOI1(m)*(XO1(m)*PH(n,m) - XOI1(m)*PHI(n,m)));

			H1(n,m,1) = 0.0;
			H1(n,m,2) = 0.0;

			E12(n,m,1) = 0.0;
			E12(n,m,2) = 0.0;
			E13(n,m,1) = 0.0;
			E13(n,m,2) = 0.0;

			E2(n,m,1) = -I*(cpow( I,n)*0.5*BJo2(n)*ZO2(m)*(XO2(m)*PHI(n,m) - XOI2(m)*PH(n,m)));
			E2(n,m,2) = -I*(cpow(-I,n)*0.5*BJo2(n)*ZOI2(m)*(XO2(m)*PH(n,m) - XOI2(m)*PHI(n,m)));

			H2(n,m,1) = 0.0;
			H2(n,m,2) = 0.0;

			E22(n,m,1) = 0.0;
			E22(n,m,2) = 0.0;
			E23(n,m,1) = 0.0;
			E23(n,m,2) = 0.0;


		}
		EH2u(n) = BJD1p2(n)/BJ1p2(n);
		EH3u(n) = BJD1p3(n)/BJ1p3(n);

		EH2l(n) = BJD2p2(n)/BJ2p2(n);
		EH3l(n) = BJD2p3(n)/BJ2p3(n);
	}

	for(int m=-N;m<=N;++m){
		for(int n=-N;n<=N;++n){
			if(n%2==0){
				H11o = BJo1(m)*(res11_n(m-n) - res11_p(n+m))*BJo1(n);
				E11o = BJo1(m)*(res11_n(m-n) - res11_p(n+m))*BJDo1(n);

				H12o = BJo1(m)*(res12_n(m-n) - res12_p(n+m))*BJo2(n);
				E12o = BJo1(m)*(res12_n(m-n) - res12_p(n+m))*BJDo2(n);

				H21o = BJo2(m)*(res21_n(m-n) - res21_p(n+m))*BJo1(n);
				E21o = BJo2(m)*(res21_n(m-n) - res21_p(n+m))*BJDo1(n);

				H22o = BJo2(m)*(res22_n(m-n) - res22_p(n+m))*BJo2(n);
				E22o = BJo2(m)*(res22_n(m-n) - res22_p(n+m))*BJDo2(n);

			}else{
				H11o = BJo1(m)*(res11_n(m-n) + res11_p(n+m))*BJo1(n);
				E11o = BJo1(m)*(res11_n(m-n) + res11_p(n+m))*BJDo1(n);

				H12o = BJo1(m)*(res12_n(m-n) + res12_p(n+m))*BJo2(n);
				E12o = BJo1(m)*(res12_n(m-n) + res12_p(n+m))*BJDo2(n);

				H21o = BJo2(m)*(res21_n(m-n) + res21_p(n+m))*BJo1(n);
				E21o = BJo2(m)*(res21_n(m-n) + res21_p(n+m))*BJDo1(n);

				H22o = BJo2(m)*(res22_n(m-n) + res22_p(n+m))*BJo2(n);
				E22o = BJo2(m)*(res22_n(m-n) + res22_p(n+m))*BJDo2(n);
			}
			//---------------------------------------------------------------------------------
			if(m==n){

				HDo1  = BHo1(n)*BJo1(n);
				EDo1  = BHo1(n)*BJDo1(n);

				HD11u = BH1e1(n)*BJ1e1(n);
				ED11u = BH1e1(n)*BJD1e1(n);

				HD22u = BJ2e1(n)*BH2e1(n);
				ED22u = BJ2e1(n)*BHD2e1(n);

				HD33u = BJ3e1(n)*BH3e1(n);
				ED33u = BJ3e1(n)*BHD3e1(n);
			//----------------------------------------------------------
				HDo2  = BHo2(n)*BJo2(n);
				EDo2  = BHo2(n)*BJDo2(n);

				HD11l = BH1e2(n)*BJ1e2(n);
				ED11l = BH1e2(n)*BJD1e2(n);

				HD22l = BJ2e2(n)*BH2e2(n);
				ED22l = BJ2e2(n)*BHD2e2(n);

				HD33l = BJ3e2(n)*BH3e2(n);
				ED33l = BJ3e2(n)*BHD3e2(n);
			}
			//---------------------------------------------------------------------------------

			EH21u = pow(-1.0, m-n)*BJ12u(m-n);
			EH12u = BJ12u(m-n);

			EH31u = pow(-1.0, m-n)*BJ13u(m-n);
			EH13u = BJ13u(m-n);

			EH32u = pow(-1.0, m-n)*BH23u(m-n);
			EH23u = BH23u(m-n);

			EH21l = pow(-1.0, m-n)*BJ12l(m-n);
			EH12l = BJ12l(m-n);

			EH31l = pow(-1.0, m-n)*BJ13l(m-n);
			EH13l = BJ13l(m-n);

			EH32l = pow(-1.0, m-n)*BH23l(m-n);
			EH23l = BH23l(m-n);

			//---------------------------------------------------------------------------------

			HD21u = BH1e1(m)*EH21u*BJ2e1(n);
			ED21u = BH1e1(m)*EH21u*BJD2e1(n);

			HD12u = BJ2e1(m)*EH12u*BH1e1(n);
			ED12u = BJ2e1(m)*EH12u*BHD1e1(n);

			HD31u = BH1e1(m)*EH31u*BJ3e1(n);
			ED31u = BH1e1(m)*EH31u*BJD3e1(n);

			HD13u = BJ3e1(m)*EH13u*BH1e1(n);
			ED13u = BJ3e1(m)*EH13u*BHD1e1(n);

			HD32u = BJ2e1(m)*EH32u*BJ3e1(n);
			ED32u = BJ2e1(m)*EH32u*BJD3e1(n);

			HD23u = BJ3e1(m)*EH23u*BJ2e1(n);
			ED23u = BJ3e1(m)*EH23u*BJD2e1(n);

			//-------------------------------------------------------------------------

			HD21l = BH1e2(m)*EH21l*BJ2e2(n);
			ED21l = BH1e2(m)*EH21l*BJD2e2(n);

			HD12l = BJ2e2(m)*EH12l*BH1e2(n);
			ED12l = BJ2e2(m)*EH12l*BHD1e2(n);

			HD31l = BH1e2(m)*EH31l*BJ3e2(n);
			ED31l = BH1e2(m)*EH31l*BJD3e2(n);

			HD13l = BJ3e2(m)*EH13l*BH1e2(n);
			ED13l = BJ3e2(m)*EH13l*BHD1e2(n);

			HD32l = BJ2e2(m)*EH32l*BJ3e2(n);
			ED32l = BJ2e2(m)*EH32l*BJD3e2(n);

			HD23l = BJ3e2(m)*EH23l*BJ2e2(n);
			ED23l = BJ3e2(m)*EH23l*BJD2e2(n);

			//-------------------------------------------------------------------------

			SM11_11(m,n) = 0.0; SM11_12(m,n) = 0.0;	SM11_13(m,n) = 0.0; SM11_14(m,n) = 0.0;
			SM11_21(m,n) = 0.0; SM11_22(m,n) = 0.0;	SM11_23(m,n) = 0.0; SM11_24(m,n) = 0.0;
			SM11_31(m,n) = 0.0; SM11_32(m,n) = 0.0;	SM11_33(m,n) = 0.0; SM11_34(m,n) = 0.0;
			SM11_41(m,n) = 0.0; SM11_42(m,n) = 0.0;	SM11_43(m,n) = 0.0; SM11_44(m,n) = 0.0;

			SM12_11(m,n) = 0.0; SM12_12(m,n) = 0.0;	SM12_13(m,n) = 0.0; SM12_14(m,n) = 0.0;
			SM12_21(m,n) = 0.0; SM12_22(m,n) = 0.0;	SM12_23(m,n) = 0.0; SM12_24(m,n) = 0.0;
			SM12_31(m,n) = 0.0; SM12_32(m,n) = 0.0;	SM12_33(m,n) = 0.0; SM12_34(m,n) = 0.0;
			SM12_41(m,n) = 0.0; SM12_42(m,n) = 0.0;	SM12_43(m,n) = 0.0; SM12_44(m,n) = 0.0;

			SM21_11(m,n) = 0.0; SM21_12(m,n) = 0.0;	SM21_13(m,n) = 0.0; SM21_14(m,n) = 0.0;
			SM21_21(m,n) = 0.0; SM21_22(m,n) = 0.0;	SM21_23(m,n) = 0.0; SM21_24(m,n) = 0.0;
			SM21_31(m,n) = 0.0; SM21_32(m,n) = 0.0;	SM21_33(m,n) = 0.0; SM21_34(m,n) = 0.0;
			SM21_41(m,n) = 0.0; SM21_42(m,n) = 0.0;	SM21_43(m,n) = 0.0; SM21_44(m,n) = 0.0;

			SM22_11(m,n) = 0.0; SM22_12(m,n) = 0.0;	SM22_13(m,n) = 0.0; SM22_14(m,n) = 0.0;
			SM22_21(m,n) = 0.0; SM22_22(m,n) = 0.0;	SM22_23(m,n) = 0.0; SM22_24(m,n) = 0.0;
			SM22_31(m,n) = 0.0; SM22_32(m,n) = 0.0;	SM22_33(m,n) = 0.0; SM22_34(m,n) = 0.0;
			SM22_41(m,n) = 0.0; SM22_42(m,n) = 0.0;	SM22_43(m,n) = 0.0; SM22_44(m,n) = 0.0;

			//--------------------------------------------------------------------------

			if(m==n){
				SM11_11(m,n) += 1.0;
				SM22_11(m,n) += 1.0;
			}
   		      //----------------------------------------------------------
			if(m==n){
				SM11_11(m,n) +=   PI/2.0/I*ro1*k*(E11o + EDo1);
				SM11_12(m,n) +=  -PI/2.0/I*ro1*I*k*Z*(H11o + HDo1);
			}else{
				SM11_11(m,n) +=   PI/2.0/I*ro1*k*E11o;
				SM11_12(m,n) +=  -PI/2.0/I*ro1*I*k*Z*H11o;
			}

			if(m==n){
				SM11_21(m,n) +=   ro1*ep1*ED11u;
				SM11_22(m,n) +=  -ro1*I*Z*HD11u;
			}
			SM11_23(m,n) +=   ro2*(ep1*ED21u - ep2*HD21u*EH2u(n));
			SM11_24(m,n) +=  -ro3*(ep1*ED31u - ep3*HD31u*EH3u(n));

			SM11_31(m,n) +=   ro1*ep1*ED12u;
			SM11_32(m,n) +=  -ro1*I*Z*HD12u;
			if(m==n){
				SM11_33(m,n) +=   ro2*(ep1*ED22u  - ep2*HD22u*EH2u(n));
			}
			SM11_34(m,n) +=  -ro3*(ep1*ED32u - ep3*HD32u*EH3u(n));

			SM11_41(m,n) +=   ro1*ep1*ED13u;
			SM11_42(m,n) +=  -ro1*I*Z*HD13u;
			SM11_43(m,n) +=   ro2*(ep1*ED23u - ep2*HD23u*EH2u(n));
			if(m==n){
				SM11_44(m,n) +=  -ro3*(ep1*ED33u  - ep3*HD33u*EH3u(n));
			}
   		      //-----------------------------------------------------------
			if(m==n){
				SM22_11(m,n) +=   PI/2.0/I*ro4*k*(E22o + EDo2);
				SM22_12(m,n) +=  -PI/2.0/I*ro4*I*k*Z*(H22o + HDo2);
			}else{
				SM22_11(m,n) +=   PI/2.0/I*ro4*k*E22o;
				SM22_12(m,n) +=  -PI/2.0/I*ro4*I*k*Z*H22o;
			}
			if(m==n){
				SM22_21(m,n) +=   ro4*ep4*ED11l;
				SM22_22(m,n) +=  -ro4*I*Z*HD11l;
			}
			SM22_23(m,n) +=   ro5*(ep4*ED21l - ep5*HD21l*EH2l(n));
			SM22_24(m,n) +=  -ro6*(ep4*ED31l - ep6*HD31l*EH3l(n));

			SM22_31(m,n) +=   ro4*ep4*ED12l;
			SM22_32(m,n) +=  -ro4*I*Z*HD12l;
			if(m==n){
				SM22_33(m,n) +=   ro5*(ep4*ED22l  - ep5*HD22l*EH2l(n));
			}
			SM22_34(m,n) +=  -ro6*(ep4*ED32l - ep6*HD32l*EH3l(n));

			SM22_41(m,n) +=   ro4*ep4*ED13l;
			SM22_42(m,n) +=  -ro4*I*Z*HD13l;
			SM22_43(m,n) +=   ro5*(ep4*ED23l - ep5*HD23l*EH2l(n));
			if(m==n){
				SM22_44(m,n) +=  -ro6*(ep4*ED33l  - ep6*HD33l*EH3l(n));
			}
    		      //-----------------------------------------------------------
			SM12_11(m,n) +=  PI/2.0/I*ro4*k*E12o;
			SM12_12(m,n) += -PI/2.0/I*ro4*I*k*Z*H12o;
   		      //-----------------------------------------------------------
			SM21_11(m,n) +=  PI/2.0/I*ro1*k*E21o;
			SM21_12(m,n) += -PI/2.0/I*ro1*I*k*Z*H21o;
   		      //-----------------------------------------------------------

		}
	}

	zgesv_(&M, &nrhs, SM, &M, pivot, EX, &M, &info);

	for(int m=1; m<=NM; ++m){
		for(int n=1; n<=NM; ++n){
			if(m==n){
				S11(m,n) = 0.0;
				S21(m,n) = 1.0;
				S12(m,n) = 1.0;
				S22(m,n) = 0.0;
			}else{
				S11(m,n) = 0.0;
				S21(m,n) = 0.0;
				S12(m,n) = 0.0;
				S22(m,n) = 0.0;
			}
			for(int p=-N; p<=N; ++p){

				// Expressions for 1st rod
				CFP1 = PI*k*ro1*cpow(I,p)*(I*Z*BJo1(p)*H1(p,n,1) - BJDo1(p)*E1(p,n,1))/kw(m);
				CFN1 = PI*k*ro1*cpow(I,p)*(I*Z*BJo1(p)*H1(p,n,2) - BJDo1(p)*E1(p,n,2))/kw(m);

				// Expressions for 2nd rod
				CFP2 = PI*k*ro4*cpow(I,p)*(I*Z*BJo2(p)*H2(p,n,1) - BJDo2(p)*E2(p,n,1))/kw(m);
				CFN2 = PI*k*ro4*cpow(I,p)*(I*Z*BJo2(p)*H2(p,n,2) - BJDo2(p)*E2(p,n,2))/kw(m);
				//==============================================================

				S11(m,n) -= (ZO1(m)*(XO1(m)*PH(p,m) - XOI1(m)*PHI(p,m)))*CFP1;
				S11(m,n) -= (ZO2(m)*(XO2(m)*PH(p,m) - XOI2(m)*PHI(p,m)))*CFP2;

				S12(m,n) -= (ZO1(m)*(XO1(m)*PH(p,m) - XOI1(m)*PHI(p,m)))*CFN1;
				S12(m,n) -= (ZO2(m)*(XO2(m)*PH(p,m) - XOI2(m)*PHI(p,m)))*CFN2;

				if(p%2==0){
					S21(m,n) -= (ZOI1(m)*(XO1(m)*PHI(p,m) - XOI1(m)*PH(p,m)))*CFP1;
					S21(m,n) -= (ZOI2(m)*(XO2(m)*PHI(p,m) - XOI2(m)*PH(p,m)))*CFP2;

					S22(m,n) -= (ZOI1(m)*(XO1(m)*PHI(p,m) - XOI1(m)*PH(p,m)))*CFN1;
					S22(m,n) -= (ZOI2(m)*(XO2(m)*PHI(p,m) - XOI2(m)*PH(p,m)))*CFN2;
				}else{
					S21(m,n) += (ZOI1(m)*(XO1(m)*PHI(p,m) - XOI1(m)*PH(p,m)))*CFP1;
					S21(m,n) += (ZOI2(m)*(XO2(m)*PHI(p,m) - XOI2(m)*PH(p,m)))*CFP2;

					S22(m,n) += (ZOI1(m)*(XO1(m)*PHI(p,m) - XOI1(m)*PH(p,m)))*CFN1;
					S22(m,n) += (ZOI2(m)*(XO2(m)*PHI(p,m) - XOI2(m)*PH(p,m)))*CFN2;
				}

			}
		}
	}


}
