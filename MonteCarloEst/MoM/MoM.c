#include<stdio.h>
#include<math.h>
#include<complex.h>


typedef _Complex double cdouble;

struct DataA{
	cdouble ep1;
	double 	 la;
	double 	ro1;
	double 	xo1;
	double 	zo1;
};

struct DataB{
	cdouble ep1;
	cdouble ep2;
	double 	 la;
	double 	ro1;
	double 	ro2;
	double 	xo1;
	double 	xo2;
	double 	zo1;
	double 	zo2;
};

struct DataC{
	cdouble ep1;
	cdouble ep2;
	cdouble ep3;

	double 	 la;

	double 	ro1;
	double 	ro2;
	double 	ro3;

	double 	xo1;
	double 	xo2;
	double 	xo3;

	double 	zo1;
	double 	zo2;
	double 	zo3;
};

struct DataD{
	cdouble ep1;
	cdouble ep2;
	cdouble ep3;
	cdouble ep4;
	cdouble ep5;
	cdouble ep6;

	double 	 la;

	double 	ro1;
	double 	ro2;
	double 	ro3;
	double 	ro4;
	double 	ro5;
	double 	ro6;

	double 	xo1;
	double 	xo2;
	double 	xo3;
	double 	xo4;
	double 	xo5;
	double 	xo6;

	double 	zo1;
	double 	zo2;
	double 	zo3;
	double 	zo4;
	double 	zo5;
	double 	zo6;
};

//--------------------------------------------------------------------------------------------------

void MoM_single_solid_rod(cdouble *epv, double la, double *rov, double *xov, double *zov, cdouble*, cdouble*, cdouble*, cdouble*, const int, const int);
void MoM_two_solid_rods(cdouble *epv, double la, double *rov, double *xov, double *zov, cdouble*, cdouble*, cdouble*, cdouble*, const int, const int);
void MoM_two_in_one(cdouble *epv, double la, double *rov, double *xov, double *zov, cdouble*, cdouble*, cdouble*, cdouble*, const int, const int);
void MoM_two_in_two(cdouble *epv, double la, double *rov, double *xov, double *zov, cdouble*, cdouble*, cdouble*, cdouble*, const int, const int);

//--------------------------------------------------------------------------------------------------

void funa(cdouble * SMN[], struct DataB dat, const int N, const int MN){

	MoM_single_solid_rod(&dat.ep1, dat.la, &dat.ro1, &dat.xo1, &dat.zo1, SMN[0], SMN[1], SMN[2], SMN[3], N, MN);

}
void funb(cdouble * SMN[], struct DataB dat, const int N, const int MN){

	MoM_two_solid_rods(&dat.ep1, dat.la, &dat.ro1, &dat.xo1, &dat.zo1, SMN[0], SMN[1], SMN[2], SMN[3], N, MN);

}
void func(cdouble * SMN[], struct DataC dat, const int N, const int MN){

	MoM_two_in_one(&dat.ep1, dat.la, &dat.ro1, &dat.xo1, &dat.zo1, SMN[0], SMN[1], SMN[2], SMN[3], N, MN);

}
void fund(cdouble * SMN[], struct DataD dat, const int N, const int MN){

	MoM_two_in_two(&dat.ep1, dat.la, &dat.ro1, &dat.xo1, &dat.zo1, SMN[0], SMN[1], SMN[2], SMN[3], N, MN);

}

