//#include<complex>
#include<complex.h>
#include<math.h>
#include<stdio.h>


#define PI 3.1415926535897931


#define BJo1(m)  BJo1[m+N+1]
#define BHo1(m)  BHo1[m+N+1]
#define BJoD1(m) BJoD1[m+N]
#define BHoD1(m) BHoD1[m+N]

#define BJp1(m)  BJp1[m+N+1]
#define BHp1(m)  BHp1[m+N+1]
#define BJpD1(m) BJpD1[m+N]
#define BHpD1(m) BHpD1[m+N]

#define EHo1(m)  EHo1[m+N]

#define EX1(m,n,l)  EX[m+N + (n-1)*WN + (l-1)*WN*NM]

#define kw(m)   kw[m-1]
#define phk(m)  phk[m-1]

#define  XO1(m)   XO1[m-1]
#define XOI1(m)  XOI1[m-1]

#define  ZO1(m)   ZO1[m-1]
#define ZOI1(m)  ZOI1[m-1]

#define  PH(m,n)   PH[m+N + (n-1)*WN]
#define PHI(m,n)   PHI[m+N + (n-1)*WN]

#define S11(m,n)   S11[m-1 + (n-1)*NM]
#define S21(m,n)   S21[m-1 + (n-1)*NM]
#define S12(m,n)   S12[m-1 + (n-1)*NM]
#define S22(m,n)   S22[m-1 + (n-1)*NM]

#define res11_p(m)  res11_p[2*N+m]
#define res11_n(m)  res11_n[2*N+m]

#define SM11(m,n)   SM[m+N + (n+N)*WN]

#define cdouble _Complex double

//typedef std::complex<double> cdouble;
//cdouble I = std::complex<double>(0.0,1.0);


extern "C" void MoM_single_solid_rod(cdouble *epv, double la, double *rov, double *xov, double *zov, cdouble*, cdouble*, cdouble*, cdouble*, const int N, const int NM);


void besseljCPX(int, cdouble, cdouble *, int);
void besselyCPX(int, cdouble, cdouble *, int);


double expint(double x, int n, char mode);

void  exp_exp_int_ss(double * la, double x, double len, const int M, const int LAS, cdouble * res);
void  exp_exp_int(double * la, double x, double y, double len, const int M, const int LAS, cdouble * res);

extern "C" void zgesv_(int*,int*,cdouble*,int*,int*,cdouble*,int*,int*);

//------------------------------------------------------------------------------------------------------
void MoM_single_solid_rod(cdouble *epv, double la, double *rov, double *xov, double *zov, cdouble * S11, cdouble * S12, cdouble * S21, cdouble * S22, const int N, const int NM){

	const int 	L = 2*N+1;
	int 		nrhs = NM*2;
	int 		info;
	int  		pivot[(4*N+2)];
	const int 	WN = 2*N+1;
	int 		M = WN;

	double 		Z = 120.0*PI;
	double 		k;

	cdouble ep1;
	double  ro1;
	double  xo1;
	double  zo1;

	ep1 = epv[0];
	ro1 = rov[0];
	xo1 = xov[0];
	zo1 = zov[0];

	cdouble  kw[NM], phk[NM];

	cdouble  ZO1[NM], ZOI1[NM];
	cdouble  XO1[NM], XOI1[NM];

	cdouble  BJo1[WN+2], BJp1[WN+2], BJoD1[WN], BJpD1[WN];
	cdouble  BHo1[WN+2], BHp1[WN+2], BHoD1[WN], BHpD1[WN];

	cdouble  SM[WN*WN];

	cdouble  res11_p[4*N+1], res11_n[4*N+1];

	cdouble  PH[WN*NM];
	cdouble  PHI[WN*NM];

	cdouble  EX[2*WN*NM];

	cdouble  EHo1[WN];
	cdouble  EHo2[WN];

	cdouble  E11o, H11o;
	cdouble  E12o, H12o;
	cdouble  E21o, H21o;
	cdouble  E22o, H22o;

	cdouble CFP1;
	cdouble CFN1;

//	xo1 += 0.5;	// change the coordinae system
//	xo2 += 0.5;
    //----------------------------------------------------------------------------

	ep1 = csqrt(ep1);

	for(int m=1; m<=NM; ++m){
		XO1(m)  = cexp(I*static_cast<double>(m)*PI*xo1);
		XOI1(m) = 1.0/XO1(m);
	}

	exp_exp_int_ss(&la,     0.0, 2.0, 2*N, 1, res11_n);
	exp_exp_int_ss(&la, 2.0*xo1, 2.0, 2*N, 1, res11_p);

     //============================================================

	k = 2.0*PI*la;

	besseljCPX(0, 	  k*ro1, &BJo1[N+1], N+2);
	besseljCPX(0, ep1*k*ro1, &BJp1[N+1], N+2);
	besselyCPX(0,	  k*ro1, &BHo1[N+1], N+2);
	besselyCPX(0, ep1*k*ro1, &BHp1[N+1], N+2);

	for(int n=0;n<=N+1;++n){
		BHo1(n) = BJo1(n) + I*BHo1(n);
		BHp1(n) = BJp1(n) + I*BHp1(n);
	}
	for(int n=1;n<=N+1;++n){
		BJo1(-n) = pow(-1.0,n)*BJo1(n);
		BJp1(-n) = pow(-1.0,n)*BJp1(n);
		BHo1(-n) = pow(-1.0,n)*BHo1(n);
		BHp1(-n) = pow(-1.0,n)*BHp1(n);
	}
	for(int n=-N;n<=N;++n){
		BJoD1(n) = 0.5*(BJo1(n-1) - BJo1(n+1));
		BJpD1(n) = 0.5*(BJp1(n-1) - BJp1(n+1));

		BHoD1(n) = 0.5*(BHo1(n-1) - BHo1(n+1));
		BHpD1(n) = 0.5*(BHp1(n-1) - BHp1(n+1));
	}

	for(int m=1; m<=NM; ++m){
		if(m==1){
			kw(1)  = sqrt(k*k - PI*PI);
			phk(1) = atan2(PI, creal(kw(1)));
		}else{
			kw(m) = I*sqrt(PI*PI*m*m - k*k);
			double kxz = cabs(static_cast<double>(m)*PI/kw(m));
			phk(m) = 0.5*I*log(fabs((1.0-kxz)/(1.0+kxz)));
			if(kxz > 1.0){
				phk(m) += PI/2.0;
			}
		}

		ZO1(m)  = cexp(I*kw(m)*zo1);
		ZOI1(m) = 1.0/ZO1(m);
	}
	for(int n=-N; n<=N; ++n){
		for(int m=1; m<=NM; ++m){
			PH(n,m)  = cexp(I*static_cast<double>(n)*phk(m));
			PHI(n,m) = 1.0/PH(n,m);

			EX1(n,m,1) = -I*(cpow( I,n)*0.5*BJo1(n)*ZO1(m)*(XO1(m)*PHI(n,m) - XOI1(m)*PH(n,m)));
			EX1(n,m,2) = -I*(cpow(-I,n)*0.5*BJo1(n)*ZOI1(m)*(XO1(m)*PH(n,m) - XOI1(m)*PHI(n,m)));
		}
		EHo1(n) = BJpD1(n)/BJp1(n);
	}

	for(int m=-N;m<=N;++m){
		for(int n=-N;n<=N;++n){
			if(n%2==0){
				H11o = BJo1(m)*(res11_n(m-n) - res11_p(n+m))*BJo1(n);
				E11o = BJo1(m)*(res11_n(m-n) - res11_p(n+m))*BJoD1(n);
			}else{
				H11o = BJo1(m)*(res11_n(m-n) + res11_p(n+m))*BJo1(n);
				E11o = BJo1(m)*(res11_n(m-n) + res11_p(n+m))*BJoD1(n);
			}
			if(m==n){
				SM11(m,n) = 1.0 + PI/2.0/I*ro1*k*((E11o + BHo1(m)*BJoD1(m)) - ep1*(H11o + BHo1(m)*BJo1(m))*EHo1(n));
			}else{
				SM11(m,n) = PI/2.0/I*ro1*k*(E11o - ep1*H11o*EHo1(n));
			}
		}
	}

	zgesv_(&M, &nrhs, SM, &M, pivot, EX, &M, &info);

	for(int m=1; m<=NM; ++m){
		for(int n=1; n<=NM; ++n){
			if(m==n){
				S11(m,n) = 0.0;
				S21(m,n) = 1.0;
				S12(m,n) = 1.0;
				S22(m,n) = 0.0;
			}else{
				S11(m,n) = 0.0;
				S21(m,n) = 0.0;
				S12(m,n) = 0.0;
				S22(m,n) = 0.0;
			}
			for(int p=-N; p<=N; ++p){

				CFP1 = PI*k*ro1*cpow(I,p)*(BJo1(p)*ep1*EHo1(p) - BJoD1(p))*EX1(p,n,1)/kw(m);
				CFN1 = PI*k*ro1*cpow(I,p)*(BJo1(p)*ep1*EHo1(p) - BJoD1(p))*EX1(p,n,2)/kw(m);
			//==========================================================
				S11(m,n) -= (ZO1(m)*(XO1(m)*PH(p,m) - XOI1(m)*PHI(p,m)))*CFP1;

				S12(m,n) -= (ZO1(m)*(XO1(m)*PH(p,m) - XOI1(m)*PHI(p,m)))*CFN1;

				if(p%2==0){
					S21(m,n) -= (ZOI1(m)*(XO1(m)*PHI(p,m) - XOI1(m)*PH(p,m)))*CFP1;

					S22(m,n) -= (ZOI1(m)*(XO1(m)*PHI(p,m) - XOI1(m)*PH(p,m)))*CFN1;
				}else{
					S21(m,n) += (ZOI1(m)*(XO1(m)*PHI(p,m) - XOI1(m)*PH(p,m)))*CFP1;

					S22(m,n) += (ZOI1(m)*(XO1(m)*PHI(p,m) - XOI1(m)*PH(p,m)))*CFN1;
				}

			}
		}
	}
}


