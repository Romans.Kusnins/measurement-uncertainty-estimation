import os
import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *
from Derivatives import *

try:
        matplot = True
        from matplotlib import pyplot as plt
except ModuleNotFoundError:
        print("The matplotlib module is not installed.")
        matplot = False


from plot2pdf import *

from Uncertainty_vs_S11 import *

#=======================================================================================
#=======================================================================================
#model = 'single-slab-guide'
#model  = 'two-slabs-guide'
#model = 'three-slabs-guide'
#model = 'three-slabs-free'
#model = 'single-slab-free'
#model = 'two-slabs-free'
#model = 'singlerod'
model = 'tworods'
#model = 'threerods'
#model = 'slab-rod'
#model = 'rod-slab'
#model = 'holed-solid'
#==================================================================
#task 		= 'sensitivity'
#task 		= 'standard'
task 		= 'total'

#sensparam 	= 'S11abs'
#sensparam 	= 'ro2'
sensparam 	= 'd1'
#sensparam 	= 'd2'
#sensparam 	= 'ep1'
#sensparam 	= 'tan1'
#sensparam 	= 'tan2'
#sensparam 	= 'a'
#sensparam 	= 'L'
#sensparam 	= 'xo1'
#sensparam 	= 'xo2'

#PREFIX 	= '_20_'
#PREFIX 	= '_29_'
#PREFIX 	= '_3_8_'
PREFIX 	= ''
#==================================================================
NM  = 3		# number of higher order waveguide modes considered
N   = 5		# number of basis functions for each rod surface

delta	= 1e-4	# increament for finite difference approximation

TOL 	= 1e-6
MAXIT 	= 20

isPDF 	= False
isPRINT	= False
#===================================================================
ylabeldelta 	= 'Standard uncertainty associated with'
ylabelsens	= 'Model sensitivity to'

xlabels	= dict()

xlabels['ep1'] 	= 'Dielectric constant of the auxiliary slab'
xlabels['ep2'] 	= 'Dielectric constant of MUT'
xlabels['d1']  	= 'Thickness of the auxiliary slab'
xlabels['d2']  	= 'Thickness of the MUT slab'
xlabels['ro1']  = 'Radius of the auxiliary rod'
xlabels['ro2']  = 'Radius of the MUT rod'
xlabels['L']    = 'Separation between the auxiliary slab and MUT slab'
#===================================================================

modeldata = dict()
sigma = dict()


if model == 'single-slab-guide':

#	modeldata['ep1']        = 10.2
	modeldata['ep1']        = [1.0, 20.0, 0.1]
	modeldata['tan1']       = 0.0023

	modeldata['d1']         = 2.5

	modeldata['f']          = 10.0
	modeldata['a']          = 22.86

	sigma['S11abs']         = S11fun

	sigma['d1']             = 0.01

	sigma['a']              = 0.01
	sigma['f']              = modeldata['f']*7.0e-6/2.0

	sigma['tan1']           = modeldata['tan1']*0.05
	#=======================================================================
	paramname		= {'S11abs': '$S_{11}$', 'd1': '$d_{1}$', 'ep1': '$\\varepsilon_{\\mathrm{r}}$', 'tan1': '$ \\tan{\\delta}$', 'a': 'a', 'f': 'f'}
	#=================================================================
	list_found = False
	for param in modeldata.keys():
		if type(modeldata[param]) is list:
			if len(modeldata[param]) > 1 and not list_found:
				var_range = modeldata[param]
				var_param = param
				list_found = True
			else:
				modeldata[param] = modeldata[param][0]

	if list_found:
		var_values 	= np.arange(*var_range)
	else:
		var_param	= 'ep1'
		var_values 	= np.asarray([modeldata['ep1']])
	#=================================================================
	std_delta = dict()	# standard uncertainties
	for param in sigma.keys():
		std_delta[param] = np.empty((var_values.shape))

	mod_sens = dict()	# sensitivity coefficients
	for param in sigma.keys():
		mod_sens[param] = np.empty((var_values.shape))

	tot_delta = np.empty((var_values.shape))	# total uncertainty
	#=================================================================
	# EVALUATE SENSITIVITY COEFFICIENTS, AS WELL AS UNCERTAINTIES
	#=================================================================
	for n in range(var_values.shape[0]):
		modeldata[var_param] = var_values[n]

		tot_delta[n] = 0.0

		for param in sigma.keys():
			mod_sens[param][n], S11abs = Derivative_single_slab_guide(modeldata, param, delta, TOL, MAXIT)

		for param in sigma.keys():
			if param == 'S11abs' and callable(sigma['S11abs']):
				sigma['S11abs'] = sigma['S11abs'](S11abs)
			std_delta[param][n] = np.abs(mod_sens[param][n]*sigma[param])
			tot_delta[n] += POW2(std_delta[param][n])

		tot_delta[n] = np.sqrt(tot_delta[n])

elif model == 'two-slabs-guide':

	modeldata['ep1']        = 4.3
	modeldata['tan1']       = 0.003

	modeldata['ep2']        = 10.2
	#modeldata['ep2']       = [1.0, 20.0, 0.1]
	modeldata['tan2']       = 0.0023

	modeldata['d1']         = 3.8
	#modeldata['d1']        = [1.0,10.0,0.1]
	modeldata['d2']         = 2.5
#	modeldata['L']          = 20.0
	modeldata['L']         = [1.0, 30.0, 0.1]
	modeldata['f']          = 10.0
	modeldata['a']          = 22.86

	sigma['S11abs']         = S11fun
	#sigma['S11abs']        = 0.01

	sigma['d1']             = 0.01
	sigma['d2']             = 0.01
	sigma['L']              = 0.01
	sigma['a']              = 0.01
	sigma['f']              = modeldata['f']*7.0e-6/2.0

	sigma['ep1']            = modeldata['ep1']*0.01
	sigma['tan1']           = 5.0e-6

	sigma['tan2']           = modeldata['tan2']*0.05

	#=======================================================================
	paramname		= {'S11abs': '$S_{11}$', 'd1': '$d_{1}$', 'd2': '$d_{2}$', 'L': 'L', 'ep1': '$\\varepsilon_{\\mathrm{r}}$', 'tan1': '$ \\tan{\\delta}$', 'tan2': '$\\tan{\\delta}$', 'a': 'a', 'f': 'f'}
	#=================================================================
	list_found = False
	for param in modeldata.keys():
		if type(modeldata[param]) is list:
			if len(modeldata[param]) > 1 and not list_found:
				var_range = modeldata[param]
				var_param = param
				list_found = True
			else:
				modeldata[param] = modeldata[param][0]

	if list_found:
		var_values 	= np.arange(*var_range)
	else:
		var_param	= 'ep1'
		var_values 	= np.asarray([modeldata['ep1']])
	#=================================================================
	std_delta = dict()	# standard uncertainties
	for param in sigma.keys():
		std_delta[param] = np.empty((var_values.shape))

	mod_sens = dict()	# sensitivity coefficients
	for param in sigma.keys():
		mod_sens[param] = np.empty((var_values.shape))

	tot_delta = np.empty((var_values.shape))	# total uncertainty
	#=================================================================
	# EVALUATE SENSITIVITY COEFFICIENTS, AS WELL AS UNCERTAINTIES
	#=================================================================
	for n in range(var_values.shape[0]):
		modeldata[var_param] = var_values[n]

		tot_delta[n] = 0.0

		for param in sigma.keys():
			mod_sens[param][n], S11abs = Derivative_two_slabs_guide(modeldata, param, delta, TOL, MAXIT)

		for param in sigma.keys():
			if param == 'S11abs' and callable(sigma['S11abs']):
				sigma['S11abs'] = sigma['S11abs'](S11abs)
			std_delta[param][n] = np.abs(mod_sens[param][n]*sigma[param])
			tot_delta[n] += POW2(std_delta[param][n])

		tot_delta[n] = np.sqrt(tot_delta[n])
#======================================================================================
elif model == 'three-slabs-guide':

	modeldata['ep1']	= 4.3			# auxiliary slab dielectric constant
	modeldata['tan1']	= 0.01			# auxiliary slab loss tangent

	modeldata['ep2']	= 10.2			# middle slab dielectric constant
#	modeldata['ep2']	= [1.0, 20.0, 0.01]	# middle slab dielectric constant
	modeldata['tan2']	= 0.01			# auxiliary slab loss tangent

	modeldata['epm']	= 2.2			# MUT slab dielectric constant
	modeldata['tanm']	= 0.01			# auxiliary slab loss tangent

	modeldata['d1']		= 5.0			# auxiliary slab thickness in mm
	modeldata['d2']		= 2.5			# MUT slab thickness in mm
#	modeldata['L']		= 22.0			# middle slab thickness in mm
	modeldata['L']		= [10.0, 30.0, 0.025]	# middle slab thickness in mm
	modeldata['f']		= 10.0			# operating frequency in GHz
	modeldata['a']		= 22.86			# waveguide width in mm

	sigma['S11abs']    	= 0.01
	sigma['d1']        	= 0.015
	sigma['d2']        	= 0.015
	sigma['L']         	= 0.015
	sigma['a']         	= 0.015
	sigma['f']         	= 0.001

	sigma['ep1']         	= modeldata['ep1']*0.01
	sigma['tan1']         	= modeldata['tan1']*0.05

	sigma['epm']         	= modeldata['epm']*0.01
	sigma['tanm']         	= modeldata['tanm']*0.05

	sigma['tan2']         	= modeldata['tan2']*0.05
	#=======================================================================
	paramname		= {'S11abs': '$S_{11}$', 'd1': '$d_{1}$', 'd2': '$d_{2}$', 'L': 'L', 'ep1': '$\\varepsilon_{\\mathrm{r}}$', 'tan1': '$ \\tan{\\delta}$', 'ep1': '$\\varepsilon_{\\mathrm{r}}$', 'tan1': '$ \\tan{\\delta}$', 'tan2': '$\\tan{\\delta}$', 'a': 'a'}
	#=================================================================
	list_found = False
	for param in modeldata.keys():
		if type(modeldata[param]) is list:
			if len(modeldata[param]) > 1 and not list_found:
				var_range = modeldata[param]
				var_param = param
				list_found = True
			else:
				modeldata[param] = modeldata[param][0]

	if list_found:
		var_values 	= np.arange(*var_range)
	else:
		var_param	= 'ep1'
		var_values 	= np.asarray([modeldata['ep1']])
	#=================================================================
	std_delta = dict()	# standard uncertainties
	for param in sigma.keys():
		std_delta[param] = np.empty((var_values.shape))

	mod_sens = dict()	# sensitivity coefficients
	for param in sigma.keys():
		mod_sens[param] = np.empty((var_values.shape))

	tot_delta = np.empty((var_values.shape))	# total uncertainty
	#=================================================================
	# EVALUATE SENSITIVITY COEFFICIENTS, AS WELL AS UNCERTAINTIES
	#=================================================================
	for n in range(var_values.shape[0]):
		modeldata[var_param] = var_values[n]

		tot_delta[n]	= 0.0

		for param in sigma.keys():
			mod_sens[param][n], S11abs = Derivative_single_slab_guide(modeldata, param, delta, TOL, MAXIT)

		for param in sigma.keys():
			if param == 'S11abs' and callable(sigma['S11abs']):
				sigma['S11abs'] = sigma['S11abs'](S11abs)
			std_delta[param][n] = np.abs(mod_sens[param][n]*sigma[param])
			tot_delta[n] += POW2(std_delta[param][n])

		tot_delta[n] = np.sqrt(tot_delta[n])
#======================================================================================
elif model == 'single-slab-free':

	modeldata['ep1']        = [1.0, 140.0, 0.1]
#	modeldata['ep1']        = 30.0
	modeldata['tan1']       = 1.0/15000.0

	modeldata['d1']         = 2.3
	modeldata['f']          = 10.0

	sigma['S11abs']         = S11fun
	sigma['d1']             = 0.01
	sigma['f']              = modeldata['f']*7.0e-6/2.0

	sigma['tan1']           = modeldata['tan1']*0.05
	#=======================================================================
	paramname		= {'S11abs': '$S_{11}$', 'd1': '$d_{1}$', 'd2': '$d_{2}$', 'L': 'L', 'ep1': '$\\varepsilon_{\\mathrm{r}}$', 'tan1': '$ \\tan{\\delta}$', 'tan2': '$\\tan{\\delta}$', 'f': 'f'}
	#=================================================================
	list_found = False
	for param in modeldata.keys():
		if type(modeldata[param]) is list:
			if len(modeldata[param]) > 1 and not list_found:
				var_range = modeldata[param]
				var_param = param
				list_found = True
			else:
				modeldata[param] = modeldata[param][0]

	if list_found:
		var_values 	= np.arange(*var_range)
	else:
		var_param	= 'ep1'
		var_values 	= np.asarray([modeldata['ep1']])
	#=================================================================
	std_delta = dict()	# standard uncertainties
	for param in sigma.keys():
		std_delta[param] = np.empty((var_values.shape))

	mod_sens = dict()	# sensitivity coefficients
	for param in sigma.keys():
		mod_sens[param] = np.empty((var_values.shape))

	tot_delta = np.empty((var_values.shape))	# total uncertainty
	#=================================================================
	# EVALUATE SENSITIVITY COEFFICIENTS, AS WELL AS UNCERTAINTIES
	#=================================================================
	for n in range(var_values.shape[0]):
		modeldata[var_param] = var_values[n]

		tot_delta[n]	= 0.0

		for param in sigma.keys():
			mod_sens[param][n], S11abs = Derivative_single_slab_free_space(modeldata, param, delta, TOL, MAXIT)

		for param in sigma.keys():
			if param == 'S11abs' and callable(sigma['S11abs']):
				sigma['S11abs'] = sigma['S11abs'](S11abs)
			std_delta[param][n] = np.abs(mod_sens[param][n]*sigma[param])
			tot_delta[n] += POW2(std_delta[param][n])

		tot_delta[n] = np.sqrt(tot_delta[n])

elif model == 'two-slabs-free':

	modeldata['ep1']        = 10.2
	modeldata['tan1']       = 0.0023

#	modeldata['ep2']        = [1.0, 40.0, 0.1]
	modeldata['ep2']        = 30.0
	modeldata['tan2']       = 1.0/15000.0

	modeldata['d1']         = 2.6
	#modeldata['d1']         = [2.0, 15.0, 0.1]
	modeldata['d2']         = 2.0
	modeldata['L']          = 13.1
	modeldata['L']          = [1.0, 31.0, 0.1]
	modeldata['f']          = 10.0

	sigma['S11abs']         = S11fun
	sigma['d1']             = 0.01
	sigma['d2']             = 0.01

	sigma['L']              = 0.01

	sigma['f']              = modeldata['f']*7.0e-6/2.0

	sigma['ep1']            = modeldata['ep1']*0.01
	sigma['tan1']           = 5.0e-5

	sigma['tan2']           = modeldata['tan2']*0.05

	#=======================================================================
	paramname		= {'S11abs': '$S_{11}$', 'd1': '$d_{1}$', 'd2': '$d_{2}$', 'L': 'L', 'ep1': '$\\varepsilon_{\\mathrm{r}}$', 'tan1': '$ \\tan{\\delta}$', 'tan2': '$\\tan{\\delta}$', 'f': 'f'}
	#=================================================================
	list_found = False
	for param in modeldata.keys():
		if type(modeldata[param]) is list:
			if len(modeldata[param]) > 1 and not list_found:
				var_range = modeldata[param]
				var_param = param
				list_found = True
			else:
				modeldata[param] = modeldata[param][0]

	if list_found:
		var_values 	= np.arange(*var_range)
	else:
		var_param	= 'ep1'
		var_values 	= np.asarray([modeldata['ep1']])
	#=================================================================
	std_delta = dict()	# standard uncertainties
	for param in sigma.keys():
		std_delta[param] = np.empty((var_values.shape))

	mod_sens = dict()	# sensitivity coefficients
	for param in sigma.keys():
		mod_sens[param] = np.empty((var_values.shape))

	tot_delta = np.empty((var_values.shape))	# total uncertainty
	#=================================================================
	# EVALUATE SENSITIVITY COEFFICIENTS, AS WELL AS UNCERTAINTIES
	#=================================================================
	for n in range(var_values.shape[0]):
		modeldata[var_param] = var_values[n]

		tot_delta[n]	= 0.0

		for param in sigma.keys():
			mod_sens[param][n], S11abs = Derivative_two_slabs_free_space(modeldata, param, delta, TOL, MAXIT)

		for param in sigma.keys():
			if param == 'S11abs' and callable(sigma['S11abs']):
				sigma['S11abs'] = sigma['S11abs'](S11abs)
			std_delta[param][n] = np.abs(mod_sens[param][n]*sigma[param])
			tot_delta[n] += POW2(std_delta[param][n])

		tot_delta[n] = np.sqrt(tot_delta[n])
#======================================================================================
elif model == 'three-slabs-free':

	modeldata['ep1']        = 10.2
	modeldata['tan1']       = 0.0023

	modeldata['ep2']        = [1.0, 50.0, 0.1]
#	modeldata['ep2']        = 30.0
	modeldata['tan2']       = 1.0/15000.0

	modeldata['epm']        = 2.2
	modeldata['tanm']       = 0.0009

	modeldata['d1']         = 7.5
#	modeldata['d1']         = 5.7
#	modeldata['d1']         = 6.0
	#modeldata['d1']        = [4.0,6.0,0.025]
	modeldata['d2']         = 2.3
	modeldata['L']		= 9.6
#	modeldata['L']          = 29.0
#	modeldata['L']          = [10.0, 40.0, 0.05]
	modeldata['f']          = 10.0
	modeldata['a']          = 22.86

	sigma['S11abs']         = S11fun

	sigma['d1']             = 0.01
	sigma['d2']             = 0.01
	sigma['L']              = 0.01
	sigma['f']              = modeldata['f']*7.0e-6/2.0
	sigma['ep1']            = modeldata['ep1']*0.01
	sigma['tan1']           = 5.0e-5

	sigma['tan2']           = modeldata['tan2']*0.05

	sigma['epm']            = modeldata['epm']*0.01
	sigma['tanm']           = 5.0e-5
	#=======================================================================
	paramname		= {'S11abs': '$S_{11}$', 'd1': '$d_{1}$', 'd2': '$d_{2}$', 'L': 'L', 'ep1': '$\\varepsilon_{\\mathrm{r}}$', 'tan1': '$ \\tan{\\delta}$', 'ep1': '$\\varepsilon_{\\mathrm{r}}$', 'tan1': '$ \\tan{\\delta}$', 'tan2': '$\\tan{\\delta}$', 'f': 'f'}
	#=======================================================================
	list_found = False
	for param in modeldata.keys():
		if type(modeldata[param]) is list:
			if len(modeldata[param]) > 1 and not list_found:
				var_range = modeldata[param]
				var_param = param
				list_found = True
			else:
				modeldata[param] = modeldata[param][0]

	if list_found:
		var_values 	= np.arange(*var_range)
	else:
		var_param	= 'ep1'
		var_values 	= np.asarray([modeldata['ep1']])
	#=================================================================
	std_delta = dict()	# standard uncertainties
	for param in sigma.keys():
		std_delta[param] = np.empty((var_values.shape))

	mod_sens = dict()	# sensitivity coefficients
	for param in sigma.keys():
		mod_sens[param] = np.empty((var_values.shape))

	tot_delta = np.empty((var_values.shape))	# total uncertainty
	#=================================================================
	# EVALUATE SENSITIVITY COEFFICIENTS, AS WELL AS UNCERTAINTIES
	#=================================================================
	for n in range(var_values.shape[0]):
		modeldata[var_param] = var_values[n]

		tot_delta[n]	= 0.0

		for param in sigma.keys():
			mod_sens[param][n], S11abs = Derivative_three_slabs_free_space(modeldata, param, delta, TOL, MAXIT)

		for param in sigma.keys():
			if param == 'S11abs' and callable(sigma['S11abs']):
				sigma['S11abs'] = sigma['S11abs'](S11abs)
			std_delta[param][n] = np.abs(mod_sens[param][n]*sigma[param])
			tot_delta[n] += POW2(std_delta[param][n])

		tot_delta[n] = np.sqrt(tot_delta[n])
#===============================================================
elif model == 'singlerod':

	modeldata['ep1']        = [1.0, 20.0, 0.1]
#	modeldata['ep1']        = 10.2
	modeldata['tan1']       = 0.0023

	modeldata['ro1']        = 2.5
	modeldata['xo1']        = 0.0

	modeldata['f']          = 10.0
	modeldata['a']          = 22.86


	sigma['S11abs']         = S11fun

	sigma['ro1']            = 0.01          	# corresponds to 0.02mm expanded uncertainty
	sigma['xo1']            = 0.01

	sigma['a']              = 0.01
	sigma['f']              = modeldata['f']*7.0e-6/2.0     # corresponds to 7 ppm accuracy

	sigma['tan1']           = modeldata['tan1']*0.05
	#=======================================================================
	paramname		= {'S11abs': '$S_{11}$', 'ro1': '$r_{1}$', 'xo1': '$x_{\\mathrm{o,1}}$', 'ro2': '$r_{2}$', 'xo2': '$x_{\\mathrm{o,2}}$', 'L': 'L', 'ep1': '$\\varepsilon_{\\mathrm{r}}$', 'tan1': '$ \\tan{\\delta}$', 'tan2': '$\\tan{\\delta}$', 'a': 'a', 'f': 'f'}
	#=================================================================
	list_found = False
	for param in modeldata.keys():
		if type(modeldata[param]) is list:
			if len(modeldata[param]) > 1 and not list_found:
				var_range = modeldata[param]
				var_param = param
				list_found = True
			else:
				modeldata[param] = modeldata[param][0]

	if list_found:
		var_values 	= np.arange(*var_range)
	else:
		var_param	= 'ep1'
		var_values 	= np.asarray([modeldata['ep1']])
	#=================================================================
	std_delta = dict()	# standard uncertainties
	for param in sigma.keys():
		std_delta[param] = np.empty((var_values.shape))

	mod_sens = dict()	# sensitivity coefficients
	for param in sigma.keys():
		mod_sens[param] = np.empty((var_values.shape))

	tot_delta = np.empty((var_values.shape))	# total uncertainty
	#=================================================================
	# EVALUATE SENSITIVITY COEFFICIENTS, AS WELL AS UNCERTAINTIES
	#=================================================================
	for n in range(var_values.shape[0]):
		modeldata[var_param] = var_values[n]

		tot_delta[n]	= 0.0

		for param in sigma.keys():
			mod_sens[param][n], S11abs = Derivative_single_rod(modeldata, param, delta, TOL, MAXIT, NM, N)

		for param in sigma.keys():
			if param == 'S11abs' and callable(sigma['S11abs']):
				sigma['S11abs'] = sigma['S11abs'](S11abs)
			std_delta[param][n] = np.abs(mod_sens[param][n]*sigma[param])
			tot_delta[n] += POW2(std_delta[param][n])

		tot_delta[n] = np.sqrt(tot_delta[n])
#===============================================================
elif model == 'tworods':

	modeldata['ep1']        = 4.3
	modeldata['tan1']       = 0.003

	modeldata['ep2']        = [1.0, 20.0, 0.1]
#	modeldata['ep2']        = 10.2
	modeldata['tan2']       = 0.0023

	modeldata['ro1']        = 5.7
#	modeldata['ro1']        = 5.8
#	modeldata['ro1']        = 5.8
#	modeldata['ro1']        = 6.5
	modeldata['xo1']        = 0.0
#	modeldata['ro1']        = [1.0, 10.0, 0.05]

	modeldata['ro2']        = 2.5
	modeldata['xo2']        = 0.0
	modeldata['L']          = 8.6
#	modeldata['L']          = [27.5]
#	modeldata['L']          = [20.0, 30.0, 0.02]

	modeldata['f']          = 10.0
	modeldata['a']          = 22.86


	sigma['S11abs']         = S11fun

	sigma['ro1']            = 0.01          	# corresponds to 0.02mm expanded uncertainty
	sigma['ro2']            = 0.01

	sigma['xo1']            = 0.01
	sigma['xo2']            = 0.01

	sigma['L']              = 0.01
	sigma['a']              = 0.01

	sigma['f']              = modeldata['f']*7.0e-6/2.0     # corresponds to 7 ppm accuracy

	sigma['ep1']            = modeldata['ep1']*0.01         # set according to Krupka 0.5%-2%
	sigma['tan1']           = 5.0e-5                        # also set according to Krupka

	sigma['tan2']           = modeldata['tan2']*0.05
	#=======================================================================
	paramname		= {'S11abs': '$S_{11}$', 'ro1': '$r_{1}$', 'xo1': '$x_{\\mathrm{o,1}}$', 'ro2': '$r_{2}$', 'xo2': '$x_{\\mathrm{o,2}}$', 'L': 'L', 'ep1': '$\\varepsilon_{\\mathrm{r}}$', 'tan1': '$ \\tan{\\delta}$', 'tan2': '$\\tan{\\delta}$', 'a': 'a', 'f': 'f'}
	#=================================================================
	list_found = False
	for param in modeldata.keys():
		if type(modeldata[param]) is list:
			if len(modeldata[param]) > 1 and not list_found:
				var_range = modeldata[param]
				var_param = param
				list_found = True
			else:
				modeldata[param] = modeldata[param][0]

	if list_found:
		var_values 	= np.arange(*var_range)
	else:
		var_param	= 'ep1'
		var_values 	= np.asarray([modeldata['ep1']])
	#=================================================================
	std_delta = dict()	# standard uncertainties
	for param in sigma.keys():
		std_delta[param] = np.empty((var_values.shape))

	mod_sens = dict()	# sensitivity coefficients
	for param in sigma.keys():
		mod_sens[param] = np.empty((var_values.shape))

	tot_delta = np.empty((var_values.shape))	# total uncertainty
	#=================================================================
	# EVALUATE SENSITIVITY COEFFICIENTS, AS WELL AS UNCERTAINTIES
	#=================================================================
	for n in range(var_values.shape[0]):
		modeldata[var_param] = var_values[n]

		tot_delta[n]	= 0.0

		for param in sigma.keys():
			mod_sens[param][n], S11abs = Derivative_two_rods(modeldata, param, delta, TOL, MAXIT, NM, N)

		for param in sigma.keys():
			if param == 'S11abs' and callable(sigma['S11abs']):
				sigma['S11abs'] = sigma['S11abs'](S11abs)
			std_delta[param][n] = np.abs(mod_sens[param][n]*sigma[param])
			tot_delta[n] += POW2(std_delta[param][n])

		tot_delta[n] = np.sqrt(tot_delta[n])


elif model == 'threerods':

	modeldata['ep1']	= 4.2			# 1st auxiliary rod dielectric constant
	modeldata['tan1']	= 0.01			# 1st auxiliary slab loss tangent

	modeldata['ep2']	= 12.5			# MUT rod dielectric constant
#	modeldata['ep2']	= [1.0, 20.0, 0.01]	# MUT rod dielectric constant
	modeldata['tan2']	= 0.01			# MUT auxiliary slab loss tangent

	modeldata['ep3']	= 4.2			# 2nd auxiliary rod dielectric constant
	modeldata['tan3']	= 0.01			# 2nd auxiliary slab loss tangent

	modeldata['ro1']	= 6.0			# 1st auxiliary rod radius in mm
	modeldata['ro2']	= 3.0			# MUT rod radius in mm
	modeldata['ro3']	= 6.0			# 2nd auxiliary rod radius in mm

	modeldata['xo1']	= 0.0			# 1st auxiliary rod offset in mm
	modeldata['xo2']	= 0.0			# MUT rod offset in mm
	modeldata['xo3']	= 0.0			# 2nd auxiliary rod offset in mm

	modeldata['L1']		= 16.0			# distance between the 1st and the 2nd rods in mm
	modeldata['L2']		= 16.0			# distance between the 2nd and the 3rd rods in mm

	modeldata['f']		= 10.0			# operating frequency in GHz
	modeldata['a']		= 22.86			# waveguide width in mm

	sigma['S11abs']    	= 0.01
	sigma['ro1']        	= 0.015
	sigma['ro2']        	= 0.015
	sigma['ro3']        	= 0.015

	sigma['xo1']        	= 0.015
	sigma['xo2']        	= 0.015
	sigma['xo3']        	= 0.015

	sigma['L1']         	= 0.015
	sigma['L2']         	= 0.015
	sigma['a']         	= 0.015
	sigma['f']         	= 0.001

	sigma['ep1']         	= modeldata['ep1']*0.01
	sigma['tan1']         	= modeldata['tan1']*0.05

	sigma['tan2']         	= modeldata['tan2']*0.05

	sigma['ep3']         	= modeldata['ep3']*0.01
	sigma['tan3']         	= modeldata['tan3']*0.05
	#=======================================================================
	paramname		= {'S11abs': '$S_{11}$', 'ro1': '$r_{1}$', 'xo1': '$x_{\\mathrm{o,1}}$', 'ro2': '$r_{2}$', 'xo2': '$x_{\\mathrm{o,2}}$', 'ro3': '$r_{3}$', 'xo3': '$x_{\\mathrm{o,3}}$', 'L': '$L$', 'ep1': '$\\varepsilon_{\\mathrm{r,1}}$', 'tan1': '$ \\tan{\\delta}$', 'ep3': '$\\varepsilon_{\\mathrm{r,2}}$', 'tan3': '$ \\tan{\\delta}$', 'tan2': '$\\tan{\\delta}$', 'a': 'a', 'f': 'f'}
	#=================================================================
	list_found = False
	for param in modeldata.keys():
		if type(modeldata[param]) is list:
			if len(modeldata[param]) > 1 and not list_found:
				var_range = modeldata[param]
				var_param = param
				list_found = True
			else:
				modeldata[param] = modeldata[param][0]

	if list_found:
		var_values 	= np.arange(*var_range)
	else:
		var_param	= 'ep1'
		var_values 	= np.asarray([modeldata['ep1']])
	#=================================================================
	std_delta = dict()	# standard uncertainties
	for param in sigma.keys():
		std_delta[param] = np.empty((var_values.shape))

	mod_sens = dict()	# sensitivity coefficients
	for param in sigma.keys():
		mod_sens[param] = np.empty((var_values.shape))

	tot_delta = np.empty((var_values.shape))	# total uncertainty
	#=================================================================
	# EVALUATE SENSITIVITY COEFFICIENTS, AS WELL AS UNCERTAINTIES
	#=================================================================
	for n in range(var_values.shape[0]):
		modeldata[var_param] = var_values[n]

		tot_delta[n]	= 0.0

		for param in sigma.keys():
			mod_sens[param][n], S11abs = Derivative_three_rods(modeldata, param, delta, TOL, MAXIT, NM, N)

		for param in sigma.keys():
			if param == 'S11abs' and callable(sigma['S11abs']):
				sigma['S11abs'] = sigma['S11abs'](S11abs)
			std_delta[param][n] = np.abs(mod_sens[param][n]*sigma[param])
			tot_delta[n] += POW2(std_delta[param][n])

		tot_delta[n] = np.sqrt(tot_delta[n])
#===============================================================
elif model == 'slab-rod':

	modeldata['ep1']	= 4.3			# auxiliary slab complex permittivity
	modeldata['tan1']	= 0.01			# auxiliary slab loss tangent
#	modeldata['ep2']	= 12.5			# MUT complex permittivity
#	modeldata['ep2']	= [1.0, 20.0, 0.01]	# MUT slab complex permittivity
	modeldata['ep2']	= [24.0]		# MUT slab complex permittivity
	modeldata['tan2']	= 0.0001		# MUT slab loss tangent

	modeldata['d1']		= 9.25			# auxiliary slab thickness in mm

	modeldata['ro2']	= 3.3			# measurable rod radius in mm
	modeldata['xo2']	= 0.0			# measurable rod offset in mm

#	modeldata['L']		= 22.0			# distance between the auxiliary slab and the MUT rods in mm
	modeldata['L']		= [1.0, 30.0, 0.1]	# distance between the auxiliary slab and the MUT rods in mm
	modeldata['f']		= 10.0			# operating frequency in GHz
	modeldata['a']		= 22.86			# waveguide width in mm

	sigma['S11abs']    	= 0.01
	sigma['d1']        	= 0.015
	sigma['ro2']        	= 0.015
	sigma['xo2']        	= 0.015
	sigma['L']         	= 0.025
	sigma['a']         	= 0.015
	sigma['f']         	= 0.001

	sigma['ep1']         	= modeldata['ep1']*0.01
	sigma['tan1']         	= modeldata['tan1']*0.05

	sigma['tan2']         	= modeldata['tan2']*0.05
	#=======================================================================
	paramname		= {'S11abs': '$S_{11}$', 'd1': '$d_{1}$', 'ro2': '$r_{2}$', 'xo2': '$x_{\\mathrm{o,2}}$', 'L': 'L', 'ep1': '$\\varepsilon_{\\mathrm{r}}$', 'tan1': '$ \\tan{\\delta}$', 'tan2': '$\\tan{\\delta}$', 'a': 'a', 'f': 'f'}
	#=================================================================
	list_found = False
	for param in modeldata.keys():
		if type(modeldata[param]) is list:
			if len(modeldata[param]) > 1 and not list_found:
				var_range = modeldata[param]
				var_param = param
				list_found = True
			else:
				modeldata[param] = modeldata[param][0]

	if list_found:
		var_values 	= np.arange(*var_range)
	else:
		var_param	= 'ep1'
		var_values 	= np.asarray([modeldata['ep1']])
	#=================================================================
	std_delta = dict()	# standard uncertainties
	for param in sigma.keys():
		std_delta[param] = np.empty((var_values.shape))

	mod_sens = dict()	# sensitivity coefficients
	for param in sigma.keys():
		mod_sens[param] = np.empty((var_values.shape))

	tot_delta = np.empty((var_values.shape))	# total uncertainty
	#=================================================================
	# EVALUATE SENSITIVITY COEFFICIENTS, AS WELL AS UNCERTAINTIES
	#=================================================================
	for n in range(var_values.shape[0]):
		modeldata[var_param] = var_values[n]

		tot_delta[n]	= 0.0

		for param in sigma.keys():
			mod_sens[param][n], S11abs = Derivative_slab_and_rod(modeldata, param, delta, TOL, MAXIT, NM, N)

		for param in sigma.keys():
			if param == 'S11abs' and callable(sigma['S11abs']):
				sigma['S11abs'] = sigma['S11abs'](S11abs)
			std_delta[param][n] = np.abs(mod_sens[param][n]*sigma[param])
			tot_delta[n] += POW2(std_delta[param][n])

		tot_delta[n] = np.sqrt(tot_delta[n])
#===============================================================
elif model == 'rod-slab':

	modeldata['ep1']	= 4.3			# auxiliary rod dielectric constant
	modeldata['tan1']	= 0.01			# auxiliary slab loss tangent

	modeldata['ep2']	= 10.2			# MUT slab dielectric constant
#	modeldata['ep2']	= [1.0, 20.0, 0.01]	# MUT slab dielectric constant
	modeldata['tan2']	= 0.001			# MUT slab loss tangent

	modeldata['ro1']	= 7.0			# auxiliary rod radius in mm
	modeldata['xo1']	= 0.0			# auxiliary rod offset in mm
	modeldata['d2']		= 2.5			# MUT slab thickness in mm
#	modeldata['L']		= 26.0			# rod-to-slab separation distance mm
	modeldata['L']		= [10.0, 30.0, 0.1]	# rod-to-slab separation distance mm
	modeldata['f']		= 10.0			# operating frequency in GHz
	modeldata['a']		= 22.86			# waveguide width in mm

	sigma['S11abs']    	= 0.01
	sigma['ro1']        	= 0.015
	sigma['xo1']        	= 0.015
	sigma['d2']        	= 0.015
	sigma['L']         	= 0.025
	sigma['a']         	= 0.015
	sigma['f']         	= 0.001

	sigma['ep1']         	= modeldata['ep1']*0.01
	sigma['tan1']         	= modeldata['tan1']*0.05

	sigma['tan2']         	= modeldata['tan2']*0.05
	#=======================================================================
	paramname		= {'S11abs': '$S_{11}$', 'ro1': '$r_{1}$', 'xo1': 'x_{\\mathrm{o,1}}', 'd2': '$d_{2}$', 'L': 'L', 'ep1': '$\\varepsilon_{\\mathrm{r}}$', 'tan1': '$\\tan{\\delta}$', 'tan2': '$\\tan{\\delta}$', 'a': 'a', 'f': 'f'}
	#=================================================================
	list_found = False
	for param in modeldata.keys():
		if type(modeldata[param]) is list:
			if len(modeldata[param]) > 1 and not list_found:
				var_range = modeldata[param]
				var_param = param
				list_found = True
			else:
				modeldata[param] = modeldata[param][0]

	if list_found:
		var_values 	= np.arange(*var_range)
	else:
		var_param	= 'ep1'
		var_values 	= np.asarray([modeldata['ep1']])
	#=================================================================
	std_delta = dict()	# standard uncertainties
	for param in sigma.keys():
		std_delta[param] = np.empty((var_values.shape))

	mod_sens = dict()	# sensitivity coefficients
	for param in sigma.keys():
		mod_sens[param] = np.empty((var_values.shape))

	tot_delta = np.empty((var_values.shape))	# total uncertainty
	#=================================================================
	# EVALUATE SENSITIVITY COEFFICIENTS, AS WELL AS UNCERTAINTIES
	#=================================================================
	for n in range(var_values.shape[0]):
		modeldata[var_param] = var_values[n]

		tot_delta[n]	= 0.0

		for param in sigma.keys():
			mod_sens[param][n], S11abs = Derivative_rod_and_slab(modeldata, param, delta, TOL, MAXIT, NM, N)

		for param in sigma.keys():
			if param == 'S11abs' and callable(sigma['S11abs']):
				sigma['S11abs'] = sigma['S11abs'](S11abs)
			std_delta[param][n] = np.abs(mod_sens[param][n]*sigma[param])
			tot_delta[n] += POW2(std_delta[param][n])

		tot_delta[n] = np.sqrt(tot_delta[n])
#===============================================================
elif model == 'holed-solid':

	modeldata['ep1']	= 4.3			# auxiliary holed rod dielectric constant
	modeldata['tan1']	= 0.01			# auxiliary holed rod loss tangent

	modeldata['ep2']	= 12.5			# MUT solid rod dielectric constant
#	modeldata['ep2']	= [1.0, 20.0, 0.01]	# MUT solid rod dielectric constant
	modeldata['tan2']	= 0.01			# MUT solid rod loss tangent

	modeldata['ro1']	= 3.0			# auxiliary rod radius in mm
	modeldata['xo1']	= 0.0			# auxiliary rod offset in mm
	modeldata['ro1in']	= 0.5			# auxiliary rod hole radius in mm
	modeldata['d1']		= 2.0			# auxiliary rod hole-to-hole separation in mm
	modeldata['ph1']	= 0.0			# auxiliary rod hole line angle in mm

	modeldata['ro2']	= 2.5			# MUT rod thicknes in mm
	modeldata['xo2']	= 0.0			# MUT rod offset in mm

	modeldata['L']		= 23.0			# interrod separation distance in mm
	modeldata['f']		= 10.0			# operating frequency in GHz
	modeldata['a']     	= 22.86			# waveguide width in mm

	sigma['S11abs']    	= 0.01

	sigma['ro1']        	= 0.015
	sigma['xo1']        	= 0.015
	sigma['ro1in']        	= 0.015
	sigma['d1']        	= 0.015
	sigma['ph1']        	= 0.05

	sigma['ro2']        	= 0.015
	sigma['xo2']        	= 0.015

	sigma['L']         	= 0.015
	sigma['a']         	= 0.015
	sigma['f']         	= 0.001

	sigma['ep1']         	= modeldata['ep1']*0.01
	sigma['tan1']         	= modeldata['tan1']*0.05

	sigma['tan2']         	= modeldata['tan2']*0.05
	#=======================================================================
	paramname		= {'S11abs': '$S_{11}$', 'ro1': '$r_{1}$', 'xo1': '$x_{\\mathrm{o,1}}$', 'ro1in': '$r_{\\mathrm{in}1}$', 'xo2': '$x_{\\mathrm{0,2}}$', 'd1': '$d_{1}$', 'ph1': '$\\varphi_{1}$', 'ro2': '$r_{2}$', 'L': 'L', 'ep1': '$\\varepsilon_{\\mathrm{r}}$', 'tan1': '$\\tan{\\delta}$', 'tan2': '$\\tan{\\delta}$', 'a': 'a', 'f': 'f'}
	#=================================================================
	list_found = False
	for param in modeldata.keys():
		if type(modeldata[param]) is list:
			if len(modeldata[param]) > 1 and not list_found:
				var_range = modeldata[param]
				var_param = param
				list_found = True
			else:
				modeldata[param] = modeldata[param][0]

	if list_found:
		var_values 	= np.arange(*var_range)
	else:
		var_param	= 'ep1'
		var_values 	= np.asarray([modeldata['ep1']])
	#=================================================================
	std_delta = dict()	# standard uncertainties
	for param in sigma.keys():
		std_delta[param] = np.empty((var_values.shape))

	mod_sens = dict()	# sensitivity coefficients
	for param in sigma.keys():
		mod_sens[param] = np.empty((var_values.shape))

	tot_delta = np.empty((var_values.shape))	# total uncertainty
	#=================================================================
	# EVALUATE SENSITIVITY COEFFICIENTS, AS WELL AS UNCERTAINTIES
	#=================================================================
	for n in range(var_values.shape[0]):
		modeldata[var_param] = var_values[n]

		tot_delta[n]	= 0.0

		for param in sigma.keys():
			mod_sens[param][n], S11abs = Derivative_holed_and_solid_rods(modeldata, param, delta, TOL, MAXIT, NM, N)

		for param in sigma.keys():
			if param == 'S11abs' and callable(sigma['S11abs']):
				sigma['S11abs'] = sigma['S11abs'](S11abs)
			std_delta[param][n] = np.abs(mod_sens[param][n]*sigma[param])
			tot_delta[n] += POW2(std_delta[param][n])

		tot_delta[n] = np.sqrt(tot_delta[n])


#==================================================================================
# SAVE DATA INTO A FILE
#==================================================================================
for param in sigma.keys():
	filename = model + '_var_' + var_param + '_sensitivity_' + param + '_EPM' + PREFIX
	np.save(filename, np.vstack([var_values, mod_sens[param]]))

	filename = model + '_var_' + var_param + '_standard_' + param + '_EPM' + PREFIX
	np.save(filename, np.vstack([var_values, std_delta[param]]))

filename = model + '_var_' + var_param + '_total_EPM' + PREFIX
np.save(filename, np.vstack([var_values, tot_delta]))
#==================================================================================
# PLOTTING CALCULATED DATA
#==================================================================================
if isPDF:
	if task == 'sensitivity':
		for param in sigma.keys():
			if sensparam == param:
				plot2latex(var_values, mod_sens[param], labels={'xlabel': xlabels[var_param], 'ylabel': ylabelsens + ' ' + paramname[param]}, ylabel_vert=True, xDecNum=1)
	elif task == 'standard':
		for param in sigma.keys():
			if sensparam == param:
				labels = {'xlabel': xlabels[var_param], 'ylabel': ylabeldelta + ' ' + paramname[param]}
				plot2latex(var_values, std_delta[param], labels=labels, ylabel_vert=True, xDecNum=1)
	else:
		plot2latex(var_values, tot_delta, labels={'xlabel': xlabels[var_param], 'ylabel': 'Total uncertainty'}, ylabel_vert=True, xDecNum=1)
elif isPRINT:
	if task == 'sensitivity':
		for param in sigma.keys():
			if sensparam == param:
				print(mod_sens[param])
	elif task == 'standard':
		for param in sigma.keys():
			if sensparam == param:
				print(std_delta[param])
	else:
		print(tot_delta)
elif matplot:
	plt.xlabel(xlabels[var_param])
	if task == 'sensitivity':
		for param in sigma.keys():
			if sensparam == param:
				plt.ylabel(ylabelsens + ' ' + paramname[param])
				plt.plot(var_values, mod_sens[param])
	elif task == 'standard':
		for param in sigma.keys():
			if sensparam == param:
				plt.ylabel(ylabeldelta + ' ' + paramname[param])
				plt.plot(var_values, std_delta[param])
	else:
		plt.ylabel('Total uncertainty.')
		plt.plot(var_values, tot_delta)

	plt.grid()
	plt.show()
else:
	pass
#       print('Calculated data have been saved into a file.')

print('--------------------------------------------------------')
