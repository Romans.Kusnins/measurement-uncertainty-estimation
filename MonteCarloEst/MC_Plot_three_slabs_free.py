import os
import numpy as np
from slab_res3md import (three_slab_free_space, three_slab_guide, two_solid_rods, three_solid_rods, slab_and_rod, rod_and_slab, holed_and_solid_rods, root_finding, POW2)
from Confidence_interval_calculation import *
from Derivatives import *

try:
        matplot = True
        from matplotlib import pyplot as plt
except ModuleNotFoundError:
        print("The matplotlib module is not installed.")
        matplot = False


from plot2pdf import *

from Uncertainty_vs_S11 import *
#==================================================================================
xMin 		= None
xMax 		= None
MaxVal 		= 100.0

#task 		= 'sensitivity'
task 		= 'total'
#---------------------------------------
sensparam 	= 'S11abs'
#---------------------------------------
var_param 	= 'L'
#---------------------------------------
quantity 	= 'var'


#PREFIX = '_29_'
PREFIX = '_6_0_'

MCNUM = 20000
#==================================================================================
isPDF 		= True
#isPRINT	= False
#==================================================================================
xlabels		= dict()

#xlabels['ep1']  = 'Dielectric constant of the auxiliary slab'
#xlabels['epm']  = 'Dielectric constant of the middle slab'
#xlabels['ep2']  = 'Dielectric constant of MUT'

#xlabels['tan1'] = 'Loss tangent of the auxiliary slab'
#xlabels['tanm'] = 'Loss tangent of the middle slab'
#xlabels['tan2'] = 'Loss tangent of MUT'

#xlabels['d1']   = 'Thickness of the auxiliary slab, mm'
#xlabels['d2']   = 'Thickness of the MUT slab, mm'
#xlabels['L']    = 'Thickness of the middle slab, mm'
#xlabels['f']    = 'Frequency, GHz'

xlabels['ep1']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,aux}}$'
xlabels['ep2']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,mut}}$'
xlabels['epm']  = '$\\varepsilon^{\\prime}_{\\mathrm{r,mut}}$'
xlabels['tan1'] = '$\\tan_{\\delta^{\\mathrm{aux}}}$'
xlabels['tan2'] = '$\\tan_{\\delta^{\\mathrm{mut}}}$'
xlabels['tanm'] = '$\\tan_{\\delta^{\\mathrm{mut}}}$'
xlabels['d1']   = '$d_{\\mathrm{aux}}$ mm'
xlabels['d2']   = '$d_{\\mathrm{mut}}$ mm'
xlabels['L']    = '$d_{\\mathrm{int}}$ mm'
xlabels['f']    = 'f, GHz'

paramname       = {'S11abs': '$u_{|S_{11}|}$', 'd1': '$u_{d_{\\mathrm{aux}}}$', 'd2': '$u_{d_{\\mathrm{mut}}}$','L': '$u_{d_{\\mathrm{in}}}$', 'ep1': '$u_{\\varepsilon^{\\prime}_{\\mathrm{r,aux}}}$',  'tan1': '$ u_{\\tan{\\delta^{\\mathrm{aux}}}}$', 'tan2': 'u_{\\tan{\\delta^{\\mathrm{mut}}}}', 'a': 'u_{a}', 'f': 'u_{f}'}
#==================================================================================
# PLOTTING CALCULATED DATA
#==================================================================================
FOLDER = './RESULTS_3SL_FREE/'

MCSTR = '{:}'.format(MCNUM)

if task == 'sensitivity':
	MCfilename 	= FOLDER + 'three_slabs_free_var_' + var_param + '_sens_' + sensparam + '_MCnum'+ PREFIX + '_' + MCSTR + '.npy'
	MCfilename_mean	= FOLDER + 'three_slabs_free_var_' + var_param + '_sens_' + sensparam + '_MCnum_mean'+ PREFIX + '_' + MCSTR + '.npy'
	EPMfilename 	= FOLDER + 'three-slabs-free_var_' + var_param + '_standard_' + sensparam + '_EPM' + PREFIX + '.npy'
else:
	MCfilename 	= FOLDER + 'three_slabs_free_var_' + var_param + '_total_MCnum' + PREFIX + '_' + MCSTR + '.npy'
	MCfilename_mean	= FOLDER + 'three_slabs_free_var_' + var_param + '_total_MCnum_mean' + PREFIX + '_' + MCSTR + '.npy'
	EPMfilename 	= FOLDER + 'three-slabs-free_var_' + var_param + '_total_EPM' + PREFIX + '.npy'

MCdata 		= np.load(MCfilename)
MCdata_mean 	= np.load(MCfilename_mean)
EPMdata 	= np.load(EPMfilename)
#==================================================================================
if MaxVal != None:
	MCdata_mean[1, MCdata_mean[1,:] > MaxVal]	= np.nan
	MCdata[1, MCdata[1,:] > MaxVal] 		= np.nan
	EPMdata[1, EPMdata[1,:] > MaxVal] 		= np.nan

if xMin != None:
	ID 	= EPMdata[0,:] >= xMin
	EPMdata = EPMdata[:,ID]

	ID 	= MCdata[0,:] >= xMin
	MCdata 	= MCdata[:,ID]

if xMax != None:
	ID 	= EPMdata[0,:] <= xMax
	EPMdata = EPMdata[:,ID]

	ID 	= MCdata[0,:] <= xMax
	MCdata 	= MCdata[:,ID]
#==================================================================================
if isPDF:

	legend = ['EPM', 'MC']

	LegendPos = [0.2, 0.7]

	if task == 'sensitivity':
		filename  = 'THREE_SL_FREE_VAR_' + var_param + '_SENS_' + sensparam + PREFIX
		labels = {'xlabel': xlabels[var_param], 'ylabel': 'Sensitivity to ' + sensparam}
	else:
		filename  = 'THREE_SL_FREE_VAR_' + var_param + '_TOTAL' + PREFIX
		labels = {'xlabel': xlabels[var_param], 'ylabel': 'Total uncertainty'}

	if quantity == 'mean':
	#	labels = {'xlabel': xlabels[var_param], 'ylabel': ylabeldelta + ' ' + paramname[param]}
		filename  = 'THREE_SL_FREE_VAR_' + var_param + '_MEAN' + PREFIX
		plot2latex(MCdata_mean[0,:], MCdata_mean[1,:], ylabel_vert=True, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, LineType=['','dash'], YZero=False, FileName=filename, LegendPosition=LegendPos)
	else:
		plot2latex([EPMdata[0,:], MCdata[0,:]], [EPMdata[1,:], MCdata[1,:]], ylabel_vert=True, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels, LineType=['','dash'], FileName=filename, LegendPosition=LegendPos)
#	plot2latex([EPMdata[0,:], MCdata[0,:]], [EPMdata[1,:], MCdata[1,:]], ylabel_vert=True, xDecNum=1, LineWidth=0.75, legend=legend, labels=labels)
elif matplot:
#	plt.ylabel(ylabeldelta + ' ' + paramname[param])
	plt.plot(MCdata[0,:], EPMdata[1,:])
	plt.grid()
	plt.show()
else:
	pass
#       print('Calculated data have been saved into a file.')

print('--------------------------------------------------------')
